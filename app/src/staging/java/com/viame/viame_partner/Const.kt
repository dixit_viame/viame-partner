package com.viame.viame_partner

const val IS_DEBUG : Boolean = true
const val APP_NAME = "ViaMe-Partner"
const val PARENT_FOLDER_PATH = "ViaMe-Partner"
const val TAG = "ViaMe-Partner.debug"
const val PREFRENCE_FILE_NAME = "ViaMe-Partner"

const val MobileData = 2
const val Vpn = 3
const val WifiData = 1

const val EXTRA_CIRCULAR_REVEAL_X = "EXTRA_CIRCULAR_REVEAL_X"
const val EXTRA_CIRCULAR_REVEAL_Y = "EXTRA_CIRCULAR_REVEAL_Y"
const val BOOKING_DATA = "Booking_order"
const val COME_FROM = "come_from"

const val DEVICE_TYPE = 2 //1 for ios, 2 for android
const val USER_TYPE = 2   //1 for customer, 2 for driver

const val HTTP_REQUEST_TIMEOUT = 30
const val MIN_BATTERY_PERCENTAGE = 5

const val NOTIFICATION_CHANNEL_ID = "com.viame.viame_partner"
const val NOTIFICATION_CHANNEL_NAME = "ViaMe-Partner"
const val PLAYSTORE_LINK = "https://play.google.com/store/apps/details?id=com.viame.driver"

//Request codes
const val GALLERY_PIC = 100
const val CAMERA_PIC  = 101
const val CROP_IMAGE  = 102
const val WORK_MANAGER_NOTIFICATION_CODE  = 103
const val ALARM_MANAGER_NOTIFICATION_CODE  = 104
const val BOOKING_NOTIFICATION_ID  = 105
const val BOOKING_TRANSFER_ID  = 106
const val ONLINE_OFFLINE_NOTIFICATION_ID  = 501

const val METER = "meters"
const val KILOMETER = "Kilometers"
const val NAUTICAL_MILES = "nauticalMiles"

enum class DriverStatusType {
    DEFAULT(0),
    ONLINE(3), OFFLINE(4);

    var id // Could be other data type besides int
            = 0

    constructor(id: Int) {
        this.id = id
    }

    companion object {
        fun fromId(id: Int): DriverStatusType? {
            for (type in DriverStatusType.values()) {
                if (type.id == id) {
                    return type
                }
            }
            return null
        }
    }
}

// Driver [st] Status
enum class MBStatusType {

    UN_ASSIGNED(0),
    REQUESTED(1),
    ACCEPTED(2),
    REJECTED_BY_CUSTOMER(3),
    DRIVER_CANCELLED(4),
    ON_THE_WAY(6),
    ARRIVED(7),
    LOADED(8),
    ARRIVED_AT_DROPOFF(9),
    COMPLETED(10),
    UNASSIGNED_BY_SERVER(11),
    UNLOADED_AND_DROP(16),
    BOOKING_EXPIRED(55),
    DEFAULT(-1);

    var id = 0 // Could be other data type besides int

    constructor(id: Int) {
        this.id = id
    }

    companion object {
        fun fromId(id: Int): MBStatusType? {
            for (type in MBStatusType.values()) {
                if (type.id == id) {
                    return type
                }
            }
            return null
        }
    }
}
