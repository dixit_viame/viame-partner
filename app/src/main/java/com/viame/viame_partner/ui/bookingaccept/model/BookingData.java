package com.viame.viame_partner.ui.bookingaccept.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BookingData implements Parcelable {

    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("helpers")
    @Expose
    private String helpers;
    @SerializedName("drop_adr")
    @Expose
    private String dropAdr;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("booking_time")
    @Expose
    private String bookingTime;
    @SerializedName("pickup_adr")
    @Expose
    private String pickupAdr;
    @SerializedName("channel_customer")
    @Expose
    private String channelCustomer;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("booking_type")
    @Expose
    private String bookingType;
    @SerializedName("serverTime")
    @Expose
    private long serverTime;
    @SerializedName("drop_zone")
    @Expose
    private String dropZone;
    @SerializedName("bid")
    @Expose
    private long bid;
    @SerializedName("client_name")
    @Expose
    private String clientName;
    @SerializedName("pickZone")
    @Expose
    private String pickZone;
    @SerializedName("ExpiryTimer")
    @Expose
    private long expiryTimer;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("status")
    @Expose
    private String status;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getHelpers() {
        return helpers;
    }

    public void setHelpers(String helpers) {
        this.helpers = helpers;
    }

    public String getDropAdr() {
        return dropAdr;
    }

    public void setDropAdr(String dropAdr) {
        this.dropAdr = dropAdr;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(String bookingTime) {
        this.bookingTime = bookingTime;
    }

    public String getPickupAdr() {
        return pickupAdr;
    }

    public void setPickupAdr(String pickupAdr) {
        this.pickupAdr = pickupAdr;
    }

    public String getChannelCustomer() {
        return channelCustomer;
    }

    public void setChannelCustomer(String channelCustomer) {
        this.channelCustomer = channelCustomer;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getBookingType() {
        return bookingType;
    }

    public void setBookingType(String bookingType) {
        this.bookingType = bookingType;
    }

    public long getServerTime() {
        return serverTime;
    }

    public void setServerTime(long serverTime) {
        this.serverTime = serverTime;
    }

    public String getDropZone() {
        return dropZone;
    }

    public void setDropZone(String dropZone) {
        this.dropZone = dropZone;
    }

    public long getBid() {
        return bid;
    }

    public void setBid(long bid) {
        this.bid = bid;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getPickZone() {
        return pickZone;
    }

    public void setPickZone(String pickZone) {
        this.pickZone = pickZone;
    }

    public long getExpiryTimer() {
        return expiryTimer;
    }

    public void setExpiryTimer(long expiryTimer) {
        this.expiryTimer = expiryTimer;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.amount);
        dest.writeString(this.helpers);
        dest.writeString(this.dropAdr);
        dest.writeString(this.distance);
        dest.writeString(this.bookingTime);
        dest.writeString(this.pickupAdr);
        dest.writeString(this.channelCustomer);
        dest.writeString(this.paymentType);
        dest.writeString(this.bookingType);
        dest.writeLong(this.serverTime);
        dest.writeString(this.dropZone);
        dest.writeLong(this.bid);
        dest.writeString(this.clientName);
        dest.writeString(this.pickZone);
        dest.writeLong(this.expiryTimer);
        dest.writeValue(this.statusCode);
        dest.writeString(this.status);
    }

    public BookingData() {
    }

    protected BookingData(Parcel in) {
        this.amount = in.readString();
        this.helpers = in.readString();
        this.dropAdr = in.readString();
        this.distance = in.readString();
        this.bookingTime = in.readString();
        this.pickupAdr = in.readString();
        this.channelCustomer = in.readString();
        this.paymentType = in.readString();
        this.bookingType = in.readString();
        this.serverTime = in.readLong();
        this.dropZone = in.readString();
        this.bid = in.readLong();
        this.clientName = in.readString();
        this.pickZone = in.readString();
        this.expiryTimer = in.readLong();
        this.statusCode = (Integer) in.readValue(Integer.class.getClassLoader());
        this.status = in.readString();
    }

    public static final Creator<BookingData> CREATOR = new Creator<BookingData>() {
        @Override
        public BookingData createFromParcel(Parcel source) {
            return new BookingData(source);
        }

        @Override
        public BookingData[] newArray(int size) {
            return new BookingData[size];
        }
    };
}
