package com.viame.viame_partner.ui.bookingdetail.details

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.viame.viame_partner.*
import com.viame.viame_partner.databinding.FragmentDetailsBinding
import com.viame.viame_partner.location.vibratePhone
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.base.BaseFragment
import com.viame.viame_partner.ui.bookingcomplete.BookCompleteImagePagerFragment
import com.viame.viame_partner.ui.bookingcomplete.BookingCompletedActivity
import com.viame.viame_partner.ui.bookingdetail.BookingDetailActivity
import com.viame.viame_partner.ui.bookingdetail.details.model.BookingStatusInputModel
import com.viame.viame_partner.ui.myorders.model.Appointment
import com.viame.viame_partner.ui.pickupdrop.PickupDropActivity
import com.viame.viame_partner.ui.transfer_order.TransferOrderActivity
import com.viame.viame_partner.utils.Alerts
import com.viame.viame_partner.utils.ConnectivityReceiver
import com.viame.viame_partner.utils.LinearLayoutManagerWrapper
import com.viame.viame_partner.utils.Utils
import com.viame.viame_partner.widgets.SlideToActView
import com.viame.viame_partner.widgets.SlideToActView.OnSlideCompleteListener
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import timber.log.Timber
import javax.inject.Inject


class DetailPagerFragment : BaseFragment<DetailPagerViewModel>() {

    private lateinit var binding: FragmentDetailsBinding
    lateinit var currentAppointment : Appointment

    @Inject
    lateinit var apiInterface: ApiInterface

    @Inject
    lateinit var connectivityReceiver: ConnectivityReceiver

    @Inject
    lateinit var models : DetailPagerViewModel

    override fun getViewModel(): DetailPagerViewModel {
        return models
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        binding  = DataBindingUtil.inflate(inflater, R.layout.fragment_details,container,false)

        if(arguments?.get(BOOKING_DATA) != null) {
            currentAppointment = arguments?.get(BOOKING_DATA) as Appointment
        }else
            return binding.root

        IS_BOOKING_STATUS_UPDATED = false
        binding.tvBid.text = currentAppointment.orderId.toString()
        binding.tvDate.text = Utils.dateFormatter(currentAppointment.bookingTime)

        try {
            if(currentAppointment.apptType == "1") {
                binding.tvBookingType.text = context?.getString(R.string.instant_booking)
                binding.tvPickupTime.text = Utils.dateFormatter(currentAppointment.bookingTime)
                binding.tvDropoffTime.text = Utils.dateFormatter(currentAppointment.bookingTime)

            }
            else
            {
                binding.tvBookingType.text = context?.getString(R.string.book_later_text)
                binding.tvPickupTime.text =
                    Utils.getDateFormatFromMiliseconds(currentAppointment.bookingLaterDateTimestamp,"MMM dd, hh:mm a")
            }
        } catch (e: Exception) {
        }

        if(currentAppointment.paymentType == "1" || currentAppointment.paymentType.equals("Card",true))
        {
            binding.llCardLayout.visibility = View.VISIBLE
            binding.llCashLayout.visibility = View.GONE
            binding.llWalletLayout.visibility = View.GONE
        }
        else  if(currentAppointment.paymentType == "2" || currentAppointment.paymentType.equals("Wallet",true))
        {
            binding.llCardLayout.visibility = View.GONE
            binding.llCashLayout.visibility = View.GONE
            binding.llWalletLayout.visibility = View.VISIBLE

        }else  if(currentAppointment.paymentType == "3" || currentAppointment.paymentType.equals("Cash",true))
        {
            binding.llCardLayout.visibility = View.GONE
            binding.llCashLayout.visibility = View.VISIBLE
            binding.llWalletLayout.visibility = View.GONE
        }

        if(currentAppointment.collectCashValue != null && currentAppointment.collectCashValue.toString().isNotEmpty())
        {
            binding.tvCODLBL.visibility = View.VISIBLE
            binding.tvCODNotes.visibility = View.VISIBLE
            binding.tvCODNotes.text = currentAppointment.collectCashValue + " "+Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)
        }else
        {
            binding.tvCODLBL.visibility = View.GONE
            binding.tvCODNotes.visibility = View.GONE
        }

        binding.tvClientName.text = currentAppointment.slaveName
        binding.tvPickupAddress.labelTextView.text = currentAppointment.addressLine1
        binding.tvDropoffAddress.labelTextView.text = currentAppointment.dropAddr1
        if(currentAppointment.statusText != null)
        {
            binding.tvStatus.text = currentAppointment.statusText.toString()
        }else
        {
            binding.tvStatus.visibility = View.GONE
            binding.tvStatusLBL.visibility = View.GONE
        }
        binding.tvAdditionalNotes.text = currentAppointment.extraNotes
        binding.btnConvenientFee.text =
            "${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} ${currentAppointment.invoice.baseFare}"

        updateStatus(currentAppointment.status.toInt())

        binding.slvEventSlider.onSlideCompleteListener = object : OnSlideCompleteListener {
            override fun onSlideComplete(view: SlideToActView) {
                Timber.e("onSlideComplete")

                connectivityReceiver.observe(this@DetailPagerFragment, Observer { connectionModel ->

                    try {
                        if (connectionModel != null && connectionModel.isConnected())
                        {
                            IS_BOOKING_STATUS_UPDATED = true
                            val mBookingStatusInputModel = BookingStatusInputModel()
                            var distance: String? = "0"
                            if (currentAppointment.status == MBStatusType.ARRIVED.id ||
                                currentAppointment.status == MBStatusType.ARRIVED_AT_DROPOFF.id) {
                                var jsonArray: JSONArray?
                                try {
                                    jsonArray = JSONArray(Preferences.getPreferenceString(PrefEntity.BOOKING_DETAILS))
                                    for (i in 0 until jsonArray.length()) {
                                        val jsonObject = jsonArray[i] as JSONObject
                                        if (jsonObject["bid"] == currentAppointment.orderId) {
                                            distance = if(jsonObject.getString("distance").isNotEmpty()) jsonObject.getString("distance") else "0"
                                        }
                                    }
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                }
                            }

                            mBookingStatusInputModel.distance = distance
                            mBookingStatusInputModel.entBookingId = currentAppointment.orderId
                            if(Preferences.getPreferenceString(PrefEntity.LATITUDE_CURRENT).isNotEmpty())
                                mBookingStatusInputModel.lat =  Preferences.getPreferenceString(PrefEntity.LATITUDE_CURRENT).toDouble()
                            else
                                mBookingStatusInputModel.lat = 0.0

                            if(Preferences.getPreferenceString(PrefEntity.LONGITUDE_CURRENT).isNotEmpty())
                                mBookingStatusInputModel.long = Preferences.getPreferenceString(PrefEntity.LONGITUDE_CURRENT).toDouble()
                            else
                                mBookingStatusInputModel.long = 0.0

                                if(currentAppointment.status == MBStatusType.ON_THE_WAY.id)
                                {
                                    currentAppointment.status = MBStatusType.ARRIVED.id
                                    mBookingStatusInputModel.entStatus = "${MBStatusType.ARRIVED.id}"
                                    models.BookingStatusAPICall(apiInterface, mBookingStatusInputModel)
                                    updateStatus(MBStatusType.ARRIVED.id)

                                }else  if(currentAppointment.status == MBStatusType.ARRIVED.id)
                                {
                                    mBookingStatusInputModel.entStatus = "${MBStatusType.LOADED.id}"
                                    currentAppointment.status = MBStatusType.LOADED.id

                                    models.BookingStatusAPICall(apiInterface, mBookingStatusInputModel)
                                    updateStatus(MBStatusType.LOADED.id)

                                }else  if(currentAppointment.status== MBStatusType.LOADED.id) {
                                    mBookingStatusInputModel.entStatus = "${MBStatusType.ARRIVED_AT_DROPOFF.id}"
                                    currentAppointment.status = MBStatusType.ARRIVED_AT_DROPOFF.id
                                    models.BookingStatusAPICall(apiInterface, mBookingStatusInputModel)
                                    updateStatus(MBStatusType.ARRIVED_AT_DROPOFF.id)

                                }else  if(currentAppointment.status == MBStatusType.ARRIVED_AT_DROPOFF.id)
                                {
                                    currentAppointment.status = MBStatusType.UNLOADED_AND_DROP.id
                                    mBookingStatusInputModel.entStatus = "${MBStatusType.UNLOADED_AND_DROP.id}"

                                    models.BookingStatusAPICall(apiInterface,mBookingStatusInputModel )
                                    updateStatus(MBStatusType.UNLOADED_AND_DROP.id)
                                }
                                else  if(currentAppointment.status == MBStatusType.UNLOADED_AND_DROP.id)
                                {
                                    binding.slvEventSlider.resetSlider()
                                }
                        }
                        else
                        {
                            if(connectionModel?.type == Vpn)
                            {
                                Alerts.showSnackBar(activity,getString(R.string.vpn_use_message))
                            }
                            else
                            {
                                Alerts.showSnackBar(activity,getString(R.string.internet_not_available))
                            }
                        }
                    } catch (e: Exception) {
                    }
                })

            }
        }

        models.getBookingStatusAPIDataResult().observe(this@DetailPagerFragment, Observer { result ->

            if (result != null) {

                if (result.status == APIRequestResponseHandler.AuthStatus.LOADING) {
                     Alerts.showProgressBar(activity)

                } else if (result.status == APIRequestResponseHandler.AuthStatus.ERROR) {
                     Alerts.dismissProgressBar()
                    Alerts.showSnackBar(activity, result.data.errMsg)

                } else if (result.status == APIRequestResponseHandler.AuthStatus.SUCCESS) {
                    Alerts.dismissProgressBar()

                    if (result.data.errFlag == 1) {
                        Alerts.showSnackBar(activity, result.data.errMsg)
                    }
                    else
                    {
                        vibratePhone(activity?.applicationContext!!,200)

                        binding.slvEventSlider.resetSlider()
                        // If Invoice is not null that means booking completed then redirect to booking complete screen.
                        if(result.data.data != null && result.data.data.invoice !=null)
                        {
                            startActivity(Intent(activity, BookingCompletedActivity::class.java).apply {
                                putExtra(BookingCompletedActivity.APPDATA,currentAppointment)
                            })
                            (activity!! as BookingDetailActivity).finish()
                        }
                    }
                }
            }
        })
        binding.slvEventSlider.onSlideResetListener = object : SlideToActView.OnSlideResetListener
        {
            override fun onSlideReset(view: SlideToActView) {
                Timber.e("onSlideReset")
            }
        }

        binding.slvEventSlider.onSlideToActAnimationEventListener = object : SlideToActView.OnSlideToActAnimationEventListener
        {
            override fun onSlideCompleteAnimationStarted(view: SlideToActView, threshold: Float) {

            }
            override fun onSlideCompleteAnimationEnded(view: SlideToActView) {
                Timber.e("Completed Animation")
            }
            override fun onSlideResetAnimationStarted(view: SlideToActView) {

            }
            override fun onSlideResetAnimationEnded(view: SlideToActView) {
            }
        }

        binding.slvEventSlider.onSlideUserFailedListener = object : SlideToActView.OnSlideUserFailedListener
        {
            override fun onSlideFailed(view: SlideToActView, isOutside: Boolean) {

            }
        }

        binding.btnMoreDetailPickup.setOnClickListener {

            startActivity(Intent(activity, PickupDropActivity::class.java).apply {
                putExtra(BOOKING_DATA, currentAppointment)
                putExtra(COME_FROM,1)
            })
        }

        binding.btnMoreDetailDropoff.setOnClickListener {

            startActivity(Intent(activity, PickupDropActivity::class.java).apply {
                putExtra(BOOKING_DATA, currentAppointment)
                putExtra(COME_FROM,2)
            })
        }

        if(currentAppointment.apptType == "1")
        {
            binding.tvBookingType.text = context?.getString(R.string.instant_booking)
        }
        else
        {
            binding.tvBookingType.text = context?.getString(R.string.book_later_text)
        }

        binding.btnTransferBooking.setOnClickListener{

            startActivity(Intent(activity, TransferOrderActivity::class.java).apply {
                putExtra(BOOKING_DATA, currentAppointment)
            })

        }

        binding.btnStatus.setOnClickListener {

            Alerts.showBottomSheetCurrentStatusDialog(activity) {
                if(it == null) {

                }
            }
        }

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if(currentAppointment.photo != null && currentAppointment.photo.size > 0 && currentAppointment.photo[0] != null)
        {
            var adapter = DetailPageImageAdapter(
                this.activity!!,
                currentAppointment.photo,
                object : ImageListEventHandler {
                    override fun onitemClick(appointment: String) {

                        var dialog = Dialog(activity as BookingDetailActivity)
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                        dialog.setContentView(R.layout.layout_dialog_imageview)
                        dialog.getWindow()?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
                        dialog.setCanceledOnTouchOutside(true)
                        dialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                        val bigImageView = dialog.findViewById(R.id.ivBigImage) as ImageView

                        Picasso.get()
                            .load(appointment)
                            .into(bigImageView)

                        val ivClose = dialog.findViewById(R.id.ivClose) as ImageView
                        ivClose.setOnClickListener {
                            dialog.dismiss()
                        }
                        dialog.show()
                        ivClose.bringToFront()
                    }
                })

            binding.rvPhotoItems.layoutManager = LinearLayoutManagerWrapper(activity, RecyclerView.HORIZONTAL,false)
            binding.rvPhotoItems.adapter = adapter
        }
        else
        {
            binding.rvPhotoItems.visibility = View.GONE
            binding.tvPhotoTitleLBL.visibility = View.GONE
        }
    }

    fun updateStatus(Status : Int)
    {
        if (Status == MBStatusType.ON_THE_WAY.id)
        {
            binding.slvEventSlider.text = "ARRIVED AT PICKUP"
        }else if (Status == MBStatusType.ARRIVED.id)
        {
            binding.slvEventSlider.text = "START"
        }else if (Status == MBStatusType.LOADED.id)
        {
            binding.slvEventSlider.text = "REACHED AT DROP LOCATION"
        }else if (Status == MBStatusType.ARRIVED_AT_DROPOFF.id)
        {
            binding.slvEventSlider.text = "UNLOAD"

        }else if (Status == MBStatusType.UNLOADED_AND_DROP.id)
        {
            binding.slvEventSlider.text = "BOOKING DELIVERED"
        }
        else if (Status == MBStatusType.COMPLETED.id)
        {
            binding.slvEventSlider.text = "BOOKING COMPLETED"
        }

    }

    companion object {
        /*
         * The fragment argument representing the section number for fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"
        var IS_BOOKING_STATUS_UPDATED = false

        /*
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int, appointment: Appointment): DetailPagerFragment {
            return DetailPagerFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                    putSerializable(BOOKING_DATA, appointment)
                }
            }
        }
    }
}