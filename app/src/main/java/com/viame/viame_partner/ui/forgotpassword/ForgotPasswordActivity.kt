package com.viame.viame_partner.ui.forgotpassword

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.DataBindingUtil.setContentView
import com.viame.viame_partner.BuildConfig
import com.viame.viame_partner.DEVICE_TYPE
import com.viame.viame_partner.R
import com.viame.viame_partner.Vpn
import com.viame.viame_partner.databinding.ActivityForgotPasswordBinding
import com.viame.viame_partner.databinding.ActivitySigninBinding
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.base.BaseActivity
import com.viame.viame_partner.ui.signin.SignInActivityViewModel
import com.viame.viame_partner.ui.signin.SignInEventHandler
import com.viame.viame_partner.ui.signin.model.SignInRequestModel
import com.viame.viame_partner.utils.Alerts
import com.viame.viame_partner.utils.ConnectivityReceiver
import com.viame.viame_partner.utils.Utils

import kotlinx.android.synthetic.main.activity_forgot_password.*
import javax.inject.Inject

class ForgotPasswordActivity :  BaseActivity<ForgotPasswordActivityViewModel>() , ForgotPasswordEventHandler {


    lateinit var binding : ActivityForgotPasswordBinding

    @Inject
    lateinit var model : ForgotPasswordActivityViewModel

    @Inject
    lateinit var apiInterface: ApiInterface

    @Inject
    lateinit var connectivityReceiver: ConnectivityReceiver

    override fun getViewModel(): ForgotPasswordActivityViewModel {
        return model
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password)

        // Setting binding params
        binding.viewModel = model
        binding.clickHandler = this

        setSupportActionBar(binding.toolbar)
        setTitle(R.string.title_activity_forgot_password)
        binding.toolbar.setTitle(getString(R.string.forgot_password))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // Watching for forgot pass result
        model.getForgotPassValidationResult().observe(this, Observer
        { result ->
            if (result != "Valid") {
                Toast.makeText(this, result, Toast.LENGTH_SHORT).show()
            } else {
                connectivityReceiver.observe(this, Observer { connectionModel ->
                    if (connectionModel != null && connectionModel.isConnected()) {
                        if (result == "Valid")
                        {
//                                    var signInRequestModel  = SignInRequestModel()
//                                    signInRequestModel.entAppversion = BuildConfig.VERSION_NAME
//                                    signInRequestModel.entDevMake = android.os.Build.MANUFACTURER
//                                    signInRequestModel.entDeviceId = Utils.getDeviceId(this)
//                                    signInRequestModel.entDeviceTime = Utils.date()
//                                    signInRequestModel.entDevModel = android.os.Build.MODEL + " "+android.os.Build.PRODUCT
//                                    signInRequestModel.entDevtype = DEVICE_TYPE
//                                    signInRequestModel.entPushToken = Preferences.getPreferenceString(
//                                        PrefEntity.FIREBASE_MESSEGING_REG_ID)
//                                    signInRequestModel.mobile = model.emailMobile
//                                    signInRequestModel.password = model.password
//
//                                    apiInterface.let { model.forgotPasswordOTPCall(signInRequestModel, it) }

                        }
                    }
                    else
                    {
                        if (connectionModel?.type == Vpn) {

                            Alerts.showSnackBar(
                                this@ForgotPasswordActivity,
                                getString(R.string.vpn_use_message)
                            )
                        } else {
                            Alerts.showSnackBar(
                                this@ForgotPasswordActivity,
                                getString(R.string.internet_not_available)
                            )

                        }
                    }
                })
            }
        })
    }

    override fun onSubmitClicked() {
            model.performValidation()
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return true
    }
}
