package com.viame.viame_partner.ui.settings;
import com.viame.viame_partner.ui.myorders.MyOrdersViewModel;

import dagger.Module;
import dagger.Provides;

@Module
public class SettingsFragmentModule {

    @Provides
    SettingsViewModel mySettingsViewModel() {
        return new SettingsViewModel();
    }


}
