package com.viame.viame_partner.ui.main.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pubnubkeys {

    @SerializedName("publishKey")
    @Expose
    private String publishKey;
    @SerializedName("subscribeKey")
    @Expose
    private String subscribeKey;

    public String getPublishKey() {
        return publishKey;
    }

    public void setPublishKey(String publishKey) {
        this.publishKey = publishKey;
    }

    public String getSubscribeKey() {
        return subscribeKey;
    }

    public void setSubscribeKey(String subscribeKey) {
        this.subscribeKey = subscribeKey;
    }
}
