package com.viame.viame_partner.ui.transfer_order

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.viame.viame_partner.*
import com.viame.viame_partner.application.CatalogApplication
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.transfer_order.model.GetAllDriverOutputModel
import com.viame.viame_partner.ui.transfer_order.model.TransferOrderInputModel
import com.viame.viame_partner.ui.transfer_order.model.TransferOrderOutputModel
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.*
import javax.inject.Inject


class TransferOrderActivityModel @Inject constructor() : ViewModel()
{

    private val GetDriversListApiResponse = MutableLiveData<APIRequestResponseHandler<GetAllDriverOutputModel>>()

    fun GetDriversListAPIDataResult(): MutableLiveData<APIRequestResponseHandler<GetAllDriverOutputModel>> = GetDriversListApiResponse

    fun GetDriversListAPICall(apiInterface : ApiInterface)
    {
        GetDriversListApiResponse.value = APIRequestResponseHandler.loading(null)

        val headerMap = HashMap<String,String>()
        headerMap["Content-Type"] = "application/json;charset=UTF-8"
        headerMap["authorization"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["token"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["userType"] = USER_TYPE.toString()

        val posts: Call<GetAllDriverOutputModel> = apiInterface.callGetDriverListMethod("$BASE_ENGINE_URL$GET_ALL_DRIVER_LIST",headerMap)
        //Enqueue the call
        posts.enqueue(object : Callback<GetAllDriverOutputModel>
        {
            @SuppressLint("TimberArgCount")
            override fun onFailure(call: Call<GetAllDriverOutputModel>, t: Throwable) {
                try {
                    GetDriversListApiResponse.value = APIRequestResponseHandler.error(t.message.toString(),null)
                    Timber.e(t.message.toString())
                } catch (e: Exception) {
                }
            }
            @SuppressLint("TimberArgCount")
            override fun onResponse(call: Call<GetAllDriverOutputModel>, response: Response<GetAllDriverOutputModel>
            ) {
                try {
                    if(response.isSuccessful) {
                        response.body().let {
                            GetDriversListApiResponse.value = APIRequestResponseHandler.success(it)
                            Timber.e(response.body().toString())
                        }
                    }

                } catch (e: Exception)
                {
                    e.printStackTrace()
                    GetDriversListApiResponse.value = APIRequestResponseHandler.error("Status code "+response.code(),null)
                }
            }
        })
    }

    private val TransferOrderApiResponse = MutableLiveData<APIRequestResponseHandler<TransferOrderOutputModel>>()

    fun TransferOrderAPIDataResult(): MutableLiveData<APIRequestResponseHandler<TransferOrderOutputModel>> = TransferOrderApiResponse

    fun TransferOrderAPICall(apiInterface : ApiInterface, transferOrderInputModel: TransferOrderInputModel)
    {
        TransferOrderApiResponse.value = APIRequestResponseHandler.loading(null)

        val headerMap = HashMap<String,String>()
        headerMap["Content-Type"] = "application/json;charset=UTF-8"
        headerMap["authorization"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["token"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["userType"] = USER_TYPE.toString()

        val posts: Call<TransferOrderOutputModel> = apiInterface.callTransferOrderMethod("$BASE_ENGINE_URL$TRANSFER_ORDER",headerMap, transferOrderInputModel)
        //Enqueue the call
        posts.enqueue(object : Callback<TransferOrderOutputModel>
        {
            @SuppressLint("TimberArgCount")
            override fun onFailure(call: Call<TransferOrderOutputModel>, t: Throwable) {

                try {
                    TransferOrderApiResponse.value = APIRequestResponseHandler.error(t.message.toString(),null)
                    Timber.e(t.message.toString())
                } catch (e: Exception) {
                }
            }

            @SuppressLint("TimberArgCount")
            override fun onResponse(call: Call<TransferOrderOutputModel>, response: Response<TransferOrderOutputModel>
            ) {
                try {
                    if(response.isSuccessful) {
                        response.body().let {

                            TransferOrderApiResponse.value = APIRequestResponseHandler.success(it)
                            Timber.e(response.body().toString())
                        }
                    }
                    else
                    {
                        val responseError = TransferOrderOutputModel()
                        try {
                            val jsonObject = JSONObject(response.errorBody()?.string()!!)
                            // {"data":"The current driver is not logged in.","error":true,"success":false,"message":"Something wen't wrong"}

                            responseError.data = jsonObject["data"].toString()
                            responseError.error = jsonObject["error"] as Boolean
                            responseError.success = jsonObject["success"] as Boolean
                            responseError.message = jsonObject["message"].toString()

                            TransferOrderApiResponse.value = APIRequestResponseHandler.error(jsonObject["data"].toString(), responseError)
                        } catch (e: Exception) {

                            responseError.data =
                                CatalogApplication.instance?.getString(R.string.something_goes_wrong)
                            responseError.message =
                                CatalogApplication.instance?.getString(R.string.something_goes_wrong)

                            TransferOrderApiResponse.value = APIRequestResponseHandler.error(
                                response.errorBody()?.string(),
                                responseError
                            )
                        }
                    }
                } catch (e: Exception)
                {
                    e.printStackTrace()
                    TransferOrderApiResponse.value = APIRequestResponseHandler.error("Status code "+response.code(),null)
                }
            }
        })
    }


}