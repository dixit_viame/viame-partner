package com.viame.viame_partner.ui.settings

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.Switch
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.ActivityCompat.recreate
import androidx.databinding.DataBindingUtil
import com.viame.viame_partner.R
import com.viame.viame_partner.databinding.FragmentSettingsBinding
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.base.BaseFragment
import com.viame.viame_partner.ui.home.HomeFragmentViewModel
import com.viame.viame_partner.ui.main.MainActivity
import com.viame.viame_partner.ui.splash.SplashScreenActivity
import javax.inject.Inject

class SettingsFragment : BaseFragment<SettingsViewModel>() {

    private lateinit var binding: FragmentSettingsBinding
    @Inject
    lateinit var model : SettingsViewModel

    override fun getViewModel(): SettingsViewModel {
        return model
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings,container,false)

        binding.darkLightModeSwitch.textOn = getString(R.string.dark_mode)
        binding.darkLightModeSwitch.textOff = getString(R.string.light_mode)

        //SWitch Light Dark mode
        binding.darkLightModeSwitch.isChecked = Preferences.getPreferenceBoolean(PrefEntity.PREFERENCE_NIGHT_MODE)


        binding.darkLightModeSwitch.setOnCheckedChangeListener { _, isChecked ->

            if(isChecked) {
                AppCompatDelegate.setDefaultNightMode(
                    AppCompatDelegate.MODE_NIGHT_YES
                )
                Preferences.setPreference(PrefEntity.PREFERENCE_NIGHT_MODE, true)
                val intent = Intent(activity, MainActivity::class.java)
                intent.flags =
                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                activity?.startActivity(intent)
                (activity)?.finish()

            }else {
                AppCompatDelegate.setDefaultNightMode(
                    AppCompatDelegate.MODE_NIGHT_NO
                )
                Preferences.setPreference(PrefEntity.PREFERENCE_NIGHT_MODE, false)
                val intent = Intent(activity, MainActivity::class.java)
                intent.flags =
                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                activity?.startActivity(intent)
                (activity)?.finish()
            }

        }

        return binding.root
    }

}