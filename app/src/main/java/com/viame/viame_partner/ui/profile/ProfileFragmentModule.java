package com.viame.viame_partner.ui.profile;

import dagger.Module;
import dagger.Provides;

@Module
public class ProfileFragmentModule {

    @Provides
    ProfileViewModel profileViewModel() {
        return new ProfileViewModel();
    }

}
