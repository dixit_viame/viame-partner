package com.viame.viame_partner.ui.myorders

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.viame.viame_partner.R
import com.viame.viame_partner.databinding.RowItemMyordersBinding
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.myorders.MyOrdersFragmentAdapter.CategoryViewHolder
import com.viame.viame_partner.ui.myorders.model.Appointment
import com.viame.viame_partner.utils.Utils
import java.util.*
class MyOrdersFragmentAdapter constructor(private val context: Context, private var categoryList: ArrayList<Appointment>, private val listener: MyOrderListEventHandler) : RecyclerView.Adapter<CategoryViewHolder>()
{
    private var layoutInflater: LayoutInflater? = null

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): CategoryViewHolder {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }
        val view = layoutInflater!!.inflate(R.layout.row_item_myorders, parent, false)
        return CategoryViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.binding!!.viewModel = categoryList[holder.adapterPosition]
        holder.binding.tvBid.text = categoryList[holder.adapterPosition].orderId.toString()
        holder.binding.tvDate.text = Utils.dateFormatter(categoryList[holder.adapterPosition].bookingTime)
        holder.binding.tvPickupTime.text = Utils.dateFormatter(categoryList[holder.adapterPosition].bookingTime)
        holder.binding.tvDropoffTime.text = Utils.dateFormatter(categoryList[holder.adapterPosition].bookingTime)
        holder.binding.tvBookingPrice.text =
            "${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} ${categoryList[holder.adapterPosition].invoice.total}"

        holder.binding.tvBookingFrom.text = categoryList[holder.adapterPosition].bookingOrigin

        if(categoryList[holder.adapterPosition].apptType == "1") {
            holder.binding.tvBookingType.text = context.getString(R.string.instant_booking)
        }else
        {
            holder.binding.tvBookingType.text = context.getString(R.string.book_later_text)
        }
        holder.binding.handlers = listener
        holder.binding.indexView = holder.binding.root
        //Triggers the View to be updated with the new values provided.
        // This method has to be run on the UI thread.
        holder.binding.executePendingBindings()
    }

    override fun getItemCount(): Int {
        return categoryList.size
    }

    fun getCategoryList(): ArrayList<Appointment> {
        return categoryList
    }

    fun setCategoryList(categories: ArrayList<Appointment>) {
        categoryList = categories
        notifyDataSetChanged()
    }

    inner class CategoryViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val binding: RowItemMyordersBinding?
        init {
            binding = DataBindingUtil.bind(v)
            v.tag = binding
        }
    }
}