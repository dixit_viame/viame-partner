package com.viame.viame_partner.ui.home

import android.Manifest
import android.app.PendingIntent
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.gson.Gson
import com.viame.viame_partner.*
import com.viame.viame_partner.application.CatalogApplication
import com.viame.viame_partner.databinding.FragmentHomeBinding
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.base.BaseFragment
import com.viame.viame_partner.ui.bookingaccept.model.BookingData
import com.viame.viame_partner.ui.home.model.GetStatusUpdateInputModel
import com.viame.viame_partner.ui.main.MainActivity
import com.viame.viame_partner.utils.Alerts
import com.viame.viame_partner.utils.LinNotify
import com.viame.viame_partner.utils.PermissionHelper
import com.viame.viame_partner.utils.permission.annotation.PermissionDenied
import com.viame.viame_partner.utils.permission.annotation.PermissionGranted
import com.viame.viame_partner.workmanager.*
import org.json.JSONObject
import timber.log.Timber
import javax.inject.Inject


class HomeFragment : BaseFragment<HomeFragmentViewModel>() , HomeFragEventHandler
{

    @Inject
    lateinit var apiInterface: ApiInterface

    @Inject
    lateinit var model : HomeFragmentViewModel

    override fun getViewModel(): HomeFragmentViewModel {
        return model
    }

    private lateinit var navigationController: NavController

    lateinit var binding: FragmentHomeBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home,container,false)
        binding.viewModel = model
        binding.clickHandler = this

        binding.fab.setOnClickListener { view ->

             Alerts.showListPopupMenu(activity,view,R.array.home_menu_list, Alerts.OnPopupWindowItemClick { position ->

             if(position == 0)
             {
                    (activity as MainActivity).onStartScanAction()
             }
            }).show()
        }

        navigationController = findNavController(binding.root.findViewById(R.id.nav_host_fragment_bottom))
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_myorder, R.id.navigation_unassigned
            )
        )
        setupActionBarWithNavController(activity as AppCompatActivity,navigationController ,appBarConfiguration)
        binding.navView.setupWithNavController(navigationController)


        return binding.root
    }



    override fun onActivityCreated(savedInstanceState: Bundle?)
    {
        super.onActivityCreated(savedInstanceState)
        var status = Preferences.getPreferenceInt(PrefEntity.DRIVER_PRESENCE_STATUS)


        if(status == DriverStatusType.DEFAULT.id)
        {
            binding.imgDriverOnlineOffline.setImageResource(R.drawable.ic_offline)
            binding.tvOnlineOffline.text = CatalogApplication.instance?.getString(R.string.you_are_offline)
            binding.tvLblOnlineOffline.text = CatalogApplication.instance?.getString(R.string.go_online_to_start_accepting_jobs)

        }
        else if(status == DriverStatusType.ONLINE.id)
        {
            binding.imgDriverOnlineOffline.setImageResource(R.drawable.ic_online)
            binding.tvOnlineOffline.text = CatalogApplication.instance?.getString(R.string.you_are_online)
            binding.tvLblOnlineOffline.text = CatalogApplication.instance?.getString(R.string.go_offline_to_stop_accepting_jobs)

            // Start Location Sending Work
            startAlarmService(activity!!.applicationContext,ALARM_MANAGER_NOTIFICATION_CODE)

        }
        else if(status == DriverStatusType.OFFLINE.id)
        {
            binding.imgDriverOnlineOffline.setImageResource(R.drawable.ic_offline)
            binding.tvOnlineOffline.text = CatalogApplication.instance?.getString(R.string.you_are_offline)
            binding.tvLblOnlineOffline.text = CatalogApplication.instance?.getString(R.string.go_online_to_start_accepting_jobs)

        }
        model.getOnlineOfflineDataResult().observe(this, Observer
        {
            result ->

            if(result != null)
            {
                try {
                    // Fedein/Fadeout animation online/offline
                    val anim = AlphaAnimation(1.0f, 0.0f)
                    anim.duration = 400
                    anim.repeatCount = 1
                    anim.repeatMode = Animation.REVERSE

                    anim.setAnimationListener(object : Animation.AnimationListener {
                        override fun onAnimationEnd(animation: Animation?) { }
                        override fun onAnimationStart(animation: Animation?) { }
                        override fun onAnimationRepeat(animation: Animation?)
                        {
                            binding.imgDriverOnlineOffline.setImageResource(result.drawableId!!)
                            binding.tvOnlineOffline.text = (result.stringTitle!!)
                            binding.tvLblOnlineOffline.text = (result.stringSubTitle!!)
                        }
                    })

                    binding.tvOnlineOffline.startAnimation(anim)
                    binding.tvLblOnlineOffline.startAnimation(anim)
                    binding.imgDriverOnlineOffline.startAnimation(anim)

                } catch (e: Exception) {
                }
            }
        })


    }

    override fun onOfflineOnlineClicked()
    {
        if(PermissionHelper.getInstance().hasPermission(activity,Manifest.permission.ACCESS_FINE_LOCATION))
        {
            var status = Preferences.getPreferenceInt(PrefEntity.DRIVER_PRESENCE_STATUS)

            if(status == DriverStatusType.DEFAULT.id || status == DriverStatusType.OFFLINE.id)
            {
                status = DriverStatusType.ONLINE.id

                // Start Location Sending Work
                startAlarmService(activity!!.applicationContext,ALARM_MANAGER_NOTIFICATION_CODE)
            }
            else if(status == DriverStatusType.DEFAULT.id || status == DriverStatusType.ONLINE.id)
            {
                status = DriverStatusType.OFFLINE.id

                // Stop Location Sending Work
                if (isMyWorkerRunning(NotifyLocationBackGroundWork.TAG, activity!!.applicationContext))
                {
                    stopLocationSendWorkManager(activity!!.applicationContext,NotifyLocationBackGroundWork.TAG)
                }
                stopAlarmService(activity!!.applicationContext,ALARM_MANAGER_NOTIFICATION_CODE)

            }

            var getStatusUpdateInputModel = GetStatusUpdateInputModel()
            getStatusUpdateInputModel.entStatus = status

            getStatusUpdateInputModel.pubnubStr = Preferences.getPreferenceString(PrefEntity.ALL_BOOKING_INFO)

            model.goToOnlineOffline(apiInterface,getStatusUpdateInputModel)
        }
        else
        {
            requestLocationPermission()
        }
    }


    private fun requestLocationPermission() {
        PermissionHelper.getInstance().requestPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
    }


    @PermissionGranted(permission = Manifest.permission.ACCESS_FINE_LOCATION)
    internal  fun cameraGranted() {
        Timber.e("ACCESS_FINE_LOCATION_GRANT")
    }


    @PermissionDenied(permission = Manifest.permission.ACCESS_FINE_LOCATION)
    internal  fun cameraDenied() {
        Timber.e("ACCESS_FINE_LOCATION_DENIED")
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        PermissionHelper.getInstance().onRequestPermissionsResult(this, permissions, grantResults)
    }
}