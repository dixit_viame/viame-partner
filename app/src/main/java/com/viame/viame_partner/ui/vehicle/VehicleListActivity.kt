package com.viame.viame_partner.ui.vehicle

import android.annotation.SuppressLint
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.core.app.NotificationManagerCompat
import androidx.core.view.ViewCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.viewpager.widget.ViewPager
import androidx.work.ListenableWorker
import com.squareup.picasso.Picasso
import com.viame.viame_partner.ONLINE_OFFLINE_NOTIFICATION_ID
import com.viame.viame_partner.R
import com.viame.viame_partner.Vpn
import com.viame.viame_partner.databinding.ActivityVehicleListBinding
import com.viame.viame_partner.extension.fromSingleToMain
import com.viame.viame_partner.firebase.unsubscribeAllTopics
import com.viame.viame_partner.homewidget.updateWidgets
import com.viame.viame_partner.location.LocationCallRepository
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.rx.Scheduler
import com.viame.viame_partner.ui.base.BaseActivity
import com.viame.viame_partner.ui.main.MainActivity
import com.viame.viame_partner.ui.splash.SplashScreenActivity
import com.viame.viame_partner.ui.vehicle.model.VehicleConfirmInputModel
import com.viame.viame_partner.ui.vehicle.model.VehicleListDataModel
import com.viame.viame_partner.ui.vehicle.ui.main.SectionsPagerAdapter
import com.viame.viame_partner.utils.Alerts
import com.viame.viame_partner.utils.ConnectivityReceiver
import com.viame.viame_partner.utils.ElevationOverlayProvider
import com.viame.viame_partner.utils.Utils
import com.viame.viame_partner.workmanager.NotifyLocationBackGroundWork
import com.viame.viame_partner.workmanager.stopLocationSendWorkManager
import timber.log.Timber
import javax.inject.Inject

class VehicleListActivity : BaseActivity<VehicleListActivityModel>(), VehicleSelectEventHandler{

    companion object {
        var PAGES = 5
        // You can choose a bigger number for LOOPS, but you know, nobody will fling
        // more than 1000 times just in order to test your "infinite" ViewPager :D
        var LOOPS = 1
        var FIRST_PAGE = PAGES * LOOPS / 2
    }

    lateinit var  binding : ActivityVehicleListBinding

    var listVehicle = ArrayList<VehicleListDataModel>()

    var selectedVehicle : VehicleListDataModel? = null

    @Inject
    lateinit var repository: LocationCallRepository

    @Inject
    lateinit var model : VehicleListActivityModel

    @Inject
    lateinit var apiInterface: ApiInterface

    @Inject
    lateinit var scheduler: Scheduler

    @Inject
    lateinit var connectivityReceiver: ConnectivityReceiver


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView<ActivityVehicleListBinding>(this@VehicleListActivity, R.layout.activity_vehicle_list)

        repository.getLocation()
        // Setting binding params
        binding.viewModel = model
        binding.clickHandler = this@VehicleListActivity

        val elevation = Utils.dpToPx(this@VehicleListActivity,10)
        val overlayProvider = ElevationOverlayProvider(this@VehicleListActivity)
        val color: Int = overlayProvider.compositeOverlayWithThemeSurfaceColorIfNeeded(elevation)
        ViewCompat.setElevation(binding.toolbar, elevation)
        binding.toolbar.setBackgroundColor(color)

        model.getVehicleDataFromPreferences()

        Picasso.get()
            .load(Preferences.getPreferenceString(PrefEntity.PROFILE_PIC))
            .placeholder(R.drawable.ic_pic_register_background_)
            .error(R.drawable.ic_pic_register_background_)
            .into(binding.imgDriverProfile)

        model.getVehicleListResult().observe(this@VehicleListActivity, Observer { result ->

            if (result != null) {

                if (result.status == APIRequestResponseHandler.AuthStatus.LOADING) {
                    Alerts.showProgressBar(this@VehicleListActivity)

                } else if (result.status == APIRequestResponseHandler.AuthStatus.ERROR) {
                    try {
                        Alerts.dismissProgressBar()
                        Alerts.showSnackBar(this@VehicleListActivity, result.data.message)
                    } catch (e: Exception) {
                    }

                } else if (result.status == APIRequestResponseHandler.AuthStatus.SUCCESS) {
                    Alerts.dismissProgressBar()

                    try {
                        if (result.data?.error!!) {
                            Alerts.showSnackBar(this@VehicleListActivity, result.data.message)
                        }
                        else
                        {
                            listVehicle = result.data.data.data as ArrayList<VehicleListDataModel>
                            if(listVehicle.size > 0) {
                                PAGES = listVehicle.size
                                FIRST_PAGE = PAGES * LOOPS / 2

                                val sectionsPagerAdapter =
                                    SectionsPagerAdapter(
                                        this@VehicleListActivity,
                                        supportFragmentManager
                                    )
                                binding.viewPager.adapter = sectionsPagerAdapter
                                binding.viewPager.setPageTransformer(false, sectionsPagerAdapter)

                                // Set current item to the middle page so we can fling to both
                                // directions left and right
                                // Set current item to the middle page so we can fling to both directions left and right
                                binding.viewPager.setCurrentItem(FIRST_PAGE)

                                // Necessary or the pager will only have one extra page to show
                                // make this at least however many pages you can see
                                // Necessary or the pager will only have one extra page to show make this at least however many pages you can see
                                binding.viewPager.setOffscreenPageLimit(4)

                                // Set margin for pages as a negative number, so a part of next and
                                // previous pages will be showed
                                // Set margin for pages as a negative number, so a part of next and previous pages will be showed
                                binding.viewPager.setPageMargin(
                                    Utils.dpToPx(
                                        this@VehicleListActivity,
                                        (-175)
                                    ).toInt()
                                )

                                binding.viewPager.addOnPageChangeListener(object :
                                    ViewPager.OnPageChangeListener {

                                    override fun onPageScrollStateChanged(state: Int) {

                                    }

                                    override fun onPageScrolled(
                                        position: Int,
                                        positionOffset: Float,
                                        positionOffsetPixels: Int
                                    ) {

                                    }

                                    override fun onPageSelected(position: Int) {

                                        Timber.e("onPageSelected $position")
                                        selectedVehicle = null
                                    }

                                })
                            }
                            else
                            {
                                Alerts.showBottomSheetSimpleConfirmationDialog(this@VehicleListActivity,
                                    "Oops!!, Your vehicle is reserved with another Partner, Please signin again",
                                    null, true, getString(R.string.action_cancel),
                                    getString(R.string.ok), object : Alerts.OnConfirmationDialog {
                                        @SuppressLint("CheckResult")
                                        override fun onYes() {

                                            connectivityReceiver.observe(this@VehicleListActivity, Observer { connectionModel ->

                                                if (connectionModel != null && connectionModel.isConnected()) {
                                                    apiInterface.let {

                                                        model.getLogoutAPIResult().observe(this@VehicleListActivity, Observer { viewResponse ->

                                                            if (viewResponse != null && viewResponse.status == APIRequestResponseHandler.AuthStatus.SUCCESS) {
                                                                Alerts.dismissProgressBar()
                                                                repository.deleteLocationWhenLogout()
                                                                    .fromSingleToMain(scheduler = scheduler).subscribe(
                                                                        { onNext ->
                                                                            println("$onNext")
                                                                            ListenableWorker.Result.success()
                                                                        },
                                                                        { onError ->
                                                                            println("$onError")
                                                                            ListenableWorker.Result.failure()
                                                                        })

                                                                unsubscribeAllTopics(
                                                                    Preferences.getPreferenceString(
                                                                        PrefEntity.PUSH_TOPICS
                                                                    )
                                                                )
                                                                stopLocationSendWorkManager(
                                                                    this@VehicleListActivity,
                                                                    NotifyLocationBackGroundWork.TAG
                                                                )

                                                                //                    // Disable Online Offline notification.
                                                                val notificationManagerCompat =
                                                                    NotificationManagerCompat.from(this@VehicleListActivity)
                                                                notificationManagerCompat.cancel(
                                                                    ONLINE_OFFLINE_NOTIFICATION_ID
                                                                )

                                                                val manager =
                                                                    this@VehicleListActivity.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                                                                manager.cancel(ONLINE_OFFLINE_NOTIFICATION_ID)

                                                                updateWidgets(this@VehicleListActivity)

                                                                val skipMessage: Boolean = Preferences.getPreferenceBoolean(PrefEntity.SKIP_PROTECTED_APP)
                                                                Preferences.removeAllPreference()
                                                                Preferences.setPreference(PrefEntity.SKIP_PROTECTED_APP,skipMessage)

                                                                val intent = Intent(
                                                                    this@VehicleListActivity,
                                                                    SplashScreenActivity::class.java
                                                                )
                                                                intent.flags =
                                                                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                                                startActivity(intent)
                                                                finish()
                                                            } else if (viewResponse != null && viewResponse.status == APIRequestResponseHandler.AuthStatus.LOADING) {
                                                                Alerts.showProgressBar(this@VehicleListActivity)
                                                            } else if (viewResponse != null && viewResponse.status == APIRequestResponseHandler.AuthStatus.ERROR) {
                                                                Alerts.dismissProgressBar()
                                                                Alerts.showSnackBar(
                                                                    this@VehicleListActivity,
                                                                    viewResponse.message
                                                                )
                                                            } else {
                                                                Alerts.dismissProgressBar()
                                                                Alerts.showSnackBar(
                                                                    this@VehicleListActivity,
                                                                    viewResponse.message
                                                                )
                                                            }
                                                        })

                                                        model.callLogoutAPI(apiInterface)
                                                    }
                                                } else {
                                                    if (connectionModel?.type == Vpn) {
                                                        Alerts.showSnackBar(
                                                            this@VehicleListActivity,
                                                            getString(R.string.vpn_use_message)
                                                        )
                                                    } else {
                                                        Alerts.showSnackBar(
                                                            this@VehicleListActivity,
                                                            getString(R.string.internet_not_available)
                                                        )
                                                    }
                                                }
                                            })

                                        }
                                        override fun onNo() {

                                        }
                                    })
                            }
                        }
                    } catch (e: Exception) {
                    }
                }
            }
        })

        connectivityReceiver.observe(this@VehicleListActivity, Observer { connectionModel ->
            if (connectionModel != null && connectionModel.isConnected())
            {

                apiInterface.let { model.getVehicleListFromAPI(apiInterface) }
            }
            else
            {
                if(connectionModel?.type == Vpn)
                {
                    Alerts.showSnackBar(this@VehicleListActivity,getString(R.string.vpn_use_message))
                }
                else
                {
                    Alerts.showSnackBar(this@VehicleListActivity,getString(R.string.internet_not_available))
                }
            }
        })

        model.getVehicleAPIDataResult().observe(this@VehicleListActivity, Observer { result ->

            if (result.data != null) {

                if (result.status == APIRequestResponseHandler.AuthStatus.LOADING) {
                    Alerts.showProgressBar(this@VehicleListActivity)

                } else if (result.status == APIRequestResponseHandler.AuthStatus.ERROR) {
                    Alerts.dismissProgressBar()
                    Alerts.showSnackBar(this@VehicleListActivity, result.message)

                } else if (result.status == APIRequestResponseHandler.AuthStatus.SUCCESS) {
                    Alerts.dismissProgressBar()

                    if (result.data.errFlag == 1) {
                        Alerts.showSnackBar(this@VehicleListActivity, result.data.errMsg)
                    } else {
                        Preferences.setPreference(PrefEntity.IS_LOGIN, true)
                        Preferences.setPreference(PrefEntity.VEHICLE_IMAGE, selectedVehicle?.image)
                        Preferences.setPreference(PrefEntity.VEHICLE_TYPE_ID, selectedVehicle?.typeId.toString())
                        Preferences.setPreference(PrefEntity.VEHICLE_WORK_PLACE_ID, selectedVehicle?.id.toString())

                        updateWidgets(this@VehicleListActivity)
                        model.getVehicleAPIDataResult().removeObservers(this@VehicleListActivity)

                        var mVarargs =  Intent(this@VehicleListActivity, MainActivity::class.java)
                        startActivityForResult(mVarargs,100)
                        this@VehicleListActivity.finish()
                    }
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 100)
        {
            this@VehicleListActivity.finish()
        }
    }

    override fun onVehicleSelect() {

        if(selectedVehicle != null)
        {
            connectivityReceiver.observe(this@VehicleListActivity, Observer { connectionModel ->
                if (connectionModel != null && connectionModel.isConnected())
                {
                        val signInRequestModel =
                            VehicleConfirmInputModel()
                        signInRequestModel.mid = Preferences.getPreferenceString(PrefEntity.MID)
                        signInRequestModel.typeId = selectedVehicle?.typeId.toString()
                        signInRequestModel.workplaceId = selectedVehicle?.id.toString()

                        apiInterface.let { model.vehicleActionCall(signInRequestModel, it) }
                }
                else
                {
                    if(connectionModel?.type == Vpn)
                    {
                        Alerts.showSnackBar(this@VehicleListActivity,getString(R.string.vpn_use_message))
                    }
                    else
                    {
                        Alerts.showSnackBar(this@VehicleListActivity,getString(R.string.internet_not_available))
                    }
                }
            })
        }
        else
        {
            Alerts.showSnackBar(this@VehicleListActivity,getString(R.string.select_vehicle_text))
        }

    }


    override fun onLogoutSelect() {

        Alerts.showBottomSheetSimpleConfirmationDialog(this@VehicleListActivity,
            getString(R.string.logout_msg),
            null, false, getString(R.string.action_cancel),
            getString(R.string.logout), object : Alerts.OnConfirmationDialog {
                @SuppressLint("CheckResult")
                override fun onYes() {

                    connectivityReceiver.observe(this@VehicleListActivity, Observer { connectionModel ->

                        if (connectionModel != null && connectionModel.isConnected()) {
                            apiInterface.let {

                                model.getLogoutAPIResult().observe(this@VehicleListActivity, Observer { viewResponse ->

                                    if (viewResponse != null && viewResponse.status == APIRequestResponseHandler.AuthStatus.SUCCESS) {
                                        Alerts.dismissProgressBar()
                                        repository.deleteLocationWhenLogout()
                                            .fromSingleToMain(scheduler = scheduler).subscribe(
                                                { onNext ->
                                                    println("$onNext")
                                                    ListenableWorker.Result.success()
                                                },
                                                { onError ->
                                                    println("$onError")
                                                    ListenableWorker.Result.failure()
                                                })

                                        unsubscribeAllTopics(
                                            Preferences.getPreferenceString(
                                                PrefEntity.PUSH_TOPICS
                                            )
                                        )
                                        stopLocationSendWorkManager(
                                            this@VehicleListActivity,
                                            NotifyLocationBackGroundWork.TAG
                                        )

                                        //                    // Disable Online Offline notification.
                                        val notificationManagerCompat =
                                            NotificationManagerCompat.from(this@VehicleListActivity)
                                        notificationManagerCompat.cancel(
                                            ONLINE_OFFLINE_NOTIFICATION_ID
                                        )

                                        val manager =
                                            this@VehicleListActivity.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                                        manager.cancel(ONLINE_OFFLINE_NOTIFICATION_ID)

                                        updateWidgets(this@VehicleListActivity)

                                        Preferences.removeAllPreference()
                                        val intent = Intent(
                                            this@VehicleListActivity,
                                            SplashScreenActivity::class.java
                                        )
                                        intent.flags =
                                            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                        startActivity(intent)
                                        finish()
                                    } else if (viewResponse != null && viewResponse.status == APIRequestResponseHandler.AuthStatus.LOADING) {
                                        Alerts.showProgressBar(this@VehicleListActivity)
                                    } else if (viewResponse != null && viewResponse.status == APIRequestResponseHandler.AuthStatus.ERROR) {
                                        Alerts.dismissProgressBar()
                                        Alerts.showSnackBar(
                                            this@VehicleListActivity,
                                            viewResponse.message
                                        )
                                    } else {
                                        Alerts.dismissProgressBar()
                                        Alerts.showSnackBar(
                                            this@VehicleListActivity,
                                            viewResponse.message
                                        )
                                    }
                                })

                                model.callLogoutAPI(apiInterface)
                            }
                        } else {
                            if (connectionModel?.type == Vpn) {
                                Alerts.showSnackBar(
                                    this@VehicleListActivity,
                                    getString(R.string.vpn_use_message)
                                )
                            } else {
                                Alerts.showSnackBar(
                                    this@VehicleListActivity,
                                    getString(R.string.internet_not_available)
                                )
                            }
                        }
                    })

                }
                override fun onNo() {

                }
            })
    }

    override fun onItemClick(position: Int) {
        Timber.e("onItemClick $position")
        selectedVehicle = listVehicle[position]
    }

    override fun getViewModel(): VehicleListActivityModel {
        return model
    }
}