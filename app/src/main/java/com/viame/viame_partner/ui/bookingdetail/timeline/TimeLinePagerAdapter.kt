package com.viame.viame_partner.ui.bookingdetail.timeline

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.viame.viame_partner.R
import com.viame.viame_partner.utils.VectorDrawableUtils
import com.viame.viame_partner.widgets.TimelineView
import kotlinx.android.synthetic.main.row_item_timeline.view.*

/**
 * Created by Vipul Asri on 13-12-2018.
 */

class TimeLinePagerAdapter(private val mFeedList: List<TimeLineModel>) : RecyclerView.Adapter<TimeLinePagerAdapter.TimeLineViewHolder>() {

    private lateinit var mLayoutInflater: LayoutInflater

    override fun getItemViewType(position: Int): Int {
        return TimelineView.getTimeLineViewType(position, itemCount)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TimeLineViewHolder {

        if(!::mLayoutInflater.isInitialized) {
            mLayoutInflater = LayoutInflater.from(parent.context)
        }

        return TimeLineViewHolder(mLayoutInflater.inflate(R.layout.row_item_timeline, parent, false), viewType)
    }

    override fun onBindViewHolder(holder: TimeLineViewHolder, position: Int) {

        val timeLineModel = mFeedList[holder.adapterPosition]

        when {
            timeLineModel.status == OrderStatus.INACTIVE -> {
                setMarker(holder, R.drawable.ic_marker_inactive, R.color.light_shadow)
            }
            timeLineModel.status == OrderStatus.ACTIVE -> {
                setMarker(holder, R.drawable.ic_marker_active, R.color.light_shadow)
            }
            else -> {
                setMarker(holder, R.drawable.ic_marker, R.color.lightGray)
            }
        }

        if (timeLineModel.date.isNotEmpty())
        {
            holder.date.visibility =View.VISIBLE
            holder.date.text = timeLineModel.date
        } else
            holder.date.visibility = View.GONE

        holder.message.text = timeLineModel.message
    }

    private fun setMarker(holder: TimeLineViewHolder, drawableResId: Int, colorFilter: Int) {
        holder.timeline.marker = VectorDrawableUtils.getDrawable(holder.itemView.context, drawableResId, ContextCompat.getColor(holder.itemView.context, colorFilter))
    }

    override fun getItemCount() = mFeedList.size

    inner class TimeLineViewHolder(itemView: View, viewType: Int) : RecyclerView.ViewHolder(itemView) {

        val date = itemView.tvTimeLineDate
        val message = itemView.tvTitle
        val timeline = itemView.timeline

        init {
            timeline.initLine(viewType)
        }
    }

}
