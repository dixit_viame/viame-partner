package com.viame.viame_partner.ui.home.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetStatusUpdateInputModel {

    @SerializedName("pubnubStr")
    @Expose
    private String pubnubStr;
    @SerializedName("ent_status")
    @Expose
    private int entStatus;

    public String getPubnubStr() {
        return pubnubStr;
    }

    public void setPubnubStr(String pubnubStr) {
        this.pubnubStr = pubnubStr;
    }

    public Integer getEntStatus() {
        return entStatus;
    }

    public void setEntStatus(Integer entStatus) {
        this.entStatus = entStatus;
    }

}
