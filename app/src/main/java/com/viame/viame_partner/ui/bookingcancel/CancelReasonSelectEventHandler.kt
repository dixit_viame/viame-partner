package com.viame.viame_partner.ui.bookingcancel

import android.view.View
import com.viame.viame_partner.ui.myorders.model.Appointment

interface CancelReasonSelectEventHandler {
    /**
     * Will be called when signIn button gets clicked
     */
    fun onItemClick(view : View, appointment: String)

}