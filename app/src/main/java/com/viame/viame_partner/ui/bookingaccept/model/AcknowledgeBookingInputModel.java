package com.viame.viame_partner.ui.bookingaccept.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AcknowledgeBookingInputModel {

    @SerializedName("ent_booking_id")
    @Expose
    private String entBookingId;

    @SerializedName("serverTime")
    @Expose
    private String serverTime;

    @SerializedName("PingId")
    @Expose
    private String pingId;

    public String getEntBookingId() {
        return entBookingId;
    }

    public void setEntBookingId(String entBookingId) {
        this.entBookingId = entBookingId;
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    public String getPingId() {
        return pingId;
    }

    public void setPingId(String pingId) {
        this.pingId = pingId;
    }
}
