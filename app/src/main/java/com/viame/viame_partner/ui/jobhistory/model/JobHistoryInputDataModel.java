package com.viame.viame_partner.ui.jobhistory.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobHistoryInputDataModel {

    @SerializedName("ent_page_index")
    @Expose
    private String entPageIndex;

    @SerializedName("ent_start_date")
    @Expose
    private String entStartDate;

    @SerializedName("ent_end_date")
    @Expose
    private String entEndDate;

    public String getEntPageIndex() {
        return entPageIndex;
    }

    public void setEntPageIndex(String entPageIndex) {
        this.entPageIndex = entPageIndex;
    }

    public String getEntStartDate() {
        return entStartDate;
    }

    public void setEntStartDate(String entStartDate) {
        this.entStartDate = entStartDate;
    }

    public String getEntEndDate() {
        return entEndDate;
    }

    public void setEntEndDate(String entEndDate) {
        this.entEndDate = entEndDate;
    }

}
