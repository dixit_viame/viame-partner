package com.viame.viame_partner.ui.splash

import android.Manifest
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.PowerManager
import com.viame.viame_partner.R
import com.viame.viame_partner.firebase.callFirebaseToGetRegistrationId
import com.viame.viame_partner.firebase.isGooglePlayServicesAvailable
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.base.BaseActivity
import com.viame.viame_partner.ui.main.MainActivity
import com.viame.viame_partner.ui.signin.SignInActivity
import com.viame.viame_partner.utils.Alerts
import com.viame.viame_partner.utils.PermissionHelper
import com.viame.viame_partner.utils.permission.annotation.PermissionDenied
import com.viame.viame_partner.utils.permission.annotation.PermissionGranted
import timber.log.Timber
import javax.inject.Inject


class SplashScreenActivity : BaseActivity<SplashScreenViewModel?>() {


    private val MY_IGNORE_OPTIMIZATION_REQUEST: Int = 10

    @Inject
    lateinit var model : SplashScreenViewModel

    var mHandler: Handler? = null
    var mRunnable = Runnable {
        if (model.isLoggedIn) {
            startActivity(Intent(this@SplashScreenActivity, MainActivity::class.java))
            finish()
        } else {
            startActivity(Intent(this@SplashScreenActivity, SignInActivity::class.java))
            finish()
        }
    }

    override fun getViewModel(): SplashScreenViewModel {
        return model
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        val pm = getSystemService(Context.POWER_SERVICE) as (PowerManager)
//        val isIgnoringBatteryOptimizations = pm.isIgnoringBatteryOptimizations(getPackageName());
//        if (!isIgnoringBatteryOptimizations) {
//            val intent = Intent()
//            intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS)
//            intent.setData(Uri.parse("package:" + getPackageName()))
//            startActivityForResult(intent, MY_IGNORE_OPTIMIZATION_REQUEST)
//        }

        if(PermissionHelper.getInstance().hasPermission(this@SplashScreenActivity,Manifest.permission.ACCESS_FINE_LOCATION)) {
            try {
                val skipMessage: Boolean = Preferences.getPreferenceBoolean(PrefEntity.SKIP_PROTECTED_APP)
                var isopensettings = false
                if (!skipMessage) {
                    for (intent in powerIntents) {
                        if (isCallable(this, intent)) {
                            isopensettings = true

                            Alerts.showBottomSheetSimpleConfirmationDialog(this@SplashScreenActivity,
                                "Permission for "+ Build.MANUFACTURER + " Protected Apps",
                                String.format(
                                    "'%s' requires permission to function properly.\nDisable '%s' from list.%n",
                                    getString(R.string.app_name),
                                    getString(R.string.app_name)
                                ), true, getString(R.string.ok),
                                "Go to settings", object : Alerts.OnConfirmationDialog {
                                    override fun onYes() {

                                        try {
                                            if (getPackageManager().resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY) != null) {
                                                startActivity(intent)
                                            }
                                            Preferences.setPreference(PrefEntity.SKIP_PROTECTED_APP,true)
                                        } catch (e: Exception) {
                                            e.printStackTrace()
                                        }finally {
                                            mHandler = Handler()
                                            mHandler!!.postDelayed(mRunnable, 5000)
                                        }
                                    }
                                    override fun onNo() {

                                    }
                                })
                        }
                    }
                }
                else
                {
                    mHandler = Handler()
                    mHandler!!.postDelayed(mRunnable, 2000)

                }

                if(!isopensettings)
                {
                    mHandler = Handler()
                    mHandler!!.postDelayed(mRunnable, 2000)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        else
        {
            requestLocationPermission()
        }
        callFirebaseToGetRegistrationId()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MY_IGNORE_OPTIMIZATION_REQUEST) {
            val pm = getSystemService(Context.POWER_SERVICE) as (PowerManager)
            val isIgnoringBatteryOptimizations = pm.isIgnoringBatteryOptimizations(getPackageName())

            if(isIgnoringBatteryOptimizations)
            {
                Timber.e("Ignoring battery optimization")
            }
            else
            {
                Timber.e("Not ignoring battery optimization")
            }
        }
    }


    private fun requestLocationPermission() {
        PermissionHelper.getInstance().requestPermission(this@SplashScreenActivity, Manifest.permission.ACCESS_FINE_LOCATION)
    }
    private var powerIntents: List<Intent> = listOf(
        Intent().setComponent(
            ComponentName(
                "com.huawei.systemmanager",
                "com.huawei.systemmanager.appcontrol.activity.StartupAppControlActivity"
            )
        ),Intent().setComponent(
            ComponentName(
                "com.huawei.systemmanager",
                "com.huawei.systemmanager.optimize.process.ProtectActivity"
            )
        ),Intent().setComponent(
            ComponentName(
                "com.huawei.systemmanager",
                "com.huawei.systemmanager.startupmgr.ui.StartupNormalAppListActivity"
            )
        ),
        Intent().setComponent(
            ComponentName(
                "com.miui.securitycenter",
                "com.miui.permcenter.autostart.AutoStartManagementActivity"
            )
        ),
        Intent().setComponent(
            ComponentName(
                "com.letv.android.letvsafe",
                "com.letv.android.letvsafe.AutobootManageActivity"
            )
        ),
        Intent().setComponent(
            ComponentName(
                "com.coloros.safecenter",
                "com.coloros.safecenter.permission.startup.StartupAppListActivity"
            )
        ),
        Intent().setComponent(
            ComponentName(
                "com.coloros.safecenter",
                "com.coloros.safecenter.startupapp.StartupAppListActivity"
            )
        ),
        Intent().setComponent(
            ComponentName(
                "com.oppo.safe",
                "com.oppo.safe.permission.startup.StartupAppListActivity"
            )
        ),
        Intent().setComponent(
            ComponentName(
                "com.iqoo.secure",
                "com.iqoo.secure.ui.phoneoptimize.AddWhiteListActivity"
            )
        ),
        Intent().setComponent(
            ComponentName(
                "com.iqoo.secure",
                "com.iqoo.secure.ui.phoneoptimize.BgStartUpManager"
            )
        ),
        Intent().setComponent(
            ComponentName(
                "com.vivo.permissionmanager",
                "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"
            )
        ),
        Intent().setComponent(
            ComponentName(
                "com.samsung.android.lool",
                "com.samsung.android.sm.ui.battery.BatteryActivity"
            )
        ),
        Intent().setComponent(
            ComponentName(
                "com.htc.pitroad",
                "com.htc.pitroad.landingpage.activity.LandingPageActivity"
            )
        ),
        Intent().setComponent(
            ComponentName(
                "com.asus.mobilemanager",
                "com.asus.mobilemanager.entry.FunctionActivity"
            )
        ).setData(Uri.parse("mobilemanager://function/entry/AutoStart"))
    )

    @PermissionGranted(permission = Manifest.permission.ACCESS_FINE_LOCATION)
    internal fun cameraGranted() {
        Timber.e("ACCESS_FINE_LOCATION_GRANT")

        val skipMessage: Boolean = Preferences.getPreferenceBoolean(PrefEntity.SKIP_PROTECTED_APP)

        try {
            var isopensettings = false
            if (!skipMessage) {
                for (intent in powerIntents) {
                    if (isCallable(this, intent)) {
                        isopensettings = true
                        Alerts.showBottomSheetSimpleConfirmationDialog(this@SplashScreenActivity,
                            "Permission for "+ Build.MANUFACTURER + " Protected Apps",
                            String.format(
                                "'%s' requires permission to function properly.\nDisable '%s' from list.%n",
                                getString(R.string.app_name),
                                getString(R.string.app_name)
                            ), true, getString(R.string.ok),
                            "Go to settings", object : Alerts.OnConfirmationDialog {
                                override fun onYes() {
                                    try {
                                        if (getPackageManager().resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY) != null) {
                                            startActivity(intent)
                                        }
                                        Preferences.setPreference(PrefEntity.SKIP_PROTECTED_APP,true)

                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                    }
                                    finally {
                                        mHandler = Handler()
                                        mHandler!!.postDelayed(mRunnable, 5000)
                                    }
                                }
                                override fun onNo() {

                            }
                        })
                    }
                }
            }
            else
            {
                mHandler = Handler()
                mHandler!!.postDelayed(mRunnable, 2000)
            }

            if(!isopensettings)
            {
                mHandler = Handler()
                mHandler!!.postDelayed(mRunnable, 2000)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    private fun isCallable(context: Context, intent: Intent): Boolean {
        val list: List<ResolveInfo> =
            context.getPackageManager().queryIntentActivities(
                intent,
                PackageManager.MATCH_DEFAULT_ONLY
            )
        return list.isNotEmpty()
    }

    @PermissionDenied(permission = Manifest.permission.ACCESS_FINE_LOCATION)
    internal fun cameraDenied() {
        Timber.e("ACCESS_FINE_LOCATION_DENIED")
        this.finish()
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        PermissionHelper.getInstance().onRequestPermissionsResult(this, permissions, grantResults)
    }

    override fun onResume() {
        super.onResume()

        isGooglePlayServicesAvailable(this@SplashScreenActivity)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mHandler != null) mHandler!!.removeCallbacks(mRunnable)
    }

}