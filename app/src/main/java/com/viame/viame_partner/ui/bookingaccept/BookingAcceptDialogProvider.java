package com.viame.viame_partner.ui.bookingaccept;

import com.viame.viame_partner.ui.bookingdetail.details.DetailPagerFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class BookingAcceptDialogProvider {

    @ContributesAndroidInjector
    abstract BookingAcceptDialogFragment contributeBookingAcceptDialogFragment();
}
