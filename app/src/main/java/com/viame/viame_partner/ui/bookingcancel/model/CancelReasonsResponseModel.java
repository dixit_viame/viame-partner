package com.viame.viame_partner.ui.bookingcancel.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CancelReasonsResponseModel {

    @SerializedName("errNum")
    @Expose
    private Integer errNum;
    @SerializedName("errFlag")
    @Expose
    private Integer errFlag;
    @SerializedName("errMsg")
    @Expose
    private String errMsg;
    @SerializedName("data")
    @Expose
    private List<String> data = null;
    @SerializedName("serverTime")
    @Expose
    private Integer serverTime;
    @SerializedName("cancelFlag")
    @Expose
    private Integer cancelFlag;

    public Integer getErrNum() {
        return errNum;
    }

    public void setErrNum(Integer errNum) {
        this.errNum = errNum;
    }

    public Integer getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(Integer errFlag) {
        this.errFlag = errFlag;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }

    public Integer getServerTime() {
        return serverTime;
    }

    public void setServerTime(Integer serverTime) {
        this.serverTime = serverTime;
    }

    public Integer getCancelFlag() {
        return cancelFlag;
    }

    public void setCancelFlag(Integer cancelFlag) {
        this.cancelFlag = cancelFlag;
    }
}
