package com.viame.viame_partner.ui.jobhistory.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.viame.viame_partner.ui.unassigned.model.UnAssignedInvoice
import com.viame.viame_partner.ui.unassigned.model.UnAssignedLocation
import java.io.Serializable

class JobHistoryAppointment : Serializable {
    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("booking_id")
    @Expose
    var bookingId: String? = null

    @SerializedName("order_id")
    @Expose
    var orderId: Long = 0

    @SerializedName("mas_id")
    @Expose
    var masId: String? = null

    @SerializedName("appointment_dt")
    @Expose
    var appointmentDt: String? = null

    @SerializedName("signatureUrl")
    @Expose
    var signatureUrl: String? = null

    @SerializedName("slave_id")
    @Expose
    var slaveId: String? = null

    @SerializedName("slavePushTopic")
    @Expose
    var slavePushTopic: String? = null

    @SerializedName("zoneType")
    @Expose
    var zoneType: String? = null

    @SerializedName("slaveEmail")
    @Expose
    var slaveEmail: String? = null

    @SerializedName("slavemobile")
    @Expose
    var slavemobile: String? = null

    @SerializedName("slaveCountryCode")
    @Expose
    var slaveCountryCode: String? = null

    @SerializedName("slaveName")
    @Expose
    var slaveName: String? = null

    @SerializedName("created_dt")
    @Expose
    var createdDt: Long = 0

    @SerializedName("status")
    @Expose
    var status: Int? = null

    @SerializedName("statusText")
    @Expose
    var statusText: String? = null

    @SerializedName("appt_type")
    @Expose
    var apptType: String? = null

    @SerializedName("apptTypeText")
    @Expose
    var apptTypeText: String? = null

    @SerializedName("serverTime")
    @Expose
    var serverTime: Long = 0

    @SerializedName("receivers")
    @Expose
    val receiversData: List<JobHistoryReceiversData>? = null

    @SerializedName("booking_time")
    @Expose
    var bookingTime: String? = null

    @SerializedName("timpeStamp_booking_time")
    @Expose
    var timpeStampBookingTime: Long = 0

    @SerializedName("booking_date_object")
    @Expose
    lateinit var bookingLaterDateTimestamp: Any

    @SerializedName("address_line1")
    @Expose
    var addressLine1: String? = null

    @SerializedName("drop_location")
    @Expose
    var dropLocation: UnAssignedLocation? = null

    @SerializedName("pickup_location")
    @Expose
    var pickupLocation: UnAssignedLocation? = null

    @SerializedName("drop_addr1")
    @Expose
    var dropAddr1: String? = null

    @SerializedName("extra_notes")
    @Expose
    var extraNotes: String? = null

    @SerializedName("pickupzoneId")
    @Expose
    var pickupzoneId: String? = null

    @SerializedName("pickupZoneName")
    @Expose
    var pickupZoneName: String? = null

    @SerializedName("dorpzoneId")
    @Expose
    var dorpzoneId: String? = null

    @SerializedName("amount")
    @Expose
    var amount: String? = null

    @SerializedName("zipcode")
    @Expose
    var zipcode: String? = null

    @SerializedName("user_device")
    @Expose
    var userDevice: String? = null

    @SerializedName("payment_type")
    @Expose
    var paymentType: String? = null

    @SerializedName("paymentTypeText")
    @Expose
    var paymentTypeText: String? = null

    @SerializedName("additional_info")
    @Expose
    var additionalInfo: String? = null

    @SerializedName("passanger_chn")
    @Expose
    var passangerChn: String? = null

    @SerializedName("vehicleTypeText")
    @Expose
    var vehicleTypeText: String? = null

    @SerializedName("vehicle_image")
    @Expose
    var vehicleImage: String? = null

    @SerializedName("invoice")
    @Expose
    var invoice: UnAssignedInvoice? = null

    @SerializedName("helpers")
    @Expose
    var helpers: Int? = null

    @SerializedName("timeZone")
    @Expose
    var timeZone: String? = null

    @SerializedName("product_value")
    @Expose
    var productValue: String? = null

    @SerializedName("booking_origin")
    @Expose
    var bookingOrigin: String? = null

    @SerializedName("paidByReceiver")
    @Expose
    var paidByReceiver: Boolean? = null

    @SerializedName("pricingType")
    @Expose
    var pricingType: String? = null




}