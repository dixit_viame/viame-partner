package com.viame.viame_partner.ui.profile

interface ProfileEventHandler {
    /**
     * Will be called when Update button gets clicked
     */
    fun onProfileUpdateClicked()

    /**
     * Will be called when Photo button gets clicked
     */
    fun onProfilePhotoClicked()

}