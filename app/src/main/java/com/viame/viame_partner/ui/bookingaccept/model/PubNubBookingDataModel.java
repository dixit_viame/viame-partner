package com.viame.viame_partner.ui.bookingaccept.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PubNubBookingDataModel {

    @SerializedName("a")
    @Expose
    private Integer a;
    @SerializedName("dt")
    @Expose
    private String dt;
    @SerializedName("pickZone")
    @Expose
    private String pickZone;
    @SerializedName("dropZone")
    @Expose
    private String dropZone;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("dropDt")
    @Expose
    private String dropDt;
    @SerializedName("bid")
    @Expose
    private String bid;
    @SerializedName("adr1")
    @Expose
    private String adr1;
    @SerializedName("drop1")
    @Expose
    private String drop1;
    @SerializedName("serverTime")
    @Expose
    private String serverTime;
    @SerializedName("chn")
    @Expose
    private String chn;
    @SerializedName("ExpiryTimer")
    @Expose
    private String expiryTimer;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("subcategory")
    @Expose
    private String subcategory;
    @SerializedName("subsubcategory")
    @Expose
    private String subsubcategory;
    @SerializedName("dis")
    @Expose
    private String dis;
    @SerializedName("goodsTypeImg")
    @Expose
    private String goodsTypeImg;
    @SerializedName("paymentType")
    @Expose
    private String paymentType;
    @SerializedName("helpers")
    @Expose
    private Integer helpers;
    @SerializedName("PingId")
    @Expose
    private String pingId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("_id")
    @Expose
    private String id;

    public Integer getA() {
        return a;
    }

    public void setA(Integer a) {
        this.a = a;
    }

    public String getDt() {
        return dt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    public String getPickZone() {
        return pickZone;
    }

    public void setPickZone(String pickZone) {
        this.pickZone = pickZone;
    }

    public String getDropZone() {
        return dropZone;
    }

    public void setDropZone(String dropZone) {
        this.dropZone = dropZone;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDropDt() {
        return dropDt;
    }

    public void setDropDt(String dropDt) {
        this.dropDt = dropDt;
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public String getAdr1() {
        return adr1;
    }

    public void setAdr1(String adr1) {
        this.adr1 = adr1;
    }

    public String getDrop1() {
        return drop1;
    }

    public void setDrop1(String drop1) {
        this.drop1 = drop1;
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    public String getChn() {
        return chn;
    }

    public void setChn(String chn) {
        this.chn = chn;
    }

    public String getExpiryTimer() {
        return expiryTimer;
    }

    public void setExpiryTimer(String expiryTimer) {
        this.expiryTimer = expiryTimer;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getSubsubcategory() {
        return subsubcategory;
    }

    public void setSubsubcategory(String subsubcategory) {
        this.subsubcategory = subsubcategory;
    }

    public String getDis() {
        return dis;
    }

    public void setDis(String dis) {
        this.dis = dis;
    }

    public String getGoodsTypeImg() {
        return goodsTypeImg;
    }

    public void setGoodsTypeImg(String goodsTypeImg) {
        this.goodsTypeImg = goodsTypeImg;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Integer getHelpers() {
        return helpers;
    }

    public void setHelpers(Integer helpers) {
        this.helpers = helpers;
    }

    public String getPingId() {
        return pingId;
    }

    public void setPingId(String pingId) {
        this.pingId = pingId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
