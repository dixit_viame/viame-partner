package com.viame.viame_partner.ui.bookingcancel;

import androidx.lifecycle.ViewModelProvider;

import com.viame.viame_partner.ui.pickupdrop.PickupDropViewModel;
import com.viame.viame_partner.utils.ViewModelProviderFactory;

import dagger.Module;
import dagger.Provides;

@Module
public class BookingCancelActivityModule {

    @Provides
    BookingCancelViewModel providesBookingCancelViewModel(){
        return new BookingCancelViewModel();
    }

    @Provides
    ViewModelProvider.Factory provideViewModelProvider(PickupDropViewModel viewModel){
        return new ViewModelProviderFactory<>(viewModel);
    }
}