package com.viame.viame_partner.ui.bookingdetail.timeline

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.viame.viame_partner.BOOKING_DATA
import com.viame.viame_partner.MBStatusType
import com.viame.viame_partner.R
import com.viame.viame_partner.databinding.FragmentTimelineBinding
import com.viame.viame_partner.ui.base.BaseFragment
import com.viame.viame_partner.ui.myorders.model.Appointment
import com.viame.viame_partner.utils.LinearLayoutManagerWrapper
import com.viame.viame_partner.utils.Utils
import java.util.*
import javax.inject.Inject

/**
 * A placeholder fragment containing a simple view.
 */
class TimelinePagerFragment : BaseFragment<TimelinePagerViewModel>() {

    private lateinit var currentAppointment: Appointment

    private lateinit var mAdapter: TimeLinePagerAdapter
    private lateinit var binding: FragmentTimelineBinding

    private var mDataList = ArrayList<TimeLineModel>()

    @Inject
    lateinit var model : TimelinePagerViewModel

    override fun getViewModel(): TimelinePagerViewModel {
        return model
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        binding  = DataBindingUtil.inflate(inflater, R.layout.fragment_timeline,container,false)

        if(arguments?.get(BOOKING_DATA) != null) {
            currentAppointment = arguments?.get(BOOKING_DATA) as Appointment
        }else
            return binding.root

        mDataList = ArrayList<TimeLineModel>()
        if(currentAppointment.activities != null) {
            for (activity in currentAppointment.activities)
            {
                mDataList.add(TimeLineModel(activity.msg + "\nBid: " + activity.bid, Utils.getDateFormatFromMiliseconds(activity.timeISO,
                    "MMM dd yyyy, hh:mm a"), if (activity.status == MBStatusType.COMPLETED.id) OrderStatus.COMPLETED else OrderStatus.ACTIVE))
            }
        }
        val mLayoutManager = LinearLayoutManagerWrapper(activity, RecyclerView.VERTICAL, false)
        binding.timeLineRecyclerView.layoutManager = mLayoutManager
        mAdapter = TimeLinePagerAdapter(mDataList)
        binding.timeLineRecyclerView.adapter = mAdapter
        return binding.root
    }

    companion object {
        /**
         * The fragment argument representing the section number for this fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        @JvmStatic
        fun newInstance(sectionNumber: Int, appointment: Appointment): TimelinePagerFragment {
            return TimelinePagerFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                    putSerializable(BOOKING_DATA, appointment)
                }
            }
        }
    }
}