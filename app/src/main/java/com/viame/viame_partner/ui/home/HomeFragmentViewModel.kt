package com.viame.viame_partner.ui.home

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.app.Presentation
import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.viame.viame_partner.*
import com.viame.viame_partner.application.CatalogApplication
import com.viame.viame_partner.database.RoomDatabaseHelper
import com.viame.viame_partner.homewidget.updateWidgets
import com.viame.viame_partner.location.LocationCallRepository
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.home.model.GetStatusUpdateInputModel
import com.viame.viame_partner.ui.home.model.GetStatusUpdateModel
import com.viame.viame_partner.ui.home.model.GotoOnlineOfflineModel
import com.viame.viame_partner.ui.main.MainActivity
import com.viame.viame_partner.ui.main.models.GetConfigOutputResponse
import com.viame.viame_partner.ui.vehicle.model.VehicleListModel
import com.viame.viame_partner.utils.LinNotify
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.ArrayList
import java.util.HashMap
import javax.inject.Inject

class HomeFragmentViewModel @Inject constructor() : ViewModel() {

    @Inject
    lateinit var dbHelper : RoomDatabaseHelper


    /**
     * Two way bind-able fields
     */
    var tvOnlineOffline : String = Preferences.getPreferenceInt(PrefEntity.DRIVER_PRESENCE_STATUS).let { block ->
        if(block == DriverStatusType.ONLINE.id)
        {
            return@let  CatalogApplication.instance!!.getString(R.string.you_are_online)
        }
        else
        {
            return@let  CatalogApplication.instance!!.getString(R.string.you_are_offline)
        }
    }

    var tvSubOnlineOffline: String =
        Preferences.getPreferenceInt(PrefEntity.DRIVER_PRESENCE_STATUS).let { block ->
              if(block == DriverStatusType.ONLINE.id)
              {
                  return@let  CatalogApplication.instance!!.getString(R.string.go_offline_to_stop_accepting_jobs)
              }
            else
              {
                  return@let  CatalogApplication.instance!!.getString(R.string.go_online_to_start_accepting_jobs)
              }
        }


    var drawableId : Int = Preferences.getPreferenceInt(PrefEntity.DRIVER_PRESENCE_STATUS).let { block ->

        if(block == DriverStatusType.ONLINE.id)
        {
            return@let R.drawable.ic_online
        }
        else
        {
            return@let R.drawable.ic_offline
        }
    }

    private val onlineOfflineResponse = MutableLiveData<GotoOnlineOfflineModel>()

    fun getOnlineOfflineDataResult(): LiveData<GotoOnlineOfflineModel> = onlineOfflineResponse


    fun goToOnlineOffline(apiInterface : ApiInterface, getStatusUpdateInputModel : GetStatusUpdateInputModel)
    {

        val headerMap = HashMap<String,String>()
        headerMap["Content-Type"] = "application/json;charset=UTF-8"
        headerMap["authorization"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["token"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["userType"] = USER_TYPE.toString()

        val posts: Call<GetStatusUpdateModel> = apiInterface.callStatusChangeMethod(headerMap,getStatusUpdateInputModel)!!

        //Enqueue the call
        posts.enqueue(object : Callback<GetStatusUpdateModel>
        {
            @SuppressLint("TimberArgCount")
            override fun onFailure(call: Call<GetStatusUpdateModel>, t: Throwable)
            {
                Timber.e(t.message.toString())
            }

            @SuppressLint("TimberArgCount")
            override fun onResponse(call: Call<GetStatusUpdateModel>, response: Response<GetStatusUpdateModel>)
            {
                if(response.isSuccessful) {

                    var mPendingIntent = PendingIntent.getActivity(
                        CatalogApplication.instance!!,
                        ONLINE_OFFLINE_NOTIFICATION_ID,
                        Intent(CatalogApplication.instance!!, MainActivity::class.java),
                        PendingIntent.FLAG_UPDATE_CURRENT)

                    if (drawableId == R.drawable.ic_offline && getStatusUpdateInputModel.entStatus == DriverStatusType.ONLINE.id)
                    {
                        drawableId = R.drawable.ic_online
                        var mGotoOnlineOfflineModel = GotoOnlineOfflineModel()
                        mGotoOnlineOfflineModel.drawableId = R.drawable.ic_online
                        mGotoOnlineOfflineModel.stringTitle =
                            CatalogApplication.instance!!.getString(R.string.you_are_online)
                        mGotoOnlineOfflineModel.stringSubTitle = CatalogApplication.instance!!.getString(R.string.go_offline_to_stop_accepting_jobs)

                        Preferences.setPreferenceInt(PrefEntity.DRIVER_PRESENCE_STATUS,
                            DriverStatusType.ONLINE.id)
                        onlineOfflineResponse.value = mGotoOnlineOfflineModel

                        LinNotify.showBigTextStyle(
                            CatalogApplication.instance!!,
                            R.drawable.ic_online,
                            R.drawable.ic_viame_notification,
                            CatalogApplication.instance!!. getString(R.string.app_name),
                            "",
                            "",
                            "You are online",
                            CatalogApplication.instance!!. getString(R.string.app_name),
                            null,
                            ONLINE_OFFLINE_NOTIFICATION_ID,
                            NOTIFICATION_CHANNEL_ID,
                            mPendingIntent,
                            null,true)
                    }
                    else if(DriverStatusType.OFFLINE.id == getStatusUpdateInputModel.entStatus)
                    {
                        drawableId = R.drawable.ic_offline
                        var  mGotoOnlineOfflineModel = GotoOnlineOfflineModel()
                        mGotoOnlineOfflineModel.drawableId = R.drawable.ic_offline
                        mGotoOnlineOfflineModel.stringTitle = CatalogApplication.instance!!.getString(R.string.you_are_offline)
                        mGotoOnlineOfflineModel.stringSubTitle = CatalogApplication.instance!!.getString(R.string.go_online_to_start_accepting_jobs)

                        Preferences.setPreferenceInt(PrefEntity.DRIVER_PRESENCE_STATUS,
                            DriverStatusType.OFFLINE.id)

                        onlineOfflineResponse.value = mGotoOnlineOfflineModel

                        LinNotify.showBigTextStyle(
                            CatalogApplication.instance!!,
                            R.drawable.ic_offline,
                            R.drawable.ic_viame_notification,
                            CatalogApplication.instance!!. getString(R.string.app_name),
                            "",
                            "",
                            "You are offline",
                            CatalogApplication.instance!!. getString(R.string.app_name),
                            null,
                            ONLINE_OFFLINE_NOTIFICATION_ID,
                            NOTIFICATION_CHANNEL_ID,
                            mPendingIntent,
                            null,true)
                    }

                    updateWidgets(CatalogApplication.instance!!.applicationContext)

                    Timber.e(response.body().toString())
                }
            }
        })
    }
}