package com.viame.viame_partner.ui.vehicle.ui.main

import android.app.Activity
import android.content.Context
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.viame.viame_partner.R
import com.viame.viame_partner.ui.vehicle.MyLinearLayout
import com.viame.viame_partner.ui.vehicle.VehicleListActivity
import com.viame.viame_partner.utils.CheckView

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val context: Context, fm: FragmentManager) : FragmentPagerAdapter(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT),
     ViewPager.PageTransformer
{
    companion object
    {
        @kotlin.jvm.JvmField
        var BIG_SCALE: Float = 1.0f
        val SMALL_SCALE = 0.7f
        val DIFF_SCALE = BIG_SCALE - SMALL_SCALE
        private var scale = 0f
    }

    override fun getItem(position: Int): Fragment
    {
        var selPosition = position
        // make the first pager bigger than others
        // make the first pager bigger than others

        if (selPosition == VehicleListActivity.FIRST_PAGE)
            scale = BIG_SCALE
        else
            scale = SMALL_SCALE

        selPosition = selPosition % VehicleListActivity.PAGES

        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        return VehicleViewPagerFragment.newInstance(context as Activity,selPosition,scale)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return ""
    }


    override fun getCount(): Int {
        return VehicleListActivity.PAGES //* VehicleListActivity.LOOPS
    }

    override fun transformPage(page: View, position: Float) {
        val myLinearLayout: MyLinearLayout =
            page.findViewById<View>(R.id.MyRootView) as MyLinearLayout
        var scale: Float = BIG_SCALE
        if (position > 0) {
            scale = scale - position * DIFF_SCALE
        } else {
            scale = scale + position * DIFF_SCALE
        }
        if (scale < 0) scale = 0f
        myLinearLayout.setScaleBoth(scale)

        val myCheckView: CheckView =
            page.findViewById<View>(R.id.vCheckView) as CheckView
        myCheckView.invalidate()
    }
}