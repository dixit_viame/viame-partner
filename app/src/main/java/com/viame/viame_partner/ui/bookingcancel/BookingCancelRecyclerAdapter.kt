package com.viame.viame_partner.ui.bookingcancel

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.viame.viame_partner.R
import com.viame.viame_partner.databinding.ItemCancelBookingBinding
import java.util.*

class BookingCancelRecyclerAdapter constructor(private val context: Context, private var categoryList: ArrayList<String>, private val listener: CancelReasonSelectEventHandler) : RecyclerView.Adapter<BookingCancelRecyclerAdapter.CategoryViewHolder>()
{
    private var layoutInflater: LayoutInflater? = null

    var indexSelected : Int = -1

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): CategoryViewHolder {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }
        val view = layoutInflater!!.inflate(R.layout.item_cancel_booking, parent, false)
        return CategoryViewHolder(view)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.binding!!.viewModel = categoryList[holder.adapterPosition]
        holder.binding.tvCancelString.text = categoryList[holder.adapterPosition]
        holder.binding.handlers = listener
        holder.binding.index = holder.binding.root
        holder.binding.vCheckView.tag = holder.adapterPosition

        holder.binding.vCheckView.isChecked = indexSelected == holder.adapterPosition
    }

    override fun getItemCount(): Int {
        return categoryList.size
    }

    fun getCategoryList(): ArrayList<String> {
        return categoryList
    }

    fun setCategoryList(categories: ArrayList<String>) {
        categoryList = categories
        notifyDataSetChanged()
    }

    inner class CategoryViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val binding: ItemCancelBookingBinding?
        init {
            binding = DataBindingUtil.bind(v)
            v.tag = binding
        }
    }
}

