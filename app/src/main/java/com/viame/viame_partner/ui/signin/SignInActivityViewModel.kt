package com.viame.viame_partner.ui.signin

import android.annotation.SuppressLint
import android.text.InputType
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.material.textfield.TextInputEditText
import com.viame.viame_partner.R
import com.viame.viame_partner.application.CatalogApplication
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.ui.signin.model.SignInRequestModel
import com.viame.viame_partner.ui.signin.model.SignInResponseModel
import com.viame.viame_partner.utils.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import javax.inject.Inject


class SignInActivityViewModel @Inject constructor() : ViewModel() {

    /**
     * Two way bind-able fields
     */
    var emailMobile: String = ""
    var password: String = ""
    var isPasswordVisible: Boolean = false

    /**
     * To pass login result to activity
     */
    private val signInValidationResult = MutableLiveData<String>()

    // data LiveData<AuthResource<User>>
    private val signInApiResponse = MutableLiveData<APIRequestResponseHandler<SignInResponseModel?>>()

    fun getSignInValidationResult(): LiveData<String> = signInValidationResult

    fun getSignInApiResult(): MutableLiveData<APIRequestResponseHandler<SignInResponseModel?>> = signInApiResponse

    /**
     * Called from activity on signIn button click
     */
    fun performValidation() {
        if (emailMobile.trim().isBlank()) {
            signInValidationResult.value = CatalogApplication.instance?.getString(R.string.enter_email_password)
            return
        } else if (!Patterns.EMAIL_ADDRESS.matcher(emailMobile.trim()).matches() && !(Utils.IsMobileNumber(
                emailMobile.trim()
            ))
        ) {
            if (Utils.isNumeric(emailMobile.trim()) && !(Utils.IsMobileNumber(emailMobile.trim()))) {
                signInValidationResult.value =
                    CatalogApplication.instance?.getString(R.string.invalid_phone)
            } else {
                signInValidationResult.value =
                    CatalogApplication.instance?.getString(R.string.invalid_email)
            }
            return
        } else if (password.trim().isBlank()) {
            signInValidationResult.value = CatalogApplication.instance?.getString(R.string.enter_password)
            return
        }
        signInValidationResult.value = "Valid"
    }

    /**
     * Called from activity on password visible/Invisible button click
     */
    fun performClickPasswordVisible(passwordEditText: TextInputEditText) {
        if (!isPasswordVisible) {
            passwordEditText.setTransformationMethod(
                PasswordTransformationMethod.getInstance()
            )
            passwordEditText.setInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
            passwordEditText.setSelection(passwordEditText.text.toString().length)
        } else {
            passwordEditText.setTransformationMethod(
                HideReturnsTransformationMethod.getInstance()
            )
            passwordEditText.setInputType(InputType.TYPE_CLASS_TEXT)
            passwordEditText.setSelection(passwordEditText.text.toString().length)
        }
        isPasswordVisible = !isPasswordVisible

    }

    fun signinCall(signInObj: SignInRequestModel,apiInterface : ApiInterface)
    {
            signInApiResponse.setValue(APIRequestResponseHandler.loading(null));

            val map = HashMap<String,String>();
            map.put("Content-Type", "application/json;charset=UTF-8");
            map.put("authorization", "ordinory");
            map.put("token", "ordinory");
            map.put("userType", "1");

            val posts: Call<SignInResponseModel> = apiInterface.callLoginMethod(map,signInObj)!!
            //Enqueue the call
            posts.enqueue(object : Callback<SignInResponseModel> {
                @SuppressLint("TimberArgCount")
                override fun onFailure(call: Call<SignInResponseModel>, t: Throwable) {

                    signInApiResponse.value = APIRequestResponseHandler.error(t.message.toString(),null)
                    Timber.e(t.message.toString())
                }

                @SuppressLint("TimberArgCount")
                override fun onResponse(call: Call<SignInResponseModel>, response: Response<SignInResponseModel>
                ) {
                    try {
                        if(response.isSuccessful) {
                            signInApiResponse.value = APIRequestResponseHandler.success(response.body()!!)

                            Timber.e(response.body().toString())
                        }
                        else
                        {
                            signInApiResponse.value = APIRequestResponseHandler.error("Status code "+response.code(),null)
                        }
                    } catch (e: Exception) {
                    }
                }
            })
    }
}




