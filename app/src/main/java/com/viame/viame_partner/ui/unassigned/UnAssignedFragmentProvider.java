package com.viame.viame_partner.ui.unassigned;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class UnAssignedFragmentProvider {

    @ContributesAndroidInjector
    abstract UnAssignedFragment contributeUnAssignedFragment();
}
