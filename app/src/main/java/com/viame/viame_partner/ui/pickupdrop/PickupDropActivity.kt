package com.viame.viame_partner.ui.pickupdrop

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.viame.viame_partner.BOOKING_DATA
import com.viame.viame_partner.COME_FROM
import com.viame.viame_partner.R
import com.viame.viame_partner.databinding.ActivityPickupDropBinding
import com.viame.viame_partner.location.bitmapDescriptorFromVector
import com.viame.viame_partner.location.polyline.AVOID
import com.viame.viame_partner.location.polyline.DirectionsApiClient
import com.viame.viame_partner.location.polyline.MODE
import com.viame.viame_partner.location.polyline.TransitOptions
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.base.BaseActivity
import com.viame.viame_partner.ui.myorders.model.Appointment
import com.viame.viame_partner.utils.Alerts
import com.viame.viame_partner.utils.Utils
import javax.inject.Inject


class PickupDropActivity :  BaseActivity<PickupDropViewModel>() , OnMapReadyCallback {

    private var directionsApiClient: DirectionsApiClient? = null
    private var LatLongB: LatLngBounds.Builder? = null
    private lateinit var currentAppointmentData: Appointment
    private lateinit var binding: ActivityPickupDropBinding
    private lateinit var mMap: GoogleMap

    var pickup : LatLng? = null
    var dropoff : LatLng? = null
    var mDropLatLong: MutableList<String>? = null
    var mPickLatLong: MutableList<String>? = null

    private var MapNavigationLat = ""
    private var MapNavigationLong = ""

    @Inject
    lateinit var model : PickupDropViewModel

    override fun getViewModel(): PickupDropViewModel {
        return model
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_pickup_drop)
        setSupportActionBar(binding.toolbar)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.toolbar.setBackgroundColor(ContextCompat.getColor(this@PickupDropActivity,R.color.transparent))
        binding.appBar.outlineProvider = null

        directionsApiClient = DirectionsApiClient(
            apiKey = getString(R.string.google_places_key))

        if(intent.getSerializableExtra(BOOKING_DATA) != null)
            currentAppointmentData = intent.getSerializableExtra(BOOKING_DATA) as Appointment

        binding.tvEmail.text = currentAppointmentData.slaveEmail
        binding.tvClientName.text = currentAppointmentData.slaveName
        binding.tvPhoneNo.labelTextView.text = currentAppointmentData.slavemobile
        binding.tvPickupTime.text = Utils.dateFormatter(currentAppointmentData.bookingTime)

        // Pickup
        if (intent.getIntExtra(COME_FROM,1) == 1)
        {
            binding.toolbar.setTitle(getString(R.string.pickup_details))
            setTitle(getString(R.string.pickup_details))
            binding.tvTitleLBL.text = getString(R.string.pickup_details)
            binding.tvPickupAddress.text = currentAppointmentData.addressLine1
            binding.tvPickupLbl.text = getString(R.string.pickup_location_time)

           MapNavigationLat = currentAppointmentData.pickupLocation.latitude.toString()
           MapNavigationLong = currentAppointmentData.pickupLocation.longitude.toString()

           binding.tvClientName.text = currentAppointmentData.slaveName
           binding.tvPhoneNo.labelTextView.text = currentAppointmentData.slavemobile
        }
        else  // Dropoff
        {

            MapNavigationLat = currentAppointmentData.dropLocation.latitude.toString()
            MapNavigationLong = currentAppointmentData.dropLocation.longitude.toString()

            binding.tvPickupAddress.text = currentAppointmentData.dropAddr1
            binding.tvPickupLbl.text = getString(R.string.dropoff_location_time)
            binding.toolbar.setTitle(getString(R.string.dropoff_details))
            setTitle(R.string.dropoff_details)
            binding.tvTitleLBL.text = getString(R.string.dropoff_details)

            binding.tvClientName.text = currentAppointmentData.receiverName
            binding.tvPhoneNo.labelTextView.text = currentAppointmentData.receiverMobile
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        binding.tvPhoneNo.trailingImageView.setOnClickListener {

            Utils.copyToClipboard(binding.tvPhoneNo.labelTextView.text.toString(),this)
        }

        binding.btnCall.setOnClickListener {

            // Pickup
            if (intent.getIntExtra(COME_FROM, 1) == 1) {

                Alerts.showBottomSheetSimpleConfirmationDialog(this@PickupDropActivity,
                getString(R.string.call_customer),
                (currentAppointmentData.slaveCountryCode ?: "+971") + currentAppointmentData.slavemobile, false, getString(R.string.action_cancel),
                getString(R.string.call_lbl), object : Alerts.OnConfirmationDialog {
                    override fun onYes() {

                        val dialIntent = Intent(
                            Intent.ACTION_DIAL,
                            Uri.parse("tel:${currentAppointmentData.slaveCountryCode}${currentAppointmentData.slavemobile}"))
                        startActivity(dialIntent)
                    }

                    override fun onNo() {

                    }
                })
            }
            else
            {
                Alerts.showBottomSheetSimpleConfirmationDialog(this@PickupDropActivity,
                    getString(R.string.call_customer),
                    (currentAppointmentData.slaveCountryCode ?: "+971") + currentAppointmentData.receiverMobile, false, getString(R.string.action_cancel),
                    getString(R.string.call_lbl), object : Alerts.OnConfirmationDialog {
                        override fun onYes() {

                            val dialIntent = Intent(
                                Intent.ACTION_DIAL,
                                Uri.parse("tel:${currentAppointmentData.slaveCountryCode ?: "+971"}${currentAppointmentData.receiverMobile}"))
                            startActivity(dialIntent)
                        }

                        override fun onNo() {

                        }
                    })
            }
        }

        binding.btnGMaps.setOnClickListener {

            packageManager?.let {
                val uri = "http://maps.google.com/maps?q=loc:$MapNavigationLat,$MapNavigationLong"
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                // intent.setPackage("com.google.android.apps.maps")
                intent.resolveActivity(it)?.let {
                    startActivity(intent)
                }?: run {
                    Alerts.showSnackBar(this,"App not found")
                }
            }
        }

        binding.btnNavigation.setOnClickListener {
            packageManager?.let {
                val navigationIntentUri =
                    Uri.parse("google.navigation:q=$MapNavigationLat,$MapNavigationLong") //creating intent with latlng

                val mapIntent = Intent(Intent.ACTION_VIEW, navigationIntentUri)
                mapIntent.setPackage("com.google.android.apps.maps")

                mapIntent.resolveActivity(it)?.let {
                    startActivity(mapIntent)
                } ?: run {
                    Alerts.showSnackBar(this, "App not found")
                }
            }
        }

        binding.imgWhatsApp.setOnClickListener {

            val whatsappText =
                "Ahlan!,\nI'm from ViaMe Delivery Services, My name is " + Preferences.getPreferenceString(
                    PrefEntity.USER_NAME
                ) + ", and I will be your delivery partner\nBooking reference: " +
                        currentAppointmentData.orderId + "\n"

            // Pickup
            if (intent.getIntExtra(COME_FROM, 1) == 1) {

                onClickWhatsApp(
                    this,
                    "${currentAppointmentData.slaveCountryCode}${currentAppointmentData.slavemobile}", whatsappText)
            }
            else
            {
                onClickWhatsApp(
                    this,
                    currentAppointmentData.receiverMobile,
                    whatsappText
                )
            }
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        try {
            mMap = googleMap

            // declare bounds object to fit whole route in screen
            LatLongB = LatLngBounds.builder()

            val style =
                MapStyleOptions.loadRawResourceStyle(this, R.raw.mapstyle_grayscale)
            mMap.setMapStyle(style)
            mMap.isIndoorEnabled = false

            mDropLatLong = ArrayList()
            mPickLatLong = ArrayList()

            // Pickup
            if (intent.getIntExtra(COME_FROM,1) == 1) {

                mDropLatLong?.add(currentAppointmentData.pickupLocation.latitude.toString())
                mDropLatLong?.add(currentAppointmentData.pickupLocation.longitude.toString())

                mPickLatLong?.add(Preferences.getPreferenceString(PrefEntity.LATITUDE_CURRENT))

                mPickLatLong?.add(Preferences.getPreferenceString(PrefEntity.LONGITUDE_CURRENT))

                // Add markers
                pickup = LatLng(mPickLatLong?.get(0)!!.toDouble(), mPickLatLong!![1].toDouble())
                dropoff = LatLng(mDropLatLong?.get(0)!!.toDouble(), mDropLatLong!![1].toDouble())


                mMap.addMarker(MarkerOptions().position(dropoff!!).title("Pickup Location").icon(
                    bitmapDescriptorFromVector(this,R.drawable.location_pin_drop)
                ).draggable(true))

                mMap.addMarker(MarkerOptions().position(pickup!!).title("Your Location").icon(
                    BitmapDescriptorFactory.fromResource(R.drawable.ic_home_motorbike)
                ).draggable(true))
            }
            else
            {
                // Drop
                mDropLatLong?.add(currentAppointmentData.dropLocation.latitude.toString())
                mDropLatLong?.add(currentAppointmentData.dropLocation.longitude.toString())

                mPickLatLong?.add(Preferences.getPreferenceString(PrefEntity.LATITUDE_CURRENT))
                mPickLatLong?.add(Preferences.getPreferenceString(PrefEntity.LONGITUDE_CURRENT))

                // Add markers
                pickup = LatLng(mPickLatLong?.get(0)!!.toDouble(), mPickLatLong!![1].toDouble())
                dropoff = LatLng(mDropLatLong?.get(0)!!.toDouble(), mDropLatLong!![1].toDouble())

                mMap.addMarker(MarkerOptions().position(dropoff!!).title("Drop-off Location").icon(
                    bitmapDescriptorFromVector(this,R.drawable.location_pin_drop)
                ).draggable(true))

                mMap.addMarker(MarkerOptions().position(pickup!!).title("Your Location").icon(
                    BitmapDescriptorFactory.fromResource(R.drawable.ic_home_motorbike)
                ).draggable(true))

            }

            this.mMap.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener {
                override fun onMarkerDragEnd(marker: Marker) {
                    getPolylineToRoute(from = marker.position, to = dropoff!!)
                }
                override fun onMarkerDragStart(p0: Marker?) {}

                override fun onMarkerDrag(p0: Marker?) {}
            })

            getPolylineToRoute(pickup!!, dropoff!!)

            val cameraPosition = CameraPosition.Builder()
                .target(LatLng(pickup!!.latitude, pickup!!.longitude)).zoom(16.00f)
                .build()
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private val mutableList: MutableList<Polyline> = mutableListOf()

    fun getPolylineToRoute(from: LatLng, to: LatLng) {
        try {
            mutableList.forEach { it.remove() }
            mutableList.clear()
            val transitOptions = TransitOptions(mode = MODE.DRIVING, whatToAvoidArray = arrayOf(AVOID.FERRIES, AVOID.HIGHWAYS, AVOID.TOLLS))
            directionsApiClient?.getRoutePolyline(origin = from, dest = to, options = transitOptions) {
                runOnUiThread {
                    it.forEach {

                        val mLatLongB = LatLngBounds.Builder()
                        mLatLongB.include(from)
                        //you can do additional polyline customization here
                        mutableList.add(mMap.addPolyline(it))
                        mLatLongB.include(to)

                        // build bounds
                        val bounds = mLatLongB.build()
                        // show map with route centered
                        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun onClickWhatsApp(activity : Activity, phone : String, text : String)
    {
        val formattedNumber = phone.replace("+", "").replace(" ", "")
        try {
            val waIntent = Intent("android.intent.action.MAIN")
            waIntent.setAction(Intent.ACTION_SEND)
            waIntent.type = "text/plain"
            waIntent.putExtra("jid", "$formattedNumber@s.whatsapp.net")
            waIntent.setPackage("com.whatsapp")
            waIntent.putExtra(Intent.EXTRA_TEXT, text)
            startActivity(Intent.createChooser(waIntent, "Share with WhatsApp"))
        }
        catch (e: PackageManager.NameNotFoundException) {
            Alerts.showSnackBar(activity, "WhatsApp not Installed")
        }
        catch (e: Exception)
        {
            Alerts.showSnackBar(activity, getString(R.string.something_goes_wrong))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> this@PickupDropActivity.finish()
        }
        return true
    }
}
