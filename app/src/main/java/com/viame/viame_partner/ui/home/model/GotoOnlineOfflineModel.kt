package com.viame.viame_partner.ui.home.model

import android.os.Parcel
import android.os.Parcelable

class GotoOnlineOfflineModel() : Parcelable
{
    var drawableId : Int? = 0

    var stringTitle : String? = ""

    var stringSubTitle : String? = ""

    constructor(parcel: Parcel) : this() {
        stringTitle = parcel.readString()
        stringSubTitle = parcel.readString()
        drawableId = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(this.stringTitle)
        parcel.writeString(this.stringSubTitle)
        parcel.writeValue(this.drawableId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GotoOnlineOfflineModel> {
        override fun createFromParcel(parcel: Parcel): GotoOnlineOfflineModel {
            return GotoOnlineOfflineModel(parcel)
        }

        override fun newArray(size: Int): Array<GotoOnlineOfflineModel?> {
            return arrayOfNulls(size)
        }
    }


}