package com.viame.viame_partner.ui.logout

import android.annotation.SuppressLint
import android.app.NotificationManager
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.NotificationManagerCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.work.ListenableWorker
import com.viame.viame_partner.ONLINE_OFFLINE_NOTIFICATION_ID
import com.viame.viame_partner.R
import com.viame.viame_partner.Vpn
import com.viame.viame_partner.databinding.FragmentLogoutBinding
import com.viame.viame_partner.extension.fromSingleToMain
import com.viame.viame_partner.firebase.unsubscribeAllTopics
import com.viame.viame_partner.homewidget.ViaMeBookingWidget
import com.viame.viame_partner.homewidget.updateWidgets
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.rx.Scheduler
import com.viame.viame_partner.ui.base.BaseFragment
import com.viame.viame_partner.ui.main.MainActivity
import com.viame.viame_partner.ui.splash.SplashScreenActivity
import com.viame.viame_partner.utils.Alerts
import com.viame.viame_partner.utils.ConnectivityReceiver
import com.viame.viame_partner.workmanager.NotifyLocationBackGroundWork
import com.viame.viame_partner.workmanager.stopLocationSendWorkManager
import javax.inject.Inject


class LogoutFragment : BaseFragment<LogoutViewModel>() {

    companion object {
        fun newInstance() = LogoutFragment()
    }

    private lateinit var binding: FragmentLogoutBinding

    @Inject
    lateinit var connectivityReceiver: ConnectivityReceiver

    @Inject
    lateinit var apiInterface: ApiInterface

    @Inject
    lateinit var models : LogoutViewModel

    @Inject
    lateinit var scheduler : Scheduler

    @SuppressLint("CheckResult")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_logout,container,false)

        binding.btnLogout.setOnClickListener {

            connectivityReceiver.observe(this@LogoutFragment, Observer { connectionModel ->

                if (connectionModel != null && connectionModel.isConnected())
                {
                    apiInterface.let {

                        models.getLogoutAPIResult().observe(this, Observer { viewResponse ->

                            if(viewResponse != null && viewResponse.status == APIRequestResponseHandler.AuthStatus.SUCCESS)
                            {
                                Alerts.dismissProgressBar()
                                models.repository.deleteLocationWhenLogout()
                                    .fromSingleToMain(scheduler = scheduler).subscribe(
                                        { onNext ->
                                            println(" $onNext")
                                            ListenableWorker.Result.success()
                                        },
                                        { onError ->
                                            println(" $onError")
                                            ListenableWorker.Result.failure()
                                        })

                                (activity as MainActivity).pubNub.unsubscribe()

                                unsubscribeAllTopics(Preferences.getPreferenceString(PrefEntity.PUSH_TOPICS))
                                stopLocationSendWorkManager(
                                    this.activity!!,
                                    NotifyLocationBackGroundWork.TAG
                                )

                                // Disable Online Offline notification.
                                val notificationManagerCompat =
                                    NotificationManagerCompat.from(activity as MainActivity)
                                notificationManagerCompat.cancel(ONLINE_OFFLINE_NOTIFICATION_ID)

                                val manager = (activity as MainActivity).getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                                manager.cancelAll()

                                updateWidgets(activity as MainActivity)
                                val skipMessage: Boolean = Preferences.getPreferenceBoolean(PrefEntity.SKIP_PROTECTED_APP)
                                Preferences.removeAllPreference()
                                Preferences.setPreference(PrefEntity.SKIP_PROTECTED_APP,skipMessage)
                                val intent = Intent(activity, SplashScreenActivity::class.java)
                                intent.flags =
                                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                activity?.startActivity(intent)
                                (activity)?.finish()
                            }
                            else  if(viewResponse != null && viewResponse.status == APIRequestResponseHandler.AuthStatus.LOADING)
                            {
                                Alerts.showProgressBar(context)
                            }
                            else  if(viewResponse != null && viewResponse.status == APIRequestResponseHandler.AuthStatus.ERROR)
                            {
                                Alerts.dismissProgressBar()
                                Alerts.showSnackBar(activity,viewResponse.message)
                            }
                            else
                            {   Alerts.dismissProgressBar()
                                Alerts.showSnackBar(activity,viewResponse.message)
                            }
                        })

                        models.callLogoutAPI(apiInterface)
                    }
                }
                else
                {
                    if(connectionModel?.type == Vpn)
                    {
                        Alerts.showSnackBar(activity,getString(R.string.vpn_use_message))
                    }
                    else
                    {
                        Alerts.showSnackBar(activity,getString(R.string.internet_not_available))
                    }
                }
            })
        }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun getViewModel(): LogoutViewModel {
        return  models
    }




}
