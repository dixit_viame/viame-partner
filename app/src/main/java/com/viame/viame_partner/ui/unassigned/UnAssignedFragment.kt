package com.viame.viame_partner.ui.unassigned

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.squareup.picasso.Picasso
import com.viame.viame_partner.MBStatusType
import com.viame.viame_partner.R
import com.viame.viame_partner.Vpn
import com.viame.viame_partner.databinding.FragmentUnassignedBinding
import com.viame.viame_partner.databinding.ItemLoadMoreBinding
import com.viame.viame_partner.databinding.RowItemUnassignedBinding
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.base.BaseFragment
import com.viame.viame_partner.ui.main.MainActivity
import com.viame.viame_partner.ui.unassigned.model.ApiDriversShipmentDetails
import com.viame.viame_partner.ui.unassigned.model.UnAssignedAcceptInputModel
import com.viame.viame_partner.ui.unassigned.model.UnAsssignedDatumModel
import com.viame.viame_partner.utils.*
import javax.inject.Inject

class UnAssignedFragment : BaseFragment<UnAssignedViewModel>() , UnAssignedOrderListEventHandler,
    SwipeRefreshLayout.OnRefreshListener {

    companion object {
        fun newInstance() = UnAssignedFragment()
    }

    var selectedAppointmentBookingId = 0L

    @Inject
    lateinit var apiInterface: ApiInterface

    @Inject
    lateinit var connectivityReceiver: ConnectivityReceiver

    @Inject
    lateinit var model : UnAssignedViewModel

    lateinit var mAdapter: CommonAdapter<UnAsssignedDatumModel>

    lateinit var binding : FragmentUnassignedBinding

    private var page = 1
    private var isNextPage = false
    private var isLoadMore = false
    private var isRefresh = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onStart() {
        super.onStart()

        (activity as MainActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        (activity as MainActivity).supportActionBar!!.setDisplayShowHomeEnabled(true)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_unassigned,container,false)

        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        try {
            model.getUnAssignedBookingAPIDataResult().observe(this@UnAssignedFragment, Observer { result ->

                if (result != null) {

                    if (result.status == APIRequestResponseHandler.AuthStatus.LOADING) {
                        if(!isRefresh && !isLoadMore)
                        binding.progressBar.visibility = View.VISIBLE

                    } else if (result.status == APIRequestResponseHandler.AuthStatus.ERROR) {
                        binding.progressBar.visibility = View.INVISIBLE

                        if(result.data != null && result.data.code == 403 && result.data.error)
                        {

                        }
                        else
                        {
                            Alerts.showSnackBar(activity, result.message)
                        }

                    } else if (result.status == APIRequestResponseHandler.AuthStatus.SUCCESS) {
                        binding.progressBar.visibility = View.INVISIBLE

                        if (result.data?.error!!) {
                            Alerts.showSnackBar(activity, result.data.message)
                        }
                        else
                        {
                            if(result.data.data.data.size > 0)
                            {
                                if (result.data.data.nextPageUrl != null) {
                                    isNextPage = true
                                    page += 1
                                } else {
                                    isNextPage = false
                                }

                                val arrData: ArrayList<UnAsssignedDatumModel> = result.data.data.data as ArrayList<UnAsssignedDatumModel>

                                if (isLoadMore) {
                                    mAdapter.stopLoadMore(arrData)
                                } else {
                                    if (isRefresh) {
                                        if (arrData.size > 0) {
                                            binding.tvLoadingTxt.visibility = View.INVISIBLE
                                            binding.myUnAssignedOrderRView.visibility = View.VISIBLE
                                            setUpUnAssignedBooking(arrData)
                                        }
                                        onItemsLoadComplete()
                                    } else {
                                        if (arrData.size > 0) {
                                            binding.myUnAssignedOrderRView.setVisibility(View.VISIBLE)
                                            setUpUnAssignedBooking(arrData)
                                        } else {
                                            if (isRefresh) {
                                                onItemsLoadComplete()
                                            }
                                            binding.myUnAssignedOrderRView.setVisibility(View.GONE)
                                            binding.tvLoadingTxt.setVisibility(View.VISIBLE)
                                        }
                                    }
                                }
                                mAdapter.isMoreDataAvailable = isNextPage

                            }
                            else
                            {
                                binding.tvLoadingTxt.text = getString(R.string.dont_have_records)
                                binding.tvLoadingTxt.visibility = View.VISIBLE
                                binding.myUnAssignedOrderRView.visibility = View.GONE
                            }
                        }
                    }
                }
            })

            connectivityReceiver.observe(this@UnAssignedFragment, Observer { connectionModel ->

                if (connectionModel != null && connectionModel.isConnected())
                {
                    apiInterface.let {
                        model.UnAssignedBookingActionCall(apiInterface,page)
                    }
                }
                else
                {
                    if(connectionModel?.type == Vpn)
                    {
                        Alerts.showSnackBar(activity,getString(R.string.vpn_use_message))
                    }
                    else
                    {
                        Alerts.showSnackBar(activity,getString(R.string.internet_not_available))
                    }
                }
            })

            model.getUnAssignedBookingAcceptAPIDataResult().observe(this@UnAssignedFragment, Observer { result ->

                if (result != null) {

                    if (result.status == APIRequestResponseHandler.AuthStatus.LOADING) {

                        Alerts.showProgressBar(activity)

                    } else if (result.status == APIRequestResponseHandler.AuthStatus.ERROR) {

                        Alerts.dismissProgressBar()
                        if(result.data != null && result.data.code == 403 && result.data.error)
                        {

                        }
                        else if(result.data != null && result.data.code == 400 && result.data.error)
                        {
                            Alerts.showSnackBar(activity, result.data.data)
                        }
                        else
                        {
                            Alerts.showSnackBar(activity, result.message)
                        }

                    } else if (result.status == APIRequestResponseHandler.AuthStatus.SUCCESS) {

                        try {
                            Alerts.dismissProgressBar()

                            if (result.data?.error!!)
                            {
                                Alerts.showSnackBar(activity, result.data.message)
                            }
                            else
                            {
                                Alerts.showBottomSheetSimpleConfirmationDialog(activity,
                                    getString(R.string.alert),
                                    "Booking reference: $selectedAppointmentBookingId is assigned to you successfully check My Orders tab.", true, null,
                                    getString(R.string.ok), object : Alerts.OnConfirmationDialog {
                                        @SuppressLint("CheckResult")
                                        override fun onYes() {

                                            val intent = Intent(activity, MainActivity::class.java)
                                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                            startActivity(intent)
                                            activity?.finish()
                                        }
                                        override fun onNo() {

                                        }
                                    })
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            })

            binding.swipeRefreshLayout.setOnRefreshListener(this)
        } catch (e: Exception) {
        }
    }

    private fun onItemsLoadComplete() { // Update the adapter and notify data set changed
        isRefresh = false
        // Stop refresh animation
        binding.swipeRefreshLayout.setRefreshing(false)
    }


    override fun onRefresh() {
        try {
            page = 1
            isLoadMore = false
            isRefresh = true
            try {
                mAdapter.data.clear()
            } catch (e: Exception) {
            }
            if(Utils.isNetworkAvailable(activity)) {
                apiInterface.let {
                    model.UnAssignedBookingActionCall(apiInterface, page)
                }

                val handler = Handler()
                handler.postDelayed({
                    if(binding.swipeRefreshLayout.isRefreshing()) {
                        binding.swipeRefreshLayout.setRefreshing(false)
                    }

                }, 1500)
            }
            else
            {
                Alerts.showSnackBar(activity,getString(R.string.internet_not_available))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    private fun setUpUnAssignedBooking(arrData: ArrayList<UnAsssignedDatumModel>) {
        mAdapter = object : CommonAdapter<UnAsssignedDatumModel>(arrData) {

            @SuppressLint("SetTextI18n")
            override fun onBind(holder: CommonViewHolder, position: Int)
            {
                try {
                    if (holder.binding is RowItemUnassignedBinding)
                    {
                        val binding: RowItemUnassignedBinding = holder.binding as RowItemUnassignedBinding
                        binding.viewModel = arrData[position]
                        binding.tvBid.text = arrData[position].orderId.toString()
                        binding.tvDate.text = Utils.dateFormatter(arrData[position].bookingTime)
                        binding.tvClientName.text = arrData[position].slaveName.toString()

                        binding.tvBookingPrice.text =
                            "${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} ${arrData[position].invoice.total}"
                        binding.tvBookingFrom.text = arrData[position].bookingOrigin

                        try {
                            if(arrData[position].statusText != null && arrData[position].statusText.isNotEmpty())
                            { binding.tvStatus.visibility = View.VISIBLE
                                binding.tvStatusLBL.visibility = View.VISIBLE
                                binding.tvStatus.text = arrData[position].statusText }
                            else{
                                binding.tvStatus.visibility = View.GONE
                                binding.tvStatusLBL.visibility = View.GONE
                            }
                        } catch (e: Exception) {
                        }

                        if(arrData[position].apptType == "1") {
                            binding.tvBookingType.text = context?.getString(R.string.instant_booking)
                            binding.tvPickupTime.text = Utils.dateFormatter(arrData[position].bookingTime)
                            binding.tvDropoffTime.text = Utils.dateFormatter(arrData[position].bookingTime)
                        }else {
                            binding.tvBookingType.text =
                                context?.getString(R.string.book_later_text)
                            binding.tvPickupTime.text =
                                    Utils.getDateFormatFromMiliseconds(arrData[position].bookingLaterDateTimestamp,"MMM dd, hh:mm a")
                        }
                        binding.handlers = this@UnAssignedFragment
                        binding.indexView = holder.binding.root

                        holder.binding.executePendingBindings()

                        Picasso.get()
                            .load(arrData[position].vehicleImage.toString())
                            .into(binding.vehicleImage)
                    }
                    else
                    {
                        val binding: ItemLoadMoreBinding = holder.binding as ItemLoadMoreBinding
                        binding.progressBar.visibility = View.VISIBLE
                    }
                } catch (e: Exception) {
                }
            }
            override fun getItemViewType(position: Int): Int {
                return if (getItem(position) == null) R.layout.item_load_more else R.layout.row_item_unassigned
            }
        }

        binding.myUnAssignedOrderRView.layoutManager =
            LinearLayoutManagerWrapper(activity, RecyclerView.VERTICAL, false)
        binding.tvLoadingTxt.visibility = View.INVISIBLE
        binding.myUnAssignedOrderRView.adapter = mAdapter
        binding.myUnAssignedOrderRView.visibility = View.VISIBLE

        mAdapter.setLoadMoreListener {
            binding.myUnAssignedOrderRView.post {
                mAdapter.startLoadMore()
                Handler().postDelayed({
                    isLoadMore = true

                    connectivityReceiver.observe(this@UnAssignedFragment, Observer { connectionModel ->

                        if (connectionModel != null && connectionModel.isConnected()) {
                            apiInterface.let { model.UnAssignedBookingActionCall(apiInterface,page) }
                        } else {
                            if(connectionModel?.type == Vpn) {
                                Alerts.showSnackBar(activity,getString(R.string.vpn_use_message))
                            } else {
                                Alerts.showSnackBar(activity,getString(R.string.internet_not_available))
                            }
                        }
                    })
                }, 1500)
            }
        }
    }


    override fun onitemClick(view: View,appointment: UnAsssignedDatumModel)
    {
        selectedAppointmentBookingId = appointment.orderId

        Alerts.showBottomSheetSimpleConfirmationDialog(activity,
            getString(R.string.alert),
            "This is unassigned Order, Do you want to accept this booking reference: $selectedAppointmentBookingId ?",
            false,
            getString(R.string.no),
            getString(R.string.yes),
            object : Alerts.OnConfirmationDialog {
                override fun onYes() {

                    connectivityReceiver.observe(
                        this@UnAssignedFragment,
                        Observer { connectionModel ->

                            if (connectionModel != null && connectionModel.isConnected()) {
                                val mApiDriverDetail = ApiDriversShipmentDetails()
                                val mUnAssignedAcceptInputModel = UnAssignedAcceptInputModel()

                                mApiDriverDetail.latitude =
                                    Preferences.getPreferenceString(PrefEntity.LATITUDE_CURRENT)
                                        .toDouble()
                                mApiDriverDetail.longitude =
                                    Preferences.getPreferenceString(PrefEntity.LONGITUDE_CURRENT)
                                        .toDouble()
                                mApiDriverDetail.status = MBStatusType.ON_THE_WAY.id

                                mUnAssignedAcceptInputModel.apiDriversShipmentDetails =
                                    mApiDriverDetail

                                apiInterface.let {
                                    model.UnAssignedBookingAcceptActionCall(
                                        apiInterface,
                                        mUnAssignedAcceptInputModel,selectedAppointmentBookingId
                                    )
                                }
                            } else {
                                if (connectionModel?.type == Vpn) {
                                    Alerts.showSnackBar(
                                        activity,
                                        getString(R.string.vpn_use_message)
                                    )
                                } else {
                                    Alerts.showSnackBar(
                                        activity,
                                        getString(R.string.internet_not_available)
                                    )
                                }
                            }
                        })

                }

                override fun onNo() {

                }
            })
    }

    override fun getViewModel(): UnAssignedViewModel {
        model = ViewModelProviders.of(this).get(UnAssignedViewModel::class.java)
        return model
    }

}
