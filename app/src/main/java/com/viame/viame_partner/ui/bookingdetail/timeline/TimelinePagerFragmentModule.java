package com.viame.viame_partner.ui.bookingdetail.timeline;
import dagger.Module;
import dagger.Provides;

@Module
public class TimelinePagerFragmentModule {

    @Provides
    TimelinePagerViewModel timelinePagerViewModel() {
        return new TimelinePagerViewModel();
    }


}
