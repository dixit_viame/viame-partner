package com.viame.viame_partner.ui.bookingaccept;

import com.viame.viame_partner.ui.bookingdetail.details.DetailPagerViewModel;

import dagger.Module;
import dagger.Provides;

@Module
public class BookingAcceptDialogModule {

    @Provides
    BookingAcceptDialogViewModel bookingAcceptDialogViewModel() {
        return new BookingAcceptDialogViewModel();
    }


}
