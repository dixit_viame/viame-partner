package com.viame.viame_partner.ui.main.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PushTopics {

    @SerializedName("allDrivers")
    @Expose
    private String allDrivers;
    @SerializedName("allCitiesDrivers")
    @Expose
    private String allCitiesDrivers;
    @SerializedName("outZoneDrivers")
    @Expose
    private String outZoneDrivers;
    @SerializedName("allCustomers")
    @Expose
    private String allCustomers;
    @SerializedName("allCitiesCustomers")
    @Expose
    private String allCitiesCustomers;
    @SerializedName("outZoneCustomers")
    @Expose
    private String outZoneCustomers;

    public String getAllDrivers() {
        return allDrivers;
    }

    public void setAllDrivers(String allDrivers) {
        this.allDrivers = allDrivers;
    }

    public String getAllCitiesDrivers() {
        return allCitiesDrivers;
    }

    public void setAllCitiesDrivers(String allCitiesDrivers) {
        this.allCitiesDrivers = allCitiesDrivers;
    }

    public String getOutZoneDrivers() {
        return outZoneDrivers;
    }

    public void setOutZoneDrivers(String outZoneDrivers) {
        this.outZoneDrivers = outZoneDrivers;
    }

    public String getAllCustomers() {
        return allCustomers;
    }

    public void setAllCustomers(String allCustomers) {
        this.allCustomers = allCustomers;
    }

    public String getAllCitiesCustomers() {
        return allCitiesCustomers;
    }

    public void setAllCitiesCustomers(String allCitiesCustomers) {
        this.allCitiesCustomers = allCitiesCustomers;
    }

    public String getOutZoneCustomers() {
        return outZoneCustomers;
    }

    public void setOutZoneCustomers(String outZoneCustomers) {
        this.outZoneCustomers = outZoneCustomers;
    }

}