package com.viame.viame_partner.ui.signup;

import androidx.lifecycle.ViewModelProvider;
import com.viame.viame_partner.utils.ViewModelProviderFactory;

import dagger.Module;
import dagger.Provides;

@Module
public class SignUpActivityModule {

    @Provides
    SignUpActivityViewModel providesLoginActivityViewModel(){
        return new SignUpActivityViewModel();
    }

    @Provides
    ViewModelProvider.Factory provideViewModelProvider(SignUpActivityViewModel viewModel){
        return new ViewModelProviderFactory<>(viewModel);
    }


}
