package com.viame.viame_partner.ui.jobhistory

import android.view.View
import com.viame.viame_partner.ui.jobhistory.model.JobHistoryAppointment
import com.viame.viame_partner.ui.myorders.model.Appointment

interface JobHistoryEventHandler {
    /**
     * Will be called when signIn button gets clicked
     */
    fun onitemClick(view : View, appointment: JobHistoryAppointment)

}