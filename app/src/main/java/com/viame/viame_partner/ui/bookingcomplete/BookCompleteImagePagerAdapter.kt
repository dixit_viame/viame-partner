package com.viame.viame_partner.ui.bookingcomplete

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class BookCompleteImagePagerAdapter(private val context: Context, arrayList: ArrayList<String>, fm: FragmentManager) : FragmentPagerAdapter(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private var arryList = arrayList

    override fun getItem(position: Int): Fragment {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        return BookCompleteImagePagerFragment.newInstance(position)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return "dddd"
    }

    override fun getCount(): Int {
        // Show 2 total pages.
        return arryList.size
    }
}