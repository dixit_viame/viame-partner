package com.viame.viame_partner.ui.bookingdetail

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.appbar.AppBarLayout
import com.viame.viame_partner.BOOKING_DATA
import com.viame.viame_partner.EXTRA_CIRCULAR_REVEAL_X
import com.viame.viame_partner.EXTRA_CIRCULAR_REVEAL_Y
import com.viame.viame_partner.R
import com.viame.viame_partner.databinding.ActivityBookingDetailBinding
import com.viame.viame_partner.location.bitmapDescriptorFromVector
import com.viame.viame_partner.location.polyline.AVOID
import com.viame.viame_partner.location.polyline.DirectionsApiClient
import com.viame.viame_partner.location.polyline.MODE
import com.viame.viame_partner.location.polyline.TransitOptions
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.ui.base.BaseActivity
import com.viame.viame_partner.ui.bookingcancel.BookingCancelActivity
import com.viame.viame_partner.ui.bookingdetail.details.DetailPagerFragment
import com.viame.viame_partner.ui.myorders.model.Appointment
import com.viame.viame_partner.utils.Alerts
import com.viame.viame_partner.utils.ConnectivityReceiver
import com.viame.viame_partner.utils.Utils
import timber.log.Timber
import javax.inject.Inject
import kotlin.math.max

open class BookingDetailActivity :  BaseActivity<BookingDetailViewModel>() , OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityBookingDetailBinding
    private var revealX = 0
    private var revealY = 0
    private var currentAppointment : Appointment? = null

    private lateinit var directionsApiClient: DirectionsApiClient

    private var LatLongB = LatLngBounds.Builder()

    @Inject
    lateinit var apiInterface: ApiInterface

    @Inject
    lateinit var connectivityReceiver: ConnectivityReceiver


    @Inject
    lateinit var model : BookingDetailViewModel

    override fun getViewModel(): BookingDetailViewModel {
        return model
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )

        binding =  DataBindingUtil.setContentView(this,R.layout.activity_booking_detail)
        setSupportActionBar(binding.toolbar)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.toolbar.setBackgroundColor(ContextCompat.getColor(this,R.color.transparent))

        binding.toolbarLayout.setTitle(getString(R.string.booking_detail))

        val params = binding.appBar.layoutParams as CoordinatorLayout.LayoutParams
        if (params.behavior == null)
            params.behavior = AppBarLayout.Behavior()
        val behaviour = params.behavior as AppBarLayout.Behavior
        behaviour.setDragCallback(object : AppBarLayout.Behavior.DragCallback() {
                override fun canDrag(appBarLayout: AppBarLayout): Boolean {
                    return false
                }
            })

        binding.fabGoogle.setOnClickListener {

            val mLatLong = currentAppointment?.dropLocation

            openGoogleMap(mLatLong?.latitude!!, mLatLong.longitude!!)
        }

        binding.fabWaze.setOnClickListener {

            val mLatLong = currentAppointment?.dropLocation

            openWaze(mLatLong?.latitude!!, mLatLong.longitude!!)
        }

        if (savedInstanceState == null && intent.hasExtra(EXTRA_CIRCULAR_REVEAL_X) && intent.hasExtra(EXTRA_CIRCULAR_REVEAL_Y))
        {
            binding.getRoot().setVisibility(View.INVISIBLE)
            revealX = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_X,
                0
            )
            revealY = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_Y,
                0
            )
            val viewTreeObserver: ViewTreeObserver = binding.getRoot().getViewTreeObserver()
            if (viewTreeObserver.isAlive) {
                viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
                    override fun onGlobalLayout() {
                        revealActivity(revealX, revealY)
                        binding.getRoot().getViewTreeObserver().removeOnGlobalLayoutListener(this)
                    }
                })
            }
        } else {
            binding.getRoot().setVisibility(View.VISIBLE)
        }

        directionsApiClient = DirectionsApiClient(
            apiKey = getString(R.string.google_places_key))

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        if(intent.hasExtra(BOOKING_DATA))
            currentAppointment = intent.getSerializableExtra(BOOKING_DATA) as Appointment?
        else
            return

        val sectionsPagerAdapter = DetailSectionsPagerAdapter(this, supportFragmentManager,currentAppointment!!)
        binding.viewpager.adapter = sectionsPagerAdapter
        binding.viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }
            override fun onPageSelected(position: Int) {

                Timber.e("onPageSelected $position")
            }
        })
        binding.tabs.setupWithViewPager(binding.viewpager)

    }

    private fun openWaze(latitude: Double, longitude: Double) {
        packageManager?.let {
            val url = "waze://?ll=$latitude,$longitude&navigate=yes"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            intent.resolveActivity(it)?.let {
                startActivity(intent)
            } ?: run {
                Alerts.showSnackBar(this,"App not found")
            }
        }
    }

    private fun openGoogleMap(latitude: Double, longitude: Double)
    {
        packageManager?.let {
        val uri = "http://maps.google.com/maps?q=loc:$latitude,$longitude"
           // var uris = String.format(Locale.ENGLISH, "geo:%f,%f", latitude, longitude);
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
       // intent.setPackage("com.google.android.apps.maps")
        intent.resolveActivity(it)?.let {
            startActivity(intent)
        }?: run {
            Alerts.showSnackBar(this,"App not found")
        }
       }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.detailpage_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if(item.itemId == R.id.action_cancel)
        {
            val mIntent = Intent(this,BookingCancelActivity::class.java)

            mIntent.putExtra(BOOKING_DATA, currentAppointment)

            startActivity(mIntent)
        }
        else if(item.itemId ==  android.R.id.home)
        {    if (DetailPagerFragment.IS_BOOKING_STATUS_UPDATED)
             {
                setResult(Activity.RESULT_OK)
             }
            this.finish()
        }
        return super.onOptionsItemSelected(item)
    }

    protected fun revealActivity(x: Int, y: Int) {
            val finalRadius = (max(
                binding.root.width,
                binding.root.height
            ) * 1.1).toFloat()
            // create the animator for this view (the start radius is zero)
            val circularReveal =
                ViewAnimationUtils.createCircularReveal(binding.root, x, y, 0f, finalRadius)
            circularReveal.duration = 500
            //            circularReveal.setInterpolator(new AccelerateInterpolator());
            // make the view visible and start the animation
            binding.root.visibility = View.VISIBLE
            circularReveal.start()
    }

    private fun unRevealActivity() {

            val finalRadius = (max(
                binding.root.width,
                binding.root.height
            ) * 1.1).toFloat()
            val circularReveal =
                ViewAnimationUtils.createCircularReveal(
                    binding.root, revealX, revealY, finalRadius, 0f
                )
            circularReveal.duration = 400
            circularReveal.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    binding.root.visibility = View.INVISIBLE
                    if (DetailPagerFragment.IS_BOOKING_STATUS_UPDATED)
                    {
                        setResult(Activity.RESULT_OK)
                    }
                    finish()
                }
            })
            circularReveal.start()
    }


    override fun onBackPressed() {
        Utils.hideSoftKeyboard(this)
        unRevealActivity()
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        try {
            mMap = googleMap

            // declare bounds object to fit whole route in screen
            LatLongB = LatLngBounds.builder()

            val style =
                MapStyleOptions.loadRawResourceStyle(this, R.raw.mapstyle_grayscale)
            mMap.setMapStyle(style)
            mMap.isIndoorEnabled = false

            val mDropLatLong = currentAppointment?.dropLocation
            val mPickLatLong = currentAppointment?.pickupLocation

            // Add markers
            val pickup = LatLng(mPickLatLong?.latitude!!,mPickLatLong.longitude)
            val dropoff = LatLng(mDropLatLong?.latitude!!,mDropLatLong.longitude)
            mMap.addMarker(MarkerOptions().position(pickup).title("Pickup Location").icon(bitmapDescriptorFromVector(this,R.drawable.location_pin_pick)).draggable(true))
            mMap.addMarker(MarkerOptions().position(dropoff).title("Drop-off Location").icon(bitmapDescriptorFromVector(this,R.drawable.location_pin_drop)).draggable(true))

            this.mMap.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener {
                override fun onMarkerDragEnd(marker: Marker) {
                    getPolylineToRoute(from = marker.position, to = dropoff)
                }
                override fun onMarkerDragStart(p0: Marker?) {}

                override fun onMarkerDrag(p0: Marker?) {}
            })

            getPolylineToRoute(pickup,dropoff)
        } catch (e: Exception) {
        }

    }

    private val mutableList: MutableList<Polyline> = mutableListOf()

    fun getPolylineToRoute(from: LatLng, to: LatLng) {
        mutableList.forEach { it.remove() }
        mutableList.clear()

        val transitOptions = TransitOptions(mode = MODE.DRIVING, whatToAvoidArray = arrayOf(AVOID.FERRIES, AVOID.HIGHWAYS, AVOID.TOLLS))

        directionsApiClient.getRoutePolyline(origin = from, dest = to, options = transitOptions) {
            runOnUiThread {
                it.forEach {

                    try {
                        val latLongBs = LatLngBounds.Builder()
                        latLongBs.include(from)
                        //you can do additional polyline customization here
                        mutableList.add(mMap.addPolyline(it))
                        latLongBs.include(to)

                        // build bounds
                        val bounds = latLongBs.build()
                        // show map with route centered
                        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))
                    } catch (e: Exception) {
                    }
                }
            }
        }
    }


}

