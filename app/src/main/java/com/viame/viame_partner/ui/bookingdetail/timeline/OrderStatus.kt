package com.viame.viame_partner.ui.bookingdetail.timeline

/**
 * Created by Vipul Asri on 07-06-2016.
 */

enum class OrderStatus {
    COMPLETED,
    ACTIVE,
    INACTIVE
}
