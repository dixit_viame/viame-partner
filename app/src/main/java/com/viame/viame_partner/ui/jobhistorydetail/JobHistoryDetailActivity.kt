package com.viame.viame_partner.ui.jobhistorydetail

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.squareup.picasso.Picasso
import com.viame.viame_partner.R
import com.viame.viame_partner.databinding.ActivityJobhistorydetailBinding
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.jobhistory.model.JobHistoryAppointment
import com.viame.viame_partner.utils.Alerts
import com.viame.viame_partner.utils.Utils
import kotlinx.android.synthetic.main.activity_jobhistorydetail.*

class JobHistoryDetailActivity : AppCompatActivity() {

    companion object
    {
        var APPDATA : String = "APPDATA"
    }

    lateinit var bindings : ActivityJobhistorydetailBinding
    var appointment : JobHistoryAppointment? = null
    var images = ArrayList<Any>()


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
        bindings = DataBindingUtil.setContentView(this,R.layout.activity_jobhistorydetail)
        setSupportActionBar(toolbar)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        bindings.toolbar.setBackgroundColor(ContextCompat.getColor(this,R.color.transparent))

        bindings.toolbarLayout.setTitle("")
        setTitle("")

        if(intent.extras!!.get(APPDATA) != null) {
            appointment = intent.extras!!.getSerializable(APPDATA) as JobHistoryAppointment?
            bindings.viewModel = appointment
            bindings.executePendingBindings()

            bindings.tvBid.text = appointment?.orderId.toString()

            try {
                if (appointment!!.receiversData?.get(0)?.photo != null && appointment!!.receiversData?.get(0)?.photo!!.isNotEmpty() && appointment!!.receiversData?.get(0)?.photo!![0] != null)
                {
                    images.addAll(appointment!!.receiversData?.get(0)?.photo!!)
                }
                else if (appointment!!.receiversData?.get(0)?.documentImage != null && appointment!!.receiversData?.get(0)?.documentImage!!.isNotEmpty() && appointment!!.receiversData?.get(0)?.documentImage!![0] != null)
                {
                    images.addAll(appointment!!.receiversData?.get(0)?.documentImage!!)
                }
                else
                    images.add("Default")
            } catch (e: Exception) {
                images.add("Default")
            }

            val adapter = HeaderImagePagerAdapter(images,supportFragmentManager)

            bindings.viewpager.adapter = adapter
            bindings.dotsIndicator.setViewPager(bindings.viewpager)

            // Necessary or the pager will only have one extra page to show
            // make this at least however many pages you can see
            // Necessary or the pager will only have one extra page to show make this at least however many pages you can see
            bindings.viewpager.setOffscreenPageLimit(4)

            bindings.tvDate.text = Utils.dateFormatter(appointment?.appointmentDt!!)
            bindings.tvSenderPhoneNo.labelTextView.text = appointment?.slavemobile

            if(appointment!!.paymentType == "1" || appointment!!.paymentType.equals("Card",true))
            {
                bindings.llCardLayout.visibility = View.VISIBLE
                bindings.llCashLayout.visibility = View.GONE
                bindings.llWalletLayout.visibility = View.GONE
            }
            else  if(appointment!!.paymentType == "2" || appointment!!.paymentType.equals("Wallet",true))
            {
                bindings.llCardLayout.visibility = View.GONE
                bindings.llCashLayout.visibility = View.GONE
                bindings.llWalletLayout.visibility = View.VISIBLE

            }else  if(appointment!!.paymentType == "3" || appointment!!.paymentType.equals("Cash",true))
            {
                bindings.llCardLayout.visibility = View.GONE
                bindings.llCashLayout.visibility = View.VISIBLE
                bindings.llWalletLayout.visibility = View.GONE
            }

            if(appointment?.apptType != null)
            {
                bindings.tvBookingType.visibility = View.VISIBLE
                bindings.tvBookingTypeLBL.visibility = View.VISIBLE

                if (appointment?.apptType == "1")
                {
                    bindings.tvBookingType.text = getString(R.string.instant_booking)
                } else
                {
                    bindings.tvBookingType.text = getString(R.string.book_later_text)
                }
            }
            else
            {
                bindings.tvBookingType.visibility = View.GONE
                bindings.tvBookingTypeLBL.visibility = View.GONE
            }

            try {
                if(appointment?.receiversData?.get(0)?.signatureUrl != null && appointment?.receiversData?.get(0)?.signatureUrl!!.isNotEmpty())
                Picasso.get().load((appointment?.receiversData?.get(0)?.signatureUrl.toString().replace("viame-media/",""))).into(bindings.ivSignatureImage)
            } catch (e: Exception) {
            }

            when (appointment!!.pricingType) {
                "0" -> {
                    bindings.llBasefare.visibility = View.VISIBLE
                    bindings.llDistancefare.visibility = View.VISIBLE
                    bindings.llTimefare.visibility = View.VISIBLE
                    bindings.llWaitingfare.visibility = View.VISIBLE
                    bindings.llHandling.visibility = View.GONE
                }
                "1" -> {
                    bindings.llBasefare.visibility = View.VISIBLE
                    bindings.llDistancefare.visibility = View.GONE
                    bindings.llTimefare.visibility = View.GONE
                    bindings.llWaitingfare.visibility = View.VISIBLE
                    bindings.llHandling.visibility = View.GONE
                }
                "2" -> {
                    bindings.llBasefare.visibility = View.VISIBLE
                    bindings.llDistancefare.visibility = View.GONE
                    bindings.llTimefare.visibility = View.GONE
                    bindings.llWaitingfare.visibility = View.VISIBLE
                    bindings.llHandling.visibility = View.GONE
                }
            }

            try {
                if(appointment!!.invoice?.tax != null)
                {
                    bindings.llVat.visibility = View.VISIBLE

                    if (appointment!!.invoice?.tax != null && appointment!!.invoice?.tax!!.isNotEmpty()) {
                        bindings.tvVat.text = appointment!!.invoice?.tax + " (" + (appointment!!.invoice?.tax) + "%)"
                        bindings.tvVatValue.setText(Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL) + "" + appointment!!.invoice?.tax)
                    }
                }
                else
                {
                    bindings.llVat.visibility = View.GONE
                }
            } catch (e: Exception) {
            }

            bindings.tvDeliveryFee.setText(Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL) + appointment!!.invoice?.baseFare)

            if (appointment!!.invoice?.baseFare  != null && appointment!!.invoice?.baseFare!!.isNotEmpty() && appointment!!.invoice?.baseFare == "0.00") {
                bindings.llBasefare.visibility = View.GONE
            } else {
                bindings.llBasefare.visibility = View.VISIBLE
            }

            if (appointment!!.invoice?.timeFare != null && appointment!!.invoice?.timeFare!!.isNotEmpty() && appointment!!.invoice?.timeFare == "0.00") {
                bindings.llTimefare.visibility = View.GONE
            } else {
                bindings.llTimefare.visibility = View.VISIBLE
            }

            if (appointment!!.invoice?.watingFee != null && appointment!!.invoice?.watingFee!!.isNotEmpty() && appointment!!.invoice?.watingFee == "0.00") {
                bindings.llWaitingfare.visibility = View.GONE
            } else {
                bindings.llWaitingfare.visibility = View.VISIBLE
            }

            if (appointment!!.invoice?.distFare != null && appointment!!.invoice?.distFare!!.isNotEmpty() && appointment!!.invoice?.distFare == "") {
                bindings.llDistancefare.visibility = View.GONE
            } else {
                bindings.llDistancefare.visibility = View.VISIBLE
            }

            if (appointment!!.invoice?.handlingFee != null && appointment!!.invoice?.handlingFee!!.isNotEmpty()  && appointment!!.invoice?.handlingFee == "0.00") {
                bindings.llHandling.visibility = View.GONE
            } else {
                bindings.llHandling.visibility = View.GONE
            }

            if (appointment!!.invoice?.tollFee != null && appointment!!.invoice?.tollFee!!.isNotEmpty()  && appointment!!.invoice?.tollFee == "0")
            {
               bindings.llToll.visibility = View.GONE
            } else {
                bindings.llToll.visibility = View.GONE
            }

            if (appointment!!.invoice?.watingFee != null && appointment!!.invoice?.watingFee!!.isNotEmpty()  && appointment!!.invoice!!.watingFee == "0.00") {
                bindings.llWaitingfare.visibility = View.GONE
            } else {
                bindings.llWaitingfare.visibility = View.VISIBLE
            }

            if (appointment!!.invoice?.discount != null && appointment!!.invoice?.discount!!.isNotEmpty()  && appointment!!.invoice?.discount == "0.00") {
                bindings.llDiscountfare.visibility = View.GONE
            } else {
                bindings.llDiscountfare.visibility = View.VISIBLE
            }

            if (appointment!!.receiversData?.get(0)?.goodType != null && appointment!!.receiversData?.get(0)?.goodType!!.isEmpty() ) {

                bindings.tvGoodsType.setText(getString(R.string.no_good_type))
            } else {
                bindings.tvGoodsType.setText(appointment!!.receiversData?.get(0)?.goodType)
            }

             bindings.tvQuantity.visibility = View.GONE
             bindings.tvQuantityLBL.visibility = View.GONE

            if (appointment!!.invoice?.subtotal != null && appointment!!.invoice?.subtotal!!.isNotEmpty())
            {
                bindings.tvSubtotalFare.text = "${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} ${appointment!!.invoice?.subtotal}"

            }

            bindings.tvDistancefareValue.text = "${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} ${appointment!!.invoice?.distFare}"

            bindings.tvTimefareValue.text = "${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} ${appointment!!.invoice?.timeFare}"

            bindings.tvBaseFareValue.text = "${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} ${appointment!!.invoice?.baseFare}"

            bindings.tvWaitingFeeValue.text = "${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} ${appointment!!.invoice?.watingFee}"

            bindings.tvTollFareValue.text = "${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} ${appointment!!.invoice?.tollFee}"

            bindings.tvHandlingValue.text = "${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} ${appointment!!.invoice?.handlingFee}"

            bindings.tvDiscountValue.text = "${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} ${appointment!!.invoice?.discount}"

            bindings.tvGrandtotalValue.text = "${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} ${appointment!!.invoice?.baseFare}"

            bindings.tvReceiverName.text = appointment!!.receiversData?.get(0)?.receiverName

            bindings.tvReceiverPhoneNo.labelTextView.text = appointment!!.receiversData?.get(0)?.receiverMobile

            bindings.tvReceiverEmail.visibility = View.GONE
            bindings.tvReceiverEmailLBL.visibility = View.GONE


            bindings.tvSenderContact.setOnClickListener {

                Alerts.showBottomSheetSimpleConfirmationDialog(this@JobHistoryDetailActivity,
                    getString(R.string.call_customer), appointment!!.slavemobile, false, getString(R.string.action_cancel),
                    getString(R.string.call_lbl), object : Alerts.OnConfirmationDialog {
                        override fun onYes() {

                            val dialIntent = Intent(
                                Intent.ACTION_DIAL,
                                Uri.parse("tel:${appointment!!.slaveCountryCode}${appointment!!.slavemobile}"))
                            startActivity(dialIntent)
                        }
                        override fun onNo() {
                        }
                    })
            }
            bindings.tvReceiverContact.setOnClickListener {

                Alerts.showBottomSheetSimpleConfirmationDialog(this@JobHistoryDetailActivity,
                    getString(R.string.call_customer), appointment!!.receiversData?.get(0)?.receiverMobile, false, getString(R.string.action_cancel),
                    getString(R.string.call_lbl), object : Alerts.OnConfirmationDialog {
                        override fun onYes() {
                            val dialIntent = Intent(
                                Intent.ACTION_DIAL,
                                Uri.parse("tel:${appointment!!.receiversData?.get(0)?.receiverCountryCode}${appointment!!.receiversData?.get(0)?.receiverMobile}"))
                            startActivity(dialIntent)
                        }
                        override fun onNo() {
                        }
                    })
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            android.R.id.home -> this@JobHistoryDetailActivity.finish()
        }
        return true
    }
}
