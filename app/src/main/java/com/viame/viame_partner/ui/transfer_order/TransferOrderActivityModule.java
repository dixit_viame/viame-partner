package com.viame.viame_partner.ui.transfer_order;

import androidx.lifecycle.ViewModelProvider;

import com.viame.viame_partner.utils.ViewModelProviderFactory;

import dagger.Module;
import dagger.Provides;


@Module
public class TransferOrderActivityModule {

    @Provides
    TransferOrderActivityModel providesTransferOrderActivityModel(){
        return new TransferOrderActivityModel();
    }

    @Provides
    ViewModelProvider.Factory provideViewModelProvider(TransferOrderActivityModel viewModel){
        return new ViewModelProviderFactory<>(viewModel);
    }

}
