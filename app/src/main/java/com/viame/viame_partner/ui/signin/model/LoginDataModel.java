package com.viame.viame_partner.ui.signin.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginDataModel {
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("chn")
    @Expose
    private String chn;
    @SerializedName("publishChn")
    @Expose
    private String publishChn;

    @SerializedName("mid")
    @Expose
    private String mid;

    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("pub_key")
    @Expose
    private String pubKey;
    @SerializedName("sub_key")
    @Expose
    private String subKey;
    @SerializedName("server_chn")
    @Expose
    private String serverChn;
    @SerializedName("presence_chn")
    @Expose
    private String presenceChn;
    @SerializedName("vehicles")
    @Expose
    private List<VehicleListData> vehicles = null;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("profilePic")
    @Expose
    private String profilePic;
    @SerializedName("pushTopic")
    @Expose
    private String pushTopic;
    @SerializedName("enableWallet")
    @Expose
    private Boolean enableWallet;
    @SerializedName("walletAmount")
    @Expose
    private String walletAmount;
    @SerializedName("softLimit")
    @Expose
    private Integer softLimit;
    @SerializedName("hardLimit")
    @Expose
    private Integer hardLimit;
    @SerializedName("reachedSoftLimit")
    @Expose
    private Boolean reachedSoftLimit;
    @SerializedName("reachedHardLimit")
    @Expose
    private Boolean reachedHardLimit;
    @SerializedName("stripeEnabled")
    @Expose
    private Boolean stripeEnabled;
    public String getChn() {
        return chn;
    }

    public void setChn(String chn) {
        this.chn = chn;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPublishChn() {
        return publishChn;
    }

    public void setPublishChn(String publishChn) {
        this.publishChn = publishChn;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPubKey() {
        return pubKey;
    }

    public void setPubKey(String pubKey) {
        this.pubKey = pubKey;
    }

    public String getSubKey() {
        return subKey;
    }

    public void setSubKey(String subKey) {
        this.subKey = subKey;
    }

    public String getServerChn() {
        return serverChn;
    }

    public void setServerChn(String serverChn) {
        this.serverChn = serverChn;
    }

    public String getPresenceChn() {
        return presenceChn;
    }

    public void setPresenceChn(String presenceChn) {
        this.presenceChn = presenceChn;
    }

    public List<VehicleListData> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<VehicleListData> vehicles) {
        this.vehicles = vehicles;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getPushTopic() {
        return pushTopic;
    }

    public void setPushTopic(String pushTopic) {
        this.pushTopic = pushTopic;
    }

    public Boolean getEnableWallet() {
        return enableWallet;
    }

    public void setEnableWallet(Boolean enableWallet) {
        this.enableWallet = enableWallet;
    }

    public String getWalletAmount() {
        return walletAmount;
    }

    public void setWalletAmount(String walletAmount) {
        this.walletAmount = walletAmount;
    }

    public Integer getSoftLimit() {
        return softLimit;
    }

    public void setSoftLimit(Integer softLimit) {
        this.softLimit = softLimit;
    }

    public Integer getHardLimit() {
        return hardLimit;
    }

    public void setHardLimit(Integer hardLimit) {
        this.hardLimit = hardLimit;
    }

    public Boolean getReachedSoftLimit() {
        return reachedSoftLimit;
    }

    public void setReachedSoftLimit(Boolean reachedSoftLimit) {
        this.reachedSoftLimit = reachedSoftLimit;
    }

    public Boolean getReachedHardLimit() {
        return reachedHardLimit;
    }

    public void setReachedHardLimit(Boolean reachedHardLimit) {
        this.reachedHardLimit = reachedHardLimit;
    }

    public Boolean getStripeEnabled() {
        return stripeEnabled;
    }

    public void setStripeEnabled(Boolean stripeEnabled) {
        this.stripeEnabled = stripeEnabled;
    }

}