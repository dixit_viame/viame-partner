package com.viame.viame_partner.ui.jobhistory.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobHistoryTotalEarning {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("amt")
    @Expose
    private String amt;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

}