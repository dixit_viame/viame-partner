package com.viame.viame_partner.ui.bookingaccept.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PubnubBookingData implements Serializable {


    @SerializedName("dropDt")
    @Expose
    private String dropDt;
    @SerializedName("a")
    @Expose
    private String a;
    @SerializedName("dropZone")
    @Expose
    private String dropZone;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("helpers")
    @Expose
    private String helpers;
    @SerializedName("chn")
    @Expose
    private String chn;
    @SerializedName("adr1")
    @Expose
    private String adr1;
    @SerializedName("dis")
    @Expose
    private String dis;
    @SerializedName("goodsTypeImg")
    @Expose
    private String goodsTypeImg;
    @SerializedName("paymentType")
    @Expose
    private String paymentType;
    @SerializedName("dt")
    @Expose
    private String dt;
    @SerializedName("drop1")
    @Expose
    private String drop1;
    @SerializedName("subsubcategory")
    @Expose
    private String subsubcategory;
    @SerializedName("serverTime")
    @Expose
    private String serverTime;
    @SerializedName("bid")
    @Expose
    private String bid;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("subcategory")
    @Expose
    private String subcategory;
    @SerializedName("pickZone")
    @Expose
    private String pickZone;
    @SerializedName("ExpiryTimer")
    @Expose
    private String expiryTimer;

    public String getDropDt() {
        return dropDt;
    }

    public void setDropDt(String dropDt) {
        this.dropDt = dropDt;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getDropZone() {
        return dropZone;
    }

    public void setDropZone(String dropZone) {
        this.dropZone = dropZone;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getHelpers() {
        return helpers;
    }

    public void setHelpers(String helpers) {
        this.helpers = helpers;
    }

    public String getChn() {
        return chn;
    }

    public void setChn(String chn) {
        this.chn = chn;
    }

    public String getAdr1() {
        return adr1;
    }

    public void setAdr1(String adr1) {
        this.adr1 = adr1;
    }

    public String getDis() {
        return dis;
    }

    public void setDis(String dis) {
        this.dis = dis;
    }

    public String getGoodsTypeImg() {
        return goodsTypeImg;
    }

    public void setGoodsTypeImg(String goodsTypeImg) {
        this.goodsTypeImg = goodsTypeImg;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getDt() {
        return dt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    public String getDrop1() {
        return drop1;
    }

    public void setDrop1(String drop1) {
        this.drop1 = drop1;
    }

    public String getSubsubcategory() {
        return subsubcategory;
    }

    public void setSubsubcategory(String subsubcategory) {
        this.subsubcategory = subsubcategory;
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getPickZone() {
        return pickZone;
    }

    public void setPickZone(String pickZone) {
        this.pickZone = pickZone;
    }

    public String getExpiryTimer() {
        return expiryTimer;
    }

    public void setExpiryTimer(String expiryTimer) {
        this.expiryTimer = expiryTimer;
    }

}
