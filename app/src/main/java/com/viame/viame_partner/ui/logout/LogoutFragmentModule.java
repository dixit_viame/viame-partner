package com.viame.viame_partner.ui.logout;
import dagger.Module;
import dagger.Provides;

@Module
public class LogoutFragmentModule {

    @Provides
    LogoutViewModel myLogoutViewModel() {
        return new LogoutViewModel();
    }


}
