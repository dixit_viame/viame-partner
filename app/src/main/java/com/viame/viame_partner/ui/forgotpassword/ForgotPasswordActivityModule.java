package com.viame.viame_partner.ui.forgotpassword;
import androidx.lifecycle.ViewModelProvider;

import com.viame.viame_partner.utils.ViewModelProviderFactory;

import dagger.Module;
import dagger.Provides;

@Module
public class ForgotPasswordActivityModule {

    @Provides
    ForgotPasswordActivityViewModel providesForgotPasswordActivityViewModel(){
        return new ForgotPasswordActivityViewModel();
    }

    @Provides
    ViewModelProvider.Factory provideViewModelProvider(ForgotPasswordActivityViewModel viewModel){
        return new ViewModelProviderFactory<>(viewModel);
    }


}
