package com.viame.viame_partner.ui.vehicle

interface VehicleSelectEventHandler {
    /**
     * Will be called when vehicle has been clicked
     */
    fun onVehicleSelect()


    /**
     * Will be called when logged out from Logout
     */
    fun onLogoutSelect()


    /**
     * Will be called when Vehicle Selected
     */
    fun onItemClick(position: Int)
}