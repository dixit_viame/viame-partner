package com.viame.viame_partner.ui.jobhistory

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.viame.viame_partner.R
import com.viame.viame_partner.application.CatalogApplication
import com.viame.viame_partner.databinding.RowItemJobhistoryBinding
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.jobhistory.model.JobHistoryAppointment
import com.viame.viame_partner.utils.Utils

class JobHistoryRecycleViewAdapter(context: Context, appointments: ArrayList<JobHistoryAppointment>, private val listener: JobHistoryEventHandler) : RecyclerView.Adapter<JobHistoryRecycleViewAdapter.CategoryViewHolder>()
{
    private val context: Context? = context
    private var layoutInflater: LayoutInflater? = null

    var appointments: ArrayList<JobHistoryAppointment>? = appointments

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): CategoryViewHolder {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(context)
        }
        val view = layoutInflater!!.inflate(R.layout.row_item_jobhistory, parent, false)
        return CategoryViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.binding!!.viewModel = appointments!![holder.adapterPosition]
        holder.binding.tvBid.text = appointments?.get(holder.adapterPosition)?.orderId.toString()
        holder.binding.tvDate.text = Utils.dateFormatter(appointments?.get(holder.adapterPosition)?.appointmentDt!!)
        holder.binding.tvPickupTime.text = Utils.dateFormatter(appointments?.get(holder.adapterPosition)?.appointmentDt!!)
        holder.binding.tvDropoffTime.text = Utils.dateFormatter(appointments?.get(holder.adapterPosition)?.appointmentDt!!)
        holder.binding.tvBookingPrice.setText("${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} ${appointments!![holder.adapterPosition].invoice?.total}")
        holder.binding.tvStatus.text = appointments!![holder.adapterPosition].bookingOrigin
        if(appointments!![holder.adapterPosition].apptType == "1")
        {
            holder.binding.tvBookingType.text = CatalogApplication.instance?.getString(R.string.instant_booking)
        }
        else
        {
            holder.binding.tvBookingType.text =  CatalogApplication.instance?.getString(R.string.book_later_text)
        }

        holder.binding.handlers = listener
        holder.binding.indexView = holder.binding.root
        //Triggers the View to be updated with the new values provided.
        // This method has to be run on the UI thread.
        holder.binding.executePendingBindings()
    }

    override fun getItemCount(): Int {
        return appointments!!.size
    }

    inner class CategoryViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val binding: RowItemJobhistoryBinding?
        init {
            binding = DataBindingUtil.bind(v)
            v.tag = binding
        }
    }
}