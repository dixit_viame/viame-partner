package com.viame.viame_partner.ui.jobhistory.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class JobHistoryInvoice implements Serializable {

    @SerializedName("timeFare")
    @Expose
    private String timeFare;
    @SerializedName("baseFare")
    @Expose
    private String baseFare;
    @SerializedName("distFare")
    @Expose
    private String distFare;
    @SerializedName("watingFee")
    @Expose
    private String watingFee;
    @SerializedName("cancelationFee")
    @Expose
    private String cancelationFee;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("tollFee")
    @Expose
    private String tollFee;

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    @SerializedName("subtotal")
    @Expose
    private String subtotal;

    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("Discount")
    @Expose
    private String discount;
    @SerializedName("appliedAmount")
    @Expose
    private String appliedAmount;
    @SerializedName("appProfitLoss")
    @Expose
    private String appProfitLoss;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("tax")
    @Expose
    private String tax;
    @SerializedName("estimateId")
    @Expose
    private String estimateId;
    @SerializedName("estimateFare")
    @Expose
    private String estimateFare;
    @SerializedName("estimateDistance")
    @Expose
    private String estimateDistance;
    @SerializedName("estimateTime")
    @Expose
    private String estimateTime;
    @SerializedName("taxEnable")
    @Expose
    private Boolean taxEnable;
    @SerializedName("handlingFee")
    @Expose
    private String handlingFee;
    @SerializedName("taxTitle")
    @Expose
    private String taxTitle;
    @SerializedName("taxCode")
    @Expose
    private String taxCode;
    @SerializedName("taxPercentage")
    @Expose
    private String taxPercentage;

    @SerializedName("taxValue")
    @Expose
    private String taxValue;

    public String getHandlingFee() {
        return handlingFee;
    }

    public void setHandlingFee(String handlingFee) {
        this.handlingFee = handlingFee;
    }

    public String getTollFee() {
        return tollFee;
    }

    public void setTollFee(String tollFee) {
        this.tollFee = tollFee;
    }

    public String getTimeFare() {
        return timeFare;
    }

    public void setTimeFare(String timeFare) {
        this.timeFare = timeFare;
    }

    public String getBaseFare() {
        return baseFare;
    }

    public void setBaseFare(String baseFare) {
        this.baseFare = baseFare;
    }

    public String getDistFare() {
        return distFare;
    }

    public void setDistFare(String distFare) {
        this.distFare = distFare;
    }

    public String getWatingFee() {
        return watingFee;
    }

    public void setWatingFee(String watingFee) {
        this.watingFee = watingFee;
    }

    public String getCancelationFee() {
        return cancelationFee;
    }

    public void setCancelationFee(String cancelationFee) {
        this.cancelationFee = cancelationFee;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getAppliedAmount() {
        return appliedAmount;
    }

    public void setAppliedAmount(String appliedAmount) {
        this.appliedAmount = appliedAmount;
    }

    public String getAppProfitLoss() {
        return appProfitLoss;
    }

    public void setAppProfitLoss(String appProfitLoss) {
        this.appProfitLoss = appProfitLoss;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getEstimateId() {
        return estimateId;
    }

    public void setEstimateId(String estimateId) {
        this.estimateId = estimateId;
    }

    public String getEstimateFare() {
        return estimateFare;
    }

    public void setEstimateFare(String estimateFare) {
        this.estimateFare = estimateFare;
    }

    public String getEstimateDistance() {
        return estimateDistance;
    }

    public void setEstimateDistance(String estimateDistance) {
        this.estimateDistance = estimateDistance;
    }

    public String getEstimateTime() {
        return estimateTime;
    }

    public void setEstimateTime(String estimateTime) {
        this.estimateTime = estimateTime;
    }

    public Boolean getTaxEnable() {
        return taxEnable;
    }

    public void setTaxEnable(Boolean taxEnable) {
        this.taxEnable = taxEnable;
    }

    public String getTaxTitle() {
        return taxTitle;
    }

    public void setTaxTitle(String taxTitle) {
        this.taxTitle = taxTitle;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getTaxPercentage() {
        return taxPercentage;
    }

    public void setTaxPercentage(String taxPercentage) {
        this.taxPercentage = taxPercentage;
    }

    public String getTaxValue() {
        return taxValue;
    }

    public void setTaxValue(String taxValue) {
        this.taxValue = taxValue;
    }


}
