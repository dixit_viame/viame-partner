package com.viame.viame_partner.ui.vehicle

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.viame.viame_partner.*
import com.viame.viame_partner.application.CatalogApplication
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.logout.model.LogoutResponseModel
import com.viame.viame_partner.ui.vehicle.model.VehicleConfirmInputModel
import com.viame.viame_partner.ui.vehicle.model.VehicleConfirmOutPutModel
import com.viame.viame_partner.ui.vehicle.model.VehicleListMainModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.*
import javax.inject.Inject


class VehicleListActivityModel @Inject constructor() : ViewModel()
{
    private val vehicleApiResponse = MutableLiveData<APIRequestResponseHandler<VehicleConfirmOutPutModel>>()

    fun getVehicleAPIDataResult(): MutableLiveData<APIRequestResponseHandler<VehicleConfirmOutPutModel>> = vehicleApiResponse
    /**
     * Two way bind-able fields
     */
    var email: String = ""
    var username: String = ""
    private var userImageUrl: String = ""


    fun getVehicleDataFromPreferences()
    {
        email = Preferences.getPreferenceString(PrefEntity.USER_NAME)
        username = Preferences.getPreferenceString(PrefEntity.USER_NAME)
        userImageUrl = Preferences.getPreferenceString(PrefEntity.PROFILE_PIC)

//        val jsonArray: JSONArray
//        val gson = Gson()
//        val vDataList = ArrayList<VehicleListModel>()
//        try {
//            jsonArray = JSONArray(Preferences.getPreferenceString(PrefEntity.VEHICLES))
//
//            for (i in 0 until jsonArray.length()) {
//                val signinDriverVehicle: VehicleListModel = gson.fromJson(jsonArray.getString(i), VehicleListModel::class.java)
//                vDataList.add(signinDriverVehicle)
//            }
//            vehicleListResponse.value = vDataList
//        } catch (e: JSONException) {
//            e.printStackTrace()
//        }
    }


    fun vehicleActionCall(vehicleObj: VehicleConfirmInputModel, apiInterface : ApiInterface)
    {
        vehicleApiResponse.value = APIRequestResponseHandler.loading(null)

        val headerMap = HashMap<String,String>()
        headerMap["Content-Type"] = "application/json;charset=UTF-8"
        headerMap["authorization"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["token"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["userType"] = USER_TYPE.toString()

        val posts: Call<VehicleConfirmOutPutModel> = apiInterface.callVehicleConfirmMethod(headerMap,vehicleObj)!!
        //Enqueue the call
        posts.enqueue(object : Callback<VehicleConfirmOutPutModel> {
            @SuppressLint("TimberArgCount")
            override fun onFailure(call: Call<VehicleConfirmOutPutModel>, t: Throwable) {

                try {
                    vehicleApiResponse.value = APIRequestResponseHandler.error(t.message.toString(),null)
                    Timber.e(t.message.toString())
                } catch (e: Exception) {
                }
            }

            @SuppressLint("TimberArgCount")
            override fun onResponse(call: Call<VehicleConfirmOutPutModel>, response: Response<VehicleConfirmOutPutModel>
            ) {
                if(response.isSuccessful) {
                    vehicleApiResponse.value = APIRequestResponseHandler.success(response.body()!!)
                    Timber.e(response.body().toString())
                }
                else
                {
                    vehicleApiResponse.value = APIRequestResponseHandler.error("Status code "+response.code(),null)

                }
            }
        })
    }

    private val vehicleListResponse = MutableLiveData<APIRequestResponseHandler<VehicleListMainModel>>()

    fun getVehicleListResult(): LiveData<APIRequestResponseHandler<VehicleListMainModel>> = vehicleListResponse

    fun getVehicleListFromAPI(apiInterface : ApiInterface)
    {
        vehicleListResponse.value = APIRequestResponseHandler.loading(null)

        val headerMap = HashMap<String,String>()
        headerMap["Content-Type"] = "application/json;charset=UTF-8"
        headerMap["authorization"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["token"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["userType"] = USER_TYPE.toString()

        val posts: Call<VehicleListMainModel> = apiInterface.callVehicleListMethod("$BASE_ENGINE_URL$VEHICLE_LIST_API",headerMap)!!

        //Enqueue the call
        posts.enqueue(object : Callback<VehicleListMainModel> {
            @SuppressLint("TimberArgCount")
            override fun onFailure(call: Call<VehicleListMainModel>, t: Throwable) {

                try {
                    vehicleListResponse.value = APIRequestResponseHandler.error(t.message.toString(),null)
                    Timber.e(t.message.toString())
                } catch (e: Exception) {
                }
            }

            @SuppressLint("TimberArgCount")
            override fun onResponse(call: Call<VehicleListMainModel>, response: Response<VehicleListMainModel>
            ) {
                if(response.isSuccessful) {
                    vehicleListResponse.value = APIRequestResponseHandler.success(response.body())
                    Timber.e(response.body().toString())
                }
                else
                {
                    vehicleListResponse.value = APIRequestResponseHandler.error("Status code "+response.code(),null)

                }
            }
        })
    }

    private val getLogoutApiResponse = MutableLiveData<APIRequestResponseHandler<LogoutResponseModel>>()

    fun getLogoutAPIResult(): MutableLiveData<APIRequestResponseHandler<LogoutResponseModel>> = getLogoutApiResponse

    fun callLogoutAPI(apiInterface : ApiInterface) {

        getLogoutApiResponse.value = APIRequestResponseHandler.loading(null)

        val headerMap = HashMap<String, String>()
        headerMap["Content-Type"] = "application/json;charset=UTF-8"
        headerMap["authorization"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["token"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["userType"] = USER_TYPE.toString()

        val posts: Call<LogoutResponseModel> =
            apiInterface.callLogoutMethod(BASE_ENGINE_URL + LOGOUT_URL, headerMap)

        //Enqueue the call
        posts.enqueue(object : Callback<LogoutResponseModel> {
            @SuppressLint("TimberArgCount")
            override fun onFailure(call: Call<LogoutResponseModel>, t: Throwable) {
                getLogoutApiResponse.value =
                    APIRequestResponseHandler.error(t.message.toString(), null)
                Timber.e(t.message.toString())
            }

            @SuppressLint("TimberArgCount")
            override fun onResponse(
                call: Call<LogoutResponseModel>,
                response: Response<LogoutResponseModel>
            ) =
                try {
                    if (response.isSuccessful) {

                        val mLogoutResponseModel = response.body()
                        if (mLogoutResponseModel?.success!!) {
                            getLogoutApiResponse.value =
                                APIRequestResponseHandler.success(mLogoutResponseModel)
                        } else {
                            getLogoutApiResponse.value = APIRequestResponseHandler.error(
                                mLogoutResponseModel.message,
                                mLogoutResponseModel
                            )
                        }

                        Timber.e(response.body().toString())
                    } else {
                        getLogoutApiResponse.value =
                            APIRequestResponseHandler.error("Status code " + response.code(), null)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()

                    getLogoutApiResponse.value = APIRequestResponseHandler.error(
                        CatalogApplication.instance?.getString(
                            R.string.something_goes_wrong
                        ), null
                    )
                }
        })
    }
}