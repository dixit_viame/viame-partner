package com.viame.viame_partner.ui.unassigned

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.viame.viame_partner.BASE_ENGINE_URL
import com.viame.viame_partner.UNASSIGNED_ACCEPT_BOOKING
import com.viame.viame_partner.UNASSIGNED_BOOKING_API
import com.viame.viame_partner.USER_TYPE
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.unassigned.model.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class UnAssignedViewModel @Inject constructor() : ViewModel() {

    private val unAssignedBookingApiResponse = MutableLiveData<APIRequestResponseHandler<UnAssignedResponseModel>>()

    fun getUnAssignedBookingAPIDataResult(): MutableLiveData<APIRequestResponseHandler<UnAssignedResponseModel>> = unAssignedBookingApiResponse

    fun UnAssignedBookingActionCall(apiInterface : ApiInterface,page : Int)
    {
        unAssignedBookingApiResponse.value = APIRequestResponseHandler.loading(null)

        val headerMap = HashMap<String,String>()
        headerMap["Content-Type"] = "application/json;charset=UTF-8"
        headerMap["authorization"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["token"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["userType"] = USER_TYPE.toString()

        val posts: Call<UnAssignedResponseModel> = apiInterface.callAllBookingsMethod("$BASE_ENGINE_URL$UNASSIGNED_BOOKING_API?page=$page",headerMap)!!
        //Enqueue the call
        posts.enqueue(object : Callback<UnAssignedResponseModel>
        {
            @SuppressLint("TimberArgCount")
            override fun onFailure(call: Call<UnAssignedResponseModel>, t: Throwable) {

                try {
                    if(t.message.toString().contains("BEGIN_OBJECT"))
                    {
                        val mVal = UnAssignedResponseModel()
                        mVal.code = 200
                        mVal.error = false
                        mVal.success = true

                        val unAssignedData = UnAssignedData()
                        unAssignedData.currentPage = 0

                        val listOfVehicleNames: MutableList<UnAsssignedDatumModel> = mutableListOf()

                        unAssignedData.data = listOfVehicleNames
                        unAssignedData.firstPageUrl = null
                        unAssignedData.nextPageUrl = null
                        mVal.data = unAssignedData

                        unAssignedBookingApiResponse.value = APIRequestResponseHandler.success(mVal)
                    }
                    else {
                        unAssignedBookingApiResponse.value =
                            APIRequestResponseHandler.error(t.message.toString(), null)
                    }
                    Timber.e(t.message.toString())
                } catch (e: Exception) {
                }
            }

            @SuppressLint("TimberArgCount")
            override fun onResponse(call: Call<UnAssignedResponseModel>, response: Response<UnAssignedResponseModel>
            ) {
                try {
                    if(response.isSuccessful) {
                        unAssignedBookingApiResponse.value = APIRequestResponseHandler.success(response.body()!!)
                        Timber.e(response.body().toString())
                    } else
                    {
                        unAssignedBookingApiResponse.value = APIRequestResponseHandler.error("Status code "+response.code(),null)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }


    private val unAssignedBookingAcceptApiResponse = MutableLiveData<APIRequestResponseHandler<UnAssignedAcceptOutputModel>>()

    fun getUnAssignedBookingAcceptAPIDataResult(): MutableLiveData<APIRequestResponseHandler<UnAssignedAcceptOutputModel>> = unAssignedBookingAcceptApiResponse

    fun UnAssignedBookingAcceptActionCall(apiInterface : ApiInterface,input : UnAssignedAcceptInputModel,bookingId:Long)
    {
        unAssignedBookingAcceptApiResponse.value = APIRequestResponseHandler.loading(null)

        val headerMap = HashMap<String,String>()
        headerMap["Content-Type"] = "application/json;charset=UTF-8"
        headerMap["authorization"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["token"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["userType"] = USER_TYPE.toString()

        val posts: Call<UnAssignedAcceptOutputModel> = apiInterface.callUnAssignedAcceptBookingsMethod("$BASE_ENGINE_URL$UNASSIGNED_ACCEPT_BOOKING$bookingId",headerMap,input)!!
        //Enqueue the call
        posts.enqueue(object : Callback<UnAssignedAcceptOutputModel>
        {
            @SuppressLint("TimberArgCount")
            override fun onFailure(call: Call<UnAssignedAcceptOutputModel>, t: Throwable) {

                try {
                    unAssignedBookingAcceptApiResponse.value = APIRequestResponseHandler.error(t.message.toString(),null)
                    Timber.e(t.message.toString())
                } catch (e: Exception) {
                }
            }

            @SuppressLint("TimberArgCount")
            override fun onResponse(call: Call<UnAssignedAcceptOutputModel>, response: Response<UnAssignedAcceptOutputModel>
            ) {
                try {
                    if(response.isSuccessful)
                    {
                        unAssignedBookingAcceptApiResponse.value = APIRequestResponseHandler.success(response.body()!!)
                        Timber.e(response.body().toString())
                    }
                    else
                    {
                        val myOrdersViewModel = UnAssignedAcceptOutputModel()
                        if(response.code() == 403)
                        {
                            myOrdersViewModel.code = 403
                            myOrdersViewModel.data = null
                            myOrdersViewModel.error = true
                            myOrdersViewModel.message = "UnAuthorised User"
                            myOrdersViewModel.success = false
                        }
                        else
                        {
                            val jsonObject = JSONObject(response.errorBody()?.string()!!)
                            myOrdersViewModel.data = jsonObject["data"].toString()
                            myOrdersViewModel.error = jsonObject["error"] as Boolean
                            myOrdersViewModel.success = jsonObject["success"] as Boolean
                            myOrdersViewModel.message = jsonObject["message"].toString()
                            myOrdersViewModel.code = response.code()
                        }
                        unAssignedBookingAcceptApiResponse.value = APIRequestResponseHandler.error("Status code "+response.code(),myOrdersViewModel)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()

                    val myOrdersViewModel = UnAssignedAcceptOutputModel()
                        myOrdersViewModel.code = 500
                        myOrdersViewModel.data = null
                        myOrdersViewModel.error = true
                        myOrdersViewModel.message = "Something goes wrong!!"
                        myOrdersViewModel.success = false
                    unAssignedBookingAcceptApiResponse.value = APIRequestResponseHandler.error("Status code "+response.code(),myOrdersViewModel)
                }
            }
        })
    }
}
