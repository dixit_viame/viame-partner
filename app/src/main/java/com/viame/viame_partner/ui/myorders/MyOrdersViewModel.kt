package com.viame.viame_partner.ui.myorders

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.viame.viame_partner.ASSIGNED_BOOKING_API
import com.viame.viame_partner.BASE_ENGINE_URL
import com.viame.viame_partner.USER_TYPE
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.myorders.model.MyOrdersOutputResponseModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class MyOrdersViewModel @Inject constructor() : ViewModel() {

    private val assignedTripApiResponse = MutableLiveData<APIRequestResponseHandler<MyOrdersOutputResponseModel>>()

    fun getMyOrderAPIDataResult(): MutableLiveData<APIRequestResponseHandler<MyOrdersOutputResponseModel>> = assignedTripApiResponse

    fun AssignedTripActionCall(apiInterface : ApiInterface,page: Int)
    {
        assignedTripApiResponse.value = APIRequestResponseHandler.loading(null)

        val headerMap = HashMap<String,String>()
        headerMap["Content-Type"] = "application/json;charset=UTF-8"
        headerMap["authorization"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["token"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["userType"] = USER_TYPE.toString()

        val posts: Call<MyOrdersOutputResponseModel> = apiInterface.callAssignedBookingsMethod("${BASE_ENGINE_URL}${ASSIGNED_BOOKING_API}?page="+page,headerMap)!!
        //Enqueue the call
        posts.enqueue(object : Callback<MyOrdersOutputResponseModel>
        {
            @SuppressLint("TimberArgCount")
            override fun onFailure(call: Call<MyOrdersOutputResponseModel>, t: Throwable) {
                try {
                    assignedTripApiResponse.value = APIRequestResponseHandler.error(t.message.toString(),null)
                    Timber.e(t.message.toString())
                } catch (e: Exception) {
                }
            }

            @SuppressLint("TimberArgCount")
            override fun onResponse(call: Call<MyOrdersOutputResponseModel>, response: Response<MyOrdersOutputResponseModel>)
            {
                try {
                    if(response.isSuccessful) {
                        assignedTripApiResponse.value =
                            APIRequestResponseHandler.success(response.body()!!)
                        Timber.e(response.body().toString())
                    } else
                    {
                        var myOrdersViewModel = MyOrdersOutputResponseModel()
                        if(response.code() == 403)
                        {
                            myOrdersViewModel.code = 403
                            myOrdersViewModel.data = null
                            myOrdersViewModel.error = true
                            myOrdersViewModel.message = "UnAuthorised User"
                            myOrdersViewModel.success = false
                        }
                        assignedTripApiResponse.value = APIRequestResponseHandler.error("Status code "+response.code(),myOrdersViewModel)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
}
