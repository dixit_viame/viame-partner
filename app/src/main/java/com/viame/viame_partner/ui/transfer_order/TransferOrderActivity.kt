package com.viame.viame_partner.ui.transfer_order

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.viame.viame_partner.BOOKING_DATA
import com.viame.viame_partner.DriverStatusType
import com.viame.viame_partner.R
import com.viame.viame_partner.Vpn
import com.viame.viame_partner.databinding.ActivityTransferOrderBinding
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.base.BaseActivity
import com.viame.viame_partner.ui.main.MainActivity
import com.viame.viame_partner.ui.myorders.model.Appointment
import com.viame.viame_partner.ui.transfer_order.model.TransferBookings
import com.viame.viame_partner.ui.transfer_order.model.TransferOrderInputModel
import com.viame.viame_partner.utils.Alerts
import com.viame.viame_partner.utils.ConnectivityReceiver
import com.viame.viame_partner.utils.Utils
import org.json.JSONObject
import javax.inject.Inject

class TransferOrderActivity : BaseActivity<TransferOrderActivityModel>() {

    private lateinit var binding: ActivityTransferOrderBinding
    private lateinit var currentAppointmentData: Appointment
    private var selectedDriverId : String = ""

    @Inject
    lateinit var apiInterface: ApiInterface

    @Inject
    lateinit var connectivityReceiver: ConnectivityReceiver

    @Inject
    lateinit var model : TransferOrderActivityModel

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this@TransferOrderActivity, R.layout.activity_transfer_order)
        setSupportActionBar(binding.toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if(intent.getSerializableExtra(BOOKING_DATA) != null)
            currentAppointmentData = intent.getSerializableExtra(BOOKING_DATA) as Appointment

        binding.toolbar.setTitle(getString(R.string.title_activity_transfer_order))
        setTitle(getString(R.string.title_activity_transfer_order))

        binding.tvBid.text = currentAppointmentData.orderId.toString()

        val mainObject = JSONObject()
        mainObject.put("bookingId",currentAppointmentData.orderId.toString())
        mainObject.put("current_assigned_driver",Preferences.getPreferenceString(PrefEntity.USER_NAME))
        mainObject.put("driver_id",currentAppointmentData.masId)

        val barcodeEncoder = BarcodeEncoder()
        val bitmap = barcodeEncoder.encodeBitmap(mainObject.toString(), BarcodeFormat.QR_CODE, 512, 512)
        binding.imgBookingQrCode.setImageBitmap(bitmap)

        model.GetDriversListAPIDataResult().observe(this@TransferOrderActivity, Observer { result ->

            if (result.data != null) {

                if (result.status == APIRequestResponseHandler.AuthStatus.LOADING) {
                      Alerts.showProgressBar(this)

                } else if (result.status == APIRequestResponseHandler.AuthStatus.ERROR) {
                     Alerts.dismissProgressBar()
                    Alerts.showSnackBar(this, result.message)

                } else if (result.status == APIRequestResponseHandler.AuthStatus.SUCCESS) {
                    Alerts.dismissProgressBar()
                    if (result.data.success) {
                        val adapter = AutoCompleteAdapter(
                            this@TransferOrderActivity,
                            android.R.layout.simple_dropdown_item_1line, android.R.id.text1,result.data.data)

                        binding.actAutoTransferTo.setThreshold(1)
                        binding.actAutoTransferTo.setAdapter(adapter)
                        binding.actAutoTransferTo.setOnItemClickListener { _, view, _, _ ->
                            val msitings = view.tag.toString().split("-")
                            selectedDriverId =  msitings[0]
                            binding.actAutoTransferTo.setText(msitings[1])
                            binding.actAutoTransferTo.setSelection(msitings[1].length)
                            Utils.hideSoftKeyboard(this@TransferOrderActivity)
                        }
                        binding.actAutoTransferTo.setOnTouchListener{ v, event ->
                            when (event?.action) {
                                MotionEvent.ACTION_DOWN ->
                                {
                                    // show all suggestions
                                    if (binding.actAutoTransferTo.text.toString() != "")
                                        adapter.getFilter().filter(null)

                                    binding.actAutoTransferTo.showDropDown()
                                }
                            }
                            v?.onTouchEvent(event) ?: true
                        }
                    }
                    else
                    {
                        Alerts.showSnackBar(this, getString(R.string.something_goes_wrong))
                    }
                }
            }
        })



        connectivityReceiver.observe(this@TransferOrderActivity, Observer { connectionModel ->

            if (connectionModel != null && connectionModel.isConnected())
            {
                apiInterface.let {

                    model.GetDriversListAPICall(apiInterface)
                }
            }
            else
            {
                if(connectionModel?.type == Vpn)
                {
                    Alerts.showSnackBar(this,getString(R.string.vpn_use_message))
                }
                else
                {
                    Alerts.showSnackBar(this,getString(R.string.internet_not_available))
                }
            }
        })


        model.TransferOrderAPIDataResult().observe(this@TransferOrderActivity, Observer { result ->

            if (result != null) {

                if (result.status == APIRequestResponseHandler.AuthStatus.LOADING) {
                    Alerts.showProgressBar(this)

                } else if (result.status == APIRequestResponseHandler.AuthStatus.ERROR) {
                    Alerts.dismissProgressBar()
                    Alerts.showSnackBar(this, result.message)

                } else if (result.status == APIRequestResponseHandler.AuthStatus.SUCCESS) {
                    Alerts.dismissProgressBar()

                    if (result.data.success) {

                        Alerts.showBottomSheetSimpleConfirmationDialog(this@TransferOrderActivity,
                            getString(R.string.alert),
                            "Your booking is transferred to "+binding.actAutoTransferTo.text.toString(), true, null,
                            getString(R.string.ok), object : Alerts.OnConfirmationDialog {
                                @SuppressLint("CheckResult")
                                override fun onYes() {

                                    val intent = Intent(this@TransferOrderActivity, MainActivity::class.java)
                                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                    startActivity(intent)
                                    this@TransferOrderActivity.finish()
                                }
                                override fun onNo() {

                                }
                            })
                    }
                    else
                    {
                        Alerts.showSnackBar(this@TransferOrderActivity, result.data.data)
                    }
                }
            }
        })


        binding.btnTransferBooking.setOnClickListener {

            if(Preferences.getPreferenceBoolean(PrefEntity.IS_LOGIN) &&
                Preferences.getPreferenceInt(PrefEntity.DRIVER_PRESENCE_STATUS) == DriverStatusType.ONLINE.id) {

                if (selectedDriverId.isNotEmpty()) {
                    if (Utils.isNetworkAvailable(this)) {
                        val transferBookings = TransferBookings()

                        transferBookings.driverId = selectedDriverId
                        transferBookings.orderId = currentAppointmentData.orderId

                        val transferOrderInputModel = TransferOrderInputModel()
                        transferOrderInputModel.transferBookings = transferBookings

                        model.TransferOrderAPICall(apiInterface, transferOrderInputModel)

                    } else {
                        Alerts.showSnackBar(this, getString(R.string.internet_not_available))
                    }
                } else {
                    Alerts.showSnackBar(this, getString(R.string.select_driver_for_transfer))
                }
            }
            else {
                Alerts.showSnackBar(this, getString(R.string.online_transfer_message))
            }
        }


    }


    override fun getViewModel(): TransferOrderActivityModel {
        return model
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> this@TransferOrderActivity.finish()
        }
        return true
    }
}
