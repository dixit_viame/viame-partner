package com.viame.viame_partner.ui.myorders

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.squareup.picasso.Picasso
import com.viame.viame_partner.*
import com.viame.viame_partner.databinding.FragmentMyOrdersBinding
import com.viame.viame_partner.databinding.ItemLoadMoreBinding
import com.viame.viame_partner.databinding.RowItemMyordersBinding
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.base.BaseFragment
import com.viame.viame_partner.ui.bookingcomplete.BookingCompletedActivity
import com.viame.viame_partner.ui.bookingdetail.BookingDetailActivity
import com.viame.viame_partner.ui.home.HomeFragment
import com.viame.viame_partner.ui.main.MainActivity
import com.viame.viame_partner.ui.myorders.model.Appointment
import com.viame.viame_partner.ui.splash.SplashScreenActivity
import com.viame.viame_partner.utils.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONArray
import org.json.JSONObject
import timber.log.Timber
import javax.inject.Inject


class MyOrdersFragment : BaseFragment<MyOrdersViewModel>() , MyOrderListEventHandler, OnRefreshListener {

    companion object {
        fun newInstance() = MyOrdersFragment()
    }

    lateinit var mAdapter: CommonAdapter<Appointment>

    lateinit var binding : FragmentMyOrdersBinding

    @Inject
    lateinit var model : MyOrdersViewModel

    override fun getViewModel(): MyOrdersViewModel {
        return model
    }
    @Inject
    lateinit var eventBus: EventBus

    @Inject
    lateinit var apiInterface: ApiInterface

    @Inject
    lateinit var connectivityReceiver: ConnectivityReceiver

    private var page = 1
    private var isNextPage = false
    private var isLoadMore = false
    private var isRefresh = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

    }

    override fun onStart() {
        super.onStart()
        eventBus.register(this)
        (activity as MainActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        (activity as MainActivity).supportActionBar!!.setDisplayShowHomeEnabled(true)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_orders,container,false)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        model = ViewModelProviders.of(this).get(MyOrdersViewModel::class.java)

        model.getMyOrderAPIDataResult().observe(this@MyOrdersFragment, Observer { result ->

            if (result != null) {

                if (result.status == APIRequestResponseHandler.AuthStatus.LOADING) {
                    if(!isRefresh && !isLoadMore)
                    binding.progressBar.visibility = View.VISIBLE

                } else if (result.status == APIRequestResponseHandler.AuthStatus.ERROR) {

                    binding.progressBar.visibility = View.INVISIBLE

                    if(result.data != null && result.data.code == 403 && result.data.error)
                    {
                        Preferences.removeAllPreference()
                        val intent = Intent(activity, SplashScreenActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        activity?.startActivity(intent)
                        (activity)?.finish()
                    }
                    else
                    {
                        Alerts.showSnackBar(activity, result.message)
                    }

                } else if (result.status == APIRequestResponseHandler.AuthStatus.SUCCESS) {

                    binding.progressBar.visibility = View.INVISIBLE

                    if (result.data.error) {
                        Alerts.showSnackBar(activity, result.message)
                    }
                    else
                    {
                        if(result.data.data.data.size > 0)
                        {
                            if (result.data.data.nextPageUrl != null) {
                                isNextPage = true
                                page += 1
                            } else {
                                isNextPage = false
                            }

                            val arrData: ArrayList<Appointment> = result.data.data.data as ArrayList<Appointment>

                            if (isLoadMore) {
                                mAdapter.stopLoadMore(arrData)
                            } else {
                                if (isRefresh) {
                                    if (arrData.size > 0) {
                                        binding.tvLoadingTxt.visibility = View.INVISIBLE
                                        binding.myorderRView.visibility = View.VISIBLE
                                        setUpAssignedBooking(arrData)
                                    }
                                    onItemsLoadComplete()
                                } else {
                                    if (arrData.size > 0) {
                                        binding.myorderRView.setVisibility(View.VISIBLE)
                                        setUpAssignedBooking(arrData)
                                    } else {
                                        if (isRefresh) {
                                            onItemsLoadComplete()
                                        }
                                        binding.myorderRView.setVisibility(View.GONE)
                                        binding.tvLoadingTxt.setVisibility(View.VISIBLE)
                                    }
                                }
                            }
                            mAdapter.isMoreDataAvailable = isNextPage

                            val pubString = ArrayList<String>()
                            val bookingDetails = JSONArray()
                            for (appData in  result.data.data.data)
                            {
                                pubString.add("${appData.orderId}|${appData.passangerChn}|${appData.status}")

                                val bookingObj = JSONObject()
                                bookingObj.put("bid",appData.orderId)
                                bookingObj.put("distance",0.0)
                                bookingObj.put("custChn",appData.passangerChn)
                                bookingObj.put("status",appData.status)
                                bookingObj.put("time_paused",0)
                                bookingObj.put("time_elapsed",0)

                                bookingDetails.put(bookingObj)
                            }

                            Preferences.setPreference(PrefEntity.ALL_BOOKING_INFO ,pubString.joinToString())
                            Preferences.setPreference(PrefEntity.BOOKING_DETAILS  ,bookingDetails.toString())
                        }
                        else
                        {
                            binding.tvLoadingTxt.text = getString(R.string.dont_have_records)
                            binding.tvLoadingTxt.visibility = View.VISIBLE
                            binding.myorderRView.visibility = View.GONE
                        }
                    }
                }
            }
        })

        connectivityReceiver.observe(this@MyOrdersFragment, Observer { connectionModel ->

            if (connectionModel != null && connectionModel.isConnected())
            {
                apiInterface.let { model.AssignedTripActionCall(apiInterface,page) }
            }
            else
            {
                if(connectionModel?.type == Vpn)
                {
                    Alerts.showSnackBar(activity, getString(R.string.vpn_use_message))
                }
                else
                {
                    Alerts.showSnackBar(activity, getString(R.string.internet_not_available))
                }
            }
        })

        binding.swipeRefreshLayout.setOnRefreshListener(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onBookingUpdate(result: String) {

        onRefresh()
    }

    private fun onItemsLoadComplete() { // Update the adapter and notify data set changed
        isRefresh = false
        // Stop refresh animation
        binding.swipeRefreshLayout.setRefreshing(false)
    }

    override fun onStop() {
        super.onStop()
        eventBus.unregister(this)
    }

    override fun onRefresh() {
        try {
            page = 1
            isLoadMore = false
            isRefresh = true
            try {
                mAdapter.data.clear()
            } catch (e: Exception) {
            }
            if(Utils.isNetworkAvailable(activity)) {
                apiInterface.let { model.AssignedTripActionCall(apiInterface, page) }

                Handler().postDelayed({
                    if(binding.swipeRefreshLayout.isRefreshing()) {
                        binding.swipeRefreshLayout.setRefreshing(false)
                    }
                }, 1500)
            }
            else
            {
                Alerts.showSnackBar(activity,getString(R.string.internet_not_available))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setUpAssignedBooking(arrData: ArrayList<Appointment>) {
        mAdapter = object : CommonAdapter<Appointment>(arrData) {

            @SuppressLint("SetTextI18n")
            override fun onBind(holder: CommonViewHolder, position: Int)
            {
                try {
                    if (holder.binding is RowItemMyordersBinding)
                    {
                        try {
                            val binding: RowItemMyordersBinding = holder.binding as RowItemMyordersBinding
                            binding.viewModel = arrData[position]
                            binding.tvBid.text = arrData[position].orderId.toString()
                            binding.tvClientName.text = arrData[position].slaveName.toString()
                            binding.tvDate.text = Utils.dateFormatter(arrData[position].bookingTime)

                            binding.tvBookingPrice.text = "${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} ${arrData[position].invoice.total}"
                            binding.tvBookingFrom.text = arrData[position].bookingOrigin

                            try {
                                if(arrData[position].statusText != null && arrData[position].statusText.isNotEmpty())
                                {   binding.tvStatus.visibility = View.VISIBLE
                                    binding.tvStatusLBL.visibility = View.VISIBLE
                                    binding.tvStatus.text = arrData[position].statusText }
                                else{
                                    binding.tvStatus.visibility = View.GONE
                                    binding.tvStatusLBL.visibility = View.GONE
                                }
                            } catch (e: Exception) {
                            }

                            if(arrData[position].apptType == "1") {
                                binding.tvBookingType.text = context?.getString(R.string.instant_booking)
                                binding.tvPickupTime.text = Utils.dateFormatter(arrData[position].bookingTime)
                                binding.tvDropoffTime.text = Utils.dateFormatter(arrData[position].bookingTime)
                            }else
                            {
                                binding.tvBookingType.text = context?.getString(R.string.book_later_text)
                                binding.tvPickupTime.text =
                                    Utils.getDateFormatFromMiliseconds(arrData[position].bookingLaterDateTimestamp,"MMM dd, hh:mm a")
                            }
                            binding.handlers = this@MyOrdersFragment
                            binding.indexView = holder.binding.root

                            holder.binding.executePendingBindings()

                            Picasso.get()
                                .load(arrData[position].vehicleImage.toString())
                                .into(binding.vehicleImage)
                        } catch (e: Exception) {
                        }
                    } else {
                        val binding: ItemLoadMoreBinding = holder.binding as ItemLoadMoreBinding
                        binding.progressBar.visibility = View.VISIBLE
                    }
                } catch (e: Exception) {
                }
            }

            override fun getItemViewType(position: Int): Int {
                return if (getItem(position) == null) R.layout.item_load_more else R.layout.row_item_myorders
            }
        }

        binding.myorderRView.layoutManager =
            LinearLayoutManagerWrapper(activity, RecyclerView.VERTICAL, false)
        binding.tvLoadingTxt.visibility = View.INVISIBLE
        binding.myorderRView.adapter = mAdapter
        binding.myorderRView.visibility = View.VISIBLE

        //setting up our OnScrollListener
        binding.myorderRView.addOnScrollListener(object : RecyclerViewHideScrollListener() {
            override fun onHide() {
                hideShowActivityToolBar(false)
            }

            override fun onShow() {
                hideShowActivityToolBar(true)
            }
        })

        mAdapter.setLoadMoreListener {
            binding.myorderRView.post {
                mAdapter.startLoadMore()
                Handler().postDelayed({
                    isLoadMore = true

                    connectivityReceiver.observe(this@MyOrdersFragment, Observer { connectionModel ->

                        if (connectionModel != null && connectionModel.isConnected()) {
                            apiInterface.let { model.AssignedTripActionCall(apiInterface,page) }
                        } else {
                            if(connectionModel?.type == Vpn) {
                                Alerts.showSnackBar(activity,getString(R.string.vpn_use_message))
                            } else {
                                Alerts.showSnackBar(activity,getString(R.string.internet_not_available))
                            }
                        }
                    })
                }, 1500)
            }
        }
    }


    override fun onitemClick(view: View,appointment: Appointment)
    {

        // If status code is 16 theat means go for booking complete screen.
        if(appointment.status == MBStatusType.UNLOADED_AND_DROP.id)
        {

            startActivity(Intent(activity, BookingCompletedActivity::class.java).apply {
                putExtra(BookingCompletedActivity.APPDATA,appointment)
            })
        }
        else
        {

        val options: ActivityOptionsCompat = ActivityOptionsCompat.makeBasic()
        val location = IntArray(2)
        view.getLocationInWindow(location)
        val revealX: Int = location[0] + view.getWidth() / 2
        val revealY: Int = location[1] + view.getHeight() / 2

        val intent = Intent(context, BookingDetailActivity::class.java)
        intent.putExtra(EXTRA_CIRCULAR_REVEAL_X, revealX)
        intent.putExtra(EXTRA_CIRCULAR_REVEAL_Y, revealY)
        intent.putExtra(BOOKING_DATA, appointment)

        ActivityCompat.startActivityForResult(this.activity!!, intent, 1002 ,options.toBundle())

        }
    }

    fun hideShowActivityToolBar(isShow: Boolean)
    {

            val fragment = (activity as MainActivity).getFragment(HomeFragment::class.java)

            if (isShow) {
//                (activity as MainActivity).binding.toolbar.animate().translationY(0.toFloat())
//                    .setInterpolator(DecelerateInterpolator()).start()

                fragment?.binding?.fab?.animate()?.translationY(0F)
                    ?.setInterpolator(DecelerateInterpolator(2F))?.start()

            } else {
//                (activity as MainActivity).binding.toolbar.animate()
//                    .translationY(-(activity as MainActivity).binding.toolbar.getBottom().toFloat())
//                    .setInterpolator(AccelerateInterpolator()).start()
                val lp: ConstraintLayout.LayoutParams = fragment?.binding?.fab?.getLayoutParams() as ConstraintLayout.LayoutParams
                val fabBottomMargin = lp.bottomMargin
                fragment.binding.fab.animate()?.translationY(
                    (fragment.binding.fab.getHeight().plus(
                        fabBottomMargin)).toFloat())
                    ?.setInterpolator(AccelerateInterpolator(2F))
                    ?.start()
            }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 1002 && resultCode == AppCompatActivity.RESULT_OK)
        {
            onRefresh()
        }
    }


}

@Suppress("UNCHECKED_CAST")
fun <F : Fragment> AppCompatActivity.getFragment(fragmentClass: Class<F>): F? {
    val navHostFragment = this.supportFragmentManager.fragments.first() as NavHostFragment

    navHostFragment.childFragmentManager.fragments.forEach {
        if (fragmentClass.isAssignableFrom(it.javaClass)) {
            return it as F
        }
    }

    return null
}