package com.viame.viame_partner.ui.profile;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ProfileFragmentProvider {

    @ContributesAndroidInjector
    abstract ProfileFragment contributeProfileFragment();
}
