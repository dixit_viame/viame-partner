package com.viame.viame_partner.ui.bookingdetail.details;

import dagger.Module;
import dagger.Provides;

@Module
public class DetailPagerFragmentModule {

    @Provides
    DetailPagerViewModel detailPagerViewModel() {
        return new DetailPagerViewModel();
    }


}
