package com.viame.viame_partner.ui.signin

interface SignInEventHandler {
    /**
     * Will be called when signIn button gets clicked
     */
    fun onSignInClicked()

    /*
     * Will be called when Password button Visible/Invisiable Clicked
     */

    fun onPasswordVisibleClicked()



    fun onForgotPassClicked()
}