package com.viame.viame_partner.ui.myorders.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

import androidx.databinding.BindingAdapter;
import androidx.lifecycle.MutableLiveData;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.viame.viame_partner.ui.unassigned.model.TimelineActivityModel;
import com.viame.viame_partner.ui.unassigned.model.UnAssignedInvoice;
import com.viame.viame_partner.ui.unassigned.model.UnAssignedLocation;
import com.viame.viame_partner.utils.Utils;

import org.json.JSONArray;

import okhttp3.internal.Util;

public class Appointment implements Serializable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("booking_id")
    @Expose
    private String bookingId;
    @SerializedName("order_id")
    @Expose
    public long orderId;
    @SerializedName("mas_id")
    @Expose
    private String masId;
    @SerializedName("slave_id")
    @Expose
    private String slaveId;
    @SerializedName("slavePushTopic")
    @Expose
    private String slavePushTopic;
    @SerializedName("zoneType")
    @Expose
    private String zoneType;
    @SerializedName("slaveEmail")
    @Expose
    private String slaveEmail;
    @SerializedName("slavemobile")
    @Expose
    private String slavemobile;
    @SerializedName("slaveCountryCode")
    @Expose
    private String slaveCountryCode;
    @SerializedName("slaveName")
    @Expose
    private String slaveName;
    @SerializedName("created_dt")
    @Expose
    private String createdDt;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("statusText")
    @Expose
    private String statusText;
    @SerializedName("appt_type")
    @Expose
    private String apptType;
    @SerializedName("apptTypeText")
    @Expose
    private String apptTypeText;
    @SerializedName("serverTime")
    @Expose
    private long serverTime;
    @SerializedName("booking_time")
    @Expose
    private String bookingTime;
    @SerializedName("timpeStamp_booking_time")
    @Expose
    private long timpeStampBookingTime;

    @SerializedName("booking_date_object")
    @Expose
    private long bookingLaterDateTimestamp;

    public long getBookingLaterDateTimestamp() {
        return bookingLaterDateTimestamp;
    }

    public void setBookingLaterDateTimestamp(long bookingLaterDateTimestamp) {
        this.bookingLaterDateTimestamp = bookingLaterDateTimestamp;
    }

    @SerializedName("address_line1")
    @Expose
    private String addressLine1;

    @SerializedName("collect_cash_value")
    @Expose
    private String collectCashValue;

    public String getCollectCashValue() {
        return collectCashValue;
    }

    public void setCollectCashValue(String collectCashValue) {
        this.collectCashValue = collectCashValue;
    }

    @SerializedName("drop_location")
    @Expose
    private UnAssignedLocation dropLocation;

    @SerializedName("pickup_location")
    @Expose
    private UnAssignedLocation pickupLocation;

    @SerializedName("drop_addr1")
    @Expose
    private String dropAddr1;

    @SerializedName("extra_notes")
    @Expose
    private String extraNotes;

    @SerializedName("pickupzoneId")
    @Expose
    private String pickupzoneId;

    @SerializedName("pickupZoneName")
    @Expose
    private String pickupZoneName;

    @SerializedName("dorpzoneId")
    @Expose
    private String dorpzoneId;

    @SerializedName("amount")
    @Expose
    private String amount;

    @SerializedName("zipcode")
    @Expose
    private String zipcode;

    @SerializedName("user_device")
    @Expose
    private String userDevice;

    @SerializedName("payment_type")
    @Expose
    private String paymentType;

    @SerializedName("paymentTypeText")
    @Expose
    private String paymentTypeText;

    @SerializedName("additional_info")
    @Expose
    private String additionalInfo;

    @SerializedName("passanger_chn")
    @Expose
    private String passangerChn;

    @SerializedName("vehicleTypeText")
    @Expose
    private String vehicleTypeText;

    @SerializedName("vehicle_image")
    @Expose
    private String vehicleImage;

    @SerializedName("invoice")
    @Expose
    private UnAssignedInvoice invoice;

    @SerializedName("helpers")
    @Expose
    private Integer helpers;

    @SerializedName("pricingType")
    @Expose
    private Integer pricingType;

    @SerializedName("timeZone")
    @Expose
    private String timeZone;

    @SerializedName("product_value")
    @Expose
    private String productValue;

    @SerializedName("booking_origin")
    @Expose
    private String bookingOrigin;

    @SerializedName("receiver_name")
    @Expose
    private String receiverName;

    @SerializedName("receiver_mobile")
    @Expose
    private String receiverMobile;

    @SerializedName("goodType")
    @Expose
    private String goodType;

    @SerializedName("photo")
    @Expose
    private List<Object> photo;

    public String getGoodType() {
        return goodType;
    }

    public void setGoodType(String goodType) {
        this.goodType = goodType;
    }

    public String getVehicleImage() {
        return vehicleImage;
    }

    public void setVehicleImage(String vehicleImage) {
        this.vehicleImage = vehicleImage;
    }

    public List<Object> getPhoto() {
        return photo;
    }

    public void setPhoto(List<Object> photo) {
        this.photo = photo;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverMobile() {
        return receiverMobile;
    }

    public void setReceiverMobile(String receiverMobile) {
        this.receiverMobile = receiverMobile;
    }

    @SerializedName("paidByReceiver")
    @Expose
    private Boolean paidByReceiver;
    @SerializedName("activities")
    @Expose
    private List<TimelineActivityModel> activities = null;

    public Appointment() {
    }

    public Integer getPricingType() {
        return pricingType;
    }

    public void setPricingType(Integer pricingType) {
        this.pricingType = pricingType;
    }

    @BindingAdapter("specialTag")
    public static void setSpecialTag(View view, Object value){
        view.setTag(value);
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public String getMasId() {
        return masId;
    }

    public void setMasId(String masId) {
        this.masId = masId;
    }

    public String getSlaveId() {
        return slaveId;
    }

    public void setSlaveId(String slaveId) {
        this.slaveId = slaveId;
    }

    public String getSlavePushTopic() {
        return slavePushTopic;
    }

    public void setSlavePushTopic(String slavePushTopic) {
        this.slavePushTopic = slavePushTopic;
    }

    public String getZoneType() {
        return zoneType;
    }

    public void setZoneType(String zoneType) {
        this.zoneType = zoneType;
    }

    public String getSlaveEmail() {
        return slaveEmail;
    }

    public void setSlaveEmail(String slaveEmail) {
        this.slaveEmail = slaveEmail;
    }

    public String getSlavemobile() {
        return slavemobile;
    }

    public void setSlavemobile(String slavemobile) {
        this.slavemobile = slavemobile;
    }

    public String getSlaveCountryCode() {
        return slaveCountryCode;
    }

    public void setSlaveCountryCode(String slaveCountryCode) {
        this.slaveCountryCode = slaveCountryCode;
    }

    public String getSlaveName() {
        return slaveName;
    }

    public void setSlaveName(String slaveName) {
        this.slaveName = slaveName;
    }

    public String getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(String createdDt) {
        this.createdDt = createdDt;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public String getApptType() {
        return apptType;
    }

    public void setApptType(String apptType) {
        this.apptType = apptType;
    }

    public String getApptTypeText() {
        return apptTypeText;
    }

    public void setApptTypeText(String apptTypeText) {
        this.apptTypeText = apptTypeText;
    }

    public long getServerTime() {
        return serverTime;
    }

    public void setServerTime(long serverTime) {
        this.serverTime = serverTime;
    }

    public String getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(String bookingTime) {
        this.bookingTime = bookingTime;
    }

    public long getTimpeStampBookingTime() {
        return timpeStampBookingTime;
    }

    public void setTimpeStampBookingTime(long timpeStampBookingTime) {
        this.timpeStampBookingTime = timpeStampBookingTime;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public UnAssignedLocation getDropLocation() {
        return dropLocation;
    }

    public void setDropLocation(UnAssignedLocation dropLocation) {
        this.dropLocation = dropLocation;
    }

    public UnAssignedLocation getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(UnAssignedLocation pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public String getDropAddr1() {
        return dropAddr1;
    }

    public void setDropAddr1(String dropAddr1) {
        this.dropAddr1 = dropAddr1;
    }

    public String getExtraNotes() {
        return extraNotes;
    }

    public void setExtraNotes(String extraNotes) {
        this.extraNotes = extraNotes;
    }

    public String getPickupzoneId() {
        return pickupzoneId;
    }

    public void setPickupzoneId(String pickupzoneId) {
        this.pickupzoneId = pickupzoneId;
    }

    public String getPickupZoneName() {
        return pickupZoneName;
    }

    public void setPickupZoneName(String pickupZoneName) {
        this.pickupZoneName = pickupZoneName;
    }

    public String getDorpzoneId() {
        return dorpzoneId;
    }

    public void setDorpzoneId(String dorpzoneId) {
        this.dorpzoneId = dorpzoneId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getUserDevice() {
        return userDevice;
    }

    public void setUserDevice(String userDevice) {
        this.userDevice = userDevice;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentTypeText() {
        return paymentTypeText;
    }

    public void setPaymentTypeText(String paymentTypeText) {
        this.paymentTypeText = paymentTypeText;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getPassangerChn() {
        return passangerChn;
    }

    public void setPassangerChn(String passangerChn) {
        this.passangerChn = passangerChn;
    }

    public String getVehicleTypeText() {
        return vehicleTypeText;
    }

    public void setVehicleTypeText(String vehicleTypeText) {
        this.vehicleTypeText = vehicleTypeText;
    }

    public UnAssignedInvoice getInvoice() {
        return invoice;
    }

    public void setInvoice(UnAssignedInvoice invoice) {
        this.invoice = invoice;
    }

    public Integer getHelpers() {
        return helpers;
    }

    public void setHelpers(Integer helpers) {
        this.helpers = helpers;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getProductValue() {
        return productValue;
    }

    public void setProductValue(String productValue) {
        this.productValue = productValue;
    }

    public String getBookingOrigin() {
        return bookingOrigin;
    }

    public void setBookingOrigin(String bookingOrigin) {
        this.bookingOrigin = bookingOrigin;
    }

    public Boolean getPaidByReceiver() {
        return paidByReceiver;
    }

    public void setPaidByReceiver(Boolean paidByReceiver) {
        this.paidByReceiver = paidByReceiver;
    }

    public List<TimelineActivityModel> getActivities() {
        return activities;
    }

    public void setActivities(List<TimelineActivityModel> activities) {
        this.activities = activities;
    }
}
