package com.viame.viame_partner.ui.splash;
import androidx.lifecycle.ViewModel;

import com.viame.viame_partner.models.MainAppStore;

import javax.inject.Inject;



public class SplashScreenViewModel extends ViewModel {

    private MainAppStore store;

    @Inject
    public SplashScreenViewModel(MainAppStore store) {
        this.store = store;
    }

    public boolean isFirstTimeLaunch(){
        return store.isFirstTimeLaunch();
    }

    public boolean isLoggedIn(){
        return store.isLoggedIn();
    }

}
