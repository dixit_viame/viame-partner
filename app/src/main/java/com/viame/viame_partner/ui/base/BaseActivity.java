package com.viame.viame_partner.ui.base;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.lifecycle.ViewModel;

import com.viame.viame_partner.utils.WindowPreferencesManager;

import dagger.android.support.DaggerAppCompatActivity;

public abstract class BaseActivity<T extends ViewModel> extends DaggerAppCompatActivity {

    public interface OnKeyboardVisibilityListener {
        void onVisibilityChanged(boolean visible);
    }

    public BaseActivity me;

    private T viewModel;

    /**
     * @return view model instance
     */
    public abstract T getViewModel();

    public void startActivity(View viewStart, String transactionName, Intent intent) {
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, viewStart, transactionName);
        ActivityCompat.startActivity(this, intent, options.toBundle());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            WindowPreferencesManager manager = new WindowPreferencesManager(this);
            manager.applyEdgeToEdgePreference(getWindow());
            me = this;
            this.viewModel = viewModel == null ? getViewModel() : viewModel;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
