package com.viame.viame_partner.ui.jobhistory

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.mikephil.charting.data.BarEntry
import com.google.android.material.tabs.TabLayout
import com.viame.viame_partner.R
import com.viame.viame_partner.databinding.FragmentJobhistoryBinding
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.ui.base.BaseFragment
import com.viame.viame_partner.ui.jobhistory.model.JobHistoryAppointment
import com.viame.viame_partner.ui.jobhistory.model.JobHistoryInputDataModel
import com.viame.viame_partner.ui.jobhistory.model.JobHistoryOututDataModel
import com.viame.viame_partner.ui.jobhistorydetail.JobHistoryDetailActivity
import com.viame.viame_partner.utils.Alerts
import com.viame.viame_partner.utils.LinearLayoutManagerWrapper
import com.viame.viame_partner.utils.Utils
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class JobHistoryFragment : BaseFragment<JobHistoryViewModel>(), TabLayout.OnTabSelectedListener,
    JobHistoryEventHandler {

    private lateinit var apiDateFormat: SimpleDateFormat
    private lateinit var tabDateFormat: SimpleDateFormat
    private lateinit var binding: FragmentJobhistoryBinding
    private var apiStartWeek: ArrayList<Date>? = ArrayList()
    private var apiEndWeek: ArrayList<Date>? = ArrayList()
    private var tabcount: Int = 12
    private var tabIncrement :Int  = 5
    private var differenceDays: Int = 0
    private var selectedWeeks: String? = ""
    private var selectedTabPosition : Int? = 0
    private val totalsForBar: ArrayList<Float>? = ArrayList()
    private val barEntries: ArrayList<BarEntry>? = ArrayList()
    private lateinit var mJobHistoryRecycleViewAdapter : JobHistoryRecycleViewAdapter

    @Inject
    lateinit var viewModels: JobHistoryViewModel

    @Inject
    lateinit var apiInterface: ApiInterface

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_jobhistory, container, false)

        tabDateFormat = SimpleDateFormat("MMM dd", Locale.US)
        apiDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)

        initTabLayout(tabcount)

        viewModels.getTripsApiResponseResult().observe(this, androidx.lifecycle.Observer { result ->

            if (result != null) {
                if (result.status == APIRequestResponseHandler.AuthStatus.LOADING) {

                    binding.progressBar.visibility = View.VISIBLE
                } else if (result.status == APIRequestResponseHandler.AuthStatus.ERROR) {

                    binding.progressBar.visibility = View.INVISIBLE
                    Alerts.showSnackBar(activity, result.message)
                } else if (result.status == APIRequestResponseHandler.AuthStatus.SUCCESS) {

                    binding.progressBar.visibility = View.INVISIBLE
                    if (result.data.error) {
                        Alerts.showSnackBar(activity, result.data.message)
                    }
                    else
                    {
                        try {
                            if(result.data.data.getmJobHistoryData().data.size > 0)
                            {
                                mJobHistoryRecycleViewAdapter = JobHistoryRecycleViewAdapter(this.activity!!, ArrayList(result.data.data.getmJobHistoryData().data),this@JobHistoryFragment)

                                handleDate(result.data, this.selectedTabPosition!!, tabcount)
                                binding.rvJobHistory.layoutManager = LinearLayoutManagerWrapper(activity, RecyclerView.VERTICAL, false)
                                binding.tvLoadingTxt.visibility = View.INVISIBLE
                                binding.rvJobHistory.adapter = mJobHistoryRecycleViewAdapter
                                binding.rvJobHistory.visibility = View.VISIBLE
                            }
                            else
                            {
                                binding.tvLoadingTxt.text = getString(R.string.dont_have_records)
                                binding.tvLoadingTxt.visibility = View.VISIBLE
                                binding.rvJobHistory.visibility = View.GONE
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            binding.tvLoadingTxt.text = getString(R.string.dont_have_records)
                            binding.tvLoadingTxt.visibility = View.VISIBLE
                            binding.rvJobHistory.visibility = View.GONE
                        }
                    }
                }
            }

        })
        return binding.root
    }

     // Initial Tab layout Setup
     private fun initTabLayout(selectableTab: Int) {
        val date = Date()
        val c = Calendar.getInstance()
        c.time = date
        binding.tabLayout.removeOnTabSelectedListener(this)
        binding.tabLayout.removeAllTabs()
        apiStartWeek!!.clear()
         apiEndWeek!!.clear()
        for (i in 0..tabcount) {
            binding.tabLayout.addTab(binding.tabLayout.newTab())
        }
        for (i in tabcount downTo 0) {
            var startDate : String
            var endDate : String
            if (i == tabcount) {
                apiEndWeek!!.add(c.time)
                endDate = tabDateFormat.format(c.time)
                c.add(Calendar.DATE, - differenceDays)
                startDate = tabDateFormat.format(c.time)
                apiStartWeek!!.add(c.time)

                c.add(Calendar.DATE, -1)
                if (differenceDays != 0) {
                    binding.tabLayout.getTabAt(i)!!.setText("$startDate-$endDate")
                } else {
                    binding.tabLayout.getTabAt(i)!!.setText(endDate)
                }
            } else {
                apiEndWeek!!.add(c.time)
                endDate = tabDateFormat.format(c.time)
                c.add(Calendar.DATE, -6)
                startDate = tabDateFormat.format(c.time)
                apiStartWeek!!.add(c.time)

                c.add(Calendar.DATE, -1)
                binding.tabLayout.getTabAt(i)!!.setText("$startDate-$endDate")
            }
        }
        Handler().postDelayed(
            {
                binding.tabLayout.getTabAt(selectableTab)?.select()
            }, 100)

        binding.tabLayout.addOnTabSelectedListener(this)
        selectedWeeks = binding.tabLayout.getTabAt(tabcount)?.getText().toString()
    }


    override fun getViewModel(): JobHistoryViewModel {
        return viewModels
    }

    override fun onTabReselected(tab: TabLayout.Tab?) {

    }

    override fun onTabUnselected(tab: TabLayout.Tab?) {

    }

    override fun onTabSelected(tab: TabLayout.Tab?) {
        binding.progressBar.visibility = View.VISIBLE
        binding.tvLoadingTxt.visibility = View.GONE
        selectedTabPosition = tab?.getPosition()

        if (selectedTabPosition == 0) {
            tabcount += tabIncrement
            initTabLayout(tabIncrement)
            return
        }
        selectedWeeks = binding.tabLayout.getTabAt(this.selectedTabPosition!!)!!.getText().toString()

        val position: Int = tabcount - this.selectedTabPosition!!
        val apiSelectedDate : String
        val apiEndDate : String
        if(position == 0) {
             apiSelectedDate = apiDateFormat.format(apiEndWeek!![position+1])
             //apiEndDate = apiDateFormat.format(apiStartWeek!![position])

            val cal = Calendar.getInstance()
            cal.add(Calendar.DAY_OF_MONTH, 1) //Adds a day
            apiEndDate = Utils.getDateFormatFromMiliseconds(cal.timeInMillis,"yyyy-MM-dd")

        }else
        {
            apiSelectedDate = apiDateFormat.format(apiStartWeek!![position])
            apiEndDate = apiDateFormat.format(apiEndWeek!![position])
        }

        getHistoy(apiSelectedDate, apiEndDate, this.selectedTabPosition!!, tabcount)
    }

    /**********************************************************************************************/
    /** */
    /**
     * <h1>getHistoy</h1>
     *
     * the service call for the trip history
     */
    private fun getHistoy(startDate: String?,endDate: String?, selectedTabPosition: Int, tabcount: Int)
    {
        try
        {
            val mJobHistoryInputDataModel = JobHistoryInputDataModel()
            mJobHistoryInputDataModel.entPageIndex = "0"
            mJobHistoryInputDataModel.entStartDate = startDate
            mJobHistoryInputDataModel.entEndDate = endDate
            viewModels.AssignedTripActionCall(mJobHistoryInputDataModel , apiInterface)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun handleDate(tripsPojo: JobHistoryOututDataModel, selectedTabPosition: Int, tabcount: Int)
    {
        var amountEarned = 0.0
        if(tripsPojo.getData().getmJobHistoryData().totalEarning != null && tripsPojo.getData().getmJobHistoryData().totalEarning.size >0) {
            for (i in 0 until tripsPojo.getData().getmJobHistoryData().totalEarning.size) {
                amountEarned += tripsPojo.getData().getmJobHistoryData().totalEarning[i].getAmt()
                    .toDouble()
            }

            for (i in 0 until tripsPojo.getData().getmJobHistoryData().totalEarning.size) {
                val amt: String = tripsPojo.getData().getmJobHistoryData().totalEarning[i].getAmt()
                if (amt.isNotEmpty()) {
                    if (amt != "NaN") {
                        totalsForBar?.add(amt.toFloat())
                    } else {
                        totalsForBar?.add(0.0f)
                    }
                } else {
                    totalsForBar?.add(0.0f)
                }
            }
        barEntries?.clear()
        var highestvalue = 0f
        if (selectedTabPosition == tabcount) {
            for (i in 0..differenceDays) {
                barEntries!!.add(BarEntry(i.toFloat(), totalsForBar!![i]))
                if (totalsForBar[i] > highestvalue) {
                    highestvalue = totalsForBar[i]
                }
            }
        } else {
            for (i in 0..6) {
                barEntries?.add(BarEntry(i.toFloat(), totalsForBar!![i]))
                if (totalsForBar != null && totalsForBar[i] > highestvalue) {
                    highestvalue = totalsForBar[i]
                }
            }
        }
        }
        setValues(ArrayList(tripsPojo.getData().getmJobHistoryData().data))
    }

    private fun setValues(
        appointments: ArrayList<JobHistoryAppointment>) {
        try {
            mJobHistoryRecycleViewAdapter.appointments = appointments
            mJobHistoryRecycleViewAdapter.notifyDataSetChanged()

        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    override fun onitemClick(view: View, appointment: JobHistoryAppointment) {

        startActivity(Intent(activity, JobHistoryDetailActivity::class.java).apply {
            putExtra(JobHistoryDetailActivity.APPDATA,appointment)
        })
    }
}