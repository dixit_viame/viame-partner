package com.viame.viame_partner.ui.jobhistory

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.viame.viame_partner.BASE_ENGINE_URL
import com.viame.viame_partner.HISTORY_BOOKING
import com.viame.viame_partner.USER_TYPE
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.jobhistory.model.JobHistoryInputDataModel
import com.viame.viame_partner.ui.jobhistory.model.JobHistoryOututDataModel
import com.viame.viame_partner.ui.myorders.model.MyOrdersOutputResponseModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.HashMap
import javax.inject.Inject

class JobHistoryViewModel @Inject constructor() : ViewModel() {

    private val TripsApiResponse = MutableLiveData<APIRequestResponseHandler<JobHistoryOututDataModel>>()

    fun getTripsApiResponseResult(): MutableLiveData<APIRequestResponseHandler<JobHistoryOututDataModel>> = TripsApiResponse

    fun AssignedTripActionCall(inputDataModel: JobHistoryInputDataModel,apiInterface : ApiInterface)
    {
        TripsApiResponse.value = APIRequestResponseHandler.loading(null)

        val headerMap = HashMap<String,String>()
        headerMap["Content-Type"] = "application/json;charset=UTF-8"
        headerMap["authorization"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["token"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["userType"] = USER_TYPE.toString()

        val posts: Call<JobHistoryOututDataModel> = apiInterface.callTripsMethod( "${BASE_ENGINE_URL}${HISTORY_BOOKING}?from_date=${inputDataModel.entStartDate}&to_date=${inputDataModel.entEndDate}",
            headerMap)!!
        //Enqueue the call
        posts.enqueue(object : Callback<JobHistoryOututDataModel>
        {
            @SuppressLint("TimberArgCount")
            override fun onFailure(call: Call<JobHistoryOututDataModel>, t: Throwable) {

                TripsApiResponse.value = APIRequestResponseHandler.error(t.message.toString(),null)
                Timber.e(t.message.toString())
            }

            @SuppressLint("TimberArgCount")
            override fun onResponse(call: Call<JobHistoryOututDataModel>, response: Response<JobHistoryOututDataModel>)
            {
                if(response.isSuccessful)
                {
                    TripsApiResponse.value = APIRequestResponseHandler.success(response.body())
                    Timber.e(response.body().toString())
                }
                else
                {
                    TripsApiResponse.value = APIRequestResponseHandler.error("Status code "+response.code(),null)

                }
            }
        })
    }


}