package com.viame.viame_partner.ui.forgotpassword

interface ForgotPasswordEventHandler {
    /**
     * Will be called when submit button gets clicked
     */
    fun onSubmitClicked()

}