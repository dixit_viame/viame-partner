package com.viame.viame_partner.ui.jobhistory.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.viame.viame_partner.ui.myorders.model.Data;
import com.viame.viame_partner.ui.unassigned.model.UnAssignedData;

public class JobHistoryOututDataModel {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private JobHistoryNestedData data;
    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("code")
    @Expose
    private Integer code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JobHistoryNestedData getData() {
        return data;
    }

    public void setData(JobHistoryNestedData data) {
        this.data = data;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

}
