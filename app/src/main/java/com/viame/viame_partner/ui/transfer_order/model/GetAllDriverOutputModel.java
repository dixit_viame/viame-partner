package com.viame.viame_partner.ui.transfer_order.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.viame.viame_partner.ui.myorders.model.Location;

import java.util.List;

public class GetAllDriverOutputModel {
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }


    public class Datum {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("location")
        @Expose
        private Location location;
        @SerializedName("vehicleTypeName")
        @Expose
        private String vehicleTypeName;
        @SerializedName("avgAcceptanceRate")
        @Expose
        private String avgAcceptanceRate;
        @SerializedName("cancelledByCustomer")
        @Expose
        private String cancelledByCustomer;
        @SerializedName("totalAccepted")
        @Expose
        private String totalAccepted;
        @SerializedName("totalBookings")
        @Expose
        private String totalBookings;
        @SerializedName("totalCancelled")
        @Expose
        private String totalCancelled;
        @SerializedName("totalCompleted")
        @Expose
        private String totalCompleted;
        @SerializedName("totalIgnored")
        @Expose
        private String totalIgnored;
        @SerializedName("totalRejected")
        @Expose
        private String totalRejected;
        @SerializedName("Device_type_")
        @Expose
        private String deviceType;
        @SerializedName("battery_Per")
        @Expose
        private String batteryPer;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Location getLocation() {
            return location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }

        public String getVehicleTypeName() {
            return vehicleTypeName;
        }

        public void setVehicleTypeName(String vehicleTypeName) {
            this.vehicleTypeName = vehicleTypeName;
        }

        public String getAvgAcceptanceRate() {
            return avgAcceptanceRate;
        }

        public void setAvgAcceptanceRate(String avgAcceptanceRate) {
            this.avgAcceptanceRate = avgAcceptanceRate;
        }

        public String getCancelledByCustomer() {
            return cancelledByCustomer;
        }

        public void setCancelledByCustomer(String cancelledByCustomer) {
            this.cancelledByCustomer = cancelledByCustomer;
        }

        public String getTotalAccepted() {
            return totalAccepted;
        }

        public void setTotalAccepted(String totalAccepted) {
            this.totalAccepted = totalAccepted;
        }

        public String getTotalBookings() {
            return totalBookings;
        }

        public void setTotalBookings(String totalBookings) {
            this.totalBookings = totalBookings;
        }

        public String getTotalCancelled() {
            return totalCancelled;
        }

        public void setTotalCancelled(String totalCancelled) {
            this.totalCancelled = totalCancelled;
        }

        public String getTotalCompleted() {
            return totalCompleted;
        }

        public void setTotalCompleted(String totalCompleted) {
            this.totalCompleted = totalCompleted;
        }

        public String getTotalIgnored() {
            return totalIgnored;
        }

        public void setTotalIgnored(String totalIgnored) {
            this.totalIgnored = totalIgnored;
        }

        public String getTotalRejected() {
            return totalRejected;
        }

        public void setTotalRejected(String totalRejected) {
            this.totalRejected = totalRejected;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getBatteryPer() {
            return batteryPer;
        }


        public String getFullName() {
            return firstName+" "+lastName;
        }

        public void setBatteryPer(String batteryPer) {
            this.batteryPer = batteryPer;
        }

    }
}