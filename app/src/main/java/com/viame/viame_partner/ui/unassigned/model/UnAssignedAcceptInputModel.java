package com.viame.viame_partner.ui.unassigned.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UnAssignedAcceptInputModel {

    @SerializedName("api_drivers_shipment_details")
    @Expose
    private ApiDriversShipmentDetails apiDriversShipmentDetails;

    public ApiDriversShipmentDetails getApiDriversShipmentDetails() {
        return apiDriversShipmentDetails;
    }

    public void setApiDriversShipmentDetails(ApiDriversShipmentDetails apiDriversShipmentDetails) {
        this.apiDriversShipmentDetails = apiDriversShipmentDetails;
    }

}
