package com.viame.viame_partner.ui.signin

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.gson.Gson
import com.viame.viame_partner.*
import com.viame.viame_partner.databinding.ActivitySigninBinding
import com.viame.viame_partner.firebase.callFirebaseToSubscribeTopic
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.base.BaseActivity
import com.viame.viame_partner.ui.forgotpassword.ForgotPasswordActivity
import com.viame.viame_partner.ui.signin.model.SignInRequestModel
import com.viame.viame_partner.ui.splash.SplashScreenActivity
import com.viame.viame_partner.ui.vehicle.VehicleListActivity
import com.viame.viame_partner.utils.Alerts
import com.viame.viame_partner.utils.ConnectivityReceiver
import com.viame.viame_partner.utils.Utils
import javax.inject.Inject


class SignInActivity : BaseActivity<SignInActivityViewModel>() , SignInEventHandler
{
    lateinit var binding : ActivitySigninBinding

    @Inject
    lateinit var model : SignInActivityViewModel

    @Inject
    lateinit var apiInterface: ApiInterface

    @Inject
    lateinit var connectivityReceiver: ConnectivityReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView<ActivitySigninBinding>(this, R.layout.activity_signin)

        // Setting binding params
        binding.viewModel = model
        binding.clickHandler = this

        binding.tvVersion.setText("Version: ${Utils.getAppVersion(this)} ${BuildConfig.FLAVOR}")

        // Watching for login result
        model.getSignInValidationResult().observe(this, Observer
        {
            result ->
            if(result != "Valid")
            {
               Toast.makeText(this, result, Toast.LENGTH_SHORT).show()
            }
            else
            {
                connectivityReceiver.observe(this, Observer { connectionModel ->
                    if (connectionModel != null && connectionModel.isConnected())
                    {
                        binding.internetLayout.root.visibility = View.GONE

                        if(result == "Valid")
                        {
                            var signInRequestModel  = SignInRequestModel()
                            signInRequestModel.entAppversion = BuildConfig.VERSION_NAME
                            signInRequestModel.entDevMake = android.os.Build.MANUFACTURER
                            signInRequestModel.entDeviceId = Utils.getDeviceId(this@SignInActivity)
                            signInRequestModel.entDeviceTime = Utils.date()
                            signInRequestModel.entDevModel = android.os.Build.MODEL + " "+android.os.Build.PRODUCT
                            signInRequestModel.entDevtype = DEVICE_TYPE
                            signInRequestModel.entPushToken = Preferences.getPreferenceString(PrefEntity.FIREBASE_MESSEGING_REG_ID)
                            signInRequestModel.mobile = model.emailMobile
                            signInRequestModel.password = model.password

                            apiInterface.let { model.signinCall(signInRequestModel, it) }
                        }
                    }
                    else
                    {
                        binding.internetLayout.root.visibility = View.VISIBLE
                        if(connectionModel?.type == Vpn)
                        {
                            binding.internetLayout.tvInternetMsg.text = getString(R.string.vpn_use_message)
                            Toast.makeText(
                                this@SignInActivity,
                                getString(R.string.vpn_use_message),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        else
                        {
                            Toast.makeText(this@SignInActivity, getString(R.string.internet_not_available), Toast.LENGTH_SHORT).show()
                        }
                    }
                })
            }
        })


        model.getSignInApiResult().observe(this, Observer
        {
            result ->

            if(result.status == APIRequestResponseHandler.AuthStatus.LOADING)
            {
                Alerts.showProgressBar(this@SignInActivity)

            }
            else if (result.status == APIRequestResponseHandler.AuthStatus.ERROR)
            {
                Alerts.dismissProgressBar()
                Alerts.showSnackBar(this@SignInActivity,result.message)

            }
            else if (result.status == APIRequestResponseHandler.AuthStatus.SUCCESS)
            {
                Alerts.dismissProgressBar()

                if(result.data?.errFlag == 1)
                {
                    Alerts.showSnackBar(this@SignInActivity,result.data.errMsg)
                }
                else
                {

                    if (result.data?.data?.vehicles?.size!! > 0)
                    {
                        Preferences.setPreference(PrefEntity.EMAIL,result.data.data?.email)
                        Preferences.setPreference(PrefEntity.PUSH_TOPICS,result.data.data?.pushTopic)
                        Preferences.setPreference(PrefEntity.AUTH_TOKEN,result.data.data?.token)
                        Preferences.setPreference(PrefEntity.USER_NAME,result.data.data?.name)
                        Preferences.setPreference(PrefEntity.REFERER_CODE,result.data.data?.code)
                        Preferences.setPreference(PrefEntity.PROFILE_PIC,result.data.data?.profilePic)
                        Preferences.setPreference(PrefEntity.SUBSCRIBE_KEY,result.data.data?.subKey)
                        Preferences.setPreference(PrefEntity.PUBLISH_KEY,result.data.data?.pubKey)
                        Preferences.setPreference(PrefEntity.SERVER_CHANNEL,result.data.data?.serverChn)
                        Preferences.setPreference(PrefEntity.PRESENCE_CHANNEL,result.data.data?.presenceChn)
                        Preferences.setPreference(PrefEntity.VEHICLES, Gson().toJson(result.data.data?.vehicles))
                        Preferences.setPreference(PrefEntity.DRIVER_CHANNEL,result.data.data?.chn)
                        Preferences.setPreferenceInt(PrefEntity.DRIVER_PRESENCE_STATUS,
                            DriverStatusType.OFFLINE.id)
                        Preferences.setPreference(PrefEntity.DRIVER_UUID,"m_" +result.data.data?.mid)
                        Preferences.setPreference(PrefEntity.MID,result.data.data?.mid)
                        Preferences.setPreference(PrefEntity.PASSWORD,binding.tiePassword.text.toString())

                        //Subscribing to topic
                        if (result.data.data?.pushTopic?.length!! > 0)
                        {
                            callFirebaseToSubscribeTopic(result.data.data?.pushTopic!!)
                        }
                        Alerts.showSnackBar(this@SignInActivity,"Login Success")

                        startActivity(Intent(this@SignInActivity, VehicleListActivity::class.java))
                        finish()
                    }
                    else
                    {
                        Alerts.showSnackBar(this@SignInActivity,getString(R.string.no_assigned_vehicle_msg))
                    }
                }
            }
        })
    }

    override fun getViewModel(): SignInActivityViewModel {
        return model;
    }

    override fun onSignInClicked() {
        model.performValidation()
    }

    override fun onPasswordVisibleClicked() {
        model.performClickPasswordVisible(binding.tiePassword)
    }

    override fun onForgotPassClicked() {
        val intent = Intent(this, ForgotPasswordActivity::class.java)
        startActivity(intent)
    }


}