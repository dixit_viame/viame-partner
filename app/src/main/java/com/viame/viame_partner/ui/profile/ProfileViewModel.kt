package com.viame.viame_partner.ui.profile

import android.annotation.SuppressLint
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.viame.viame_partner.BASE_ENGINE_URL
import com.viame.viame_partner.PROFILE_API
import com.viame.viame_partner.R
import com.viame.viame_partner.application.CatalogApplication
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.profile.model.NewProfileDataModel
import com.viame.viame_partner.ui.profile.model.ProfileDataModel
import com.viame.viame_partner.ui.profile.model.UpdateProfileDataModel
import com.viame.viame_partner.utils.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import javax.inject.Inject

class ProfileViewModel @Inject constructor(): ViewModel() {

    /**
     * Two way bind-able fields
     */
    var mobile: String = ""
    var password: String = ""
    var email : String = ""
    var firstname : String = ""
    /**
     * To pass login result to activity
     */
    private val profileValidationResult = MutableLiveData<String>()

    fun getProfileValidationResult(): LiveData<String> = profileValidationResult

    /**
     * To pass login result to activity
     */

    private val profileFetchResponse = MutableLiveData<APIRequestResponseHandler<ProfileDataModel?>>()

    fun getProfileDataFetchResult(): LiveData<APIRequestResponseHandler<ProfileDataModel?>> = profileFetchResponse


    fun profileDetailFetch(apiInterface : ApiInterface)
    {
        profileFetchResponse.setValue(APIRequestResponseHandler.loading(null));

        val map = HashMap<String,String>()
        map.put("Content-Type", "application/json;charset=UTF-8")
        map.put("authorization", Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN))
        map.put("token", Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN))
        map.put("userType", "1")

        val posts: Call<ProfileDataModel>? = apiInterface.callProfileDetailMethod("${BASE_ENGINE_URL}${PROFILE_API}",map)
        //Enqueue the call
        posts!!.enqueue(object : Callback<ProfileDataModel> {
            @SuppressLint("TimberArgCount")
            override fun onFailure(call: Call<ProfileDataModel>, t: Throwable) {

                profileFetchResponse.value = APIRequestResponseHandler.error(t.message.toString(),null)
                Timber.e(t.message.toString())
            }

            @SuppressLint("TimberArgCount")
            override fun onResponse(call: Call<ProfileDataModel>, response: Response<ProfileDataModel>
            ) {
                if(response.isSuccessful) {
                    profileFetchResponse.value =
                        APIRequestResponseHandler.success((response.body()!!))

                    Timber.e(response.body().toString())
                } else
                {
                    profileFetchResponse.value = APIRequestResponseHandler.error("Status code "+response.code(),null)

                }
            }
        })
    }

    /**
     * Called from activity on signIn button click
     */
    fun performValidation() {
        if (firstname.trim().isBlank()) {
            profileValidationResult.value = CatalogApplication.instance?.getString(R.string.enter_first_name)
            return
        }
        else if (mobile.trim().isBlank()) {
            profileValidationResult.value =
                CatalogApplication.instance?.getString(R.string.enter_mobile_number)
            return
        } else if (email.trim().isBlank()) {
            profileValidationResult.value =
                CatalogApplication.instance?.getString(R.string.enter_email_password)
            return
        }
         else if (password.trim().isBlank()) {
            profileValidationResult.value = CatalogApplication.instance?.getString(R.string.enter_password)
            return
        }
        profileValidationResult.value = "Valid"
    }


    private val profileUpdateResponse = MutableLiveData<APIRequestResponseHandler<UpdateProfileDataModel?>>()

    fun getProfileUpdateResult(): LiveData<APIRequestResponseHandler<UpdateProfileDataModel?>> = profileUpdateResponse

    fun profileUpdateDetail(apiInterface : ApiInterface, newProfileDataModel : NewProfileDataModel)
    {
        profileUpdateResponse.setValue(APIRequestResponseHandler.loading(null));

        val map = HashMap<String,String>()
        map.put("Content-Type", "application/json;charset=UTF-8")
        map.put("authorization", Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN))
        map.put("token", Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN))
        map.put("userType", "1")

        val posts: Call<UpdateProfileDataModel>? = apiInterface.callUpdateProfileDetailMethod("${BASE_ENGINE_URL}${PROFILE_API}",map,newProfileDataModel)
        //Enqueue the call
        posts!!.enqueue(object : Callback<UpdateProfileDataModel> {
            @SuppressLint("TimberArgCount")
            override fun onFailure(call: Call<UpdateProfileDataModel>, t: Throwable) {

                profileUpdateResponse.value = APIRequestResponseHandler.error(t.message.toString(),null)
                Timber.e(t.message.toString())
            }

            @SuppressLint("TimberArgCount")
            override fun onResponse(call: Call<UpdateProfileDataModel>, response: Response<UpdateProfileDataModel>
            ) {
                if(response.isSuccessful)
                {
                    profileUpdateResponse.value =
                        APIRequestResponseHandler.success((response.body()!!))
                    Timber.e(response.body().toString())
                }
                else
                {
                    profileUpdateResponse.value = APIRequestResponseHandler.error("Status code "+response.code(),null)
                }
            }
        })
    }

}
