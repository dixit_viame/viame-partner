package com.viame.viame_partner.ui.jobhistory.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.viame.viame_partner.ui.myorders.model.Location;

import java.io.Serializable;
import java.util.List;

public class JobHistoryShipmentDetail implements Serializable {

    @SerializedName("subid")
    @Expose
    private String subid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("AproxDropTime")
    @Expose
    private String aproxDropTime;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("landmark")
    @Expose
    private String landmark;
    @SerializedName("flat_number")
    @Expose
    private String flatNumber;
    @SerializedName("zip_code")
    @Expose
    private String zipCode;
    @SerializedName("zone1")
    @Expose
    private String zone1;
    @SerializedName("zone2")
    @Expose
    private String zone2;
    @SerializedName("goodType")
    @Expose
    private String goodType;
    @SerializedName("loadType")
    @Expose
    private String loadType;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("productname")
    @Expose
    private String productname;
    @SerializedName("photo")
    @Expose
    private List<String> photo = null;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("volume")
    @Expose
    private String volume;
    @SerializedName("length")
    @Expose
    private String length;
    @SerializedName("width")
    @Expose
    private String width;
    @SerializedName("height")
    @Expose
    private String height;
    @SerializedName("dimensionUnit")
    @Expose
    private String dimensionUnit;
    @SerializedName("ApproxDistance")
    @Expose
    private String approxDistance;
    @SerializedName("ApproxFare")
    @Expose
    private String approxFare;
    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("completedTime")
    @Expose
    private String completedTime;
    @SerializedName("signatureUrl")
    @Expose
    private String signatureUrl;
    @SerializedName("driverPhoto")
    @Expose
    private String driverPhoto;
    @SerializedName("startTime")
    @Expose
    private String startTime;
    @SerializedName("Fare")
    @Expose
    private String fare;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("DriverOnTheWayTime")
    @Expose
    private String driverOnTheWayTime;
    @SerializedName("DriverLoadedStartedTime")
    @Expose
    private String driverLoadedStartedTime;
    @SerializedName("DriverArrivedTime")
    @Expose
    private String driverArrivedTime;
    @SerializedName("DriverAcceptedTime")
    @Expose
    private String driverAcceptedTime;
    @SerializedName("DriverCompletedTime")
    @Expose
    private String driverCompletedTime;
    @SerializedName("DriverDropedTime")
    @Expose
    private String driverDropedTime;
    @SerializedName("DriverReachedDropLocationTime")
    @Expose
    private String driverReachedDropLocationTime;
    @SerializedName("DriverTripDistance")
    @Expose
    private String driverTripDistance;
    @SerializedName("DriverAcceptedTimeISO")
    @Expose
    private String driverAcceptedTimeISO;

    @SerializedName("DriverArrivedTimeISO")
    @Expose
    private String driverArrivedTimeISO;

    @SerializedName("DriverPickupDistance")
    @Expose
    private String driverPickupDistance;

    @SerializedName("DriverLoadedStartedTimeISO")
    @Expose
    private String driverLoadedStartedTimeISO;

    @SerializedName("DriverReachedDropLocationTimeISO")
    @Expose
    private String driverReachedDropLocationTimeISO;

    @SerializedName("DriverDropedTimeISO")
    @Expose
    private String driverDropedTimeISO;

    @SerializedName("DriverCompletedTimeISO")
    @Expose
    private String DriverCompletedTimeISO;

    @SerializedName("documentImage")
    @Expose
    private List<String> documentImage;

    public String getDriverArrivedTimeISO() {
        return driverArrivedTimeISO;
    }

    public void setDriverArrivedTimeISO(String driverArrivedTimeISO) {
        this.driverArrivedTimeISO = driverArrivedTimeISO;
    }

    public String getDriverPickupDistance() {
        return driverPickupDistance;
    }

    public void setDriverPickupDistance(String driverPickupDistance) {
        this.driverPickupDistance = driverPickupDistance;
    }

    public String getDriverLoadedStartedTimeISO() {
        return driverLoadedStartedTimeISO;
    }

    public void setDriverLoadedStartedTimeISO(String driverLoadedStartedTimeISO) {
        this.driverLoadedStartedTimeISO = driverLoadedStartedTimeISO;
    }

    public String getDriverReachedDropLocationTimeISO() {
        return driverReachedDropLocationTimeISO;
    }

    public void setDriverReachedDropLocationTimeISO(String driverReachedDropLocationTimeISO) {
        this.driverReachedDropLocationTimeISO = driverReachedDropLocationTimeISO;
    }

    public String getDriverDropedTimeISO() {
        return driverDropedTimeISO;
    }

    public void setDriverDropedTimeISO(String driverDropedTimeISO) {
        this.driverDropedTimeISO = driverDropedTimeISO;
    }

    public String getDriverCompletedTimeISO() {
        return DriverCompletedTimeISO;
    }

    public void setDriverCompletedTimeISO(String driverCompletedTimeISO) {
        DriverCompletedTimeISO = driverCompletedTimeISO;
    }

    public List<String> getDocumentImage() {
        return documentImage;
    }

    public void setDocumentImage(List<String> documentImage) {
        this.documentImage = documentImage;
    }

    public String getSubid() {
        return subid;
    }

    public void setSubid(String subid) {
        this.subid = subid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAproxDropTime() {
        return aproxDropTime;
    }

    public void setAproxDropTime(String aproxDropTime) {
        this.aproxDropTime = aproxDropTime;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(String flatNumber) {
        this.flatNumber = flatNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getZone1() {
        return zone1;
    }

    public void setZone1(String zone1) {
        this.zone1 = zone1;
    }

    public String getZone2() {
        return zone2;
    }

    public void setZone2(String zone2) {
        this.zone2 = zone2;
    }

    public String getGoodType() {
        return goodType;
    }

    public void setGoodType(String goodType) {
        this.goodType = goodType;
    }

    public String getLoadType() {
        return loadType;
    }

    public void setLoadType(String loadType) {
        this.loadType = loadType;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public List<String> getPhoto() {
        return photo;
    }

    public void setPhoto(List<String> photo) {
        this.photo = photo;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getDimensionUnit() {
        return dimensionUnit;
    }

    public void setDimensionUnit(String dimensionUnit) {
        this.dimensionUnit = dimensionUnit;
    }

    public String getApproxDistance() {
        return approxDistance;
    }

    public void setApproxDistance(String approxDistance) {
        this.approxDistance = approxDistance;
    }

    public String getApproxFare() {
        return approxFare;
    }

    public void setApproxFare(String approxFare) {
        this.approxFare = approxFare;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCompletedTime() {
        return completedTime;
    }

    public void setCompletedTime(String completedTime) {
        this.completedTime = completedTime;
    }

    public String getSignatureUrl() {
        return signatureUrl;
    }

    public void setSignatureUrl(String signatureUrl) {
        this.signatureUrl = signatureUrl;
    }

    public String getDriverPhoto() {
        return driverPhoto;
    }

    public void setDriverPhoto(String driverPhoto) {
        this.driverPhoto = driverPhoto;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getFare() {
        return fare;
    }

    public void setFare(String fare) {
        this.fare = fare;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getDriverOnTheWayTime() {
        return driverOnTheWayTime;
    }

    public void setDriverOnTheWayTime(String driverOnTheWayTime) {
        this.driverOnTheWayTime = driverOnTheWayTime;
    }

    public String getDriverLoadedStartedTime() {
        return driverLoadedStartedTime;
    }

    public void setDriverLoadedStartedTime(String driverLoadedStartedTime) {
        this.driverLoadedStartedTime = driverLoadedStartedTime;
    }

    public String getDriverArrivedTime() {
        return driverArrivedTime;
    }

    public void setDriverArrivedTime(String driverArrivedTime) {
        this.driverArrivedTime = driverArrivedTime;
    }

    public String getDriverAcceptedTime() {
        return driverAcceptedTime;
    }

    public void setDriverAcceptedTime(String driverAcceptedTime) {
        this.driverAcceptedTime = driverAcceptedTime;
    }

    public String getDriverCompletedTime() {
        return driverCompletedTime;
    }

    public void setDriverCompletedTime(String driverCompletedTime) {
        this.driverCompletedTime = driverCompletedTime;
    }

    public String getDriverDropedTime() {
        return driverDropedTime;
    }

    public void setDriverDropedTime(String driverDropedTime) {
        this.driverDropedTime = driverDropedTime;
    }

    public String getDriverReachedDropLocationTime() {
        return driverReachedDropLocationTime;
    }

    public void setDriverReachedDropLocationTime(String driverReachedDropLocationTime) {
        this.driverReachedDropLocationTime = driverReachedDropLocationTime;
    }

    public String getDriverTripDistance() {
        return driverTripDistance;
    }

    public void setDriverTripDistance(String driverTripDistance) {
        this.driverTripDistance = driverTripDistance;
    }

    public String getDriverAcceptedTimeISO() {
        return driverAcceptedTimeISO;
    }

    public void setDriverAcceptedTimeISO(String driverAcceptedTimeISO) {
        this.driverAcceptedTimeISO = driverAcceptedTimeISO;
    }


}
