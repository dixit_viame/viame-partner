package com.viame.viame_partner.ui.bookingdetail.details

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.viame.viame_partner.R
import com.viame.viame_partner.databinding.RowImagesListBinding

class DetailPageImageAdapter constructor (private val context: Context, private var categoryList: List<Any>,
                                          private val listener: ImageListEventHandler) : RecyclerView.Adapter<DetailPageImageAdapter.ImageViewHolder>()
{
    private var layoutInflater: LayoutInflater? = null

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): ImageViewHolder
    {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(context)
        }
        val view = layoutInflater!!.inflate(R.layout.row_images_list, parent, false)
        return ImageViewHolder(view)
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int)
    {
            holder.binding?.viewModel = categoryList[holder.adapterPosition] as String
            holder.binding?.handlers = listener
            Picasso.get()
                .load(categoryList[holder.adapterPosition] as String)
                .into(holder.binding?.iv)
            holder.binding?.executePendingBindings()
    }

    override fun getItemCount(): Int
    {
        return categoryList.size
    }

    class ImageViewHolder(v: View) : RecyclerView.ViewHolder(v)
    {
        val binding: RowImagesListBinding?
        init
        {
            binding = DataBindingUtil.bind(v)
            v.tag = binding
        }
    }
}