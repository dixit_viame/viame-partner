package com.viame.viame_partner.ui.home

interface HomeFragEventHandler {
    /**
     * Will be called when home online/offline button gets clicked
     */
    fun onOfflineOnlineClicked()

}