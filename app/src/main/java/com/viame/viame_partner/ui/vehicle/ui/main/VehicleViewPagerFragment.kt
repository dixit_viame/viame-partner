package com.viame.viame_partner.ui.vehicle.ui.main

import android.annotation.SuppressLint
import android.app.Activity
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.squareup.picasso.Picasso
import com.viame.viame_partner.R
import com.viame.viame_partner.databinding.FragmentVehicleListBinding
import com.viame.viame_partner.ui.vehicle.VehicleListActivity
import com.viame.viame_partner.ui.vehicle.VehicleSelectEventHandler
import com.viame.viame_partner.ui.vehicle.model.VehicleListDataModel
import com.viame.viame_partner.ui.vehicle.model.VehicleListModel
import com.viame.viame_partner.utils.CheckView
import timber.log.Timber
import javax.inject.Inject

/**
 * A placeholder fragment containing a simple view.
 */
class VehicleViewPagerFragment : Fragment() {

    private lateinit var context: Activity
    lateinit var binding : FragmentVehicleListBinding
    private var listVehicles = ArrayList<VehicleListDataModel>()


    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        if (container == null) {
            return null
        }
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_vehicle_list,container,false)
        listVehicles = (context as VehicleListActivity).listVehicle
        val scale = this.arguments!!.getFloat(ARG_SCALE)
        binding.MyRootView.setScaleBoth(scale)

        Picasso.get()
            .load(listVehicles[arguments!!.getInt(ARG_SECTION_NUMBER)].image)
            .placeholder(R.drawable.ic_pic_register_background_)
            .error(R.drawable.ic_pic_register_background_)
            .into(binding.imgVehicle);

        binding.tvCarModel.text = listVehicles[arguments!!.getInt(ARG_SECTION_NUMBER)].model
        binding.tvPlatNumber.text = listVehicles[arguments!!.getInt(ARG_SECTION_NUMBER)].platNo
        binding.tvVehicleName.text = listVehicles[arguments!!.getInt(ARG_SECTION_NUMBER)].type

        binding.vCheckView.setListener(CheckView.OnCheckedChangeListener { isChecked: Boolean ->

            if(isChecked)
            {
                (context as VehicleListActivity).onItemClick(arguments!!.getInt(ARG_SECTION_NUMBER))
            }
        })


        binding.vCheckView.isChecked = false

        return binding.root
    }

    companion object {
        /**
         * The fragment argument representing the section number for this fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"
        private const val ARG_SCALE = "section_scale"

        /**
         * Returns a new instance of this fragment for the given section number.
         */
        @JvmStatic
        fun newInstance(context : Activity ,sectionNumber: Int,scale: Float): VehicleViewPagerFragment {
            return VehicleViewPagerFragment().apply {
                this.context = context
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                    putFloat(ARG_SCALE, scale)
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()

        binding.vCheckView.isChecked = false
    }

}