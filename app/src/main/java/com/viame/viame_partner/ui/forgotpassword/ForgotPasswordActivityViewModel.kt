package com.viame.viame_partner.ui.forgotpassword

import android.annotation.SuppressLint
import android.text.InputType
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.material.textfield.TextInputEditText
import com.viame.viame_partner.R
import com.viame.viame_partner.application.CatalogApplication
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.ui.signin.model.SignInRequestModel
import com.viame.viame_partner.ui.signin.model.SignInResponseModel
import com.viame.viame_partner.utils.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import javax.inject.Inject


class ForgotPasswordActivityViewModel @Inject constructor() : ViewModel() {

    /**
     * Two way bind-able fields
     */
    var emailMobile: String = ""

    /**
     * To pass login result to activity
     */
    private val forgotPassValidationResult = MutableLiveData<String>()

    fun getForgotPassValidationResult(): LiveData<String> = forgotPassValidationResult


    private val forgotPassApiResponse = MutableLiveData<APIRequestResponseHandler<SignInResponseModel?>>()

    fun getSignInApiResult(): MutableLiveData<APIRequestResponseHandler<SignInResponseModel?>> = forgotPassApiResponse

    /**
     * Called from activity on signIn button click
     */
    fun performValidation() {
        if (emailMobile.trim().isBlank())
        {
            forgotPassValidationResult.value = CatalogApplication.instance?.getString(R.string.enter_mobile_number)

            return
        } else if (Utils.isNumeric(emailMobile.trim()) && !(Utils.IsMobileNumber(emailMobile.trim())))
        {
            forgotPassValidationResult.value = CatalogApplication.instance?.getString(R.string.invalid_phone)
            return
        }
        forgotPassValidationResult.value = "Valid"
    }


    fun forgotPasswordOTPCall(signInObj: SignInRequestModel,apiInterface : ApiInterface)
    {
        forgotPassApiResponse.setValue(APIRequestResponseHandler.loading(null));

            val map = HashMap<String,String>();
            map["Content-Type"] = "application/json;charset=UTF-8"
            map["authorization"] = "ordinory"
            map["token"] = "ordinory"
            map["userType"] = "1"

            val posts: Call<SignInResponseModel> = apiInterface.callLoginMethod(map,signInObj)!!
            //Enqueue the call
            posts.enqueue(object : Callback<SignInResponseModel> {
                @SuppressLint("TimberArgCount")
                override fun onFailure(call: Call<SignInResponseModel>, t: Throwable) {

                    forgotPassApiResponse.value = APIRequestResponseHandler.error(t.message.toString(),null)
                    Timber.e(t.message.toString())
                }

                @SuppressLint("TimberArgCount")
                override fun onResponse(call: Call<SignInResponseModel>, response: Response<SignInResponseModel>
                ) {
                    try {
                        if(response.isSuccessful) {
                            forgotPassApiResponse.value = APIRequestResponseHandler.success(response.body()!!)

                            Timber.e(response.body().toString())
                        }
                        else
                        {
                            forgotPassApiResponse.value = APIRequestResponseHandler.error("Status code "+response.code(),null)
                        }
                    } catch (e: Exception) {
                    }
                }
            })
    }
}




