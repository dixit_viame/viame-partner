package com.viame.viame_partner.ui.unassigned;

import dagger.Module;
import dagger.Provides;

@Module
public class UnAssignedFragmentModule {

    @Provides
    UnAssignedViewModel categoryUnAssignedViewModel() {
        return new UnAssignedViewModel();
    }


}
