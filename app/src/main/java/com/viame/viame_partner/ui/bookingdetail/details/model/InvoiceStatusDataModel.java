package com.viame.viame_partner.ui.bookingdetail.details.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.viame.viame_partner.ui.myorders.model.Invoice;

public class InvoiceStatusDataModel {


    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("invoice")
    @Expose
    private Invoice invoice;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }
}
