package com.viame.viame_partner.ui.base;

import android.content.Context;

import androidx.lifecycle.ViewModel;

import dagger.android.support.DaggerDialogFragment;
import dagger.android.support.DaggerFragment;

public abstract class BaseDialogFragment<T extends ViewModel> extends DaggerDialogFragment {

    private T viewModel;

    /**
     * @return view model instance
     */
    public abstract T getViewModel();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        viewModel = getViewModel();
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

}
