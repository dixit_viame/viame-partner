package com.viame.viame_partner.ui.unassigned.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UnAssignedInvoice implements Serializable {

    @SerializedName("timeFare")
    @Expose
    private String timeFare;
    @SerializedName("baseFare")
    @Expose
    private String baseFare;
    @SerializedName("distFare")
    @Expose
    private String distFare;
    @SerializedName("watingFee")
    @Expose
    private String watingFee;

    @SerializedName("handlingFee")
    @Expose
    private String handlingFee;

    public String getTollFee() {
        return tollFee;
    }

    public void setTollFee(String tollFee) {
        this.tollFee = tollFee;
    }

    @SerializedName("tollFee")
    @Expose
    private String tollFee;

    public String getHandlingFee() {
        return handlingFee;
    }

    public void setHandlingFee(String handlingFee) {
        this.handlingFee = handlingFee;
    }

    @SerializedName("cancelationFee")
    @Expose
    private String cancelationFee;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("time")
    @Expose
    private long time;
    @SerializedName("Discount")
    @Expose
    private String discount;
    @SerializedName("appliedAmount")
    @Expose
    private String appliedAmount;
    @SerializedName("appProfitLoss")
    @Expose
    private String appProfitLoss;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("tax")
    @Expose
    private String tax;
    @SerializedName("estimateId")
    @Expose
    private long estimateId;
    @SerializedName("estimateFare")
    @Expose
    private Integer estimateFare;
    @SerializedName("estimateDistance")
    @Expose
    private Double estimateDistance;
    @SerializedName("estimateTime")
    @Expose
    private String estimateTime;
    @SerializedName("appcom")
    @Expose
    private String appcom;
    @SerializedName("deliveryFare")
    @Expose
    private String deliveryFare;
    @SerializedName("loadWaitingTime")
    @Expose
    private long loadWaitingTime;
    @SerializedName("masEarning")
    @Expose
    private String masEarning;
    @SerializedName("minFare")
    @Expose
    private String minFare;
    @SerializedName("subtotal")
    @Expose
    private String subtotal;
    @SerializedName("totalWaitingTime")
    @Expose
    private long totalWaitingTime;
    @SerializedName("unloadWaitingTime")
    @Expose
    private long unloadWaitingTime;
    @SerializedName("watingtime")
    @Expose
    private String watingtime;

    public String getTimeFare() {
        return timeFare;
    }

    public void setTimeFare(String timeFare) {
        this.timeFare = timeFare;
    }

    public String getBaseFare() {
        return baseFare;
    }

    public void setBaseFare(String baseFare) {
        this.baseFare = baseFare;
    }

    public String getDistFare() {
        return distFare;
    }

    public void setDistFare(String distFare) {
        this.distFare = distFare;
    }

    public String getWatingFee() {
        return watingFee;
    }

    public void setWatingFee(String watingFee) {
        this.watingFee = watingFee;
    }

    public String getCancelationFee() {
        return cancelationFee;
    }

    public void setCancelationFee(String cancelationFee) {
        this.cancelationFee = cancelationFee;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getAppliedAmount() {
        return appliedAmount;
    }

    public void setAppliedAmount(String appliedAmount) {
        this.appliedAmount = appliedAmount;
    }

    public String getAppProfitLoss() {
        return appProfitLoss;
    }

    public void setAppProfitLoss(String appProfitLoss) {
        this.appProfitLoss = appProfitLoss;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public long getEstimateId() {
        return estimateId;
    }

    public void setEstimateId(long estimateId) {
        this.estimateId = estimateId;
    }

    public Integer getEstimateFare() {
        return estimateFare;
    }

    public void setEstimateFare(Integer estimateFare) {
        this.estimateFare = estimateFare;
    }

    public Double getEstimateDistance() {
        return estimateDistance;
    }

    public void setEstimateDistance(Double estimateDistance) {
        this.estimateDistance = estimateDistance;
    }

    public String getEstimateTime() {
        return estimateTime;
    }

    public void setEstimateTime(String estimateTime) {
        this.estimateTime = estimateTime;
    }

    public String getAppcom() {
        return appcom;
    }

    public void setAppcom(String appcom) {
        this.appcom = appcom;
    }

    public String getDeliveryFare() {
        return deliveryFare;
    }

    public void setDeliveryFare(String deliveryFare) {
        this.deliveryFare = deliveryFare;
    }

    public long getLoadWaitingTime() {
        return loadWaitingTime;
    }

    public void setLoadWaitingTime(long loadWaitingTime) {
        this.loadWaitingTime = loadWaitingTime;
    }

    public String getMasEarning() {
        return masEarning;
    }

    public void setMasEarning(String masEarning) {
        this.masEarning = masEarning;
    }

    public String getMinFare() {
        return minFare;
    }

    public void setMinFare(String minFare) {
        this.minFare = minFare;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public long getTotalWaitingTime() {
        return totalWaitingTime;
    }

    public void setTotalWaitingTime(long totalWaitingTime) {
        this.totalWaitingTime = totalWaitingTime;
    }

    public long getUnloadWaitingTime() {
        return unloadWaitingTime;
    }

    public void setUnloadWaitingTime(long unloadWaitingTime) {
        this.unloadWaitingTime = unloadWaitingTime;
    }

    public String getWatingtime() {
        return watingtime;
    }

    public void setWatingtime(String watingtime) {
        this.watingtime = watingtime;
    }

}
