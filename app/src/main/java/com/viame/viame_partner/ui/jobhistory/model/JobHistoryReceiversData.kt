package com.viame.viame_partner.ui.jobhistory.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class JobHistoryReceiversData : Serializable {
    @SerializedName("goodType")
    @Expose
    var goodType: String? = null

    @SerializedName("signatureUrl")
    @Expose
    var signatureUrl: String? = null

    @SerializedName("photo")
    @Expose
    var photo: List<String>? = null

    @SerializedName("documentImage")
    @Expose
    var documentImage: List<String>? = null

    @SerializedName("name")
    @Expose
    var receiverName: String? = null

    @SerializedName("mobile")
    @Expose
    var receiverMobile: String? = null

    @SerializedName("countryCode")
    @Expose
    var receiverCountryCode: String? = null


}