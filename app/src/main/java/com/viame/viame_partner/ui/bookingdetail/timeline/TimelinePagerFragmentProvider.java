package com.viame.viame_partner.ui.bookingdetail.timeline;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class TimelinePagerFragmentProvider {

    @ContributesAndroidInjector
    abstract TimelinePagerFragment contributeTimelinePagerFragment();
}
