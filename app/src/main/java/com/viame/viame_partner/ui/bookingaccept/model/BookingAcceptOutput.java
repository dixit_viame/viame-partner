package com.viame.viame_partner.ui.bookingaccept.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookingAcceptOutput {

    @SerializedName("errNum")
    @Expose
    private String errNum;

    @SerializedName("errMsg")
    @Expose
    private String errMsg;

    @SerializedName("errFlag")
    @Expose
    private int errFlag;

    @SerializedName("otp")
    @Expose
    private String otp;

    public String getErrNum() {
        return errNum;
    }

    public void setErrNum(String errNum) {
        this.errNum = errNum;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public int getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(int errFlag) {
        this.errFlag = errFlag;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
