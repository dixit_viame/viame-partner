package com.viame.viame_partner.ui.bookingdetail;
import androidx.lifecycle.ViewModelProvider;

import com.viame.viame_partner.utils.ViewModelProviderFactory;

import dagger.Module;
import dagger.Provides;

@Module
public class BookingDetailActivityModule {

    @Provides
    BookingDetailViewModel providesBookingDetailViewModel(){
        return new BookingDetailViewModel();
    }

    @Provides
    ViewModelProvider.Factory provideViewModelProvider(BookingDetailViewModel viewModel){
        return new ViewModelProviderFactory<>(viewModel);
    }


}
