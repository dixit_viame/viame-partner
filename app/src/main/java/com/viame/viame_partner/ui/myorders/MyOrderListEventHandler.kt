package com.viame.viame_partner.ui.myorders

import android.view.View
import com.viame.viame_partner.ui.myorders.model.Appointment

interface MyOrderListEventHandler {
    /**
     * Will be called when signIn button gets clicked
     */
    fun onitemClick(view : View, appointment: Appointment)

}