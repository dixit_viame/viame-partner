package com.viame.viame_partner.ui.main.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PresenceSettings {

    @SerializedName("presenceTime")
    @Expose
    private Integer presenceTime;
    @SerializedName("DistanceForLogingLatLongs")
    @Expose
    private Integer distanceForLogingLatLongs;

    public Integer getPresenceTime() {
        return presenceTime;
    }

    public void setPresenceTime(Integer presenceTime) {
        this.presenceTime = presenceTime;
    }

    public Integer getDistanceForLogingLatLongs() {
        return distanceForLogingLatLongs;
    }

    public void setDistanceForLogingLatLongs(Integer distanceForLogingLatLongs) {
        this.distanceForLogingLatLongs = distanceForLogingLatLongs;
    }

}