package com.viame.viame_partner.ui.vehicle.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VehicleListData {

    @SerializedName("data")
    @Expose
    private List<VehicleListDataModel> data = null;

    public List<VehicleListDataModel> getData() {
        return data;
    }

    public void setData(List<VehicleListDataModel> data) {
        this.data = data;
    }
}
