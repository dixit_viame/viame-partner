package com.viame.viame_partner.ui.vehicle.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VehicleConfirmInputModel {

    @SerializedName("workplace_id")
    @Expose
    private String workplaceId;
    @SerializedName("type_id")
    @Expose
    private String typeId;
    @SerializedName("mid")
    @Expose
    private String mid;

    public String getWorkplaceId() {
        return workplaceId;
    }

    public void setWorkplaceId(String workplaceId) {
        this.workplaceId = workplaceId;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

}
