package com.viame.viame_partner.ui.bookingdetail.details

import android.view.View
import com.viame.viame_partner.ui.myorders.model.Appointment

interface ImageListEventHandler {
    /**
     * Will be called when signIn button gets clicked
     */
    fun onitemClick(appointment: String)

}