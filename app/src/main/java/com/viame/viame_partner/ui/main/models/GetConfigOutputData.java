package com.viame.viame_partner.ui.main.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetConfigOutputData {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("currencySymbol")
    @Expose
    private String currencySymbol;
    @SerializedName("custGoogleMapKeys")
    @Expose
    private List<String> custGoogleMapKeys = null;
    @SerializedName("custGooglePlaceKeys")
    @Expose
    private List<String> custGooglePlaceKeys = null;
    @SerializedName("DriverGoogleMapKeys")
    @Expose
    private List<String> driverGoogleMapKeys = null;
    @SerializedName("mileage_metric")
    @Expose
    private String mileageMetric;
    @SerializedName("mileage_metric_text")
    @Expose
    private String mileageMetricText;
    @SerializedName("presenceSettings")
    @Expose
    private PresenceSettings presenceSettings;
    @SerializedName("pubnubkeys")
    @Expose
    private Pubnubkeys pubnubkeys;
    @SerializedName("pushTopics")
    @Expose
    private PushTopics pushTopics;
    @SerializedName("system_timezone")
    @Expose
    private String systemTimezone;
    @SerializedName("walletSettings")
    @Expose
    private WalletSettings walletSettings;
    @SerializedName("weight_metric")
    @Expose
    private String weightMetric;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public List<String> getCustGoogleMapKeys() {
        return custGoogleMapKeys;
    }

    public void setCustGoogleMapKeys(List<String> custGoogleMapKeys) {
        this.custGoogleMapKeys = custGoogleMapKeys;
    }

    public List<String> getCustGooglePlaceKeys() {
        return custGooglePlaceKeys;
    }

    public void setCustGooglePlaceKeys(List<String> custGooglePlaceKeys) {
        this.custGooglePlaceKeys = custGooglePlaceKeys;
    }
    public List<String> getDriverGoogleMapKeys() {
        return driverGoogleMapKeys;
    }

    public void setDriverGoogleMapKeys(List<String> driverGoogleMapKeys) {
        this.driverGoogleMapKeys = driverGoogleMapKeys;
    }

    public String getMileageMetric() {
        return mileageMetric;
    }

    public void setMileageMetric(String mileageMetric) {
        this.mileageMetric = mileageMetric;
    }

    public String getMileageMetricText() {
        return mileageMetricText;
    }

    public void setMileageMetricText(String mileageMetricText) {
        this.mileageMetricText = mileageMetricText;
    }


    public PresenceSettings getPresenceSettings() {
        return presenceSettings;
    }

    public void setPresenceSettings(PresenceSettings presenceSettings) {
        this.presenceSettings = presenceSettings;
    }


    public Pubnubkeys getPubnubkeys() {
        return pubnubkeys;
    }

    public void setPubnubkeys(Pubnubkeys pubnubkeys) {
        this.pubnubkeys = pubnubkeys;
    }


    public PushTopics getPushTopics() {
        return pushTopics;
    }

    public void setPushTopics(PushTopics pushTopics) {
        this.pushTopics = pushTopics;
    }


    public String getSystemTimezone() {
        return systemTimezone;
    }

    public void setSystemTimezone(String systemTimezone) {
        this.systemTimezone = systemTimezone;
    }

    public WalletSettings getWalletSettings() {
        return walletSettings;
    }

    public void setWalletSettings(WalletSettings walletSettings) {
        this.walletSettings = walletSettings;
    }

    public String getWeightMetric() {
        return weightMetric;
    }

    public void setWeightMetric(String weightMetric) {
        this.weightMetric = weightMetric;
    }

}
