package com.viame.viame_partner.ui.bookingcomplete

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.ScrollView
import androidx.core.content.ContextCompat
import androidx.core.net.toFile
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.github.gcacace.signaturepad.views.SignaturePad
import com.viame.viame_partner.*
import com.viame.viame_partner.aws.AWSEntity
import com.viame.viame_partner.aws.AWSRepository
import com.viame.viame_partner.databinding.ActivityBookingCompletedBinding
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.base.BaseActivity
import com.viame.viame_partner.ui.bookingdetail.details.model.BookingStatusInputModel
import com.viame.viame_partner.ui.jobhistorydetail.JobHistoryDetailActivity
import com.viame.viame_partner.ui.main.MainActivity
import com.viame.viame_partner.ui.myorders.model.Appointment
import com.viame.viame_partner.utils.*
import com.viame.viame_partner.utils.permission.annotation.PermissionDenied
import com.viame.viame_partner.utils.permission.annotation.PermissionGranted
import com.yalantis.ucrop.UCrop
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_booking_completed.*
import timber.log.Timber
import java.io.File
import javax.inject.Inject

class BookingCompletedActivity : BaseActivity<BookingCompletedActivityViewModel>(),
    SignaturePad.OnSignedListener {

    companion object
    {
        var APPDATA : String = "APPDATA"
    }

    @Inject
    lateinit var apiInterface: ApiInterface

    @Inject
    lateinit var connectivityReceiver: ConnectivityReceiver

    @Inject
    lateinit var sAWSRepository: AWSRepository

    lateinit var mHandlePictureEvents : HandlePictureEvents

    private var signBitmap: Bitmap? = null
    lateinit var bindings : ActivityBookingCompletedBinding

    var appointment : Appointment? = null
    var images = ArrayList<String>()
    var adapter : BookCompleteImagePagerAdapter? = null

    @Inject
    lateinit var model : BookingCompletedActivityViewModel

    override fun getViewModel(): BookingCompletedActivityViewModel {
        return model
    }

    @SuppressLint("ClickableViewAccessibility", "SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        bindings = DataBindingUtil.setContentView(this@BookingCompletedActivity,R.layout.activity_booking_completed)
        setSupportActionBar(toolbar)

        mHandlePictureEvents = HandlePictureEvents(this@BookingCompletedActivity,null)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        bindings.toolbar.setBackgroundColor(ContextCompat.getColor(this@BookingCompletedActivity,R.color.transparent))

        bindings.toolbarLayout.setTitle("")
        setTitle("")

        if(intent.extras!!.get(JobHistoryDetailActivity.APPDATA) != null)
        {
            appointment = intent.extras!!.getSerializable(JobHistoryDetailActivity.APPDATA) as Appointment?
            bindings.viewModel = appointment
            bindings.executePendingBindings()

            bindings.signaturePad.setOnSignedListener(this@BookingCompletedActivity)

            bindings.tvDate.text = Utils.dateFormatter(appointment?.bookingTime)
            bindings.tvSenderPhoneNo.labelTextView.text = appointment?.slavemobile

            if(appointment?.apptType == "1")
            {
                bindings.tvBookingType.text = getString(R.string.instant_booking)
            }
            else
            {
                bindings.tvBookingType.text = getString(R.string.book_later_text)
            }

            if(appointment?.receiverName  !=null && appointment?.receiverMobile !=null)
            {
                bindings.tieReceiverName.setText(appointment!!.receiverName)
                bindings.tieReceiverContact.setText(appointment!!.receiverMobile)
            }

            if(appointment!!.paymentType == "1" || appointment!!.paymentType.equals("Card",true))
            {
                bindings.llCardLayout.visibility = View.VISIBLE
                bindings.llCashLayout.visibility = View.GONE
                bindings.llWalletLayout.visibility = View.GONE
            }
            else  if(appointment!!.paymentType == "2" || appointment!!.paymentType.equals("Wallet",true))
            {
                bindings.llCardLayout.visibility = View.GONE
                bindings.llCashLayout.visibility = View.GONE
                bindings.llWalletLayout.visibility = View.VISIBLE

            }else  if(appointment!!.paymentType == "3" || appointment!!.paymentType.equals("Cash",true))
            {
                bindings.llCardLayout.visibility = View.GONE
                bindings.llCashLayout.visibility = View.VISIBLE
                bindings.llWalletLayout.visibility = View.GONE
            }


            if (appointment!!.pricingType == 0)
            {
                bindings.llBasefare.visibility = View.VISIBLE
                bindings.llDistancefare.visibility = View.VISIBLE
                bindings.llTimefare.visibility = View.VISIBLE
                bindings.llWaitingfare.visibility = View.VISIBLE
                bindings.llHandling.visibility = View.GONE
            }
            else if (appointment!!.pricingType == 1)
            {
                bindings.llBasefare.visibility = View.VISIBLE
                bindings.llDistancefare.visibility = View.GONE
                bindings.llTimefare.visibility = View.GONE
                bindings.llWaitingfare.visibility = View.VISIBLE
                bindings.llHandling.visibility = View.GONE
            }
            else if (appointment!!.pricingType == 2)
            {
                bindings.llBasefare.visibility = View.VISIBLE
                bindings.llDistancefare.visibility = View.GONE
                bindings.llTimefare.visibility = View.GONE
                bindings.llWaitingfare.visibility = View.VISIBLE
                bindings.llHandling.visibility = View.GONE
            }

            if(appointment!!.invoice.tax.toInt() > 0)
            {
//                bindings.llVat.visibility = View.VISIBLE
////
////                if (appointment!!.invoice. != null && appointment!!.invoice.taxPercentage!!.isNotEmpty()) {
////                    bindings.tvVat.text = appointment!!.invoice.taxTitle + " (" + appointment!!.invoice.taxPercentage + "%)"
////                    bindings.tvVatValue.setText(Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL) + "" + appointment!!.invoice.tax)
////                }
            }
            else
            {
                bindings.llVat.visibility = View.GONE
            }

            bindings.tvDeliveryFee.setText(Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL) + appointment!!.invoice.baseFare)

            if (appointment!!.invoice.baseFare != null && appointment!!.invoice.baseFare!!.isNotEmpty() && appointment!!.invoice.baseFare == "0.00") {
                bindings.llBasefare.visibility = View.GONE
            } else {
                bindings.llBasefare.visibility = View.VISIBLE
            }

            if (appointment!!.invoice.timeFare != null && appointment!!.invoice.timeFare!!.isNotEmpty() && appointment!!.invoice.timeFare == "0.00") {
                bindings.llTimefare.visibility = View.GONE
            } else {
                bindings.llTimefare.visibility = View.VISIBLE
            }

            if (appointment!!.invoice.watingFee != null && appointment!!.invoice.watingFee!!.isNotEmpty() && appointment!!.invoice.watingFee == "0.00") {
                bindings.llWaitingfare.visibility = View.GONE
            } else {
                bindings.llWaitingfare.visibility = View.VISIBLE
            }

            if (appointment!!.invoice.distFare != null && appointment!!.invoice.distFare!!.isNotEmpty() && appointment!!.invoice.distFare == "") {
                bindings.llDistancefare.visibility = View.GONE
            } else {
                bindings.llDistancefare.visibility = View.VISIBLE
            }

//            if (appointment!!.invoice.baseFare != null && appointment!!.invoice.handlingFee.isNotEmpty()  && appointment!!.invoice.handlingFee.equals("0.00")) {
//                bindings.llHandling.visibility = View.GONE
//            } else {
                bindings.llHandling.visibility = View.GONE
//            }

//            if (appointment!!.invoice.tollFee != null && appointment!!.invoice.tollFee.isNotEmpty()  && appointment!!.invoice.tollFee.equals("0"))
//            {
//                bindings.llToll.visibility = View.GONE
//            } else {
                bindings.llToll.visibility = View.GONE
            //}

            if (appointment!!.invoice.watingFee != null && appointment!!.invoice.watingFee.isNotEmpty()  && appointment!!.invoice.watingFee == "0.00") {
                bindings.llWaitingfare.visibility = View.GONE
            } else {
                bindings.llWaitingfare.visibility = View.VISIBLE
            }

            if (appointment!!.invoice.discount != null && appointment!!.invoice.discount.isNotEmpty()  && appointment!!.invoice.discount == "0.00") {
                bindings.llDiscountfare.visibility = View.GONE
            } else {
                bindings.llDiscountfare.visibility = View.VISIBLE
            }

            if (appointment!!.goodType != null && appointment!!.goodType.isEmpty()) {

                bindings.tvGoodsType.setText(getString(R.string.no_good_type))

            } else {
                bindings.tvGoodsType.setText(appointment!!.goodType)
            }

            //if (appointment!!. != null && appointment!!.shipemntDetails.get(0).quantity.isEmpty())
           // {
                bindings.tvQuantity.setText("Loose")
           // }
           // else
           // {
            //    bindings.tvQuantity.setText(appointment!!.shipemntDetails.get(0).quantity)
           // }

            if (appointment!!.invoice.subtotal != null && appointment!!.invoice.subtotal!!.isNotEmpty())
            {
                bindings.tvSubtotalFare.text = "${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} ${appointment!!.invoice.subtotal}"

            }

            bindings.tvBid.text = appointment!!.orderId.toString()

            bindings.tvDistancefareValue.text = "${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} ${appointment!!.invoice.distFare}"

            bindings.tvTimefareValue.text = "${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} ${appointment!!.invoice.timeFare}"

            bindings.tvBaseFareValue.text = "${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} ${appointment!!.invoice.baseFare}"

            bindings.tvWaitingFeeValue.text = "${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} ${appointment!!.invoice.watingFee}"

          //  bindings.tvTollFareValue.text = "${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} ${appointment!!.invoice.tollFee}"

          //  bindings.tvHandlingValue.text = "${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} ${appointment!!.invoice.handlingFee}"

            bindings.tvDiscountValue.text = "${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} ${appointment!!.invoice.discount}"

            bindings.tvGrandtotalValue.text = "${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} ${appointment!!.invoice.baseFare}"

//            bindings.tvReceiverName.text = appointment!!.shipemntDetails[0].name
//
//            bindings.tvReceiverPhoneNo.labelTextView.text = appointment!!.shipemntDetails[0].mobile
//
//            bindings.tieReceiverEmail.text = appointment!!. shipemntDetails[0].email

            bindings.tvSenderContact.setOnClickListener {

                Alerts.showBottomSheetSimpleConfirmationDialog(this@BookingCompletedActivity,
                    getString(R.string.call_customer), appointment!!.slavemobile, false, getString(R.string.action_cancel),
                    getString(R.string.call_lbl), object : Alerts.OnConfirmationDialog {
                        override fun onYes() {

                            val dialIntent = Intent(
                                Intent.ACTION_DIAL,
                                Uri.parse("tel:${appointment!!.slaveCountryCode}${appointment!!.slavemobile}"))
                            startActivity(dialIntent)
                        }

                        override fun onNo() {

                        }
                    })

            }

            bindings.ivSignatureRefreshImage.setOnClickListener {

                bindings.signaturePad.clear()

            }

        }

        bindings.mainNestedScrollView.post { bindings.mainNestedScrollView.fullScroll(ScrollView.FOCUS_DOWN) }

        bindings.fabAddImages.setOnClickListener {

            if(! PermissionHelper.getInstance().hasPermission(this@BookingCompletedActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) )
            {
                requestStoragePermission()
            }
            else if(! PermissionHelper.getInstance().hasPermission(this@BookingCompletedActivity, Manifest.permission.CAMERA) )
            {
                requestCameraPermission()
            }
            else
            {
                mHandlePictureEvents.openDialog()
            }
        }

        model.getBookingStatusAPIDataResult().observe(this@BookingCompletedActivity, Observer { result ->

            if (result != null) {

                if (result.status == APIRequestResponseHandler.AuthStatus.LOADING) {
                    Alerts.showProgressBar(this)

                } else if (result.status == APIRequestResponseHandler.AuthStatus.ERROR) {
                    Alerts.dismissProgressBar()
                    Alerts.showSnackBar(this, result.data.errMsg)

                } else if (result.status == APIRequestResponseHandler.AuthStatus.SUCCESS) {
                    try {
                        Alerts.dismissProgressBar()

                        if (result.data?.errFlag == 1)
                        {
                            Alerts.showSnackBar(this, result.data.errMsg)
                        }
                        else
                        {
                            // If Invoice is not null that means booking completed then redirect to booking complete screen.
                            result.data.let {

                                Alerts.showBottomSheetSimpleConfirmationDialog(this@BookingCompletedActivity,
                                    getString(R.string.alert),
                                    result.data.errMsg, true, null,
                                    getString(R.string.ok), object : Alerts.OnConfirmationDialog {
                                        @SuppressLint("CheckResult")
                                        override fun onYes()
                                        {
                                            val intent = Intent(this@BookingCompletedActivity, MainActivity::class.java)
                                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                            startActivity(intent)
                                            this@BookingCompletedActivity.finish()
                                        }
                                        override fun onNo() {

                                        }
                                    })
                            }
                        }
                    } catch (e: Exception) {
                    }
                }
            }
        })

        bindings.btnConfirm.setOnClickListener {

            try {
                if(bindings.tieReceiverName.text?.toString()?.isNotEmpty()!!)
                {

                        if(bindings.tieReceiverContact.text?.toString()?.isNotEmpty()!!)
                        {

                            if(signBitmap != null)
                            {
                                if(! PermissionHelper.getInstance().hasPermission(this@BookingCompletedActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) )
                                {
                                    requestStoragePermission()
                                }
                                else {

                                    try {
                                        val uriFile = mHandlePictureEvents.saveBitmapToFile(signBitmap)

                                        if (Utils.isNetworkAvailable(this)) {

                                            if (uriFile.exists()) {

                                                // If payment is paid by receiver than show popup of cash collect from receiver.
                                                if (appointment?.paidByReceiver != null && appointment?.paidByReceiver!!) {
                                                    showAmtCollectPopUp(uriFile)
                                                } else {
                                                    Alerts.showProgressBar(this@BookingCompletedActivity)
                                                    sAWSRepository.uploadDrawing(
                                                        AWSEntity(
                                                            id = 1000,
                                                            fileName = AMAZON_SIGNATURE_FOLDER + uriFile?.name!!,
                                                            imagePath = uriFile.absolutePath,
                                                            lastModified = System.currentTimeMillis()
                                                        )
                                                    ).observeOn(AndroidSchedulers.mainThread())
                                                        .subscribe(
                                                            { d: AWSEntity ->

                                                                try {
                                                                    Timber.e("Uploaded Successfully ${d.imagePath}")
                                                                    Alerts.dismissProgressBar()

                                                                    android.os.Handler().postDelayed({

                                                                        connectivityReceiver.observe(
                                                                            this@BookingCompletedActivity,
                                                                            Observer { connectionModel ->

                                                                                try {
                                                                                    if (connectionModel != null && connectionModel.isConnected()) {
                                                                                        val mBookingStatusInputModel =
                                                                                            BookingStatusInputModel()

                                                                                        var docs = ""
                                                                                        try {
                                                                                            if (images.size > 0) {
                                                                                                for (i in images.indices) {
                                                                                                    docs =
                                                                                                        if (docs.isEmpty()) {
                                                                                                            images[i]
                                                                                                        } else {
                                                                                                            docs + "," + images[i]
                                                                                                        }
                                                                                                }
                                                                                            }
                                                                                        } catch (e: Exception) {
                                                                                        }
                                                                                        mBookingStatusInputModel.entDocuments =
                                                                                            docs

                                                                                        val amt: Double =
                                                                                            appointment?.getInvoice()?.total?.toDouble()!!

                                                                                        mBookingStatusInputModel.cashCollected =
                                                                                            amt.toString()
                                                                                        mBookingStatusInputModel.entRating =
                                                                                            "5"
                                                                                        mBookingStatusInputModel.entReceiverName =
                                                                                            bindings.tieReceiverName.text.toString()
                                                                                        //                                                        mBookingStatusInputModel.entReceiverPhone = bindings.tieReceiverEmail.text.toString()
                                                                                        mBookingStatusInputModel.entReceiverPhone =
                                                                                            bindings.tieReceiverContact.text.toString()
                                                                                        mBookingStatusInputModel.entHandlingFee =
                                                                                            "0.0"
                                                                                        mBookingStatusInputModel.entTollFee =
                                                                                            "0.0"
                                                                                        mBookingStatusInputModel.entSignatureUrl =
                                                                                            d.imagePath

                                                                                        mBookingStatusInputModel.entBookingId =
                                                                                            appointment?.orderId!!.toLong()
                                                                                        if (Preferences.getPreferenceString(
                                                                                                PrefEntity.LATITUDE_CURRENT
                                                                                            ).isNotEmpty()
                                                                                        )
                                                                                            mBookingStatusInputModel.lat =
                                                                                                Preferences.getPreferenceString(
                                                                                                    PrefEntity.LATITUDE_CURRENT
                                                                                                ).toDouble()
                                                                                        else
                                                                                            mBookingStatusInputModel.lat =
                                                                                                0.0

                                                                                        if (Preferences.getPreferenceString(
                                                                                                PrefEntity.LONGITUDE_CURRENT
                                                                                            ).isNotEmpty()
                                                                                        )
                                                                                            mBookingStatusInputModel.long =
                                                                                                Preferences.getPreferenceString(
                                                                                                    PrefEntity.LONGITUDE_CURRENT
                                                                                                ).toDouble()
                                                                                        else
                                                                                            mBookingStatusInputModel.long =
                                                                                                0.0

                                                                                        mBookingStatusInputModel.entStatus =
                                                                                            "${MBStatusType.COMPLETED.id}"

                                                                                        model.BookingStatusAPICall(
                                                                                            apiInterface,
                                                                                            mBookingStatusInputModel
                                                                                        )

                                                                                    } else {
                                                                                        if (connectionModel?.type == Vpn) {
                                                                                            Alerts.showSnackBar(
                                                                                                this,
                                                                                                getString(R.string.vpn_use_message)
                                                                                            )
                                                                                        } else {
                                                                                            Alerts.showSnackBar(
                                                                                                this,
                                                                                                getString(R.string.internet_not_available)
                                                                                            )
                                                                                        }
                                                                                    }
                                                                                } catch (e: Exception) {
                                                                                }
                                                                            })

                                                                        // If Signature file exist then it will be delete after use.
                                                                        if(uriFile.exists())
                                                                            uriFile.delete()

                                                                    }, 200)
                                                                } catch (e: Exception) {

                                                                    Alerts.showSnackBar(
                                                                        this@BookingCompletedActivity,
                                                                        getString(R.string.something_goes_wrong)
                                                                    )
                                                                }

                                                            },
                                                            {

                                                                Alerts.dismissProgressBar()
                                                                Alerts.showSnackBar(
                                                                    this@BookingCompletedActivity,
                                                                    getString(R.string.something_goes_wrong)
                                                                )

                                                            }, {}
                                                        )
                                                }
                                            } else {
                                                Alerts.showSnackBar(
                                                    this@BookingCompletedActivity,
                                                    getString(R.string.something_goes_wrong)
                                                )

                                            }
                                        } else {
                                            Alerts.showSnackBar(
                                                this@BookingCompletedActivity,
                                                getString(R.string.internet_not_available)
                                            )
                                        }
                                    } catch (e: Exception) {
                                        Alerts.showSnackBar(
                                            this@BookingCompletedActivity,
                                            getString(R.string.something_goes_wrong)
                                        )
                                    }
                                }

                            }
                            else
                            {
                                Alerts.showSnackBar(this@BookingCompletedActivity,getString(R.string.draw_your_sign))

                            }
                        }
                        else
                        {
                            Alerts.showSnackBar(this@BookingCompletedActivity,getString(R.string.enter_reciver_contact))
                        }
                }
                else
                {
                    Alerts.showSnackBar(this@BookingCompletedActivity,getString(R.string.enter_reciver_name))
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Alerts.showSnackBar(
                    this@BookingCompletedActivity,
                    getString(R.string.something_goes_wrong)
                )
            }
        }
    }

    private fun showAmtCollectPopUp(uriFile: File) {

        Alerts.showBottomSheetSimpleConfirmationDialog(this@BookingCompletedActivity,
            getString(R.string.collect_cash),
            getString(R.string.grand_total) + " : " + Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL) + " " + (appointment?.invoice?.total),
            true,
            null,
            getString(R.string.ok),
            object : Alerts.OnConfirmationDialog {
                @SuppressLint("CheckResult")
                override fun onYes() {


                    if(Utils.isNetworkAvailable(this@BookingCompletedActivity)) {
                        Alerts.showProgressBar(this@BookingCompletedActivity)

                        sAWSRepository.uploadDrawing(
                            AWSEntity(
                                id = 1000,
                                fileName = AMAZON_SIGNATURE_FOLDER + uriFile.name,
                                imagePath = uriFile.absolutePath,
                                lastModified = System.currentTimeMillis()
                            )
                        ).observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                { d: AWSEntity ->

                                    Timber.e("Uploaded Successfully ${d.imagePath}")
                                    Alerts.dismissProgressBar()
                                    android.os.Handler().post {

                                        connectivityReceiver.observe(
                                            this@BookingCompletedActivity,
                                            Observer { connectionModel ->

                                                if (connectionModel != null && connectionModel.isConnected()) {
                                                    val mBookingStatusInputModel =
                                                        BookingStatusInputModel()

                                                    var docs = ""
                                                    try {
                                                        if (images.size > 0) {
                                                            for (i in images.indices) {
                                                                docs = if (docs.isEmpty()) {
                                                                    images[i]
                                                                } else {
                                                                    docs + "," + images[i]
                                                                }
                                                            }
                                                        }
                                                    } catch (e: Exception) {
                                                    }
                                                    mBookingStatusInputModel.entDocuments =
                                                        docs

                                                    val amt: Double =
                                                        appointment?.getInvoice()?.total?.toDouble()!!

                                                    mBookingStatusInputModel.cashCollected =
                                                        amt.toString()
                                                    mBookingStatusInputModel.entRating = "5"
                                                    mBookingStatusInputModel.entReceiverName =
                                                        bindings.tieReceiverName.text.toString()

                                                    mBookingStatusInputModel.entReceiverPhone =
                                                        bindings.tieReceiverContact.text.toString()
                                                    mBookingStatusInputModel.entHandlingFee =
                                                        "0.0"
                                                    mBookingStatusInputModel.entTollFee =
                                                        "0.0"
                                                    mBookingStatusInputModel.entSignatureUrl =
                                                        d.imagePath

                                                    mBookingStatusInputModel.entBookingId =
                                                        appointment?.orderId!!.toLong()
                                                    if (Preferences.getPreferenceString(
                                                            PrefEntity.LATITUDE_CURRENT
                                                        ).isNotEmpty()
                                                    )
                                                        mBookingStatusInputModel.lat =
                                                            Preferences.getPreferenceString(
                                                                PrefEntity.LATITUDE_CURRENT
                                                            ).toDouble()
                                                    else
                                                        mBookingStatusInputModel.lat = 0.0

                                                    if (Preferences.getPreferenceString(
                                                            PrefEntity.LONGITUDE_CURRENT
                                                        ).isNotEmpty()
                                                    )
                                                        mBookingStatusInputModel.long =
                                                            Preferences.getPreferenceString(
                                                                PrefEntity.LONGITUDE_CURRENT
                                                            ).toDouble()
                                                    else
                                                        mBookingStatusInputModel.long = 0.0

                                                    mBookingStatusInputModel.entStatus =
                                                        "${MBStatusType.COMPLETED.id}"

                                                    model.BookingStatusAPICall(
                                                        apiInterface,
                                                        mBookingStatusInputModel
                                                    )

                                                } else {
                                                    if (connectionModel?.type == Vpn) {
                                                        Alerts.showSnackBar(
                                                            this@BookingCompletedActivity,
                                                            getString(R.string.vpn_use_message)
                                                        )
                                                    } else {
                                                        Alerts.showSnackBar(this@BookingCompletedActivity, getString(R.string.internet_not_available))
                                                    }
                                                }
                                            })

                                        // If Signature file exist then it will be delete after use.
                                        if(uriFile.exists())
                                         uriFile.delete()
                                    }
                                },
                                {

                                    Alerts.dismissProgressBar()
                                    Alerts.showSnackBar(this@BookingCompletedActivity, getString(R.string.something_goes_wrong))

                                }, {}
                            )
                    }
                    else
                    {
                        Alerts.showSnackBar(this@BookingCompletedActivity, getString(R.string.internet_not_available))
                    }
                }

                override fun onNo() {

                }
            })
    }


    /**
     * This is an overrided method, got a call, when an activity opens by StartActivityForResult(), and return something back to its calling activity.
     *
     * @param requestCode returning the request code.
     * @param resultCode  returning the result code.
     * @param data        contains the actual data.
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (resultCode == Activity.RESULT_CANCELED) //result code to check is the result is ok or not
            {
                if(requestCode == CROP_IMAGE) {
                    if (data != null)
                    {
                        val cropError = UCrop.getError(data)
                        Alerts.showSnackBar(this@BookingCompletedActivity,
                            "Crop Error : $cropError"
                        )
                    }
                }
                return
            }
            else
            {
                when (requestCode)
                {
                    CAMERA_PIC -> mHandlePictureEvents.startCropImage(mHandlePictureEvents.newFile)
                    GALLERY_PIC -> {
                        Timber.d("onActivityResult: $data")
                        if (data != null) {
                            mHandlePictureEvents.gallery(data.data)
                        }
                    }
                    CROP_IMAGE -> {
                        val resultUri = UCrop.getOutput(data!!)
                        Timber.d("onActivityResult: $resultUri")

                        if (Utils.isNetworkAvailable(this))
                        {
                            if(resultUri?.toFile()?.exists()!!)
                            {

                            Alerts.showProgressBar(this@BookingCompletedActivity)
                            sAWSRepository.uploadDrawing(
                                AWSEntity(
                                    id = 1000,
                                    fileName = AMAZON_SHIPMENT_FOLDER + resultUri.toFile().name,
                                    imagePath = resultUri.toFile().absolutePath,
                                    lastModified = System.currentTimeMillis()
                                )
                            ).observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                    { d: AWSEntity ->

                                        Timber.e("Uploaded Successfully ${d.imagePath}")
                                        Alerts.dismissProgressBar()
                                        android.os.Handler().postDelayed({
                                            if (images.contains("Default"))
                                                images.remove("Default")
                                            images.add(d.imagePath)
                                            adapter = BookCompleteImagePagerAdapter(
                                                this@BookingCompletedActivity,
                                                images,
                                                supportFragmentManager
                                            )
                                            bindings.viewpager.adapter = adapter
                                            bindings.dotsIndicator.setViewPager(bindings.viewpager)
                                            adapter!!.notifyDataSetChanged()
                                        }, 1000)
                                    },
                                    {
                                        Alerts.dismissProgressBar()
                                        Alerts.showSnackBar(
                                            this,
                                            getString(R.string.something_goes_wrong)
                                        )
                                    }, {}
                                )
                            } else {
                                Alerts.showSnackBar(this, getString(R.string.something_goes_wrong))
                            }
                        }
                        else
                        {
                            Alerts.showSnackBar(this, getString(R.string.internet_not_available))
                        }
                    }
                }
            }
        } catch (e: Exception) {
        }
    }

    private fun requestStoragePermission() {
        PermissionHelper.getInstance().requestPermission(this@BookingCompletedActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }

    private fun requestCameraPermission() {
        PermissionHelper.getInstance().requestPermission(this@BookingCompletedActivity, Manifest.permission.CAMERA)
    }

    @PermissionGranted(permission = Manifest.permission.CAMERA)
     fun cameraGranted() {
        Timber.i("CAMERA_GRANT")
    }

    @PermissionDenied(permission = Manifest.permission.CAMERA)
     fun cameraDenied() {
        Timber.i("CAMERA_DENIED")
    }

    @PermissionGranted(permission = Manifest.permission.WRITE_EXTERNAL_STORAGE)
     fun storageGrant() {
        Timber.i("WRITE_EXTERNAL_STORAGE_GRANT")
    }

    @PermissionDenied(permission = Manifest.permission.WRITE_EXTERNAL_STORAGE)
     fun storageDenied() {
        Timber.i("WRITE_EXTERNAL_STORAGE_DENIED")
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        PermissionHelper.getInstance().onRequestPermissionsResult(this@BookingCompletedActivity, permissions, grantResults)
    }


    override fun onStartSigning() {



    }

    override fun onClear() {

        signBitmap = null
    }

    override fun onSigned() {

        signBitmap = bindings.signaturePad.getSignatureBitmap()
    }


}
