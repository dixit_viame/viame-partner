package com.viame.viame_partner.ui.jobhistory;
import com.viame.viame_partner.ui.home.HomeFragmentViewModel;

import dagger.Module;
import dagger.Provides;

@Module
public class JobHistoryFragmentModule {

    @Provides
    JobHistoryViewModel jobHistoryViewModel() {
        return new JobHistoryViewModel();
    }


}
