package com.viame.viame_partner.ui.settings;
import com.viame.viame_partner.ui.myorders.MyOrdersFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class SettingsFragmentProvider {

    @ContributesAndroidInjector
    abstract SettingsFragment contributeSettingsFragment();
}