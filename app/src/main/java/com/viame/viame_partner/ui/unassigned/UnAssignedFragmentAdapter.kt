package com.viame.viame_partner.ui.unassigned

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.viame.viame_partner.R
import com.viame.viame_partner.databinding.RowItemUnassignedBinding
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.unassigned.model.UnAsssignedDatumModel
import com.viame.viame_partner.utils.Utils
import java.util.*
class UnAssignedFragmentAdapter constructor(private val context: Context, private var categoryList: ArrayList<UnAsssignedDatumModel>, private val listener: UnAssignedOrderListEventHandler)
    : RecyclerView.Adapter<UnAssignedFragmentAdapter.UnAssignedViewHolder>()
{
    private var layoutInflater: LayoutInflater? = null

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): UnAssignedViewHolder {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }
        val view = layoutInflater!!.inflate(R.layout.row_item_unassigned, parent, false)
        return UnAssignedViewHolder(view)
    }

    override fun onBindViewHolder(holder: UnAssignedViewHolder, position: Int) {
        holder.binding!!.viewModel = categoryList[holder.adapterPosition]
        holder.binding.tvBid.text = categoryList[holder.adapterPosition].orderId.toString()
        holder.binding.tvClientName.text = categoryList[holder.adapterPosition].slaveName.toString()
        holder.binding.tvDate.text = Utils.getDateFormatFromMiliseconds(categoryList[holder.adapterPosition].timpeStampBookingTime.toLong(),"MMM dd, hh:mm a")
        holder.binding.tvPickupTime.text = Utils.dateFormatter(categoryList[holder.adapterPosition].bookingTime)
        holder.binding.tvDropoffTime.text = Utils.dateFormatter(categoryList[holder.adapterPosition].bookingTime)
        holder.binding.tvBookingPrice.text = "${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} "+categoryList[holder.adapterPosition].invoice.total

        holder.binding.tvBookingFrom.text = categoryList[holder.adapterPosition].bookingOrigin

        if(categoryList[holder.adapterPosition].apptType == "1") {
            holder.binding.tvBookingType.text = context.getString(R.string.instant_booking)
        }else
        {
            holder.binding.tvBookingType.text = context.getString(R.string.book_later_text)
        }

        holder.binding.handlers = listener
        holder.binding.indexView = holder.binding.root
        //Triggers the View to be updated with the new values provided.
        // This method has to be run on the UI thread.
        holder.binding.executePendingBindings()
    }

    override fun getItemCount(): Int {
        return categoryList.size
    }

    fun getCategoryList(): ArrayList<UnAsssignedDatumModel> {
        return categoryList
    }

    fun setCategoryList(categories: ArrayList<UnAsssignedDatumModel>) {
        categoryList = categories
        notifyDataSetChanged()
    }

    inner class UnAssignedViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val binding: RowItemUnassignedBinding?
        init {
            binding = DataBindingUtil.bind(v)
            v.tag = binding
        }
    }
}