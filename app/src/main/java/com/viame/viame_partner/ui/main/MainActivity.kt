package com.viame.viame_partner.ui.main

import android.annotation.SuppressLint
import android.app.Activity
import android.app.KeyguardManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.*
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.core.app.ActivityCompat
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.zxing.integration.android.IntentIntegrator
import com.pubnub.api.PubNub
import com.squareup.picasso.Picasso
import com.viame.viame_partner.BuildConfig
import com.viame.viame_partner.MIN_BATTERY_PERCENTAGE
import com.viame.viame_partner.R
import com.viame.viame_partner.Vpn
import com.viame.viame_partner.databinding.ActivityMainBinding
import com.viame.viame_partner.location.LocationCallRepository
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.rx.Scheduler
import com.viame.viame_partner.service.MyLocationBindService
import com.viame.viame_partner.ui.base.BaseActivity
import com.viame.viame_partner.ui.bookingaccept.BookingAcceptDialogFragment
import com.viame.viame_partner.ui.transfer_order.model.TransferBookings
import com.viame.viame_partner.ui.transfer_order.model.TransferOrderInputModel
import com.viame.viame_partner.utils.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONObject
import timber.log.Timber
import javax.inject.Inject
import kotlin.Boolean as Boolean1


open class MainActivity : BaseActivity<MainActivityViewModel>() , SearchView.OnQueryTextListener, AppBarConfiguration.OnNavigateUpListener,
    BookingAcceptDialogFragment.onBookingAcceptOrRejectListener
{

    var mService: Messenger? = null
    var mBound = false
    companion object
    {
        const val NOTIFICATION_ACTION = "com.viame.notification.action"
        const val NOTIFICATION_COME_FROM = "com.viame.comefrom.action"
        const val NOTIFICATION_DATA = "com.viame.data.action"
        const val NOTIFICATION_BOOKING_RESULT = "com.viame.booking.result.action"

        // Result
        //1. = Booking Accepted
        //2. = Booking Error ----> open booking
        //3. = open App ----> open booking
    }

    private var backPressedTime : Long = 0

    lateinit var appBarConfiguration: AppBarConfiguration

    @Inject
    lateinit var model : MainActivityViewModel

    lateinit var binding : ActivityMainBinding

    @Inject
    lateinit var scheduler: Scheduler

    @Inject
    lateinit var apiInterface: ApiInterface

    @Inject
    lateinit var gpsStatusListener: GpsStatusListener

    @Inject
    lateinit var pubNub: PubNub

    @Inject
    lateinit var eventBus: EventBus

    @Inject
    lateinit var repository: LocationCallRepository

    @Inject
    lateinit var connectivityReceiver : ConnectivityReceiver

    var menuItem : MenuItem? = null


    @Inject
    lateinit var batteryStatusListener: BatteryStatusListener

    private lateinit var navController : NavController


    @SuppressLint("SetTextI18n", "InvalidWakeLockTag")
    override fun onCreate(savedInstanceState: Bundle?)
    {
        try {
            super.onCreate(savedInstanceState)
            if (!Utils.isGooglePlayServicesAvailable(this@MainActivity)) {
               Toast.makeText(this, "Google Play Services is not available", Toast.LENGTH_LONG).show();
               finish();
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
                try {// Keep screen always on, unless the user interacts (wakes the mess up...)
                    setTurnScreenOn(true)
                    setShowWhenLocked(true)
                    (getSystemService(KeyguardManager::class.java) as KeyguardManager).requestDismissKeyguard(this,
                        object: KeyguardManager.KeyguardDismissCallback(){
                            override fun onDismissCancelled() {
                                Timber.tag("Keyguard").e("Cancelled")
                            }

                            override fun onDismissError() {
                                Timber.tag("Keyguard").e("Error")
                            }

                            override fun onDismissSucceeded() {
                                Timber.tag("Keyguard").e("Success")
                            }
                        }
                    )
                    window.addFlags(
                        WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON
                                        or WindowManager.LayoutParams.FLAG_LAYOUT_ATTACHED_IN_DECOR or WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR or
                                        WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                    )
                } catch (e: Exception) {
                }
            } else {
                this.window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON
                        or WindowManager.LayoutParams.FLAG_LAYOUT_ATTACHED_IN_DECOR or WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR or
                        WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN)
            }
            binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
            setSupportActionBar(binding.toolbar)

            navController = Navigation.findNavController(this,R.id.nav_host_fragment)
            // Passing each menu ID as a set of Ids because each
            // menu should be considered as top level destinations.
            appBarConfiguration = AppBarConfiguration(
                setOf(
                    R.id.nav_home,
                    R.id.nav_book_history,
                    R.id.nav_profile,
                    R.id.nav_invite,
                    R.id.nav_settings,
                    R.id.nav_notifications,
                    R.id.nav_support,
                    R.id.nav_logout
                 ), binding.drawerLayout
            )
            setupActionBarWithNavController(navController, appBarConfiguration)
            binding.navView.setupWithNavController(navController)

            binding.navView.setNavigationItemSelectedListener { menu ->
                menuItem = menu
                Handler().postDelayed({
                when(menu.itemId)
                {
                    R.id.nav_home -> navController.navigate(R.id.nav_home)

                    R.id.nav_book_history -> navController.navigate(R.id.nav_book_history)

                    R.id.nav_invite -> navController.navigate(R.id.nav_invite)

                    R.id.nav_profile -> navController.navigate(R.id.nav_profile)

                    R.id.nav_settings -> navController.navigate(R.id.nav_settings)

                    R.id.nav_notifications -> navController.navigate(R.id.nav_notifications)

                    R.id.nav_support -> navController.navigate(R.id.nav_support)

                    R.id.nav_logout -> navController.navigate(R.id.nav_logout)
                }
                }, 400L)

                //set item as selected to persist highlight
                menu.isChecked = true
                // close drawer when item is tapped
                binding.drawerLayout.closeDrawers()
                false
            }

            try {
                val headerLayout = binding.navView.getHeaderView(0) as View
                val versionTextView = headerLayout.findViewById(R.id.tvVersion) as TextView
                versionTextView.setText("Version: ${Utils.getAppVersion(this)} ${BuildConfig.FLAVOR}")

                val imageViewVehicle = headerLayout.findViewById(R.id.vehicleImage) as ImageView

                Picasso.get()
                    .load(Preferences.getPreferenceString(PrefEntity.VEHICLE_IMAGE))
                    .into(imageViewVehicle)
            } catch (e: Exception) {
            }

            pubNub.subscribe()
                .withPresence()
                .channels(listOf(Preferences.getPreferenceString(PrefEntity.PRESENCE_CHANNEL), Preferences.getPreferenceString(PrefEntity.DRIVER_CHANNEL)))
                .execute()

            if(intent.hasExtra(NOTIFICATION_COME_FROM) && intent.getStringExtra(NOTIFICATION_COME_FROM) == "Notification" && intent.hasExtra(
                    NOTIFICATION_BOOKING_RESULT) && (intent.getIntExtra(NOTIFICATION_BOOKING_RESULT,0) == 3))
            {
                Handler().postDelayed({
                    try {
                        val mBundle  = Bundle()
                        mBundle.putInt(NOTIFICATION_ACTION, intent.getIntExtra(NOTIFICATION_ACTION,0))
                        mBundle.putParcelable(NOTIFICATION_DATA, intent.getParcelableExtra(NOTIFICATION_DATA))

                        val mBookingAcceptDialogFragmenst : BookingAcceptDialogFragment = BookingAcceptDialogFragment.newInstance(monBookingAcceptOrRejectListener = this)
                        mBookingAcceptDialogFragmenst.arguments = mBundle
                        mBookingAcceptDialogFragmenst.showNow(supportFragmentManager,BookingAcceptDialogFragment.TAG)
                    } catch (e: Exception) {
                    }
                },1000)
            }

            model.TransferOrderQRCodeAPIDataResult().observe(this@MainActivity, Observer { result ->

                if (result != null) {

                    if (result.status == APIRequestResponseHandler.AuthStatus.LOADING) {
                        Alerts.showProgressBar(this@MainActivity)

                    } else if (result.status == APIRequestResponseHandler.AuthStatus.ERROR) {
                        Alerts.dismissProgressBar()
                        Alerts.showSnackBar(this@MainActivity, result.message)

                    } else if (result.status == APIRequestResponseHandler.AuthStatus.SUCCESS) {
                        Alerts.dismissProgressBar()

                        if (result.data.success) {

                            Alerts.showBottomSheetSimpleConfirmationDialog(this@MainActivity,
                                getString(R.string.alert),
                                "That booking is transferred to you", true, null,
                                getString(R.string.ok), object : Alerts.OnConfirmationDialog {
                                    @SuppressLint("CheckResult")
                                    override fun onYes() {

                                        val intent = Intent(this@MainActivity, MainActivity::class.java)
                                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                        startActivity(intent)
                                        this@MainActivity.finish()
                                    }
                                    override fun onNo() {

                                    }
                                })
                        }
                        else
                        {
                            Alerts.showSnackBar(this@MainActivity, result.data.data)
                        }
                    }
                }
            })

            Preferences.setPreference(PrefEntity.IS_MAIN_ACTIVITY_IN_FOREGROUND, true)
        } catch (e: Exception) {
        }
    }


    /** Defines callbacks for service binding, passed to bindService()  */
    private val connection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(
            className: ComponentName,
            service: IBinder
        ) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            mService = Messenger(service)
            mBound = true
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            mBound = false
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unbindService(connection)
        mBound = false
    }

    override fun onBookingAccepted() {
        try {
            val intent = Intent(this@MainActivity, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            this@MainActivity.finish()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onBookingRejected() {

    }

    override fun onBookingFailedToAccept() {

    }

    override fun onBookingTimerFinished() {

    }

    override fun onResume() {
        super.onResume()

        eventBus.post("profileUpdate")
        Preferences.setPreference(PrefEntity.IS_MAIN_ACTIVITY_IN_FOREGROUND, true)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean1 {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)

        val searchItem = menu.findItem(R.id.action_search)
        val searchView : SearchView = searchItem.getActionView() as SearchView
        searchView.setQueryHint("Search Booking")
        searchView.setOnQueryTextListener(this@MainActivity)
        searchView.setIconified(false)

        return true
    }

    override fun onSupportNavigateUp(): kotlin.Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun getViewModel(): MainActivityViewModel? {
        return model
    }

    fun onStartScanAction()
    {
        val integrator = IntentIntegrator(this@MainActivity)
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE) // optional
        integrator.setCameraId(0)  // Use a specific camera of the device
        integrator.setOrientationLocked(true) // allow barcode scanner in potrait mode
        integrator.initiateScan()
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {

            if (navController.getCurrentDestination()!!.getId() != R.id.nav_home)
            {
                navController.navigate(R.id.nav_home)
            }
            else
            {
               if (backPressedTime + 2000 > System.currentTimeMillis())
               {
                   super.onBackPressed()
                   ActivityCompat.finishAffinity(this)
               }
               else
               {
                  Alerts.showSnackBar(this@MainActivity, resources.getString(R.string.double_press_exit))
               }
               backPressedTime = System.currentTimeMillis()
            }
        }
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        if(resultCode == Activity.RESULT_OK && requestCode == 1002)
        {
            Handler().postDelayed({

                eventBus.post("bookingUpdate")

            },1500)
        }

        val result =
            IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            val value = result.contents
            if (value != null) {

                Timber.e("qrcode = %s", value)

                val jsonObject = JSONObject(value)

                Alerts.showBottomSheetSimpleConfirmationDialog(this@MainActivity,
                    getString(R.string.alert),
                    "Are you sure, you want ${jsonObject["current_assigned_driver"]}'s booking?\nBooking Id: ${jsonObject["bookingId"]}", false, getString(R.string.no),
                    getString(R.string.yes), object : Alerts.OnConfirmationDialog {
                        @SuppressLint("CheckResult")
                        override fun onYes() {

                            if(jsonObject["driver_id"].toString().isNotEmpty())
                            {
                                if(Utils.isNetworkAvailable(this@MainActivity))
                                {
                                    val transferBookings  = TransferBookings()

                                    transferBookings.driverId = Preferences.getPreferenceString(PrefEntity.MID)
                                    transferBookings.orderId = jsonObject["bookingId"].toString().toLong()

                                    val transferOrderInputModel = TransferOrderInputModel()
                                    transferOrderInputModel.transferBookings = transferBookings

                                    model.TransferOrderQRCodeAPICall(apiInterface,transferOrderInputModel)

                                }
                                else
                                {
                                    Alerts.showSnackBar(this@MainActivity,getString(R.string.internet_not_available))
                                }
                            }
                            else
                            {
                                Alerts.showSnackBar(this@MainActivity, getString(R.string.select_driver_for_transfer))
                            }

                        }
                        override fun onNo() {

                        }
                    })



            } else {
                Alerts.showSnackBar(this,getString(R.string.qr_code_failed))
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }


    @SuppressLint("SetTextI18n")
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onProfileUpdate(result: String) {
        (binding.navView.getHeaderView(0).findViewById(R.id.tvDriverName) as TextView).text =
            "Hi ${Preferences.getPreferenceString(PrefEntity.USER_NAME)}"

        if(Preferences.getPreferenceString(PrefEntity.EMAIL)!!.isNotEmpty())
        (binding.navView.getHeaderView(0).findViewById(R.id.tvDriverEmail) as TextView).text = Preferences.getPreferenceString(PrefEntity.EMAIL)

        Picasso.get()
            .load(Preferences.getPreferenceString(PrefEntity.PROFILE_PIC))
            .placeholder(R.drawable.ic_pic_register_background_)
            .error(R.drawable.ic_pic_register_background_)
            .into((binding.navView.getHeaderView(0).findViewById(R.id.imgDriverProfile) as ImageView))
    }

    public override fun onStart() {
        super.onStart()
        // Bind to LocalService
        val intent = Intent(this, MyLocationBindService::class.java)
        //startService(intent);
        bindService(intent, connection, Context.BIND_AUTO_CREATE)

        model.getConfigAPIResult().observe(this, Observer { result ->

            if(result != null)
            {
                if (result.status == APIRequestResponseHandler.AuthStatus.SUCCESS)
                {
                    if (result.data.success)
                    {
                        Preferences.setPreference(
                            PrefEntity.MIN_DIST_FOR_ROUTE_ARRRAY,
                            "${result.data.data.presenceSettings.distanceForLogingLatLongs}"
                        )
                        Preferences.setPreference(
                            PrefEntity.CURRENCY_SYMBOL,
                            result.data.data.currencySymbol
                        )
                        Preferences.setPreference(
                            PrefEntity.MILEAGE_METRIC_SYMBOL,
                            result.data.data.mileageMetric
                        )
                        Preferences.setPreference(
                            PrefEntity.PRESENCE_INTERVAL_TIME,
                            "${result.data.data.presenceSettings.presenceTime}"
                        )

                        Preferences.setPreference(
                            PrefEntity.PUB_NUB_SCHEDULE_INTERVAL_TIME,
                            "0"
                        )

                        if (result.data.data.mileageMetric.isNotEmpty() && result.data.data.mileageMetric.equals("Km", true))
                        {
                            Preferences.setPreference(PrefEntity.DISTANCE_CONVERSION_UNIT, "0.001")
                        } else {
                            Preferences.setPreference(
                                PrefEntity.DISTANCE_CONVERSION_UNIT,
                                "0.00062137"
                            )
                        }

                        Preferences.setPreference(PrefEntity.IS_WALLET_ENABLE , result.data.data.walletSettings.walletDriverEnable)
                        if (result.data.data.walletSettings.walletDriverEnable)
                        {
                            Preferences.setPreference(PrefEntity.WALLET_AMOUNT, result.data.data.walletSettings.hardLimitDriver.toString())
                            Preferences.setPreference(PrefEntity.SOFT_LIMIT, result.data.data.walletSettings.softLimitDriver.toString())
                            Preferences.setPreference(PrefEntity.HARD_LIMIT, result.data.data.walletSettings.hardLimitDriver.toString())
                        }

//                        if(result.data.data. != null && result.data.data.appVersion!!.isNotEmpty())
//                        {
//                            checkVersion(result.data.data.appVersion, result.data.data.mandatory)
//                        }
                    }
                    else
                    {
                        Alerts.showSnackBar(this,result.message)
                    }
                }
            }
        })

        connectivityReceiver.observe(this@MainActivity, Observer { connectionModel ->

            if (connectionModel != null && connectionModel.isConnected())
            {
                pubNub.subscribe()
                    .withPresence()
                    .channels(listOf(Preferences.getPreferenceString(PrefEntity.PRESENCE_CHANNEL), Preferences.getPreferenceString(PrefEntity.DRIVER_CHANNEL)))
                    .execute()
                binding.internetLayout.root.visibility = View.GONE
                model.callConfigAPI(apiInterface)
            }
            else
            {
                pubNub.unsubscribe()
                binding.internetLayout.root.visibility = View.VISIBLE
                if(connectionModel?.type == Vpn) {
                    binding.internetLayout.tvInternetMsg.text = getString(R.string.vpn_use_message)
                    Toast.makeText(
                        this@MainActivity,
                        getString(R.string.vpn_use_message),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })

        gpsStatusListener.observe(this@MainActivity, Observer { gpsStatus ->
            when (gpsStatus) {
                is GpsStatus.Enabled ->
                {
                    binding.gpsLayout.root.visibility = View.GONE
                    Preferences.setPreference(PrefEntity.GPS_STATUS, true)
                }
                is GpsStatus.Disabled ->
                {
                    Preferences.setPreference(PrefEntity.GPS_STATUS, false)
                    binding.gpsLayout.root.visibility = View.VISIBLE
                }
            }
        })

        batteryStatusListener.observe(this@MainActivity, Observer { batteyInfo ->
            if(batteyInfo != null)
            {
                Preferences.setPreferenceInt(PrefEntity.BATTERY_PERCENTAGE,batteyInfo.percent)
                if(batteyInfo.percent < MIN_BATTERY_PERCENTAGE)
                {
                    Alerts.showBottomSheetSimpleConfirmationDialog(this@MainActivity,
                        getString(R.string.alert),
                        getString(R.string.battery_alert), false, getString(R.string.action_cancel),
                        getString(R.string.ok), object : Alerts.OnConfirmationDialog {
                            override fun onYes() {
                            }
                            override fun onNo() {
                            }
                      })
                }
            }
        })

        try {
            eventBus.register(this)
        } catch (e: Exception) {
        }
    }

    public override fun onPause() {
        super.onPause()
        try {
            Preferences.setPreference(PrefEntity.IS_MAIN_ACTIVITY_IN_FOREGROUND, false)
            eventBus.unregister(this)
        } catch (e: Exception) {
        }
    }

    override fun onQueryTextSubmit(query: String?): Boolean1 {
        Toast.makeText(this, "Search $query", Toast.LENGTH_SHORT).show()
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean1 {
        return true
    }


}

