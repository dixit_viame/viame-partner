package com.viame.viame_partner.ui.transfer_order.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransferBookings {

    @SerializedName("driver_id")
    @Expose
    private String driverId;
    @SerializedName("order_id")
    @Expose
    private long orderId;

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

}
