package com.viame.viame_partner.ui.bookingcomplete

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.viame.viame_partner.USER_TYPE
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.bookingdetail.details.model.BookingStatusInputModel
import com.viame.viame_partner.ui.bookingdetail.details.model.BookingStatusOutputModel
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class BookingCompletedActivityViewModel @Inject constructor() : ViewModel()
{

    private val BookingStatusApiResponse = MutableLiveData<APIRequestResponseHandler<BookingStatusOutputModel>>()

    fun getBookingStatusAPIDataResult(): MutableLiveData<APIRequestResponseHandler<BookingStatusOutputModel>> = BookingStatusApiResponse


    fun BookingStatusAPICall(apiInterface : ApiInterface, bookingStatusInputModel: BookingStatusInputModel)
    {
        BookingStatusApiResponse.value = APIRequestResponseHandler.loading(null)

        val headerMap = HashMap<String,String>();
        headerMap.put("Content-Type", "application/json;charset=UTF-8");
        headerMap.put("authorization", Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN));
        headerMap.put("token",  Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN));
        headerMap.put("userType", USER_TYPE.toString());

        val posts: Call<BookingStatusOutputModel> = apiInterface.callBookingStatusMethod(headerMap,bookingStatusInputModel)!!
        //Enqueue the call
        posts.enqueue(object : Callback<BookingStatusOutputModel>
        {
            @SuppressLint("TimberArgCount")
            override fun onFailure(call: Call<BookingStatusOutputModel>, t: Throwable) {

                try {
                    var myOrdersViewModel = BookingStatusOutputModel()
                    myOrdersViewModel.errNum = 500
                    myOrdersViewModel.data = null
                    myOrdersViewModel.errFlag = 1
                    myOrdersViewModel.errMsg = t.message

                    BookingStatusApiResponse.value = APIRequestResponseHandler.error(t.message.toString(),myOrdersViewModel)
                    Timber.e(t.message.toString())
                } catch (e: Exception) {
                }
            }

            @SuppressLint("TimberArgCount")
            override fun onResponse(call: Call<BookingStatusOutputModel>, response: Response<BookingStatusOutputModel>
            ) {
                try {
                    if(response.isSuccessful) {
                        response.body().let {
                            BookingStatusApiResponse.value = APIRequestResponseHandler.success(it)
                            Timber.e(response.body().toString())
                        }
                    }
                    else
                    {
                        try {
                            var jsonObj = JSONObject(response.errorBody().toString())

                            var myOrdersViewModel = BookingStatusOutputModel()
                            if (response.code() == 403)
                            {
                                myOrdersViewModel.errNum = 403
                                myOrdersViewModel.data = null
                                myOrdersViewModel.errFlag = 1
                                myOrdersViewModel.errMsg = "UnAuthorised User"
                            }
                            else
                            {
                                myOrdersViewModel.data = null
                                myOrdersViewModel.errNum = jsonObj["errNum"] as Int?
                                myOrdersViewModel.errFlag = jsonObj["errFlag"] as Int?
                                myOrdersViewModel.errMsg = jsonObj["errMsg"] as String?
                            }
                            BookingStatusApiResponse.value = APIRequestResponseHandler.error("Status code "+response.code(), myOrdersViewModel)
                        } catch (e: Exception) {
                            e.printStackTrace()
                            BookingStatusApiResponse.value = APIRequestResponseHandler.error("Status code "+response.code(),null)
                        }
                    }

                } catch (e: Exception)
                {
                    e.printStackTrace()

                    var myOrdersViewModel = BookingStatusOutputModel()
                        myOrdersViewModel.errNum = 500
                        myOrdersViewModel.data = null
                        myOrdersViewModel.errFlag = 1
                        myOrdersViewModel.errMsg = "Something went wrong"

                    BookingStatusApiResponse.value = APIRequestResponseHandler.error("Status code "+response.code(),myOrdersViewModel)
                }
            }
        })
    }

}