package com.viame.viame_partner.ui.bookingcomplete;

import dagger.Module;
import dagger.Provides;

@Module
public class BookingCompletedActivityModule {

    @Provides
    BookingCompletedActivityViewModel provideBookingCompletedActivityViewModel(){
        return new BookingCompletedActivityViewModel();
    }

}