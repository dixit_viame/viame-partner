package com.viame.viame_partner.ui.unassigned.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TimelineActivityModel implements Serializable {


    @SerializedName("bid")
    @Expose
    private long bid;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("time")
    @Expose
    private long time;
    @SerializedName("by")
    @Expose
    private String by;
    @SerializedName("timeISO")
    @Expose
    private long timeISO;

    public long getTimeISO() {
        return timeISO;
    }

    public void setTimeISO(long timeISO) {
        this.timeISO = timeISO;
    }

    public long getBid() {
        return bid;
    }

    public void setBid(long bid) {
        this.bid = bid;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getBy() {
        return by;
    }

    public void setBy(String by) {
        this.by = by;
    }
}
