package com.viame.viame_partner.ui.bookingdetail.details;

import com.viame.viame_partner.ui.bookingdetail.timeline.TimelinePagerFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class DetailPagerFragmentProvider {

    @ContributesAndroidInjector
    abstract DetailPagerFragment contributeDetailPagerFragment();
}
