package com.viame.viame_partner.ui.transfer_order.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.viame.viame_partner.ui.myorders.model.Location;

public class BookingTransferFirebaseModel {

    @SerializedName("drop")
    @Expose
    private Location drop;
    @SerializedName("driver_name")
    @Expose
    private String driverName;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("driver_id")
    @Expose
    private String driverId;
    @SerializedName("transfer_text")
    @Expose
    private String transferText;
    @SerializedName("total_amount")
    @Expose
    private String totalAmount;
    @SerializedName("drop_address")
    @Expose
    private String dropAddress;
    @SerializedName("pickup")
    @Expose
    private Location pickup;
    @SerializedName("pickup_address")
    @Expose
    private String pickupAddress;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("bid")
    @Expose
    private long bid;
    @SerializedName("booking_timestamp")
    @Expose
    private long bookingTimestamp;

    public Location getDrop() {
        return drop;
    }

    public void setDrop(Location drop) {
        this.drop = drop;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getTransferText() {
        return transferText;
    }

    public void setTransferText(String transferText) {
        this.transferText = transferText;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getDropAddress() {
        return dropAddress;
    }

    public void setDropAddress(String dropAddress) {
        this.dropAddress = dropAddress;
    }

    public Location getPickup() {
        return pickup;
    }

    public void setPickup(Location pickup) {
        this.pickup = pickup;
    }

    public String getPickupAddress() {
        return pickupAddress;
    }

    public void setPickupAddress(String pickupAddress) {
        this.pickupAddress = pickupAddress;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public long getBid() {
        return bid;
    }

    public void setBid(long bid) {
        this.bid = bid;
    }

    public long getBookingTimestamp() {
        return bookingTimestamp;
    }

    public void setBookingTimestamp(long bookingTimestamp) {
        this.bookingTimestamp = bookingTimestamp;
    }

}
