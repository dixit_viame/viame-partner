package com.viame.viame_partner.ui.logout

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.viame.viame_partner.BASE_ENGINE_URL
import com.viame.viame_partner.LOGOUT_URL
import com.viame.viame_partner.R
import com.viame.viame_partner.USER_TYPE
import com.viame.viame_partner.application.CatalogApplication
import com.viame.viame_partner.location.LocationCallRepository
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.logout.model.LogoutResponseModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.HashMap
import javax.inject.Inject

class LogoutViewModel @Inject constructor()  : ViewModel() {

    @Inject
    lateinit var repository: LocationCallRepository

    private val getLogoutApiResponse = MutableLiveData<APIRequestResponseHandler<LogoutResponseModel>>()

    fun getLogoutAPIResult(): MutableLiveData<APIRequestResponseHandler<LogoutResponseModel>> = getLogoutApiResponse

    fun callLogoutAPI(apiInterface : ApiInterface) {

        getLogoutApiResponse.value = APIRequestResponseHandler.loading(null)

        val headerMap = HashMap<String,String>();
        headerMap["Content-Type"] = "application/json;charset=UTF-8";
        headerMap["authorization"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN);
        headerMap["token"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN);
        headerMap["userType"] = USER_TYPE.toString();

        val posts: Call<LogoutResponseModel> = apiInterface.callLogoutMethod(BASE_ENGINE_URL+LOGOUT_URL,headerMap)

        //Enqueue the call
        posts.enqueue(object : Callback<LogoutResponseModel> {
            @SuppressLint("TimberArgCount")
            override fun onFailure(call: Call<LogoutResponseModel>, t: Throwable) {
                getLogoutApiResponse.value = APIRequestResponseHandler.error(t.message.toString(),null)
                Timber.e(t.message.toString())
            }

            @SuppressLint("TimberArgCount")
            override fun onResponse(call: Call<LogoutResponseModel>, response: Response<LogoutResponseModel>) =
                try {
                    if(response.isSuccessful) {

                        val mLogoutResponseModel = response.body()
                        if(mLogoutResponseModel?.success!!)
                        {
                            getLogoutApiResponse.value = APIRequestResponseHandler.success(mLogoutResponseModel)
                        }
                        else
                        {
                            getLogoutApiResponse.value = APIRequestResponseHandler.error(mLogoutResponseModel.message,mLogoutResponseModel)
                        }

                        Timber.e(response.body().toString())
                    } else {
                        getLogoutApiResponse.value = APIRequestResponseHandler.error("Status code "+response.code(),null)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()

                    getLogoutApiResponse.value = APIRequestResponseHandler.error(
                        CatalogApplication.instance?.getString(
                            R.string.something_goes_wrong),null)
                }
        })
    }
}
