package com.viame.viame_partner.ui.vehicle.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VehicleListDataModel {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("modelId")
    @Expose
    private String modelId;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("makeId")
    @Expose
    private String makeId;
    @SerializedName("make")
    @Expose
    private String make;
    @SerializedName("platNo")
    @Expose
    private String platNo;
    @SerializedName("account_type")
    @Expose
    private Integer accountType;
    @SerializedName("mas_id")
    @Expose
    private String masId;
    @SerializedName("type_id")
    @Expose
    private String typeId;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("licence_numer")
    @Expose
    private String licenceNumer;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMakeId() {
        return makeId;
    }

    public void setMakeId(String makeId) {
        this.makeId = makeId;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getPlatNo() {
        return platNo;
    }

    public void setPlatNo(String platNo) {
        this.platNo = platNo;
    }

    public Integer getAccountType() {
        return accountType;
    }

    public void setAccountType(Integer accountType) {
        this.accountType = accountType;
    }

    public String getMasId() {
        return masId;
    }

    public void setMasId(String masId) {
        this.masId = masId;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLicenceNumer() {
        return licenceNumer;
    }

    public void setLicenceNumer(String licenceNumer) {
        this.licenceNumer = licenceNumer;
    }

}
