package com.viame.viame_partner.ui.pickupdrop;
import androidx.lifecycle.ViewModelProvider;
import com.viame.viame_partner.utils.ViewModelProviderFactory;

import dagger.Module;
import dagger.Provides;

@Module
public class PickupDropActivityModule {

    @Provides
    PickupDropViewModel providesPickupDropViewModel(){
        return new PickupDropViewModel();
    }

    @Provides
    ViewModelProvider.Factory provideViewModelProvider(PickupDropViewModel viewModel){
        return new ViewModelProviderFactory<>(viewModel);
    }
}
