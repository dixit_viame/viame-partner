package com.viame.viame_partner.ui.bookingaccept

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.Dialog
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.NotificationManagerCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.viame.viame_partner.MBStatusType
import com.viame.viame_partner.R
import com.viame.viame_partner.Vpn
import com.viame.viame_partner.databinding.FragmentBookingAcceptDialogBinding
import com.viame.viame_partner.location.LocationCallRepository
import com.viame.viame_partner.location.vibratePhone
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.rx.Scheduler
import com.viame.viame_partner.ui.base.BaseDialogFragment
import com.viame.viame_partner.ui.bookingaccept.model.BookingAcceptInput
import com.viame.viame_partner.ui.bookingaccept.model.BookingData
import com.viame.viame_partner.ui.main.MainActivity
import com.viame.viame_partner.utils.Alerts
import com.viame.viame_partner.utils.ConnectivityReceiver
import com.viame.viame_partner.utils.Utils
import com.viame.viame_partner.widgets.countdownview.countdownview.CountDownListener
import timber.log.Timber
import javax.inject.Inject


class BookingAcceptDialogFragment constructor(mListener : onBookingAcceptOrRejectListener): BaseDialogFragment<BookingAcceptDialogViewModel>(),
    CountDownListener {

    interface onBookingAcceptOrRejectListener
    {
        fun onBookingAccepted()
        fun onBookingRejected()
        fun onBookingFailedToAccept()
        fun onBookingTimerFinished()
    }

    private lateinit var mediaPlayer: MediaPlayer

    var monBookingAcceptOrRejectListener: onBookingAcceptOrRejectListener = mListener

    companion object {
        val TAG: String? = BookingAcceptDialogFragment.toString()

        fun newInstance(monBookingAcceptOrRejectListener : onBookingAcceptOrRejectListener) = BookingAcceptDialogFragment(monBookingAcceptOrRejectListener)
    }

    private var WAVE_LOADING_TIMESTAMP : Long = 100
    private var CIRCULAR_PROGRESS_MAX : Float = 1F
    private var COUNTDOWN_VIEW_TIMESTAMP : Long = 1000


    private var notificationId: Int? = 0

    private lateinit var binding: FragmentBookingAcceptDialogBinding

    @Inject
    lateinit var apiInterface : ApiInterface

    @Inject
    lateinit var connectivityReceiver: ConnectivityReceiver

    @Inject
    lateinit var models : BookingAcceptDialogViewModel

    lateinit var bookingData: BookingData

    @Inject
    lateinit var repository : LocationCallRepository

   // @Inject
   // lateinit var mediaPlayer: MediaPlayer



    @Inject
    lateinit var scheduler : Scheduler


    override fun getViewModel(): BookingAcceptDialogViewModel {
        return models
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        if(arguments?.get(MainActivity.NOTIFICATION_ACTION) != null)
        {
            bookingData  = arguments?.getParcelable<BookingData>(MainActivity.NOTIFICATION_DATA) as BookingData
            notificationId  = arguments?.getInt(MainActivity.NOTIFICATION_ACTION)

            activity?.let {
                vibratePhone(it,0)

               // var manager = it.getSystemService(Context.AUDIO_SERVICE) as AudioManager
               // manager.setStreamVolume(AudioManager.STREAM_MUSIC, manager.getStreamMaxVolume(AudioManager.STREAM_RING), 0);
            }
        }

         if(bookingData.expiryTimer.toInt() >0 ) {
             WAVE_LOADING_TIMESTAMP *= bookingData.expiryTimer.toInt()
             CIRCULAR_PROGRESS_MAX *= bookingData.expiryTimer.toFloat()
             COUNTDOWN_VIEW_TIMESTAMP *= bookingData.expiryTimer.toInt()
         }
        else
         {
             WAVE_LOADING_TIMESTAMP = 3000
             CIRCULAR_PROGRESS_MAX = 30F
             COUNTDOWN_VIEW_TIMESTAMP = 30000
         }

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_booking_accept_dialog,container,false)
        binding.viewModel = bookingData
        binding.executePendingBindings()

        binding.tvClientName.setText(bookingData.clientName)
        binding.tvBookingType.setText(bookingData.paymentType)
        binding.tvBid.text = "${bookingData.bid}"

        binding.tvBookingAmount.text = "${Preferences.getPreferenceString(PrefEntity.CURRENCY_SYMBOL)} ${bookingData.amount}"
        binding.tvPickupAddress.labelTextView.text = bookingData.pickupAdr
        binding.tvDropoffAddress.labelTextView.text = bookingData.dropAdr

        binding.waveLoadingView.setAnimDuration(WAVE_LOADING_TIMESTAMP)
        binding.countDownView.setStartDuration(COUNTDOWN_VIEW_TIMESTAMP)
        binding.countDownView.start()

        try {
            mediaPlayer = MediaPlayer.create(activity,R.raw.viame_bass)
            mediaPlayer .setLooping(true)
            mediaPlayer.start()
        } catch (e : Exception)
        {   e.printStackTrace()
            Timber.e ( "Error setting data source", e )
        }

        binding.countDownView.setListener(this)
        binding.circularProgress.progress = 0F
        binding.circularProgress.progressMax = CIRCULAR_PROGRESS_MAX
        binding.circularProgress.setProgressWithAnimation(CIRCULAR_PROGRESS_MAX,COUNTDOWN_VIEW_TIMESTAMP)

        binding.MainLoadingView.setOnTouchListener { v, event ->
            when (event?.action) {
                MotionEvent.ACTION_DOWN ->
                {
                    try {
                        try {
                            mediaPlayer.stop()
                            binding.countDownView.stop()
                        } catch (e: Exception) {
                        }
                        binding.circularProgress.indeterminateMode = true

                        connectivityReceiver.observe(this@BookingAcceptDialogFragment, Observer { connectionModel ->

                            if (connectionModel != null && connectionModel.isConnected())
                            {
                                apiInterface.let {

                                    models.getBookingAcceptAPIResult().observe(this, Observer { viewResponse ->

                                        if(viewResponse != null && viewResponse.status == APIRequestResponseHandler.AuthStatus.SUCCESS)
                                        {
                                            Alerts.dismissProgressBar()
                                            val notificationManagerCompat = NotificationManagerCompat.from(context!!)
                                            notificationManagerCompat.cancel(this.notificationId!!.toInt())

                                            val manager = activity?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                                            manager.cancel(notificationId!!)

                                            Toast.makeText(binding.root.context ,"Booking Accepted!", Toast.LENGTH_LONG).show()

                                            monBookingAcceptOrRejectListener.onBookingAccepted()
                                            this@BookingAcceptDialogFragment.dismiss()
                                        }
                                        else  if(viewResponse != null && viewResponse.status == APIRequestResponseHandler.AuthStatus.LOADING)
                                        {
                                            Alerts.showProgressBar(context)
                                        }
                                        else  if(viewResponse != null && viewResponse.status == APIRequestResponseHandler.AuthStatus.ERROR)
                                        {
                                            Alerts.dismissProgressBar()

                                            monBookingAcceptOrRejectListener.onBookingFailedToAccept()
                                            this@BookingAcceptDialogFragment.dismiss()

                                            Toast.makeText(binding.root.context , viewResponse.message, Toast.LENGTH_LONG).show()
                                        }
                                        else
                                        {
                                            Alerts.showSnackBar(activity,viewResponse.message)
                                        }
                                    })
                                                var mbookingAcceptInput = BookingAcceptInput()
                                                mbookingAcceptInput.bid = "${bookingData.bid}"
                                                mbookingAcceptInput.entStatus = MBStatusType.ON_THE_WAY.id.toString()

                                                mbookingAcceptInput.lattitude = Preferences.getPreferenceString(PrefEntity.LATITUDE_CURRENT).toDouble()
                                                mbookingAcceptInput.longitude = Preferences.getPreferenceString(PrefEntity.LONGITUDE_CURRENT).toDouble()

                                                models.callBookingAcceptAPI(apiInterface,mbookingAcceptInput)
                                    }
                            }
                            else
                            {
                                if(connectionModel?.type == Vpn)
                                {
                                    Alerts.showSnackBar(activity,getString(R.string.vpn_use_message))
                                }
                                else
                                {
                                    Alerts.showSnackBar(activity,getString(R.string.internet_not_available))
                                }
                            }
                        })
                    } catch (e: Exception) {
                        e.printStackTrace()
                        monBookingAcceptOrRejectListener.onBookingFailedToAccept()
                        this@BookingAcceptDialogFragment.dismiss()
                    }
                }
            }
            v?.onTouchEvent(event) ?: true
        }

        binding.btnejectOrder.setOnClickListener {

            Toast.makeText(binding.root.context ,"Booking Rejected!!", Toast.LENGTH_LONG).show()

            try {
                mediaPlayer.stop()
                binding.countDownView.stop()
            } catch (e: Exception) {
            }
            connectivityReceiver.observe(this@BookingAcceptDialogFragment, Observer { connectionModel ->

                if (connectionModel != null && connectionModel.isConnected())
                {
                    apiInterface.let {

                        models.getBookingAcceptAPIResult().observe(this, Observer { viewResponse ->

                            if(viewResponse != null && viewResponse.status == APIRequestResponseHandler.AuthStatus.SUCCESS)
                            {
                                Alerts.dismissProgressBar()
                                val notificationManagerCompat = NotificationManagerCompat.from(context!!)
                                notificationManagerCompat.cancel(this.notificationId!!.toInt())

                                val manager = activity?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                                manager.cancel(notificationId!!)

                                monBookingAcceptOrRejectListener.onBookingRejected()
                                this@BookingAcceptDialogFragment.dismiss()
                            }
                            else  if(viewResponse != null && viewResponse.status == APIRequestResponseHandler.AuthStatus.LOADING)
                            {
                                Alerts.showProgressBar(context)
                            }
                            else  if(viewResponse != null && viewResponse.status == APIRequestResponseHandler.AuthStatus.ERROR)
                            {
                                Alerts.dismissProgressBar()

                                monBookingAcceptOrRejectListener.onBookingFailedToAccept()
                                this@BookingAcceptDialogFragment.dismiss()

                                Toast.makeText(binding.root.context , viewResponse.message, Toast.LENGTH_LONG).show()
                            }
                            else
                            {
                                Alerts.showSnackBar(activity,viewResponse.message)
                            }
                        })
                        var mbookingAcceptInput = BookingAcceptInput()
                        mbookingAcceptInput.bid = "${bookingData.bid}"
                        mbookingAcceptInput.entStatus = MBStatusType.REJECTED_BY_CUSTOMER.id.toString()

                        mbookingAcceptInput.lattitude = Preferences.getPreferenceString(PrefEntity.LATITUDE_CURRENT).toDouble()
                        mbookingAcceptInput.longitude = Preferences.getPreferenceString(PrefEntity.LONGITUDE_CURRENT).toDouble()

                        models.callBookingAcceptAPI(apiInterface,mbookingAcceptInput)
                    }
                }
                else
                {
                    if(connectionModel?.type == Vpn)
                    {
                        Alerts.showSnackBar(activity,getString(R.string.vpn_use_message))
                    }
                    else
                    {
                        Alerts.showSnackBar(activity,getString(R.string.internet_not_available))
                    }
                }
            })
        }
        return binding.root
    }

    override fun onPause() {
        super.onPause()
        try {
            mediaPlayer.stop()
            binding.countDownView.stop()
        } catch (e: Exception) {
        }
    }

    override fun onLowMemory() {
        super.onLowMemory()
        try {
            mediaPlayer.stop()
            binding.countDownView.stop()
        } catch (e: Exception) {
        }
    }

    override fun onStart() {
        super.onStart()
        val dialog: Dialog? = dialog
        if (dialog != null) {
            dialog.setCancelable(false)
            dialog.setCanceledOnTouchOutside(false)
            dialog.getWindow()?.setLayout(Utils.getScreenWidth()-150, Utils.getScreenHeight()-120)
            dialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    override fun onFinishCountDown() {
        try {
            Toast.makeText(binding.root.context ,"Booking Timer Finished!!", Toast.LENGTH_LONG).show()
            this@BookingAcceptDialogFragment.dismiss()
            monBookingAcceptOrRejectListener.onBookingTimerFinished()
        } catch (e: Exception) {
        }
    }

    fun BookingAcceptDialogFragment()
    {

    }

}
