package com.viame.viame_partner.ui.transfer_order.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransferOrderInputModel {


    @SerializedName("transfer_bookings")
    @Expose
    private TransferBookings transferBookings;

    public TransferBookings getTransferBookings() {
        return transferBookings;
    }

    public void setTransferBookings(TransferBookings transferBookings) {
        this.transferBookings = transferBookings;
    }

}
