package com.viame.viame_partner.ui.jobhistory.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobHistoryNestedData {

    @SerializedName("appointments")
    @Expose
    private JobHistoryData mJobHistoryData;

    public JobHistoryData getmJobHistoryData() {
        return mJobHistoryData;
    }

    public void setmJobHistoryData(JobHistoryData mJobHistoryData) {
        this.mJobHistoryData = mJobHistoryData;
    }
}
