package com.viame.viame_partner.ui.splash;

import androidx.lifecycle.ViewModelProvider;

import com.viame.viame_partner.models.MainAppStore;
import com.viame.viame_partner.utils.ViewModelProviderFactory;

import dagger.Module;
import dagger.Provides;

@Module
public class SplashScreenActivityModule {

    @Provides
    SplashScreenViewModel providesViewModel(MainAppStore store){
        return new SplashScreenViewModel(store);
    }

    @Provides
    ViewModelProvider.Factory provideViewModelProvider(SplashScreenViewModel viewModel){
        return new ViewModelProviderFactory<>(viewModel);
    }
}
