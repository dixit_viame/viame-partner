package com.viame.viame_partner.ui.jobhistory;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class JobHistoryFragmentProvider {

    @ContributesAndroidInjector
    abstract JobHistoryFragment contributeJobHistoryFragment();
}
