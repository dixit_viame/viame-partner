package com.viame.viame_partner.ui.bookingaccept.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookingDataResponse {

    @SerializedName("bookingData")
    @Expose
    private BookingData bookingData;

    public BookingData getBookingData() {
        return bookingData;
    }

    public void setBookingData(BookingData bookingData) {
        this.bookingData = bookingData;
    }
}
