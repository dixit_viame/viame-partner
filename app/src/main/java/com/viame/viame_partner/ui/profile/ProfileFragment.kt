package com.viame.viame_partner.ui.profile

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.net.toFile
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.squareup.picasso.Picasso
import com.viame.viame_partner.*
import com.viame.viame_partner.application.CatalogApplication
import com.viame.viame_partner.aws.AWSEntity
import com.viame.viame_partner.aws.AWSRepository
import com.viame.viame_partner.databinding.FragmentProfileBinding
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.base.BaseFragment
import com.viame.viame_partner.ui.profile.model.DriversProfileDataModel
import com.viame.viame_partner.ui.profile.model.NewProfileDataModel
import com.viame.viame_partner.ui.profile.model.ProfileData
import com.viame.viame_partner.utils.*
import com.viame.viame_partner.utils.permission.annotation.PermissionDenied
import com.viame.viame_partner.utils.permission.annotation.PermissionGranted
import com.yalantis.ucrop.UCrop
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_home.*
import org.greenrobot.eventbus.EventBus
import timber.log.Timber
import javax.inject.Inject


class ProfileFragment : BaseFragment<ProfileViewModel>(),ProfileEventHandler,TextWatcher {

    companion object {
        fun newInstance() = ProfileFragment()
    }

    private lateinit var binding: FragmentProfileBinding

    @Inject
    lateinit var model : ProfileViewModel

    @Inject
    lateinit var eventBus: EventBus

    @Inject
    lateinit var apiInterface: ApiInterface

    @Inject
    lateinit var connectivityReceiver: ConnectivityReceiver

    @Inject
    lateinit var sAWSRepository: AWSRepository

    lateinit var mHandlePictureEvents : HandlePictureEvents

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
        mHandlePictureEvents = HandlePictureEvents(activity,this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_profile,container,false)
        binding.clickHandler = this

        binding.tilFirstName.isEnabled = true
        binding.tilMobileNumber.isEnabled = true
        binding.tilPassword.isEnabled = true

        binding.tilEmail.isEnabled = false
        binding.tilMake.isEnabled = false
        binding.tilModel.isEnabled = false
        binding.tilVehicalNumber.isEnabled = false
        binding.tilVehicalType.isEnabled = false

        binding.btnUpdate.isEnabled = false

        binding.tieEmail.addTextChangedListener(this)
        binding.tieMobileNumber.addTextChangedListener(this)
        binding.tiePassword.addTextChangedListener(this)
        binding.tieFirstName.addTextChangedListener(this)


        model.getProfileDataFetchResult().observe(this, Observer {

            if(it.status == APIRequestResponseHandler.AuthStatus.LOADING)
            {
                Alerts.showProgressBar(activity)
            }
            else if (it.status == APIRequestResponseHandler.AuthStatus.ERROR)
            {
                Alerts.dismissProgressBar()
                Alerts.showSnackBar(activity,it.status.name)
            }
            else if (it.status == APIRequestResponseHandler.AuthStatus.SUCCESS)
            {
                Alerts.dismissProgressBar()

                if(it.data?.error!!)
                {
                    Alerts.showSnackBar(activity,it.status.name)
                }
                else
                {
                    val profileData = it.data.data as ProfileData
                    binding.tiePassword.setText(Preferences.getPreferenceString(PrefEntity.PASSWORD))

                    binding.viewModel = profileData
                    binding.executePendingBindings()

                    binding.tieLastName.setText(profileData.lastName)
                    binding.tieFirstName.setText(profileData.firstName)

                    binding.tieMobileNumber.setText(profileData.mobile)

                    binding.tieEmail.setText(profileData.email)

                    binding.tieVehicalType.setText(profileData.vehicleTypeName)
                    binding.tieModel.setText(profileData.vehicleModel)
                    binding.tieVehicalNumber.setText(profileData.vehiclePlatNo)
                    binding.tieMake.setText(profileData.vehicleMake)

                    Picasso.get()
                            .load(profileData.image)
                            .placeholder(R.drawable.ic_pic_register_background_)
                            .error(R.drawable.ic_pic_register_background_)
                            .into((binding.imgDriverProfile as ImageView))

                    binding.imgDriverProfile.tag = profileData.image
                }
            }
        })

        model.getProfileUpdateResult().observe(this, Observer {

            if(it.status == APIRequestResponseHandler.AuthStatus.LOADING)
            {
                Alerts.showProgressBar(activity)
            }
            else if (it.status == APIRequestResponseHandler.AuthStatus.ERROR)
            {
                Alerts.dismissProgressBar()
                Alerts.showSnackBar(activity,it.status.name)
            }
            else if (it.status == APIRequestResponseHandler.AuthStatus.SUCCESS)
            {
                Alerts.dismissProgressBar()

                if(it.data?.error!!)
                {
                    Alerts.showSnackBar(activity,it.status.name)
                }
                else
                {
                    val profileData = it.data.data

                    binding.tiePassword.setText(Preferences.getPreferenceString(PrefEntity.PASSWORD))
                    binding.tieMobileNumber.setText(profileData.mobile)
                    binding.tieEmail.setText(Preferences.getPreferenceString(PrefEntity.EMAIL))
                    binding.tieVehicalType.setText(profileData.vehicleTypeName)
                    binding.tieModel.setText(profileData.vehicleModel)
                    binding.tieVehicalNumber.setText(profileData.vehiclePlatNo)
                    if(profileData.lastName != null )
                    {
                        binding.tieLastName.setText(profileData.lastName)
                        binding.tieFirstName.setText(profileData.firstName)


                        Preferences.setPreference(PrefEntity.USER_NAME,"${profileData.firstName} ${profileData.lastName}")
                    }
                    else
                    {
                        binding.tieLastName.setText("")
                        binding.tieFirstName.setText(profileData.firstName)

                        Preferences.setPreference(PrefEntity.USER_NAME, profileData.firstName)
                    }

                    Picasso.get()
                        .load(profileData.image)
                        .placeholder(R.drawable.ic_pic_register_background_)
                        .error(R.drawable.ic_pic_register_background_)
                        .into((binding.imgDriverProfile as ImageView))

                    binding.imgDriverProfile.tag = profileData.image

                    Preferences.setPreference(PrefEntity.PROFILE_PIC, profileData.image)
                    Preferences.setPreference(PrefEntity.PASSWORD, binding.tiePassword.text.toString())

                    eventBus.post("profileUpdate")

                    Alerts.showSnackBar(activity,getString(R.string.profile_updated_successfully))

                }
            }
        })


        connectivityReceiver.observe(this, Observer { connectionModel ->
            if (connectionModel != null && connectionModel.isConnected())
            {
                  apiInterface.let { model.profileDetailFetch(apiInterface) }
            }
            else
            {
                if(connectionModel?.type == Vpn) {
                    Alerts.showSnackBar(activity,getString(R.string.vpn_use_message))
                }
                else
                {
                    Alerts.showSnackBar(activity,getString(R.string.internet_not_available))
                }
            }
        })

        return binding.root
    }

    override fun onPause() {
        super.onPause()
//        // Clear transfer listeners to prevent memory leak, or else this activity won't be garbage collected.
//        connectivityReceiver.removeObservers(this)
//        model.getProfileDataFetchResult().removeObservers(this)
//        model.getProfileValidationResult().removeObservers(this)
    }

    override fun getViewModel(): ProfileViewModel {
       return model
    }

    override fun onProfileUpdateClicked() {

        try {
            connectivityReceiver.observe(this, Observer { connectionModel ->
                if (connectionModel != null && connectionModel.isConnected())
                {
                    if (binding.tieFirstName.text.toString().trim().isBlank()) {

                        binding.tilFirstName.isErrorEnabled = true
                        binding.tilFirstName.error = CatalogApplication.instance?.getString(R.string.enter_first_name)

                    }else if (binding.tieLastName.text.toString().trim().isBlank()) {

                    binding.tilLastName.isErrorEnabled = true
                    binding.tilLastName.error = CatalogApplication.instance?.getString(R.string.enter_last_name)

                    }
                    else if (binding.tieMobileNumber.text.toString().trim().isBlank()) {

                        binding.tilMobileNumber.isErrorEnabled = true
                        binding.tilMobileNumber.error = CatalogApplication.instance?.getString(R.string.enter_mobile_number)

                    }
                    else if (binding.tiePassword.text.toString().trim().isBlank()) {

                        binding.tilPassword.isErrorEnabled = true
                        binding.tilPassword.error = CatalogApplication.instance?.getString(R.string.enter_password)

                    }
                    else if(binding.imgDriverProfile.tag == null)
                    {
                        Alerts.showSnackBar(activity,CatalogApplication.instance?.getString(R.string.enter_profile_pic))

                    }
                    else
                    {
                        binding.tilPassword.isErrorEnabled = false
                        binding.tilEmail.isErrorEnabled = false
                        binding.tilMobileNumber.isErrorEnabled = false
                        binding.tilFirstName.isErrorEnabled = false
                        binding.tilLastName.isErrorEnabled = false

                        val profileDataModel = NewProfileDataModel()

                        val driversProfileDataModel = DriversProfileDataModel()

                        driversProfileDataModel.firstName = binding.tieFirstName.text.toString()

                        driversProfileDataModel.lastName = binding.tieLastName.text.toString()

                        driversProfileDataModel.image = binding.imgDriverProfile.tag as String?

                        driversProfileDataModel.mobile = binding.tieMobileNumber.text.toString()

                        driversProfileDataModel.password = binding.tiePassword.text.toString()


                        profileDataModel.driversProfile = driversProfileDataModel

                        apiInterface.let { model.profileUpdateDetail(apiInterface,profileDataModel) }
                    }
                }
                else
                {
                    if(connectionModel?.type == Vpn) {
                        Alerts.showSnackBar(activity,getString(R.string.vpn_use_message))
                    }
                    else
                    {
                        Alerts.showSnackBar(activity,getString(R.string.internet_not_available))
                    }
                }
            })
        } catch (e: Exception) {
        }
    }

    override fun onProfilePhotoClicked() {

        try {
            if(! PermissionHelper.getInstance().hasPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) )
            {
                requestStoragePermission()
            }
            else if(! PermissionHelper.getInstance().hasPermission(activity, Manifest.permission.CAMERA) )
            {
                requestCameraPermission()
            }
            else
            {
                mHandlePictureEvents.openDialog()
            }
        } catch (e: Exception) {
        }
    }

    /**
     * This is an overrided method, got a call, when an activity opens by StartActivityForResult(), and return something back to its calling activity.
     *
     * @param requestCode returning the request code.
     * @param resultCode  returning the result code.
     * @param data        contains the actual data.
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            super.onActivityResult(requestCode, resultCode, data)
            if (resultCode == Activity.RESULT_CANCELED) //result code to check is the result is ok or not
            {
                if(requestCode == CROP_IMAGE) {
                    if (data != null) {
                        val cropError = UCrop.getError(data)
                        Alerts.showSnackBar(activity, "Crop Error : $cropError")
                    }
                }
                return
            }
            else
            {
              when (requestCode)
              {
                CAMERA_PIC -> mHandlePictureEvents.startCropImage(mHandlePictureEvents.newFile)
                GALLERY_PIC -> {
                    Timber.d("onActivityResult: $data")

                    if (data != null) {
                        mHandlePictureEvents.gallery(data.data)
                    }
                }
                CROP_IMAGE -> {
                    val resultUri = UCrop.getOutput(data!!)
                    Timber.d("onActivityResult: $resultUri")

                    if(Utils.isNetworkAvailable(activity))
                    {
                        if(resultUri?.toFile()?.exists()!!) {

                            Alerts.showProgressBar(activity)
                            sAWSRepository.uploadDrawing(
                                AWSEntity(
                                    id = 1000,
                                    fileName = AMAZON_PROFILE_FOLDER + resultUri.toFile().name,
                                    imagePath = resultUri.toFile().absolutePath,
                                    lastModified = System.currentTimeMillis()
                                )
                            ).observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                    { d: AWSEntity ->
                                        Alerts.dismissProgressBar()
                                        Timber.e("Uploaded Successfully ${d.imagePath}")
                                        Picasso.get()
                                            .load(d.imagePath)
                                            .into((binding.imgDriverProfile as ImageView))
                                        binding.imgDriverProfile.tag = d.imagePath

                                    },
                                    {
                                        Alerts.dismissProgressBar()
                                        Alerts.showSnackBar(
                                            activity,
                                            getString(R.string.something_goes_wrong)
                                        )
                                    }, {}
                                )
                        }else
                        {
                            Alerts.showSnackBar(
                                activity,
                                getString(R.string.something_goes_wrong)
                            )
                        }
                    }
                    else
                    {
                        Alerts.showSnackBar(
                            activity,
                            getString(R.string.internet_not_available)
                        )
                    }
                }
              }
            }
        } catch (e: Exception) {
        }
    }

    private fun requestStoragePermission() {
        PermissionHelper.getInstance().requestPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }

    private fun requestCameraPermission() {
        PermissionHelper.getInstance().requestPermission(activity, Manifest.permission.CAMERA)
    }

    @PermissionGranted(permission = Manifest.permission.CAMERA)
    fun cameraGranted() {
        Timber.i("CAMERA_GRANT")

    }

    @PermissionDenied(permission = Manifest.permission.CAMERA)
    fun cameraDenied() {
        Timber.i("CAMERA_DENIED")
    }

    @PermissionGranted(permission = Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun storageGrant() {
        Timber.i("WRITE_EXTERNAL_STORAGE_GRANT")
    }

    @PermissionDenied(permission = Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun storageDenied() {
        Timber.i("WRITE_EXTERNAL_STORAGE_DENIED")
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        PermissionHelper.getInstance().onRequestPermissionsResult(this, permissions, grantResults)
    }

    override fun afterTextChanged(s: Editable?) {
        binding.btnUpdate.isEnabled = true
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

    }

}
