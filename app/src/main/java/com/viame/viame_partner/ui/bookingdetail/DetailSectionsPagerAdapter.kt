package com.viame.viame_partner.ui.bookingdetail

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.viame.viame_partner.R
import com.viame.viame_partner.ui.bookingdetail.details.DetailPagerFragment
import com.viame.viame_partner.ui.bookingdetail.timeline.TimelinePagerFragment
import com.viame.viame_partner.ui.myorders.model.Appointment


/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class DetailSectionsPagerAdapter(private val context: Context, fm: FragmentManager,
                                 private val appointment: Appointment) : FragmentPagerAdapter(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {


    override fun getItem(position: Int): Fragment {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
    if(position == 0)
       return DetailPagerFragment.newInstance(position + 1, appointment)
    else
       return TimelinePagerFragment.newInstance(position + 1, appointment)
    }

    override fun getPageTitle(position: Int): CharSequence? {

        if (position == 0)
        return context.getString(R.string.detail_page_detail_tab)
        else
        return context.getString(R.string.detail_page_timeline_tab)
    }

    override fun getCount(): Int {
        // Show 2 total pages.
        return 2
    }
}