package com.viame.viame_partner.ui.unassigned

import android.view.View
import com.viame.viame_partner.ui.myorders.model.Appointment
import com.viame.viame_partner.ui.unassigned.model.UnAssignedData
import com.viame.viame_partner.ui.unassigned.model.UnAsssignedDatumModel

interface UnAssignedOrderListEventHandler {
    /**
     * Will be called when signIn button gets clicked
     */
    fun onitemClick(view : View, appointment: UnAsssignedDatumModel)

}