package com.viame.viame_partner.ui.invite

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.viame.viame_partner.PLAYSTORE_LINK
import com.viame.viame_partner.R
import com.viame.viame_partner.databinding.FragmentInviteBinding

class InviteFragment : Fragment() {

    private lateinit var slideshowViewModel: InviteViewModel
    lateinit var binding : FragmentInviteBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
     binding = DataBindingUtil.inflate(inflater,R.layout.fragment_invite, container, false)

        var message =
            (resources.getString(R.string.invite_msg_1) + "\n" + resources.getString(R.string.invite_msg_2)
                    + " '" + resources.getString(R.string.app_name) + "'. "
                    + resources.getString(R.string.invite_msg_4) + "\n" + " " + PLAYSTORE_LINK)

        binding.btnShareNow.setOnClickListener(View.OnClickListener {

            try {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.type = "text/plain"
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.install_viame_partner))
                shareIntent.putExtra(Intent.EXTRA_TEXT, message)
                startActivity(Intent.createChooser(shareIntent, "choose one"))
            } catch (e: Exception) { //e.toString();
            }
        })
        return binding.root
    }
}