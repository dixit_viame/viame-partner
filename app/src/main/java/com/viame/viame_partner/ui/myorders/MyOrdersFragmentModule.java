package com.viame.viame_partner.ui.myorders;
import dagger.Module;
import dagger.Provides;

@Module
public class MyOrdersFragmentModule {

    @Provides
    MyOrdersViewModel myOrdersViewModel() {
        return new MyOrdersViewModel();
    }


}
