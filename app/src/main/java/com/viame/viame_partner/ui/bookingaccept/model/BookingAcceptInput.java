package com.viame.viame_partner.ui.bookingaccept.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookingAcceptInput {

    @SerializedName("ent_booking_id")
    @Expose
    private String Bid;

    @SerializedName("ent_status")
    @Expose
    private String entStatus;

    @SerializedName("lat")
    @Expose
    private Double lattitude;

    @SerializedName("long")
    @Expose
    private Double longitude;

    public String getBid() {
        return Bid;
    }

    public void setBid(String bid) {
        Bid = bid;
    }

    public String getEntStatus() {
        return entStatus;
    }

    public void setEntStatus(String entStatus) {
        this.entStatus = entStatus;
    }

    public Double getLattitude() {
        return lattitude;
    }

    public void setLattitude(Double lattitude) {
        this.lattitude = lattitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
