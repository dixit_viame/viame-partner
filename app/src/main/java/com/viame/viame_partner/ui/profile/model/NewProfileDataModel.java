package com.viame.viame_partner.ui.profile.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewProfileDataModel {

    @SerializedName("drivers_profile")
    @Expose
    private DriversProfileDataModel driversProfile;

    public DriversProfileDataModel getDriversProfile() {
        return driversProfile;
    }

    public void setDriversProfile(DriversProfileDataModel driversProfile) {
        this.driversProfile = driversProfile;
    }
}
