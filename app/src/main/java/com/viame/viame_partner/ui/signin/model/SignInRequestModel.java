package com.viame.viame_partner.ui.signin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SignInRequestModel {

    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("ent_deviceId")
    @Expose
    private String entDeviceId;
    @SerializedName("ent_push_token")
    @Expose
    private String entPushToken;
    @SerializedName("ent_appversion")
    @Expose
    private String entAppversion;
    @SerializedName("ent_devMake")
    @Expose
    private String entDevMake;
    @SerializedName("ent_devModel")
    @Expose
    private String entDevModel;
    @SerializedName("ent_devtype")
    @Expose
    private Integer entDevtype;
    @SerializedName("ent_device_time")
    @Expose
    private String entDeviceTime;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEntDeviceId() {
        return entDeviceId;
    }

    public void setEntDeviceId(String entDeviceId) {
        this.entDeviceId = entDeviceId;
    }

    public String getEntPushToken() {
        return entPushToken;
    }

    public void setEntPushToken(String entPushToken) {
        this.entPushToken = entPushToken;
    }

    public String getEntAppversion() {
        return entAppversion;
    }

    public void setEntAppversion(String entAppversion) {
        this.entAppversion = entAppversion;
    }

    public String getEntDevMake() {
        return entDevMake;
    }

    public void setEntDevMake(String entDevMake) {
        this.entDevMake = entDevMake;
    }

    public String getEntDevModel() {
        return entDevModel;
    }

    public void setEntDevModel(String entDevModel) {
        this.entDevModel = entDevModel;
    }

    public Integer getEntDevtype() {
        return entDevtype;
    }

    public void setEntDevtype(Integer entDevtype) {
        this.entDevtype = entDevtype;
    }

    public String getEntDeviceTime() {
        return entDeviceTime;
    }

    public void setEntDeviceTime(String entDeviceTime) {
        this.entDeviceTime = entDeviceTime;
    }

}
