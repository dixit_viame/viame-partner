package com.viame.viame_partner.ui.jobhistorydetail

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.squareup.picasso.Picasso
import com.viame.viame_partner.R
import com.viame.viame_partner.databinding.FragmentHeaderViewBinding
import com.viame.viame_partner.utils.Alerts

/**
 * A placeholder fragment containing a simple view.
 */
class HeaderImagePagerFragment : Fragment() {

    lateinit var binding : FragmentHeaderViewBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_header_view, container, false)

        try {
            if((activity as JobHistoryDetailActivity).images.size > 0)
            {
                if((activity as JobHistoryDetailActivity).images[0] == "Default")
                {
                    binding.imageViewHeader.scaleType = ImageView.ScaleType.FIT_CENTER
                    binding.imageViewHeader.setImageResource(R.drawable.ic_default_image)
                }
                else {
                    if((activity as JobHistoryDetailActivity).images[arguments!!.getInt(ARG_SECTION_NUMBER)].toString().isNotEmpty())
                    {
                        binding.imageViewHeader.scaleType = ImageView.ScaleType.CENTER_CROP
                        Picasso.get().load(
                            (activity as JobHistoryDetailActivity).images[arguments!!.getInt(
                                ARG_SECTION_NUMBER
                            )].toString()
                        ).into(binding.imageViewHeader)

                    }
                    else
                    {
                        binding.imageViewHeader.scaleType = ImageView.ScaleType.FIT_CENTER
                        binding.imageViewHeader.setImageResource(R.drawable.ic_default_image)
                    }
                }
            }
            else
            {
                binding.imageViewHeader.scaleType = ImageView.ScaleType.FIT_CENTER
                binding.imageViewHeader.setImageResource(R.drawable.ic_default_image)
            }

            binding.imageViewHeader.setOnClickListener(View.OnClickListener {

                try {
                    if((activity as JobHistoryDetailActivity).images[arguments!!.getInt(ARG_SECTION_NUMBER)] != "Default" &&
                        (activity as JobHistoryDetailActivity).images[arguments!!.getInt(ARG_SECTION_NUMBER)].toString().isNotEmpty())
                    {
                        var dialog = Dialog(activity as JobHistoryDetailActivity)
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                        dialog.setContentView(R.layout.layout_dialog_imageview)
                        dialog.getWindow()?.setLayout(
                            WindowManager.LayoutParams.MATCH_PARENT,
                            WindowManager.LayoutParams.MATCH_PARENT
                        );
                        dialog.setCanceledOnTouchOutside(true)
                        dialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                        val bigImageView = dialog.findViewById(R.id.ivBigImage) as ImageView

                        Picasso.get()
                            .load(
                                (activity as JobHistoryDetailActivity).images[arguments!!.getInt(
                                    ARG_SECTION_NUMBER
                                )].toString()
                            )
                            .into(bigImageView)

                        val ivClose = dialog.findViewById(R.id.ivClose) as ImageView

                        ivClose.setOnClickListener {
                            dialog.dismiss()
                        }
                        dialog.show()
                        ivClose.bringToFront()
                    }
                    else
                    {
                        Alerts.showSnackBar(activity,"No image found")
                    }
                } catch (e: Exception) {
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return binding.root
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int): HeaderImagePagerFragment {
            return HeaderImagePagerFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }
}