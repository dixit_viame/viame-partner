package com.viame.viame_partner.ui.myorders;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MyOrdersFragmentProvider {

    @ContributesAndroidInjector
    abstract MyOrdersFragment contributeMyOrdersFragment();
}