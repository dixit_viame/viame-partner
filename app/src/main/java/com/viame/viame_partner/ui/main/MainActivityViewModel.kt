package com.viame.viame_partner.ui.main

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.work.ListenableWorker
import com.viame.viame_partner.*
import com.viame.viame_partner.application.CatalogApplication
import com.viame.viame_partner.extension.fromSingleToMain
import com.viame.viame_partner.extension.fromWorkerToMain
import com.viame.viame_partner.firebase.unsubscribeAllTopics
import com.viame.viame_partner.location.LocationCallRepository
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.rx.Scheduler
import com.viame.viame_partner.ui.main.models.GetConfigOutputResponse
import com.viame.viame_partner.ui.myorders.model.MyOrdersOutputResponseModel
import com.viame.viame_partner.ui.splash.SplashScreenActivity
import com.viame.viame_partner.ui.transfer_order.model.TransferOrderInputModel
import com.viame.viame_partner.ui.transfer_order.model.TransferOrderOutputModel
import com.viame.viame_partner.utils.Alerts
import com.viame.viame_partner.workmanager.NotifyLocationBackGroundWork
import com.viame.viame_partner.workmanager.stopLocationSendWorkManager
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.HashMap
import javax.inject.Inject

class MainActivityViewModel @Inject constructor() : ViewModel()
{
    @Inject
    lateinit var repository: LocationCallRepository

    private val getConfigApiResponse = MutableLiveData<APIRequestResponseHandler<GetConfigOutputResponse>>()

    fun getConfigAPIResult(): MutableLiveData<APIRequestResponseHandler<GetConfigOutputResponse>> = getConfigApiResponse

    fun callConfigAPI(apiInterface : ApiInterface) {

        getConfigApiResponse.value = APIRequestResponseHandler.loading(null)

        val headerMap = HashMap<String,String>()
        headerMap["Content-Type"] = "application/json;charset=UTF-8"
        headerMap["authorization"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["token"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["userType"] = USER_TYPE.toString()

        val posts: Call<GetConfigOutputResponse> = apiInterface.callConfigDataMethod("$BASE_ENGINE_URL$GET_CONFIG_API",headerMap)!!

        //Enqueue the call
        posts.enqueue(object : Callback<GetConfigOutputResponse>
        {
            @SuppressLint("TimberArgCount")
            override fun onFailure(call: Call<GetConfigOutputResponse>, t: Throwable)
            {
                getConfigApiResponse.value = APIRequestResponseHandler.error(t.message.toString(),null)
                Timber.e(t.message.toString())
            }

            @SuppressLint("TimberArgCount")
            override fun onResponse(call: Call<GetConfigOutputResponse>, response: Response<GetConfigOutputResponse>)
            {
                try {
                    if(response.isSuccessful)
                    {
                        getConfigApiResponse.value = APIRequestResponseHandler.success(response.body()!!)
                        Timber.e(response.body().toString())
                    }
                    else
                    {
                        getConfigApiResponse.value = APIRequestResponseHandler.error("Status code "+response.code(),null)

                    }
                } catch (e: Exception) {
                }
            }
        })
    }


    private val TransferOrderQRCodeApiResponse = MutableLiveData<APIRequestResponseHandler<TransferOrderOutputModel>>()

    fun TransferOrderQRCodeAPIDataResult(): MutableLiveData<APIRequestResponseHandler<TransferOrderOutputModel>> = TransferOrderQRCodeApiResponse

    fun TransferOrderQRCodeAPICall(apiInterface : ApiInterface, transferOrderInputModel: TransferOrderInputModel)
    {
        TransferOrderQRCodeApiResponse.value = APIRequestResponseHandler.loading(null)

        val headerMap = HashMap<String,String>()
        headerMap["Content-Type"] = "application/json;charset=UTF-8"
        headerMap["authorization"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["token"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["userType"] = USER_TYPE.toString()

        val posts: Call<TransferOrderOutputModel> = apiInterface.callTransferOrderMethod("$BASE_ENGINE_URL$TRANSFER_ORDER",headerMap, transferOrderInputModel)
        //Enqueue the call
        posts.enqueue(object : Callback<TransferOrderOutputModel>
        {
            @SuppressLint("TimberArgCount")
            override fun onFailure(call: Call<TransferOrderOutputModel>, t: Throwable) {

                try {
                    TransferOrderQRCodeApiResponse.value = APIRequestResponseHandler.error(t.message.toString(),null)
                    Timber.e(t.message.toString())
                } catch (e: Exception) {
                }
            }

            @SuppressLint("TimberArgCount")
            override fun onResponse(call: Call<TransferOrderOutputModel>, response: Response<TransferOrderOutputModel>
            ) {
                if(response.isSuccessful) {

                    response.body()!!.let {
                        TransferOrderQRCodeApiResponse.value =
                            APIRequestResponseHandler.success(response.body()!!)
                        Timber.e(response.body().toString())
                    }
                }
                else
                {
                    val responseError = TransferOrderOutputModel()
                    try {
                        val jsonObject = JSONObject(response.errorBody()?.string()!!)
                        // {"data":"The current driver is not logged in.","error":true,"success":false,"message":"Something wen't wrong"}

                        responseError.data = jsonObject["data"].toString()
                        responseError.error = jsonObject["error"] as Boolean
                        responseError.success = jsonObject["success"] as Boolean
                        responseError.message = jsonObject["message"].toString()

                        TransferOrderQRCodeApiResponse.value = APIRequestResponseHandler.error(jsonObject["data"].toString(), responseError)
                    } catch (e: Exception) {

                        responseError.data = CatalogApplication.instance?.getString(R.string.something_goes_wrong)
                        responseError.message = CatalogApplication.instance?.getString(R.string.something_goes_wrong)

                        TransferOrderQRCodeApiResponse.value = APIRequestResponseHandler.error(response.errorBody()?.string(), responseError)
                    }
                }
            }
        })
    }
}