package com.viame.viame_partner.ui.bookingaccept

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.viame.viame_partner.BASE_ENGINE_URL
import com.viame.viame_partner.LOGOUT_URL
import com.viame.viame_partner.USER_TYPE
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.bookingaccept.model.BookingAcceptInput
import com.viame.viame_partner.ui.bookingaccept.model.BookingAcceptOutput
import com.viame.viame_partner.ui.logout.model.LogoutResponseModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.HashMap
import javax.inject.Inject

class BookingAcceptDialogViewModel @Inject constructor() : ViewModel() {


    private val getBookingAcceptApiResponse = MutableLiveData<APIRequestResponseHandler<BookingAcceptOutput>>()

    fun getBookingAcceptAPIResult(): MutableLiveData<APIRequestResponseHandler<BookingAcceptOutput>> = getBookingAcceptApiResponse

    fun callBookingAcceptAPI(apiInterface : ApiInterface,input : BookingAcceptInput) {

        getBookingAcceptApiResponse.value = APIRequestResponseHandler.loading(null)

        val headerMap = HashMap<String,String>();
        headerMap.put("Content-Type", "application/json;charset=UTF-8");
        headerMap.put("authorization", Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN));
        headerMap.put("token",  Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN));
        headerMap.put("userType", USER_TYPE.toString());

        val posts: Call<BookingAcceptOutput> = apiInterface.callRespondToBookingMethod(headerMap,input)

        //Enqueue the call
        posts.enqueue(object : Callback<BookingAcceptOutput> {

            @SuppressLint("TimberArgCount")
            override fun onFailure(call: Call<BookingAcceptOutput>, t: Throwable)
            {
                getBookingAcceptApiResponse.value = APIRequestResponseHandler.error(t.message.toString(),null)
                Timber.e(t.message.toString())
            }

            @SuppressLint("TimberArgCount")
            override fun onResponse(call: Call<BookingAcceptOutput>, response: Response<BookingAcceptOutput>) =
                try {
                    if(response.isSuccessful) {

                        val mLogoutResponseModel = response.body()
                        if(mLogoutResponseModel?.errFlag == 0)
                        {
                            getBookingAcceptApiResponse.value = APIRequestResponseHandler.success(mLogoutResponseModel)
                        }
                        else
                        {
                            getBookingAcceptApiResponse.value = APIRequestResponseHandler.error(mLogoutResponseModel?.errMsg, mLogoutResponseModel)
                        }

                        Timber.e(response.body().toString())
                    } else {
                        getBookingAcceptApiResponse.value = APIRequestResponseHandler.error("Status code "+response.code(),null)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
        })
    }
}
