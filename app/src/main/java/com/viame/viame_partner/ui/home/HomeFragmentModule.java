package com.viame.viame_partner.ui.home;
import dagger.Module;
import dagger.Provides;

@Module
public class HomeFragmentModule {

    @Provides
    HomeFragmentViewModel categoryViewModel() {
        return new HomeFragmentViewModel();
    }


}
