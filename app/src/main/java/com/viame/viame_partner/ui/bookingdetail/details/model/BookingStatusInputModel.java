package com.viame.viame_partner.ui.bookingdetail.details.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.stringtemplate.v4.ST;

public class BookingStatusInputModel {


    @SerializedName("ent_status")
    @Expose
    private String entStatus;
    @SerializedName("ent_booking_id")
    @Expose
    private long entBookingId;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("long")
    @Expose
    private Double _long;
    @SerializedName("distance")
    @Expose
    private String distance;

    public Double get_long() {
        return _long;
    }

    public void set_long(Double _long) {
        this._long = _long;
    }

    public String getEntSignatureUrl() {
        return entSignatureUrl;
    }

    public void setEntSignatureUrl(String entSignatureUrl) {
        this.entSignatureUrl = entSignatureUrl;
    }

    public String getEntTollFee() {
        return entTollFee;
    }

    public void setEntTollFee(String entTollFee) {
        this.entTollFee = entTollFee;
    }

    public String getEntHandlingFee() {
        return entHandlingFee;
    }

    public void setEntHandlingFee(String entHandlingFee) {
        this.entHandlingFee = entHandlingFee;
    }

    public String getEntReceiverName() {
        return entReceiverName;
    }

    public void setEntReceiverName(String entReceiverName) {
        this.entReceiverName = entReceiverName;
    }

    public String getEntReceiverPhone() {
        return entReceiverPhone;
    }

    public void setEntReceiverPhone(String entReceiverPhone) {
        this.entReceiverPhone = entReceiverPhone;
    }

    public String getEntRating() {
        return entRating;
    }

    public void setEntRating(String entRating) {
        this.entRating = entRating;
    }

    public String getEntDocuments() {
        return entDocuments;
    }

    public void setEntDocuments(String entDocuments) {
        this.entDocuments = entDocuments;
    }

    public String getCashCollected() {
        return cashCollected;
    }

    public void setCashCollected(String cashCollected) {
        this.cashCollected = cashCollected;
    }

    @SerializedName("ent_signatureUrl")
    @Expose
    private String entSignatureUrl;
    @SerializedName("ent_tollFee")
    @Expose
    private String entTollFee;
    @SerializedName("ent_handlingFee")
    @Expose
    private String entHandlingFee;
    @SerializedName("ent_receiverName")
    @Expose
    private String entReceiverName;
    @SerializedName("ent_receiverPhone")
    @Expose
    private String entReceiverPhone;
    @SerializedName("ent_rating")
    @Expose
    private String entRating;
    @SerializedName("ent_documents")
    @Expose
    private String entDocuments;
    @SerializedName("cashCollected")
    @Expose
    private String cashCollected;

    public String getEntStatus() {
        return entStatus;
    }

    public void setEntStatus(String entStatus) {
        this.entStatus = entStatus;
    }

    public long getEntBookingId() {
        return entBookingId;
    }

    public void setEntBookingId(long entBookingId) {
        this.entBookingId = entBookingId;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLong() {
        return _long;
    }

    public void setLong(Double _long) {
        this._long = _long;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }
}
