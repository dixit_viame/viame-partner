package com.viame.viame_partner.ui.vehicle;

import androidx.lifecycle.ViewModelProvider;
import com.viame.viame_partner.utils.ViewModelProviderFactory;

import dagger.Module;
import dagger.Provides;


@Module
public class VehicleListActivityModule {

    @Provides
    VehicleListActivityModel providesVehicleListActivityModel(){
        return new VehicleListActivityModel();
    }

    @Provides
    ViewModelProvider.Factory provideViewModelProvider(VehicleListActivityModel viewModel){
        return new ViewModelProviderFactory<>(viewModel);
    }

}
