package com.viame.viame_partner.ui.bookingcancel

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.viame.viame_partner.BASE_URL
import com.viame.viame_partner.CANCEL_ORDER_URL
import com.viame.viame_partner.USER_TYPE
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.bookingcancel.model.CancelBookingInputModel
import com.viame.viame_partner.ui.bookingcancel.model.CancelReasonsResponseModel
import com.viame.viame_partner.ui.bookingdetail.details.model.BookingStatusInputModel
import com.viame.viame_partner.ui.bookingdetail.details.model.BookingStatusOutputModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.HashMap
import javax.inject.Inject

class BookingCancelViewModel @Inject constructor() : ViewModel()
{

    private val BookingCancelReasonsApiResponse = MutableLiveData<APIRequestResponseHandler<CancelReasonsResponseModel>>()

    fun getBookingCancelReasonsAPIDataResult(): MutableLiveData<APIRequestResponseHandler<CancelReasonsResponseModel>> = BookingCancelReasonsApiResponse


    fun BookingCancelReasonsAPICall(apiInterface : ApiInterface)
    {
        BookingCancelReasonsApiResponse.value = APIRequestResponseHandler.loading(null)

        val headerMap = HashMap<String,String>();
        headerMap["Content-Type"] = "application/json;charset=UTF-8";
        headerMap["authorization"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN);
        headerMap["token"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN);
        headerMap["userType"] = USER_TYPE.toString()

        val posts: Call<CancelReasonsResponseModel> = apiInterface.callBookingCancelReasonMethod("$BASE_URL$CANCEL_ORDER_URL/$USER_TYPE/0",headerMap)
        //Enqueue the call
        posts.enqueue(object : Callback<CancelReasonsResponseModel>
        {
            @SuppressLint("TimberArgCount")
            override fun onFailure(call: Call<CancelReasonsResponseModel>, t: Throwable) {

                try {
                    BookingCancelReasonsApiResponse.value = APIRequestResponseHandler.error(t.message.toString(),null)
                    Timber.e(t.message.toString())
                } catch (e: Exception) {
                }
            }

            @SuppressLint("TimberArgCount")
            override fun onResponse(call: Call<CancelReasonsResponseModel>, response: Response<CancelReasonsResponseModel>
            ) {
                try {
                    if(response.isSuccessful) {
                        response.body().let {

                            BookingCancelReasonsApiResponse.value =
                                APIRequestResponseHandler.success(it)
                            Timber.e(response.body().toString())
                        }
                    }
                } catch (e: Exception)
                {
                    e.printStackTrace()
                    BookingCancelReasonsApiResponse.value = APIRequestResponseHandler.error("Status code "+response.code(),null)
                }
            }
        })
    }


    private val BookingCancelApiResponse = MutableLiveData<APIRequestResponseHandler<CancelReasonsResponseModel>>()

    fun getBookingCancelAPIDataResult(): MutableLiveData<APIRequestResponseHandler<CancelReasonsResponseModel>> = BookingCancelApiResponse

    fun BookingCancelAPICall(apiInterface : ApiInterface,model: CancelBookingInputModel)
    {
        BookingCancelApiResponse.value = APIRequestResponseHandler.loading(null)

        val headerMap = HashMap<String,String>()
        headerMap["Content-Type"] = "application/json;charset=UTF-8"
        headerMap["authorization"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["token"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["userType"] = USER_TYPE.toString()

        val posts: Call<CancelReasonsResponseModel> = apiInterface.callBookingCancelMethod(headerMap,model)
        //Enqueue the call
        posts.enqueue(object : Callback<CancelReasonsResponseModel>
        {
            @SuppressLint("TimberArgCount")
            override fun onFailure(call: Call<CancelReasonsResponseModel>, t: Throwable) {

                try {
                    BookingCancelApiResponse.value = APIRequestResponseHandler.error(t.message.toString(),null)
                    Timber.e(t.message.toString())
                } catch (e: Exception) {
                }
            }

            @SuppressLint("TimberArgCount")
            override fun onResponse(call: Call<CancelReasonsResponseModel>, response: Response<CancelReasonsResponseModel>
            ) {
                try {
                    if(response.isSuccessful) {
                        response.body().let {

                            BookingCancelApiResponse.value = APIRequestResponseHandler.success(it)
                            Timber.e(response.body().toString())
                        }
                    }
                } catch (e: Exception)
                {
                    e.printStackTrace()
                    BookingCancelApiResponse.value = APIRequestResponseHandler.error("Status code "+response.code(),null)
                }
            }
        })
    }

}