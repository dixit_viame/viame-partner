package com.viame.viame_partner.ui.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.viame.viame_partner.ui.main.MainActivity
import timber.log.Timber

class MyAppOpenReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {


        Timber.e("Receiver-----Called")

        if(intent.extras != null) {
            val intents = Intent(context, MainActivity::class.java)
            intent.extras?.let { intents.putExtras(it) }
            intents.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intents)
        }

    }
}
