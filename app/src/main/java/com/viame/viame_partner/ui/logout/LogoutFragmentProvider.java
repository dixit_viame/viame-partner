package com.viame.viame_partner.ui.logout;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class LogoutFragmentProvider {

    @ContributesAndroidInjector
    abstract LogoutFragment contributeLogoutFragment();
}