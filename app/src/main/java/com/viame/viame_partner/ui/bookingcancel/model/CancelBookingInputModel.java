package com.viame.viame_partner.ui.bookingcancel.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CancelBookingInputModel {

    @SerializedName("ent_booking_id")
    @Expose
    private String entBookingId;

    @SerializedName("ent_status")
    @Expose
    private Integer entStatus;

    @SerializedName("ent_reason")
    @Expose
    private String entReason;

    public String getEntBookingId() {
        return entBookingId;
    }

    public void setEntBookingId(String entBookingId) {
        this.entBookingId = entBookingId;
    }

    public Integer getEntStatus() {
        return entStatus;
    }

    public void setEntStatus(Integer entStatus) {
        this.entStatus = entStatus;
    }

    public String getEntReason() {
        return entReason;
    }

    public void setEntReason(String entReason) {
        this.entReason = entReason;
    }
}
