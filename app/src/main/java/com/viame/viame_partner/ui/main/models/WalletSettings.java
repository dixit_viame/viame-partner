package com.viame.viame_partner.ui.main.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WalletSettings {

    @SerializedName("softLimitShipper")
    @Expose
    private Integer softLimitShipper;
    @SerializedName("hardLimitShipper")
    @Expose
    private Integer hardLimitShipper;
    @SerializedName("walletShipperEnable")
    @Expose
    private Boolean walletShipperEnable;
    @SerializedName("paidByReceiver")
    @Expose
    private Boolean paidByReceiver;
    @SerializedName("softLimitDriver")
    @Expose
    private Integer softLimitDriver;
    @SerializedName("hardLimitDriver")
    @Expose
    private Integer hardLimitDriver;
    @SerializedName("walletDriverEnable")
    @Expose
    private Boolean walletDriverEnable;

    public Integer getSoftLimitShipper() {
        return softLimitShipper;
    }

    public void setSoftLimitShipper(Integer softLimitShipper) {
        this.softLimitShipper = softLimitShipper;
    }

    public Integer getHardLimitShipper() {
        return hardLimitShipper;
    }

    public void setHardLimitShipper(Integer hardLimitShipper) {
        this.hardLimitShipper = hardLimitShipper;
    }

    public Boolean getWalletShipperEnable() {
        return walletShipperEnable;
    }

    public void setWalletShipperEnable(Boolean walletShipperEnable) {
        this.walletShipperEnable = walletShipperEnable;
    }

    public Boolean getPaidByReceiver() {
        return paidByReceiver;
    }

    public void setPaidByReceiver(Boolean paidByReceiver) {
        this.paidByReceiver = paidByReceiver;
    }

    public Integer getSoftLimitDriver() {
        return softLimitDriver;
    }

    public void setSoftLimitDriver(Integer softLimitDriver) {
        this.softLimitDriver = softLimitDriver;
    }

    public Integer getHardLimitDriver() {
        return hardLimitDriver;
    }

    public void setHardLimitDriver(Integer hardLimitDriver) {
        this.hardLimitDriver = hardLimitDriver;
    }

    public Boolean getWalletDriverEnable() {
        return walletDriverEnable;
    }

    public void setWalletDriverEnable(Boolean walletDriverEnable) {
        this.walletDriverEnable = walletDriverEnable;
    }

}
