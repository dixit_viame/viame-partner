package com.viame.viame_partner.ui.bookingdetail.timeline

import kotlinx.android.parcel.Parcelize
import android.os.Parcelable as Parcelable

/**
 * Created by Vipul Asri on 05-12-2015.
 */
@Parcelize
class TimeLineModel(
        var message: String,
        var date: String,
        var status: OrderStatus
) : Parcelable
