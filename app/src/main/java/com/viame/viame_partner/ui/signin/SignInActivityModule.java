package com.viame.viame_partner.ui.signin;
import androidx.lifecycle.ViewModelProvider;

import com.viame.viame_partner.utils.ViewModelProviderFactory;

import dagger.Module;
import dagger.Provides;

@Module
public class SignInActivityModule {

    @Provides
    SignInActivityViewModel providesLoginActivityViewModel(){
        return new SignInActivityViewModel();
    }

    @Provides
    ViewModelProvider.Factory provideViewModelProvider(SignInActivityViewModel viewModel){
        return new ViewModelProviderFactory<>(viewModel);
    }


}
