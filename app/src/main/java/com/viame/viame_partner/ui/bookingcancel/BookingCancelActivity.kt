package com.viame.viame_partner.ui.bookingcancel

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.viame.viame_partner.BOOKING_DATA
import com.viame.viame_partner.MBStatusType
import com.viame.viame_partner.R
import com.viame.viame_partner.Vpn
import com.viame.viame_partner.databinding.ActivityBookingCancelBinding
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.ui.base.BaseActivity
import com.viame.viame_partner.ui.bookingcancel.model.CancelBookingInputModel
import com.viame.viame_partner.ui.main.MainActivity
import com.viame.viame_partner.ui.myorders.model.Appointment
import com.viame.viame_partner.utils.Alerts
import com.viame.viame_partner.utils.ConnectivityReceiver
import com.viame.viame_partner.utils.LinearLayoutManagerWrapper
import javax.inject.Inject

class BookingCancelActivity : BaseActivity<BookingCancelViewModel>() , CancelReasonSelectEventHandler {

    private lateinit var adapter: BookingCancelRecyclerAdapter

    private lateinit var binding: ActivityBookingCancelBinding

    var currentAppointment : Appointment? = null

    @Inject
    lateinit var apiInterface: ApiInterface

    @Inject
    lateinit var connectivityReceiver: ConnectivityReceiver

    @Inject
    lateinit var models : BookingCancelViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_booking_cancel)
        setSupportActionBar(binding.toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        binding.toolbar.setTitle(getString(R.string.cancel_booking))
        setTitle(getString(R.string.cancel_booking))

        if(intent.hasExtra(BOOKING_DATA))
            currentAppointment = intent.getSerializableExtra(BOOKING_DATA) as Appointment?
        else
            return

        models.getBookingCancelReasonsAPIDataResult().observe(this@BookingCancelActivity, Observer { result ->

            if (result.data != null) {

                if (result.status == APIRequestResponseHandler.AuthStatus.LOADING) {
                    //  Alerts.showProgressBar(activity)
                    binding.progressBar.visibility = View.VISIBLE

                } else if (result.status == APIRequestResponseHandler.AuthStatus.ERROR) {
                    // Alerts.dismissProgressBar()
                    binding.progressBar.visibility = View.INVISIBLE
                    Alerts.showSnackBar(this, result.message)

                } else if (result.status == APIRequestResponseHandler.AuthStatus.SUCCESS) {
                    //Alerts.dismissProgressBar()
                    binding.progressBar.visibility = View.INVISIBLE

                    if (result.data.errFlag == 1) {
                        Alerts.showSnackBar(this, result.data.errMsg)
                    }
                    else
                    {
                        if(result.data.data.size > 0)
                        {
                            adapter = BookingCancelRecyclerAdapter(
                                this,
                                ArrayList(result.data.data),
                                this@BookingCancelActivity)

                            binding.rvCancelReason.layoutManager =
                                LinearLayoutManagerWrapper(this, RecyclerView.VERTICAL, false)

                            binding.rvCancelReason.adapter = adapter
                            binding.rvCancelReason.visibility = View.VISIBLE
                        }
                    }
                }
            }
        })



        connectivityReceiver.observe(this@BookingCancelActivity, Observer { connectionModel ->

            if (connectionModel != null && connectionModel.isConnected())
            {
                apiInterface.let {

                    models.BookingCancelReasonsAPICall(apiInterface)
                }
            }
            else
            {
                if(connectionModel?.type == Vpn)
                {
                    Alerts.showSnackBar(this,getString(R.string.vpn_use_message))
                }
                else
                {
                    Alerts.showSnackBar(this,getString(R.string.internet_not_available))
                }
            }
        })

        binding.btnCancel.setOnClickListener {

            if(adapter.indexSelected != -1 ||  binding.tieCancelReason.text!!.isNotEmpty())
            {

                connectivityReceiver.observe(this@BookingCancelActivity, Observer { connectionModel ->

                    if (connectionModel != null && connectionModel.isConnected())
                    {
                        val cancelViewModel = CancelBookingInputModel()
                        cancelViewModel.entBookingId = currentAppointment?.orderId.toString()
                        cancelViewModel.entStatus = MBStatusType.DRIVER_CANCELLED.id
                        cancelViewModel.entReason = binding.tieCancelReason.text.toString()

                        apiInterface.let {

                            models.BookingCancelAPICall(apiInterface,cancelViewModel)
                        }
                    }
                    else
                    {
                        if(connectionModel?.type == Vpn)
                        {
                            Alerts.showSnackBar(this,getString(R.string.vpn_use_message))
                        }
                        else
                        {
                            Alerts.showSnackBar(this,getString(R.string.internet_not_available))
                        }
                    }
                })
            }
            else
            {
                Alerts.showSnackBar(this, getString(R.string.select_valid_reason))
            }
        }

        models.getBookingCancelAPIDataResult().observe(this@BookingCancelActivity, Observer { result ->

            if (result.data != null) {

                if (result.status == APIRequestResponseHandler.AuthStatus.LOADING) {
                    //  Alerts.showProgressBar(activity)
                    binding.progressBar.visibility = View.VISIBLE

                } else if (result.status == APIRequestResponseHandler.AuthStatus.ERROR) {
                    // Alerts.dismissProgressBar()
                    binding.progressBar.visibility = View.INVISIBLE
                    Alerts.showSnackBar(this, result.message)

                } else if (result.status == APIRequestResponseHandler.AuthStatus.SUCCESS) {
                    //Alerts.dismissProgressBar()
                    binding.progressBar.visibility = View.INVISIBLE

                    if (result.data.errFlag == 1) {
                        Alerts.showSnackBar(this, result.data.errMsg)
                    }
                    else
                    {
                            Alerts.showBottomSheetSimpleConfirmationDialog(this@BookingCancelActivity,
                                getString(R.string.alert),
                                result.data.errMsg, true, null,
                                getString(R.string.ok), object : Alerts.OnConfirmationDialog {
                                    @SuppressLint("CheckResult")
                                    override fun onYes() {

                                        val intent = Intent(this@BookingCancelActivity, MainActivity::class.java)
                                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                        startActivity(intent)
                                        this@BookingCancelActivity.finish()
                                    }
                                    override fun onNo() {

                                    }
                                })
                        }
                }
            }
        })


    }

    override fun getViewModel(): BookingCancelViewModel {
        return models
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            android.R.id.home -> this@BookingCancelActivity.finish()
        }
        return true
    }


    override fun onItemClick(view: View, appointment: String) {

        val checkView = view.findViewById(R.id.vCheckView) as AppCompatCheckBox

        adapter.indexSelected = checkView.tag as Int
        adapter.notifyDataSetChanged()

        binding.tieCancelReason.setText(adapter.getCategoryList()[adapter.indexSelected])

    }

}
