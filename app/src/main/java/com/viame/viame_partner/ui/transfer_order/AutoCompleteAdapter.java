package com.viame.viame_partner.ui.transfer_order;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.viame.viame_partner.R;
import com.viame.viame_partner.ui.transfer_order.model.GetAllDriverOutputModel;

public class AutoCompleteAdapter extends ArrayAdapter<GetAllDriverOutputModel.Datum> implements Filterable {

    private ArrayList<GetAllDriverOutputModel.Datum> fullList;
    private ArrayList<GetAllDriverOutputModel.Datum> mOriginalValues;
    private ArrayFilter mFilter;

    public AutoCompleteAdapter(Context context, int resource, int textViewResourceId, List<GetAllDriverOutputModel.Datum> objects) {

        super(context, resource, textViewResourceId, objects);
        fullList = (ArrayList<GetAllDriverOutputModel.Datum>) objects;
        mOriginalValues = new ArrayList<GetAllDriverOutputModel.Datum>(fullList);
    }

    @Override
    public int getCount() {
        return fullList.size();
    }

    @Override
    public GetAllDriverOutputModel.Datum getItem(int position) {
        return fullList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
        }

        TextView strName = (TextView) convertView.findViewById(android.R.id.text1);
        strName.setText(getItem(position).getFullName());
        convertView.setTag(getItem(position).getId()+"-"+getItem(position).getFirstName()+ " "+getItem(position).getLastName());
        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new ArrayFilter();
        }
        return mFilter;
    }


    private class ArrayFilter extends Filter {
        private Object lock = new Object();

        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            FilterResults results = new FilterResults();

            if (mOriginalValues == null) {
                synchronized (lock) {
                    mOriginalValues = new ArrayList<GetAllDriverOutputModel.Datum>(fullList);
                }
            }

            if (prefix == null || prefix.length() == 0) {
                synchronized (lock) {
                    ArrayList<GetAllDriverOutputModel.Datum> list = new ArrayList<GetAllDriverOutputModel.Datum>(mOriginalValues);
                    results.values = list;
                    results.count = list.size();
                }
            } else {
                final String prefixString = prefix.toString().toLowerCase();

                ArrayList<GetAllDriverOutputModel.Datum> values = mOriginalValues;
                int count = values.size();

                ArrayList<GetAllDriverOutputModel.Datum> newValues = new ArrayList<GetAllDriverOutputModel.Datum>(count);

                for (int i = 0; i < count; i++) {
                    String item = values.get(i).getFullName();
                    if (item.toLowerCase().contains(prefixString)) {
                        newValues.add(values.get(i));
                    }

                }

                results.values = newValues;
                results.count = newValues.size();
            }

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            if(results.values!=null){
                fullList = (ArrayList<GetAllDriverOutputModel.Datum>) results.values;
            }else{
                fullList = new ArrayList<GetAllDriverOutputModel.Datum>();
            }
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}