package com.viame.viame_partner.database

import androidx.room.Database
import androidx.room.RoomDatabase


@Database(entities = [LocationTable::class], version = 1)
abstract class RoomDatabaseHelper : RoomDatabase() {
    abstract fun locationDao(): LocationDao
}