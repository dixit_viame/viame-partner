package com.viame.viame_partner.database

import androidx.room.*
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface LocationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(location: LocationTable): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(locations: List<LocationTable>): List<Long>

    @Query(value = "SELECT * FROM location_table WHERE id = :id")
    fun selectById(id: Long): Flowable<LocationTable>

    @Query(value = "SELECT * FROM location_table")
    fun selectAll(): Flowable<List<LocationTable>>

    @Query(value = "SELECT * FROM location_table ORDER BY timestamp DESC LIMIT 2")
    fun selectLastTwoRecords(): Single<List<LocationTable>>

    @Update
    fun update(location: LocationTable): Int

    @Query(value = "DELETE FROM location_table WHERE id = :id")
    fun deleteById(id: Long): Int

    @Query(value = "DELETE FROM location_table")
    fun deleteAll(): Int

}