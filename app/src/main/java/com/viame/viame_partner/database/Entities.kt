package com.viame.viame_partner.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "location_table")
data class LocationTable(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Long,
    @ColumnInfo(name = "latitude") var latitude: Double,
    @ColumnInfo(name = "longitude") var longitude: Double,
    @ColumnInfo(name = "timestamp") val timestamp: Long,
    @ColumnInfo(name = "altitude") val altitude: String,
    @ColumnInfo(name = "speed") val speed: String,
    @ColumnInfo(name = "bearing") val bearing: String,
    @ColumnInfo(name = "provider") val provider: String,
    @ColumnInfo(name = "accuracy") val accuracy: String
)