package com.viame.viame_partner.dagger.modules;

import android.content.Context;

import androidx.work.WorkerFactory;

import com.amazonaws.mobile.config.AWSConfiguration;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.services.s3.AmazonS3Client;
import com.viame.viame_partner.aws.AWSHelperImpl;
import com.viame.viame_partner.aws.AWSRepository;
import dagger.Module;
import dagger.Provides;
import dagger.Reusable;
import kotlin.jvm.JvmSuppressWildcards;

@Module
public class AWSModule {

    @JvmSuppressWildcards
    @Provides
    @Reusable
    AWSHelperImpl providesAWSHelperImpl(Context  mContext)
    {
        return new AWSHelperImpl(mContext);
    }

    @JvmSuppressWildcards
    @Provides
    @Reusable
    AmazonS3Client providesAmazonS3Client(AWSHelperImpl mAWSHelperImpl)  {
        AmazonS3Client s3 = new AmazonS3Client(mAWSHelperImpl.getAWSCredentialsProvider());
        s3.setRegion(Region.getRegion(mAWSHelperImpl.getS3BucketRegion()));
        return s3;
    }


    @JvmSuppressWildcards
    @Provides
    @Reusable
    TransferUtility providesTransferUtilityprovidesTransferUtility(Context context,AmazonS3Client s3, AWSHelperImpl sAWSHelper)  {
        return TransferUtility.builder()
                .context(context)
                .s3Client(s3)
                .awsConfiguration(new AWSConfiguration(context))
                //.defaultBucket(sAWSHelper.getS3BucketName())
                .build();
    }

    @JvmSuppressWildcards
    @Provides
    @Reusable
    AWSRepository providesRemoteDrawingRepository(AmazonS3Client s3 ,
                                                  TransferUtility transferUtility,
                                                  AWSHelperImpl awsHelper)
    {
        return new AWSRepository(s3, transferUtility, awsHelper);
    }


}
