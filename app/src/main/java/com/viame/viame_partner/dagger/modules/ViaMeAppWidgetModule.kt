package com.viame.viame_partner.dagger.modules

import com.viame.viame_partner.homewidget.ViaMeBookingWidget
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ViaMeAppWidgetModule {
    @ContributesAndroidInjector
    abstract fun contributeViaMeBookingWidget(): ViaMeBookingWidget
}