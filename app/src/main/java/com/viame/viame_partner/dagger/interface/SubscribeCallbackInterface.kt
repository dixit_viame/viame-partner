package com.viame.viame_partner.dagger.`interface`

import com.pubnub.api.models.consumer.PNStatus
import com.pubnub.api.models.consumer.pubsub.PNMessageResult
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult

interface SubscribeCallbackInterface
{
    fun onPNConnected()
    fun onNewMessage(result: PNMessageResult?)
    fun onPresence(presence: PNPresenceEventResult?)
    fun onStatusUpdate(status: PNStatus)
}