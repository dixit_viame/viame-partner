package com.viame.viame_partner.dagger.modules

import androidx.work.ListenableWorker
import com.squareup.inject.assisted.dagger2.AssistedModule
import com.viame.viame_partner.workmanager.ChildWorkerFactory
import com.viame.viame_partner.workmanager.NotifyLocationBackGroundWork
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass


@Module(includes = [AssistedInject_SampleAssistedInjectModule::class])
@AssistedModule
interface SampleAssistedInjectModule

@MapKey
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class WorkerKey(val value: KClass<out ListenableWorker>)

@Module
interface WorkerBindingModule {
    @Binds
    @IntoMap
    @WorkerKey(NotifyLocationBackGroundWork::class)
    fun bindHelloWorldWorker(factory: NotifyLocationBackGroundWork.Factory): ChildWorkerFactory
}