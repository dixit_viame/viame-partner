package com.viame.viame_partner.dagger.modules;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class MediaModule
{

    @Singleton
    @Provides
    MediaPlayer providesMediaPlayer(){
        MediaPlayer mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setLooping(true);
        return mediaPlayer;
    }

}
