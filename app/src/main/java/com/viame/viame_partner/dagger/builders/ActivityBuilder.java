package com.viame.viame_partner.dagger.builders;

import com.viame.viame_partner.ui.bookingaccept.BookingAcceptDialogProvider;
import com.viame.viame_partner.ui.bookingcancel.BookingCancelActivity;
import com.viame.viame_partner.ui.bookingcancel.BookingCancelActivityModule;
import com.viame.viame_partner.ui.bookingcomplete.BookingCompletedActivity;
import com.viame.viame_partner.ui.bookingcomplete.BookingCompletedActivityModule;
import com.viame.viame_partner.ui.bookingdetail.BookingDetailActivity;
import com.viame.viame_partner.ui.bookingdetail.BookingDetailActivityModule;
import com.viame.viame_partner.ui.bookingdetail.details.DetailPagerFragmentProvider;
import com.viame.viame_partner.ui.bookingdetail.timeline.TimelinePagerFragmentProvider;
import com.viame.viame_partner.ui.forgotpassword.ForgotPasswordActivity;
import com.viame.viame_partner.ui.forgotpassword.ForgotPasswordActivityModule;
import com.viame.viame_partner.ui.home.HomeFragmentProvider;
import com.viame.viame_partner.ui.jobhistory.JobHistoryFragment;
import com.viame.viame_partner.ui.jobhistory.JobHistoryFragmentProvider;
import com.viame.viame_partner.ui.logout.LogoutFragmentProvider;
import com.viame.viame_partner.ui.main.MainActivity;
import com.viame.viame_partner.ui.main.MainActivityModule;
import com.viame.viame_partner.ui.myorders.MyOrdersFragmentProvider;
import com.viame.viame_partner.ui.pickupdrop.PickupDropActivity;
import com.viame.viame_partner.ui.pickupdrop.PickupDropActivityModule;
import com.viame.viame_partner.ui.profile.ProfileFragmentProvider;
import com.viame.viame_partner.ui.settings.SettingsFragmentProvider;
import com.viame.viame_partner.ui.signin.SignInActivity;
import com.viame.viame_partner.ui.signin.SignInActivityModule;
import com.viame.viame_partner.ui.splash.SplashScreenActivity;
import com.viame.viame_partner.ui.splash.SplashScreenActivityModule;
import com.viame.viame_partner.ui.transfer_order.TransferOrderActivity;
import com.viame.viame_partner.ui.transfer_order.TransferOrderActivityModel;
import com.viame.viame_partner.ui.transfer_order.TransferOrderActivityModule;
import com.viame.viame_partner.ui.unassigned.UnAssignedFragmentProvider;
import com.viame.viame_partner.ui.vehicle.VehicleListActivity;
import com.viame.viame_partner.ui.vehicle.VehicleListActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;


@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = SplashScreenActivityModule.class)
    abstract SplashScreenActivity contributeSplashScreenActivity();

    @ContributesAndroidInjector(modules = SignInActivityModule.class)
    abstract SignInActivity contributeSignInActivity();

    @ContributesAndroidInjector(modules = VehicleListActivityModule.class)
    abstract VehicleListActivity contributeVehicleListActivity();

    @ContributesAndroidInjector(modules = { MainActivityModule.class,
             HomeFragmentProvider.class,
             MyOrdersFragmentProvider.class,
             UnAssignedFragmentProvider.class,
             BookingAcceptDialogProvider.class,
             ProfileFragmentProvider.class,
             JobHistoryFragmentProvider.class,
             LogoutFragmentProvider.class,
             SettingsFragmentProvider.class})
    abstract MainActivity contributeMainActivity();

    @ContributesAndroidInjector(modules = { BookingDetailActivityModule.class,
            DetailPagerFragmentProvider.class,
            TimelinePagerFragmentProvider.class})
    abstract BookingDetailActivity contributeBookingDetailActivity();

    @ContributesAndroidInjector(modules = PickupDropActivityModule.class)
    abstract PickupDropActivity contributePickupDropActivity();

    @ContributesAndroidInjector(modules = TransferOrderActivityModule.class)
    abstract TransferOrderActivity contributeTransferOrderActivity();

    @ContributesAndroidInjector(modules = BookingCancelActivityModule.class)
    abstract BookingCancelActivity contributeBookingCancelActivity();

    @ContributesAndroidInjector(modules = BookingCompletedActivityModule.class)
    abstract BookingCompletedActivity contributeBookingCompletedActivity();

    @ContributesAndroidInjector(modules = ForgotPasswordActivityModule.class)
    abstract ForgotPasswordActivity contributeForgotPasswordActivity();
}