package com.viame.viame_partner.dagger.modules;

import android.content.Context;

import androidx.room.Room;

import com.google.android.gms.location.LocationRequest;
import com.google.gson.Gson;
import com.viame.viame_partner.application.CatalogApplication;
import com.viame.viame_partner.database.RoomDatabaseHelper;
import com.viame.viame_partner.location.LocationCallRepository;
import com.viame.viame_partner.models.MainAppStore;
import com.viame.viame_partner.models.OfflineStore;
import com.viame.viame_partner.prefs.PrefEntity;
import com.viame.viame_partner.prefs.Preferences;
import com.viame.viame_partner.rx.RxBus;
import com.viame.viame_partner.utils.BatteryStatusListener;
import com.viame.viame_partner.utils.ConnectivityReceiver;
import com.viame.viame_partner.utils.GpsStatusListener;
import com.viame.viame_partner.utils.Utils;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import kotlin.jvm.JvmStatic;
import prithvi.io.workmanager.utility.rx.AppScheduler;
import com.viame.viame_partner.rx.Scheduler;

import org.greenrobot.eventbus.EventBus;


@Module
public class HorizonAppModule {

    @Singleton
    @Provides
    Context provideContext(CatalogApplication application) {
        return application;
    }

    @Singleton
    @Provides
    MainAppStore provideMainAppStore(OfflineStore offlineStore) {
        return new MainAppStore(offlineStore);
    }

    @Singleton
    @Provides
    Gson provideGson() {
        return new Gson();
    }

    @Singleton
    @Provides
    Utils provideUtils(Context context) {
        return new Utils(context);
    }

    @Singleton
    @Provides
    ConnectivityReceiver provideConnectivityReceiver(Context context) {
        return new ConnectivityReceiver(context);
    }

    @Singleton
    @Provides
    GpsStatusListener provideGpsStatusListener(Context context) {
        return new GpsStatusListener(context);
    }

    @Singleton
    @Provides
    BatteryStatusListener provideBatteryStatusListener(Context context) {
        return new BatteryStatusListener(context);
    }

    @Provides
    @Singleton
    @JvmStatic
    Scheduler provideScheduler() {
        return new AppScheduler();
    }

    @Provides
    @Singleton
    @JvmStatic
    LocationRequest locationRequest() {
        return LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(Preferences.getPreferenceInt(PrefEntity.PRESENCE_INTERVAL_TIME) * 1000)
                .setSmallestDisplacement(20);
    }

    @Provides
    @Singleton
    EventBus provideEventBus() {
        return EventBus.getDefault();
    }

    @Provides
    @Singleton
    @JvmStatic
    RxBus provideRxBus() {
        return new RxBus();
    }

    @Provides
    @Singleton
    @JvmStatic
    LocationCallRepository provideLocationCallRepository(CatalogApplication application, RoomDatabaseHelper helper)
    {
        return new LocationCallRepository(application,helper);
    }

    @Provides
    @Singleton
    @JvmStatic
    RoomDatabaseHelper provideRoomDatabaseHelper(Context context)
    {
        RoomDatabaseHelper db = Room.databaseBuilder(context,
                RoomDatabaseHelper.class, "ViaMe_Partner.db").allowMainThreadQueries().build();
        return db;
    }
}
