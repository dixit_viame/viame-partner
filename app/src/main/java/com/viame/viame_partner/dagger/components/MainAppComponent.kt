package com.viame.viame_partner.dagger.components

import com.viame.viame_partner.application.CatalogApplication
import com.viame.viame_partner.dagger.builders.ActivityBuilder
import com.viame.viame_partner.dagger.modules.*
import com.viame.viame_partner.workmanager.SampleWorkerFactory
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [  AndroidSupportInjectionModule::class,
                        HorizonAppModule::class,
                        NetworkModule::class,
                        ActivityBuilder::class,
                        AWSModule::class,
                        PubNubModule::class,
                        ViaMeAppWidgetModule::class,
                        FirebaseServiceModule::class,
                        SampleAssistedInjectModule::class,
                        WorkerBindingModule::class,
                        BroadcastReceiverModule::class,
                        MediaModule::class,
                        IntentServiceModule::class])
interface MainAppComponent : AndroidInjector<CatalogApplication?> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<CatalogApplication?>() {}

    fun factory(): SampleWorkerFactory?

    fun create(): MainAppComponent?
}