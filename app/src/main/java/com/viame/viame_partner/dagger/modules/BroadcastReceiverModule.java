package com.viame.viame_partner.dagger.modules;

import com.viame.viame_partner.workmanager.AlarmReceiver;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class BroadcastReceiverModule {

    @ContributesAndroidInjector
    abstract AlarmReceiver contributesAlarmReceiver();

}
