package com.viame.viame_partner.dagger.modules;

import com.viame.viame_partner.network.ApiInterface;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

import static com.viame.viame_partner.APIKt.BASE_URL;
import static com.viame.viame_partner.ConstKt.HTTP_REQUEST_TIMEOUT;

@Module
public class NetworkModule {

    @Singleton
    @Provides
    final OkHttpClient providesClient(){
        // HttpLoggingInterceptor
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(message -> Timber.i(message));
        httpLoggingInterceptor.level(HttpLoggingInterceptor.Level.BODY);
        /*
         * injection of interceptors to handle encryption and decryption
         */
        //Encryption Interceptor
        //EncryptionInterceptor encryptionInterceptor = new EncryptionInterceptor(new EncryptionImpl());
        //Decryption Interceptor
        // DecryptionInterceptor decryptionInterceptor = new DecryptionInterceptor(new DecryptionImpl());

        //.addInterceptor(encryptionInterceptor)
        //.addInterceptor(decryptionInterceptor)

        return new OkHttpClient.Builder()
                .connectTimeout(HTTP_REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(HTTP_REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(httpLoggingInterceptor)
                //.addInterceptor(encryptionInterceptor)
                //.addInterceptor(decryptionInterceptor)
                .build();
    }

    @Singleton
    @Provides
    Retrofit provideRetrofit(){
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(providesClient())
                .build();
    }

    @Singleton
    @Provides
    ApiInterface provideApiService(Retrofit retrofit){
        return retrofit.create(ApiInterface.class);
    }

}
