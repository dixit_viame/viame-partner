package com.viame.viame_partner.dagger.modules

import com.viame.viame_partner.firebase.MyFirebaseMessagingService
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FirebaseServiceModule {
    @ContributesAndroidInjector
    abstract fun contributeMyFirebaseMessagingService(): MyFirebaseMessagingService
}