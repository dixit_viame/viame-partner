package com.viame.viame_partner.dagger.modules

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.core.app.NotificationManagerCompat
import com.google.gson.Gson
import com.pubnub.api.PNConfiguration
import com.pubnub.api.PubNub
import com.pubnub.api.callbacks.SubscribeCallback
import com.pubnub.api.enums.PNHeartbeatNotificationOptions
import com.pubnub.api.enums.PNLogVerbosity
import com.pubnub.api.enums.PNStatusCategory
import com.pubnub.api.models.consumer.PNStatus
import com.pubnub.api.models.consumer.pubsub.PNMessageResult
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult
import com.viame.viame_partner.*
import com.viame.viame_partner.application.CatalogApplication
import com.viame.viame_partner.aws.AWSHelperImpl
import com.viame.viame_partner.dagger.`interface`.SubscribeCallbackInterface
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.bookingaccept.model.BookingAcceptInput
import com.viame.viame_partner.ui.bookingaccept.model.BookingAcceptOutput
import com.viame.viame_partner.ui.bookingaccept.model.BookingData
import com.viame.viame_partner.ui.bookingaccept.model.PubNubBookingDataModel
import com.viame.viame_partner.ui.main.MainActivity
import com.viame.viame_partner.utils.Alerts
import com.viame.viame_partner.utils.LinNotify
import com.viame.viame_partner.utils.Utils
import dagger.Module
import dagger.Provides
import dagger.Reusable
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.HashMap
import javax.inject.Inject
import javax.inject.Singleton

@Module
class PubNubModule {

    companion object{
     var pubNubCallbacks : SubscribeCallbackInterface? = null
    }


    @Provides
    @Singleton
    fun providesPNConfiguration(): PNConfiguration {
        Timber.i("UUID "+Preferences.getPreferenceString(PrefEntity.DRIVER_UUID))
        return PNConfiguration().setSubscribeKey(Preferences.getPreferenceString(PrefEntity.SUBSCRIBE_KEY))
            .setPublishKey(Preferences.getPreferenceString(PrefEntity.PUBLISH_KEY))
            .setUuid(Preferences.getPreferenceString(PrefEntity.DRIVER_UUID))
            .setSecure(false)
            .setHeartbeatNotificationOptions(PNHeartbeatNotificationOptions.NONE)
            .setLogVerbosity(PNLogVerbosity.NONE)
            .setPresenceTimeoutWithCustomInterval(30, 10)
    }

    @Provides
    @Singleton
    fun providesPubNub(
        pnConfiguration: PNConfiguration?,
        subscribeCallback: SubscribeCallback?
    ): PubNub {
        val pubNub = PubNub(pnConfiguration)
        pubNub.subscribe()
            .withPresence()
            .channels(listOf(Preferences.getPreferenceString(PrefEntity.PRESENCE_CHANNEL),
                       Preferences.getPreferenceString(PrefEntity.DRIVER_CHANNEL)))
            .execute()
        pubNub.addListener(subscribeCallback)
        return pubNub
    }


    @Provides
    @Singleton
    fun providesSubscribeCallback(context: Context): SubscribeCallback {
        return object : SubscribeCallback() {
            override fun status(pubnub: PubNub?, status: PNStatus) {
                pubNubCallbacks?.onStatusUpdate(status)
                try { // handle any status
                    if (status.category == PNStatusCategory.PNUnexpectedDisconnectCategory) {
                        // internet got lost, do some magic and call reconnect when ready
                        if (Utils.isNetworkAvailable(context) && pubnub != null)
                            pubnub.reconnect()

                    } else if (status.category == PNStatusCategory.PNConnectedCategory)
                    {
                        Timber.e("subscribe() PNConnectedCategory :")
                    } else if (status.category == PNStatusCategory.PNReconnectedCategory) {

                        Timber.e("subscribe() PNReconnectedCategory :")
                    } else if (status.category == PNStatusCategory.PNDecryptionErrorCategory) {

                        Timber.e("subscribe() PNDecryptionErrorCategory :")
                    } else if (status.category == PNStatusCategory.PNTimeoutCategory) { // do some magic and call reconnect when ready

                        Timber.e("subscribe() PNConnectedCategory :")
                        pubnub!!.reconnect()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun message(pubnub: PubNub?, message: PNMessageResult?) {
                pubNubCallbacks?.onNewMessage(message)

                Timber.e("PubNub Message : "+ (message?.message))

                // Old Booking Data : Message data payload:{"a":11,"dt":"2020-2-18 16:18:00","pickZone":"Dubai","dropZone":"Dubai",
                // "amount":"35.00","dropDt":"2020-02-18 16:26:00","bid":1582028332483,"adr1":"Capital Golden Tower - Dubai - United Arab Emirates",
                // "drop1":"Unnamed Road - دبي - United Arab Emirates","serverTime":1582028352,"chn":"slave_4894897fc701dc9b",
                // "ExpiryTimer":"30","category":"","subcategory":"","subsubcategory":"","dis":"6.79","goodsTypeImg":"",
                // "paymentType":"Cash","helpers":0,"drop_adr":"Unnamed Road - دبي - United Arab Emirates",
                // "pickup_adr":"Capital Golden Tower - Dubai - United Arab Emirates","PingId":1,"created_at":"2020-02-18T12:19:11.824Z",
                // "updated_at":"2020-02-18T12:19:11.824Z","_id":"5e4bd63f1d2dfa0019801d55"}

//                try {
//                    if(Preferences.getPreferenceBoolean(PrefEntity.IS_LOGIN) &&
//                        Preferences.getPreferenceInt(PrefEntity.DRIVER_PRESENCE_STATUS) == DriverStatusType.ONLINE.id) {
//                        val rndInt = (0..1000).random()
//
//                        val mbookingData = Gson().fromJson(message?.message.toString(), PubNubBookingDataModel::class.java)
//
//                        if (mbookingData != null && mbookingData.a == 11) {
//
//                            var mbookingDataResponse = BookingData()
//
//                            mbookingDataResponse.bid = mbookingData.bid.toLong()
//                            mbookingDataResponse.amount = mbookingData.amount.toString()
//                            mbookingDataResponse.bookingTime = mbookingData.createdAt
//                            mbookingDataResponse.bookingType = "COD"
//                            mbookingDataResponse.channelCustomer = mbookingData.chn
//                            mbookingDataResponse.clientName = "ViaMe"
//                            mbookingDataResponse.distance = mbookingData.dis
//                            mbookingDataResponse.dropAdr = mbookingData.drop1
//                            mbookingDataResponse.pickupAdr = mbookingData.adr1
//                            mbookingDataResponse.expiryTimer = mbookingData.expiryTimer.toLong()
//                            mbookingDataResponse.helpers = mbookingData.helpers.toString()
//                            mbookingDataResponse.serverTime = mbookingData.serverTime.toLong()
//
//                            val intent = Intent(CatalogApplication.instance, MainActivity::class.java)
//                            intent.flags =
//                                Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
//
//                            var mBundle = Bundle()
//                            mBundle.putInt(
//                                MainActivity.NOTIFICATION_ACTION,
//                                rndInt
//                            )
//                            mBundle.putString(MainActivity.NOTIFICATION_COME_FROM, "Notification")
//                            mBundle.putSerializable(
//                                MainActivity.NOTIFICATION_DATA,
//                                mbookingDataResponse
//                            )
//                            mBundle.putInt(MainActivity.NOTIFICATION_BOOKING_RESULT, 3)
//                            intent.putExtras(mBundle)
//                            CatalogApplication.instance?.startActivity(intent)
//
//                            val pendingIntent = PendingIntent.getActivity(
//                                CatalogApplication.instance,
//                                BOOKING_NOTIFICATION_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT
//                            )
//
//                            LinNotify.showBigTextStyle(
//                                CatalogApplication.instance,
//                                R.drawable.ic_viame_notification,
//                                R.drawable.ic_viame_notification,
//                                CatalogApplication.instance?.getString(R.string.new_booking),
//                                "Bid = " + mbookingDataResponse.bid,
//                                CatalogApplication.instance?.getString(R.string.new_booking),
//                                "Pickup: " + mbookingDataResponse.pickupAdr + "\nDropoff: " + mbookingDataResponse.dropAdr
//                                        + "\nBookingType: COD\nAmount: " + mbookingDataResponse.amount,
//                                CatalogApplication.instance?.getString(R.string.new_booking),
//                                null,
//                                BOOKING_NOTIFICATION_ID,
//                                NOTIFICATION_CHANNEL_ID,
//                                pendingIntent,
//                                mBundle,false
//                            )
//                        }
//                    }
//                } catch (e: Exception) {
//                    e.printStackTrace()
//                }
            }

            override fun presence(pubnub: PubNub?, presence: PNPresenceEventResult?) {
                pubNubCallbacks?.onPresence(presence)
            }
        }
    }
}