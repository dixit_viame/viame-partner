package com.viame.viame_partner.dagger.modules;

import com.viame.viame_partner.service.BigTextBookingIntentService;
import com.viame.viame_partner.service.MyLocationBindService;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class IntentServiceModule {

    @ContributesAndroidInjector
    abstract BigTextBookingIntentService contibuteService();

    @ContributesAndroidInjector
    abstract MyLocationBindService contibuteLocationService();
}