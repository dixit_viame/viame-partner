package com.viame.viame_partner.models;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import javax.inject.Inject;


public class MainAppStore {

    private OfflineStore offlineStore;

    @Inject
    public MainAppStore(OfflineStore offlineStore){
        this.offlineStore = offlineStore;
    }

    public void cacheUser(@NonNull UserProfile userProfile){
        offlineStore.cacheUser(userProfile);
    }

    @Nullable
    public UserProfile getCachedUser(){
        return offlineStore.getCachedUser();
    }

    public void setFirstTimeLaunch(){
        offlineStore.setFirstTimeLaunch();
    }

    public boolean isFirstTimeLaunch(){
        return offlineStore.isFirstTimeLaunch();
    }

    public boolean isLoggedIn(){
        return offlineStore.isLoggedIn();
    }

    public void setLoggedIn(boolean loggedIn){
        offlineStore.setLoggedIn(loggedIn);
    }

}
