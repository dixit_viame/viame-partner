package com.viame.viame_partner.models;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.viame.viame_partner.prefs.PrefEntity;
import com.viame.viame_partner.prefs.Preferences;

import javax.inject.Inject;


public class OfflineStore {


    @Inject
    public OfflineStore(){

    }

    public void cacheUser(@NonNull UserProfile userProfile){

    }

    @Nullable
    public UserProfile getCachedUser(){
        return new UserProfile(); //prefManager.getCachedUser();
    }

    public void setFirstTimeLaunch(){
        //prefManager.setFirstTimeLaunch(false);
    }

    public boolean isFirstTimeLaunch(){
        return true ; //prefManager.isFirstTimeLaunch();
    }

    public boolean isLoggedIn(){
        return  Preferences.getPreferenceBoolean(PrefEntity.IS_LOGIN);
    }

    public void setLoggedIn(boolean loggedIn){
        //prefManager.setLoggedIn(loggedIn);

    }
}
