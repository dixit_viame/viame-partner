package com.viame.viame_partner.network;

import com.viame.viame_partner.location.model.PublishLocationModel;
import com.viame.viame_partner.location.model.PublishLocationResponseModel;
import com.viame.viame_partner.ui.bookingaccept.model.AcknowledgeBookingInputModel;
import com.viame.viame_partner.ui.bookingaccept.model.BookingAcceptInput;
import com.viame.viame_partner.ui.bookingaccept.model.BookingAcceptOutput;
import com.viame.viame_partner.ui.bookingcancel.model.CancelBookingInputModel;
import com.viame.viame_partner.ui.bookingcancel.model.CancelReasonsResponseModel;
import com.viame.viame_partner.ui.bookingdetail.details.model.BookingStatusInputModel;
import com.viame.viame_partner.ui.bookingdetail.details.model.BookingStatusOutputModel;
import com.viame.viame_partner.ui.home.model.GetStatusUpdateInputModel;
import com.viame.viame_partner.ui.home.model.GetStatusUpdateModel;
import com.viame.viame_partner.ui.jobhistory.model.JobHistoryInputDataModel;
import com.viame.viame_partner.ui.jobhistory.model.JobHistoryOututDataModel;
import com.viame.viame_partner.ui.logout.model.LogoutResponseModel;
import com.viame.viame_partner.ui.main.models.GetConfigOutputResponse;
import com.viame.viame_partner.ui.myorders.model.MyOrdersOutputResponseModel;
import com.viame.viame_partner.ui.profile.model.NewProfileDataModel;
import com.viame.viame_partner.ui.profile.model.ProfileDataModel;
import com.viame.viame_partner.ui.profile.model.UpdateProfileDataModel;
import com.viame.viame_partner.ui.signin.model.SignInRequestModel;
import com.viame.viame_partner.ui.signin.model.SignInResponseModel;
import com.viame.viame_partner.ui.transfer_order.model.GetAllDriverOutputModel;
import com.viame.viame_partner.ui.transfer_order.model.TransferOrderInputModel;
import com.viame.viame_partner.ui.transfer_order.model.TransferOrderOutputModel;
import com.viame.viame_partner.ui.unassigned.model.UnAssignedAcceptInputModel;
import com.viame.viame_partner.ui.unassigned.model.UnAssignedAcceptOutputModel;
import com.viame.viame_partner.ui.unassigned.model.UnAssignedResponseModel;
import com.viame.viame_partner.ui.vehicle.model.VehicleConfirmInputModel;
import com.viame.viame_partner.ui.vehicle.model.VehicleConfirmOutPutModel;
import com.viame.viame_partner.ui.vehicle.model.VehicleListMainModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Url;

public interface ApiInterface {

    // Sign In : SignIn Screen
    @POST("/master/signin")
    Call<SignInResponseModel> callLoginMethod(@HeaderMap Map<String, String> headers, @Body SignInRequestModel order);

    // Vehicle List Confirm : VehicleList Screen
    @POST("/master/vehicleDefault")
    Call<VehicleConfirmOutPutModel> callVehicleConfirmMethod(@HeaderMap Map<String, String> headers, @Body VehicleConfirmInputModel data);

    // Getting Vehicle List : VehicleList Screen
    @GET()
    Call<VehicleListMainModel> callVehicleListMethod(@Url String URL, @HeaderMap Map<String, String> headers);

    // Assigned Trips : MyOrdersFragment
    @GET()
    Call<MyOrdersOutputResponseModel> callAssignedBookingsMethod(@Url String URL,@HeaderMap Map<String, String> headers);

    // UnAssigned Trips : UnAssignFragment
    @GET()
    Call<UnAssignedResponseModel> callAllBookingsMethod(@Url String URL, @HeaderMap Map<String, String> headers);

    // UnAssigned Accept booking Trips : UnAssignFragment
    @PUT()
    Call<UnAssignedAcceptOutputModel> callUnAssignedAcceptBookingsMethod(@Url String URL, @HeaderMap Map<String, String> headers,@Body UnAssignedAcceptInputModel model);

    // get Profile Details : Profile Screen
    @GET()
    Call<ProfileDataModel> callProfileDetailMethod(@Url String url,@HeaderMap Map<String, String> headers);

    // update Profile Details : Profile Screen
    @PUT()
    Call<UpdateProfileDataModel> callUpdateProfileDetailMethod(@Url String url, @HeaderMap Map<String, String> headers, @Body NewProfileDataModel model);

    // get master Trips : Job History Screen
    @GET()
    Call<JobHistoryOututDataModel> callTripsMethod(@Url String URL,@HeaderMap Map<String, String> headers);

    // get All Configuration data : Main Activity
    @GET() //
    Call<GetConfigOutputResponse> callConfigDataMethod(@Url String URL,@HeaderMap Map<String, String> headers);

    // get status details online : 3 / offline : 4 : Home Screen
    @PUT("/master/status")
    Call<GetStatusUpdateModel> callStatusChangeMethod(@HeaderMap Map<String, String> headers,@Body GetStatusUpdateInputModel data);

    // publish current location : NotifyLocationBackGroundWork : WorKManager worker
    @PUT("/master/location")
    Call<PublishLocationResponseModel> callPublishLocationMethod(@HeaderMap Map<String, String> headers, @Body PublishLocationModel data);

    // Logout : LogoutFragment : BASE_URL_ENGINE
    @POST()
    Call<LogoutResponseModel> callLogoutMethod(@Url String url,@HeaderMap Map<String, String> headers);

    // Respond to booking, Accept/Reject : BookingAcceptDialogFragment : BASE_URL_ENGINE
    @POST("/master/respondToRequest")
    Call<BookingAcceptOutput> callRespondToBookingMethod(@HeaderMap Map<String, String> headers, @Body BookingAcceptInput data);

    // Respond to booking, Accept/Reject : BookingAcceptDialogFragment : BASE_URL_ENGINE
    @POST("/master/ackbooking")
    Call<String> callAckBookingMethod(@HeaderMap Map<String, String> headers, @Body AcknowledgeBookingInputModel data);

    // send status of booking, bookingStatus : DetailPagerFragment
    @PUT("/master/bookingStatus")
    Call<BookingStatusOutputModel> callBookingStatusMethod(@HeaderMap Map<String, String> headers, @Body BookingStatusInputModel data);

    // cancel booking reason : BookingCancelActivity
    @GET()
    Call<CancelReasonsResponseModel> callBookingCancelReasonMethod(@Url String url , @HeaderMap Map<String, String> headers);

    // cancel booking : BookingCancelActivity
    @PUT("/master/cancelBooking")
    Call<CancelReasonsResponseModel> callBookingCancelMethod(@HeaderMap Map<String, String> headers, @Body CancelBookingInputModel model);

    @GET()
    Call<GetAllDriverOutputModel> callGetDriverListMethod(@Url String url, @HeaderMap Map<String, String> headers);

    @POST()
    Call<TransferOrderOutputModel> callTransferOrderMethod(@Url String url, @HeaderMap Map<String, String> headers, @Body TransferOrderInputModel model);

}
