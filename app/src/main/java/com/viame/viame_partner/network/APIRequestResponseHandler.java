package com.viame.viame_partner.network;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class APIRequestResponseHandler<T> {

    @NonNull
    public final AuthStatus status;

    public enum AuthStatus { SUCCESS, ERROR, LOADING, FAILED}

    public final T data;

    @Nullable
    public final String message;


    public APIRequestResponseHandler(@NonNull AuthStatus status, @Nullable T data, @Nullable String message) {
        this.status = status;
        this.data = data;
        this.message = message;
    }

    public static <T> APIRequestResponseHandler<T> success (T data) {
        return new APIRequestResponseHandler<>(AuthStatus.SUCCESS, data, null);
    }

    public static <T> APIRequestResponseHandler<T> error(String msg, T data) {
        return new APIRequestResponseHandler<>(AuthStatus.ERROR, data, msg);
    }

    public static <T> APIRequestResponseHandler<T> loading(T data) {
        return new APIRequestResponseHandler<>(AuthStatus.LOADING, data, null);
    }

    public static <T> APIRequestResponseHandler<T> failed() {
        return new APIRequestResponseHandler<>(AuthStatus.FAILED, null, null);
    }

}