package com.viame.viame_partner.prefs;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class PrefEntity {

	public static final String AUTH_TOKEN = "auth_token";
	public static final String IS_LOGIN = "isLogin";

    @Nullable
    public static final String USER_NAME = "login_username";
    @Nullable
    public static final String PASSWORD = "login_password";
    @Nullable
    public static final String KEY_EDGE_TO_EDGE_ENABLED = "edge_to_edge_enabled";
    @Nullable
    public static final String PREFERENCE_NIGHT_MODE = "preference_night_mode";
    @Nullable
    public static final String FIREBASE_MESSEGING_REG_ID = "firebase_registration_id";
    @Nullable
    public static final String PUSH_TOPICS = "firebase_push_topics";
    @Nullable
    public static final String REFERER_CODE = "referer_code";
    @Nullable
    public static final String PROFILE_PIC = "profile_pic";
    @Nullable
    public static final String SUBSCRIBE_KEY = "subscribe_key";
    @Nullable
    public static final String PUBLISH_KEY = "publish_key";
    @Nullable
    public static final String SERVER_CHANNEL = "server_channel";
    @Nullable
    public static final String PRESENCE_CHANNEL = "presence_channel";
    @Nullable
    public static final String VEHICLES = "vehicles_list";
    @Nullable
    public static final String DRIVER_CHANNEL = "driver_channel";
    @NotNull
    public static final String DRIVER_PRESENCE_STATUS = "driver_presence_status";
    @Nullable
    public static final String MID = "user_mid";
    @Nullable
    public static final String VEHICLE_TYPE_ID = "vehicle_type_id";
    @Nullable
    public static final String VEHICLE_WORK_PLACE_ID = "vehicle_work_place_id";

    // Currency symbol that is defined by admin panel Like AED, Dollar etc.
    @Nullable
    public static final String CURRENCY_SYMBOL = "currency_symbol";

    // Driver Email
    @Nullable
    public static final String EMAIL = "user_email";

    // This is the distance that will update lat-long of driver to customer.
    @NotNull
    public static final String MIN_DIST_FOR_ROUTE_ARRRAY = "min_distance_for_route_array";

    // Mileage symbol that is desinged by admin panel Like KM, Mile etc.
    @Nullable
    public static final String MILEAGE_METRIC_SYMBOL = "mileage_unit";

    // Fetching location from GPS sensor etc. every 2 second
    @Nullable
    public static final String PRESENCE_INTERVAL_TIME = "presence_interval_time";

    // Publish location send to server
    @Nullable
    public static final String PUB_NUB_SCHEDULE_INTERVAL_TIME = "pub_nub_driver_api_interval";

    // Distance conversion unit
    @Nullable
    public static final String DISTANCE_CONVERSION_UNIT = "distance_conversion_unit";

    // Check The Wallet is available or not.
    @NotNull
    public static final String IS_WALLET_ENABLE = "is_wallet_available";

    // Current wallet amount
    @NotNull
    public static final String WALLET_AMOUNT = "wallet_amount";

    // Current Soft limit : virtual card limit
    @NotNull
    public static final String SOFT_LIMIT = "soft_limit";

    // Current Hard limit : cash limit
    @NotNull
    public static final String HARD_LIMIT = "hard_limit";

    // Data for call driver status API with bookingid|customerchannel|status with comma seperated.
    @NotNull
    public static final String ALL_BOOKING_INFO = "all_booking_info";

    @NotNull
    public static final String LONGITUDE_CURRENT = "current_longitude";

    @NotNull
    public static final String LATITUDE_CURRENT = "current_latitude";

    @NotNull
    public static final String BATTERY_PERCENTAGE = "battery_percentage";

    // all bookings : [{"bid":"1578719601398","distance":0,"custChn":"slave_2252998998","status":"8","time_paused":1579326281407,"time_elapsed":15}]
    @NotNull
    public static final String BOOKING_DETAILS = "booking_details";

    @NotNull
    public static final String DISTANCE_IN_DOUBLE = "distance_in_double";

    @NotNull
    public static final String DISTANCE = "distance";

    @NotNull
    public static final String GPS_STATUS = "gps_status";

    @Nullable
    public static final String DRIVER_UUID = "driver_uuid";

    @Nullable
    public static final String VEHICLE_IMAGE = "vehicle_image_path";

    @Nullable
    public static final String SKIP_PROTECTED_APP = "skipProtectedAppCheck";

    @NotNull
    public static final String IS_MAIN_ACTIVITY_IN_FOREGROUND = "main_activity_foreground";
}

