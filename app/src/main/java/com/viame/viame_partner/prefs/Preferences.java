package com.viame.viame_partner.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.viame.viame_partner.ConstKt;
import com.viame.viame_partner.application.CatalogApplication;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;


public class Preferences {

	  /**
     * @param context - pass context
     * @return SharedPreferences
     */
    public static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(ConstKt.PREFRENCE_FILE_NAME, Context.MODE_PRIVATE);
    }

    /**
     * @param key     - Constant key, will be used for accessing the stored value
     * @param val     - String value to be stored
     */
    public static void setPreference(String key, String val) {
        SharedPreferences settings = Preferences.getSharedPreferences(CatalogApplication.getInstance());
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, val);
        editor.commit();
       
    }

    /**
     * @param key     - Constant key, will be used for accessing the stored value
     * @param val     - String value to be stored
     */
    public static void setPreferenceFloat(String key, float val) {
        SharedPreferences settings = Preferences.getSharedPreferences(CatalogApplication.getInstance());
        SharedPreferences.Editor editor = settings.edit();
        editor.putFloat(key, val);
        editor.commit();
    }


    /**
     * @param key
     * @param val
     */
    public static void setPreference(String key, boolean val) {
        SharedPreferences settings = Preferences.getSharedPreferences(CatalogApplication.getInstance());
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key, val);
        editor.commit();
    }

    /**
     * @param key
     * @param val
     */
    public static void setPreferenceInt(String key, int val) {
        SharedPreferences settings = Preferences.getSharedPreferences(CatalogApplication.getInstance());
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, val);
        editor.commit();
    }


    /**
     * Add preferences
     *
     * @param key     - Constant key, will be used for accessing the stored value
     * @param val     - long value to be stored, mostly used to store FB Session value
     */
    public static void setPreferenceLong(String key, long val) {
        SharedPreferences settings = Preferences.getSharedPreferences(CatalogApplication.getInstance());
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(key, val);
        editor.commit();
    }
    

    /**
     * Add preferences
     *
     * @param key     - Constant key, will be used for accessing the stored value
     * @param array<String> val    - ArrayList<String> value to be stored, mostly used to store FB Session value
     */
    
    public static boolean setPreferenceArray(String key, ArrayList<String> array) {
        SharedPreferences prefs = Preferences.getSharedPreferences(CatalogApplication.getInstance());
        SharedPreferences.Editor editor = prefs.edit();  
        editor.putInt(key +"_size", array.size());  
        for(int i=0;i<array.size();i++)  
            editor.putString(key + "_" + i, array.get(i));  
        return editor.commit();  
    } 
    
    
    public static void clearPreferenceArray(String key)
    {
        SharedPreferences settings = Preferences.getSharedPreferences(CatalogApplication.getInstance());
        
        if(getPreferenceArray(key) != null && getPreferenceArray(key).size()>0)
        {        
        	for(String element :getPreferenceArray(key))
        	{
		        if (findPrefrenceKey( element) != null && settings.contains(findPrefrenceKey(element)))
		        {		        	
		            SharedPreferences.Editor editor = settings.edit();
		            editor.remove(findPrefrenceKey(element));
		            editor.commit();
		        }
        	}
        }
    }
    
    
   public static String findPrefrenceKey(String value)
    {
    	 SharedPreferences settings = Preferences.getSharedPreferences(CatalogApplication.getInstance());
         
         Map<String, ?> editor = settings.getAll();
         
         for (Entry<String, ?> entry : editor.entrySet())
         {
        	 if (value.equals(entry.getValue())) {
                 return entry.getKey();
             }
         }
       return null; // not found
    }
    

    /**
     * Remove preference key
     *
     * @param key     - the key which you stored before
     */
    public static void removePreference(String key) {
        SharedPreferences settings = Preferences.getSharedPreferences(CatalogApplication.getInstance());
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(key);
        editor.commit();
    }

    /**
     * Get preference value by passing related key
     *
     * @param key     - key value used when adding preference
     * @return - String value
     */
    public static String getPreferenceString(String key) {
        SharedPreferences prefs = Preferences.getSharedPreferences(CatalogApplication.getInstance());
        return prefs.getString(key, "");
    }
    
    /**
     * Get preference ArrayList<String> value by passing related key
     *
     * @param key     - key value used when adding preference
     * @return - ArrayList<String> value
     */
    
    public static ArrayList<String> getPreferenceArray(String key) {
        SharedPreferences prefs = Preferences.getSharedPreferences(CatalogApplication.getInstance());
        int size = prefs.getInt( key + "_size", 0);  
        ArrayList<String> array= new ArrayList<String>(size);  
        for(int i=0;i<size;i++)  
        	array.add(prefs.getString(key + "_" + i, null));  
        return array;  
    }  
    
    
  

    /**
     * Get preference value by passing related key
     *
     * @param key     - key value used when adding preference
     * @return - long value
     */
    public static long getPreferenceLong(String key) {
        SharedPreferences prefs = Preferences.getSharedPreferences(CatalogApplication.getInstance());
        return prefs.getLong(key, 0);

    }


    public static boolean getPreferenceBoolean(String key) {
        SharedPreferences prefs = Preferences.getSharedPreferences(CatalogApplication.getInstance());
        return prefs.getBoolean(key, false);

    }

    public static int getPreferenceInt(String key) {
        SharedPreferences prefs = Preferences.getSharedPreferences(CatalogApplication.getInstance());
        return prefs.getInt(key, 0);

    }


    /**
     * Clear all stored  preferences
     *
     */
    public static void removeAllPreference() {
        SharedPreferences settings = Preferences.getSharedPreferences(CatalogApplication.getInstance());
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();
    }
    
    /**
     * Clear all stored  preferences
     *
     */
    public static String  getAllPreference() {
        SharedPreferences settings = Preferences.getSharedPreferences(CatalogApplication.getInstance());
        Map<String, ?> editor = settings.getAll();
        String text="";

        try
        {
          for (Entry<String, ?> entry : editor.entrySet())
          {
        	  String key = entry.getKey();
        	  Object value = entry.getValue();
        	  // do stuff
        	  text+="\t"+key+" = "+value+"\t";
        	}        
        }
         catch (Exception e) {
			// TODO: handle exception
        	 e.printStackTrace();
		}
        
		return text;
       
    }

}
