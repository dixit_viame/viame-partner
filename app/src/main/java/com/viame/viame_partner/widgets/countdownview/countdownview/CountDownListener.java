package com.viame.viame_partner.widgets.countdownview.countdownview;

public interface CountDownListener {
	public void onFinishCountDown();
}