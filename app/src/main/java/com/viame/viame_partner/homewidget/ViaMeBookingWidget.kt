package com.viame.viame_partner.homewidget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Icon
import android.widget.RemoteViews
import com.viame.viame_partner.DriverStatusType
import com.viame.viame_partner.R
import com.viame.viame_partner.application.CatalogApplication
import com.viame.viame_partner.homewidget.ViaMeBookingWidget.Companion.onClickOpenApp
import com.viame.viame_partner.homewidget.ViaMeBookingWidget.Companion.onClickOpenStartApp
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.main.MainActivity
import com.viame.viame_partner.ui.splash.SplashScreenActivity
import dagger.android.AndroidInjection
import timber.log.Timber
import javax.inject.Inject


/**
 * Implementation of App Widget functionality.
 */
class ViaMeBookingWidget : AppWidgetProvider() {


    @Inject
    lateinit var apiInterface : ApiInterface

    override fun onReceive(context: Context?, intent: Intent?) {
        super.onReceive(context, intent)
        AndroidInjection.inject(this, context)
    }


    companion object {
        const val onClickOpenApp = "openAppTag"
        const val onClickOpenStartApp = "openAppStartTag"
    }

    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        AndroidInjection.inject(this, context)
        Timber.e("-------onUpdate------ ")
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }

    override fun onEnabled(context: Context) {
        AndroidInjection.inject(this, context)
        // Enter relevant functionality for when the first widget is created
        Timber.e("-------onEnabled------ ")
    }

    override fun onDisabled(context: Context) {
        AndroidInjection.inject(this, context)
        // Enter relevant functionality for when the last widget is disabled
        Timber.e("-------onDisabled------ ")
    }
}

internal fun updateAppWidget(
    context: Context,
    appWidgetManager: AppWidgetManager,
    appWidgetId: Int
) {
    // Construct the RemoteViews object
    val views : RemoteViews

    // Check wheather user is login or not if not the redirect to login page.
    if(Preferences.getPreferenceBoolean(PrefEntity.IS_LOGIN))
    {
        views = RemoteViews(context.packageName, R.layout.via_me_booking_widget)

        val status = Preferences.getPreferenceInt(PrefEntity.DRIVER_PRESENCE_STATUS)

        if (status == DriverStatusType.ONLINE.id)
        {
            views.setImageViewIcon(
                R.id.imgDriverOnlineOffline,
                Icon.createWithResource(CatalogApplication.instance, R.drawable.ic_online)
            )
            views.setTextViewText(
                R.id.tvOnlineOffline,
                CatalogApplication.instance?.getString(R.string.you_are_online)
            )
            views.setTextViewText(
                R.id.tvLblOnlineOffline,
                CatalogApplication.instance?.getString(R.string.go_offline_to_stop_accepting_jobs)
            )

        } else if (status == DriverStatusType.OFFLINE.id)
        {
            views.setImageViewIcon(
                R.id.imgDriverOnlineOffline,
                Icon.createWithResource(CatalogApplication.instance, R.drawable.ic_offline)
            )
            views.setTextViewText(
                R.id.tvOnlineOffline,
                CatalogApplication.instance?.getString(R.string.you_are_offline)
            )
            views.setTextViewText(
                R.id.tvLblOnlineOffline,
                CatalogApplication.instance?.getString(R.string.go_online_to_start_accepting_jobs)
            )
        }

        views.setOnClickPendingIntent(
            R.id.btnOpenApp,
            getPendingSelfIntent(context, onClickOpenApp)
        )
    }
    else
    {
        views = RemoteViews(context.packageName, R.layout.via_me_login_widget)

        views.setOnClickPendingIntent(
            R.id.btnOpenFromStartApp,
            getPendingSelfIntent(context, onClickOpenStartApp)
        )
    }

    Timber.e("-------updateAppWidget------ ")
    // Instruct the widget manager to update the widget
    appWidgetManager.updateAppWidget(appWidgetId, views)
}

fun getPendingSelfIntent(
    context: Context?,
    action: String?
): PendingIntent? {
    val intent = Intent(context, PendingReceiver::class.java)
    intent.action = action
    return PendingIntent.getBroadcast(context, 123, intent, PendingIntent.FLAG_UPDATE_CURRENT)
}


class PendingReceiver : BroadcastReceiver()
{
    override fun onReceive(context: Context?, intent: Intent?) {

        if (onClickOpenApp == intent?.action)
        {
            val intents = Intent(context, MainActivity::class.java)
            intents.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            context?.startActivity(intents)

        }
        else if (onClickOpenStartApp == intent?.action)
        {
            val intents = Intent(context, SplashScreenActivity::class.java)
            intents.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            context?.startActivity(intents)
        }
    }


}