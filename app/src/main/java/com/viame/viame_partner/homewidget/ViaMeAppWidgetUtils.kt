package com.viame.viame_partner.homewidget

import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent

// Send Message to AppWidget update when logged out.
fun updateWidgets(context: Context) {

    val intent = Intent(context.applicationContext, ViaMeBookingWidget::class.java)
    intent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
    val widgetManager = AppWidgetManager.getInstance(context)
    val ids = widgetManager.getAppWidgetIds(
        ComponentName(
            context,
            ViaMeBookingWidget::class.java
        )
    )
    widgetManager.notifyAppWidgetViewDataChanged(ids, android.R.id.list)
    // Use an array and EXTRA_APPWIDGET_IDS instead of AppWidgetManager.EXTRA_APPWIDGET_ID,
    // since it seems the onUpdate() is only fired on that:
    intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids)
    context.sendBroadcast(intent)

}