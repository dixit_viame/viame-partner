package com.viame.viame_partner.extension

import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kotlin.reflect.full.cast
import com.viame.viame_partner.rx.Scheduler
import io.reactivex.Single
import kotlin.reflect.KClass

fun <T> Flowable<T>.fromWorkerToMain(scheduler: Scheduler): Flowable<T> =
        this.subscribeOn(scheduler.io()).observeOn(scheduler.mainThread())

fun <T> Single<T>.fromSingleToMain(scheduler: Scheduler): Single<T> =
        this.subscribeOn(scheduler.io()).observeOn(scheduler.mainThread())

fun Disposable.addTo(compositeDisposable: CompositeDisposable) =
        compositeDisposable.add(this)

fun <T : Any, U: Any> Observable<T>.filterClass(kClass: KClass<U>): Observable<U> = this
        .filter { it::class == kClass }
        .map { kClass.cast(it) }


fun <T : Any, U: Any> Flowable<T>.filterClass(kClass: KClass<U>): Flowable<U> = this
        .filter { it::class == kClass }
        .map { kClass.cast(it) }
