
package com.viame.viame_partner.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.view.View;
import android.view.Window;

import androidx.annotation.ColorInt;
import androidx.annotation.RequiresApi;
import androidx.core.graphics.ColorUtils;

import com.viame.viame_partner.prefs.PrefEntity;
import com.viame.viame_partner.prefs.Preferences;

import static android.graphics.Color.TRANSPARENT;
import static android.view.View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR;
import static android.view.View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
import static android.view.View.SYSTEM_UI_FLAG_VISIBLE;

/** Helper that saves the current window preferences for the Catalog. */
public class WindowPreferencesManager {

  private static final int EDGE_TO_EDGE_BAR_ALPHA = 128;

  @RequiresApi(VERSION_CODES.LOLLIPOP)
  private static final int EDGE_TO_EDGE_FLAGS =
      View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;

  private final Context context;

  public WindowPreferencesManager(Context context) {
    this.context = context;
  }

  @SuppressWarnings("ApplySharedPref")
  public void toggleEdgeToEdgeEnabled() {
    Preferences.setPreference(PrefEntity.KEY_EDGE_TO_EDGE_ENABLED,!isEdgeToEdgeEnabled());
  }

  public boolean isEdgeToEdgeEnabled() {
    return Preferences.getPreferenceBoolean(PrefEntity.KEY_EDGE_TO_EDGE_ENABLED);
  }

  @SuppressWarnings("RestrictTo")
  public void applyEdgeToEdgePreference(Window window) {
    boolean edgeToEdgeEnabled = isEdgeToEdgeEnabled();

    int statusBarColor = getStatusBarColor(isEdgeToEdgeEnabled());
    int navbarColor = getNavBarColor(isEdgeToEdgeEnabled());

    boolean lightBackground = isColorLight(
        MaterialColors.getColor(context, android.R.attr.colorBackground, Color.BLACK));
    boolean lightNavbar = isColorLight(navbarColor);
    boolean showDarkNavbarIcons = lightNavbar || (navbarColor == TRANSPARENT && lightBackground);

    View decorView = window.getDecorView();
    int currentStatusBar = VERSION.SDK_INT >= VERSION_CODES.M
        ? decorView.getSystemUiVisibility() & SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        : 0;
    int currentNavBar = showDarkNavbarIcons && VERSION.SDK_INT >= VERSION_CODES.O
        ? SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
        : 0;

    window.setNavigationBarColor(navbarColor);
    window.setStatusBarColor(statusBarColor);
    int systemUiVisibility = (edgeToEdgeEnabled ? EDGE_TO_EDGE_FLAGS : SYSTEM_UI_FLAG_VISIBLE)
        | currentStatusBar
        | currentNavBar;

    decorView.setSystemUiVisibility(systemUiVisibility);
  }

  @SuppressWarnings("RestrictTo")
  @TargetApi(VERSION_CODES.LOLLIPOP)
  private int getStatusBarColor(boolean isEdgeToEdgeEnabled) {
    if (isEdgeToEdgeEnabled) {
      return TRANSPARENT;
    }
    return MaterialColors.getColor(context, android.R.attr.statusBarColor, Color.BLACK);
  }

  @SuppressWarnings("RestrictTo")
  @TargetApi(VERSION_CODES.LOLLIPOP)
  private int getNavBarColor(boolean isEdgeToEdgeEnabled) {
    if (isEdgeToEdgeEnabled && VERSION.SDK_INT < VERSION_CODES.O_MR1) {
      int opaqueNavBarColor =
          MaterialColors.getColor(context, android.R.attr.navigationBarColor, Color.BLACK);
      return ColorUtils.setAlphaComponent(opaqueNavBarColor, EDGE_TO_EDGE_BAR_ALPHA);
    }
    if (isEdgeToEdgeEnabled) {
      return TRANSPARENT;
    }
    return MaterialColors.getColor(context, android.R.attr.navigationBarColor, Color.BLACK);
  }

  private static boolean isColorLight(@ColorInt int color) {
    return color != TRANSPARENT && ColorUtils.calculateLuminance(color) > 0.5;
  }

}
