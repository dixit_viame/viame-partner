package com.viame.viame_partner.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewParent;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Dimension;
import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.viame.viame_partner.R;
import com.viame.viame_partner.application.CatalogApplication;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Timer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import timber.log.Timber;

public class Utils {

    Context mConText;

    public Utils(Context context) {
        mConText = context;
    }


    public static int getStatusBarHeight(Context context) {
        int height = (int) context.getResources().getDimension(R.dimen.dimen_24dp);
        return height;
    }


    public static void copyToClipboard(String copyText,Context context) {
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager)
                    context.getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData
                    .newPlainText("Your OTP", copyText);
            clipboard.setPrimaryClip(clip);
        try {
            Alerts.showSnackBar((Activity) context,"Copy to Clipboard");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean isNetworkAvail = false;
        try {
            if (cm != null) {
                if (Build.VERSION.SDK_INT < 23) {
                    final NetworkInfo ni = cm.getActiveNetworkInfo();

                    if (ni != null) {
                        isNetworkAvail = (ni.isConnected() && (ni.getType() == ConnectivityManager.TYPE_WIFI || ni.getType() == ConnectivityManager.TYPE_MOBILE));

                        return isNetworkAvail;
                    }
                } else {
                    final Network n = cm.getActiveNetwork();

                    if (n != null) {
                        final NetworkCapabilities nc = cm.getNetworkCapabilities(n);

                        isNetworkAvail = (nc.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || nc.hasTransport(NetworkCapabilities.TRANSPORT_WIFI));

                        return isNetworkAvail;
                    }
                }
            }
            return isNetworkAvail;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cm != null) {
                cm = null;
            }
        }
        return false;
    }

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public static float dpToPx(@NonNull Context context, @Dimension(unit = Dimension.DP) int dp) {
        Resources r = context.getResources();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
    }

    public static int dpToPx(float dp, Context context) {
        return dpToPx(dp, context.getResources());
    }

    private static int dpToPx(float dp, Resources resources) {
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.getDisplayMetrics());
        return (int) px;
    }

    /**
     * <h2>copyStream</h2>
     * <p>
     * method to copy the input stream to output stream
     * </P>
     *
     * @param input:  contains the image input byte streams
     * @param output: image output streams
     * @throws IOException : exception
     */
    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    /**
     * +     * <h2>dateFormatter</h2>
     * +     * This method is used to convert the date format to MMM dd, hh:mm a
     * +     * <p>
     * +     *     Convert the date from yyyy-MM-dd HH:mm to MMM dd, hh:mm a
     * +     * </p>
     * +     * @param dateToBeConverted input the date to be converted
     * +     * @return returns the Converted date
     * +
     */
    public static String dateFormatter(String dateToBeConverted) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        SimpleDateFormat fort = new SimpleDateFormat("MMM dd, hh:mm a", Locale.US);
        Date date;
        try {
            if (dateToBeConverted != null) {
                date = formatter.parse(dateToBeConverted);
                return fort.format(date);
            }
        } catch (Exception e) {
        }
        return "";
    }


    /**
     * Return date in specified format.
     * @param milliSeconds Date in milliseconds
     * @param dateFormat Date format
     * @return String representing date in specified format
     */
    public static String getDateFormatFromMiliseconds(long milliSeconds, String dateFormat)
    {
        try {
            // Create a DateFormatter object for displaying date in specified format.
            SimpleDateFormat formatter = new SimpleDateFormat(dateFormat, Locale.US);

            // Create a calendar object that will convert the date and time value in milliseconds to date.
           // Calendar calendar = Calendar.getInstance();
           // calendar.setTimeInMillis(milliSeconds);
            formatter.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
            Date dateTime = new Date(milliSeconds);
            return formatter.format(dateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getDateCurrentTimeZone(long timestamp) {
        try{
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = TimeZone.getTimeZone("GMT");
            calendar.setTimeInMillis(timestamp * 1000);
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, hh:mm a", Locale.US);
            Date currenTimeZone = (Date) calendar.getTime();
            return sdf.format(currenTimeZone);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    public static String dateFormatterTimeZone(String dateToBeConverted) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        SimpleDateFormat fort = new SimpleDateFormat("MMM dd yyyy, hh:mm a", Locale.US);
        Date date;
        try {
            if (dateToBeConverted != null) {
                formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
                date = formatter.parse(dateToBeConverted);
                return fort.format(date);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Returns the absolute elevation of the parent of the provided {@code view}, or in other words,
     * the sum of the elevations of all ancestors of the {@code view}.
     */
    public static float getParentAbsoluteElevation(@NonNull View view) {
        float absoluteElevation = 0;
        ViewParent viewParent = view.getParent();
        while (viewParent instanceof View) {
            absoluteElevation += ViewCompat.getElevation((View) viewParent);
            viewParent = viewParent.getParent();
        }
        return absoluteElevation;
    }

    /*
      // Get current deviceId
     */

    public static String getDeviceId(Context context) {
        @SuppressLint("HardwareIds")
        String androidId = Settings.Secure.getString(context.getContentResolver(),Settings.Secure.ANDROID_ID);
        Timber.e("device id is : "+androidId);
        return androidId;
    }

    /*
     * GETTING CURRENT DATE
     */
    public static String date() {
        Calendar calendar = Calendar.getInstance(Locale.US);
        Date date = calendar.getTime();
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        return formater.format(date);
    }

    public static boolean isGooglePlayServicesAvailable(Context context) {
        int status = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GoogleApiAvailability.getInstance().getErrorDialog((Activity) context, status,0).show();
            Toast.makeText(context, "Google Play Services is not Available", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    /**
     * Hides the soft keyboard
     */
    public static void hideSoftKeyboard(Activity me) {
        if (me.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) me.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(me.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static boolean isNumeric(String str)
    {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }

    public static void hideSoftKeyboard(Dialog me) {
        if (me.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) CatalogApplication.getInstance().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(me.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static void showSoftKeyboard(Dialog me, EditText edt) {
        //if (me.getCurrentFocus() != null) {
        try {
            InputMethodManager imm = (InputMethodManager) CatalogApplication.getInstance().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(edt, InputMethodManager.SHOW_IMPLICIT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * <h2>getAppVersion</h2>
     * <p>
     * method to get Application's version code from the {@code PackageManager}.
     *  </p>
     *  @return Application's version code
     */
    public static String getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public static boolean IsMobileNumber(String mstring)
    {
        String expression = "^([0-9\\+]|\\(\\d{1,2}\\))[0-9\\-\\. ]{2,15}$";
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(mstring);
        return matcher.matches();
    }
}
