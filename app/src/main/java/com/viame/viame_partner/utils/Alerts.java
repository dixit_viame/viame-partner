package com.viame.viame_partner.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;

import androidx.appcompat.widget.ListPopupWindow;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.viame.viame_partner.R;
import com.viame.viame_partner.databinding.DialogCurrentBookingStatusBinding;
import com.viame.viame_partner.databinding.DialogOneTwoButtonsBinding;
import com.viame.viame_partner.databinding.DialogRecyclerBinding;
import com.viame.viame_partner.databinding.RowItemPopupDialogBinding;

import java.util.List;




/**
 * <h1>Alerts</h1>
 * <p>
 *     class to show different types of alerts
 * </p>
 * @since 23/7/15.
 */
public class Alerts extends Activity {

    public interface OnPopupWindowItemClick
    {
        void setOnItemClick(int position);

    }

    public static ListPopupWindow showListPopupMenu(Context context,View v,int arrayListId, OnPopupWindowItemClick mOnPopupWindowItemClick) {
        ListPopupWindow listPopupWindow =
                new ListPopupWindow(context, null, R.attr.listPopupWindowStyle);
        ArrayAdapter<CharSequence> adapter =
                new ArrayAdapter<>(
                        context,
                        R.layout.cat_popup_item,
                        context.getResources().getStringArray(arrayListId));
        listPopupWindow.setAdapter(adapter);
        listPopupWindow.setHorizontalOffset( -200 );
        listPopupWindow.setVerticalOffset( -50 );
        listPopupWindow.setWidth( (int) Utils.dpToPx(context,200));
        listPopupWindow.setModal( true );
        listPopupWindow.setHeight( ListPopupWindow.WRAP_CONTENT );
        listPopupWindow.setAnchorView(v);
        listPopupWindow.setOnItemClickListener(
                (parent, view, position, id) -> {

                    if(mOnPopupWindowItemClick != null)
                       mOnPopupWindowItemClick.setOnItemClick(position);

                    listPopupWindow.dismiss();
                });
        if(listPopupWindow.isShowing()) {
            listPopupWindow.dismiss();
          }
        return listPopupWindow;
    }


    public interface OnConfirmationDialog {
        void onYes();

        void onNo();
    }

    public interface OnSearchableDialog<T> {
        void onItemSelected(T t);
    }



    private static Dialog dialog;

    public static void showProgressBar(Context context) {

        dismissProgressBar();

        try {
            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_progress);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void dismissProgressBar() {
        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
                dialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void showBottomSheetSimpleConfirmationDialog(final Activity context, String titleText, String subTitleText,
                                                               boolean showOneButton, String negativeText, String positiveText,
                                                               final OnConfirmationDialog onConfirmationDialog)
    {

        try {
            final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(context);
            mBottomSheetDialog.setCancelable(false);
            mBottomSheetDialog.setCanceledOnTouchOutside(false);
            mBottomSheetDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
            View sheetView = context.getLayoutInflater().inflate(R.layout.dialog_one_two_buttons, null);

            final DialogOneTwoButtonsBinding binding = DialogOneTwoButtonsBinding.bind(sheetView);

            mBottomSheetDialog.setContentView(binding.getRoot());
            View v1 = (View) sheetView.getParent();
            v1.setBackgroundColor(Color.TRANSPARENT);

            binding.tvTitle.setText(titleText);
            binding.tvPositive.setText(positiveText);

            binding.tvTitle.setTypeface(TypefaceCache.getInstance().getTypeface(TypefaceCache.POPPINS_MEDIUM));
            binding.tvPositive.setTypeface(TypefaceCache.getInstance().getTypeface(TypefaceCache.POPPINS_MEDIUM));
            binding.tvSubTitle.setTypeface(TypefaceCache.getInstance().getTypeface(TypefaceCache.POPPINS_REGULAR));
            binding.tvNegative.setTypeface(TypefaceCache.getInstance().getTypeface(TypefaceCache.POPPINS_MEDIUM));

            if (subTitleText != null) {
                binding.tvSubTitle.setText(subTitleText);
                binding.tvSubTitle.setVisibility(View.VISIBLE);
            } else {
                binding.tvSubTitle.setVisibility(View.GONE);
            }

            if (negativeText != null) {
                binding.tvNegative.setText(negativeText);
            }

            if (showOneButton) {
                binding.tvNegative.setVisibility(View.GONE);
            } else {
                binding.tvNegative.setVisibility(View.VISIBLE);
            }

            binding.tvPositive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (onConfirmationDialog != null) {
                        onConfirmationDialog.onYes();
                    }
                    mBottomSheetDialog.dismiss();
                }
            });

            binding.tvNegative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (onConfirmationDialog != null) {
                        onConfirmationDialog.onNo();
                    }
                    mBottomSheetDialog.dismiss();
                }
            });

            mBottomSheetDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showBottomSheetListDialog(final Activity context, final List<?> data, String title, final OnSearchableDialog onSearchableDialog) {

        try {
            final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(context);

            View sheetView = context.getLayoutInflater().inflate(R.layout.dialog_recycler, null);

            final DialogRecyclerBinding binding = DialogRecyclerBinding.bind(sheetView);
            binding.rvItems.setVisibility(View.VISIBLE);
            //binding.loader.setVisibility(View.GONE);
            binding.rvItems.setLayoutManager(new LinearLayoutManagerWrapper(context,RecyclerView.VERTICAL,false));
            binding.rvItems.setNestedScrollingEnabled(false);

            mBottomSheetDialog.setContentView(binding.getRoot());
            View v1 = (View) sheetView.getParent();
            v1.setBackgroundColor(Color.TRANSPARENT);

            binding.tvTitle.setTypeface(TypefaceCache.getInstance().getTypeface(TypefaceCache.POPPINS_MEDIUM));

            if (title != null) {
                binding.tvTitle.setText(title);
                binding.tvTitle.setVisibility(View.VISIBLE);
                binding.vw1.setVisibility(View.VISIBLE);
            } else {
                binding.tvTitle.setVisibility(View.GONE);
                binding.vw1.setVisibility(View.GONE);
            }

            class GenderAdapter extends RecyclerView.Adapter<GenderAdapter.MyViewHolder> {

                RowItemPopupDialogBinding binder;

                class MyViewHolder extends RecyclerView.ViewHolder {
                    RowItemPopupDialogBinding binder;

                    public MyViewHolder(RowItemPopupDialogBinding binding) {
                        super(binding.getRoot());
                        this.binder = binding;
                        binder.tvItem.setTypeface(TypefaceCache.getInstance().getTypeface(TypefaceCache.POPPINS_MEDIUM));
                    }
                }

                @Override
                public void onBindViewHolder(GenderAdapter.MyViewHolder holder, final int position) {
                    holder.binder.setItem(data.get(holder.getAdapterPosition()));
                    final RowItemPopupDialogBinding binding = ((RowItemPopupDialogBinding) holder.binder);
                    holder.binder.getRoot().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (onSearchableDialog != null)
                                onSearchableDialog.onItemSelected(data.get(holder.getAdapterPosition()));
                            mBottomSheetDialog.dismiss();
                        }
                    });
                }

                @Override
                public int getItemCount() {
                    return data.size();
                }

                @Override
                public GenderAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                    binder = RowItemPopupDialogBinding.inflate(context.getLayoutInflater(), parent, false);
                    return new GenderAdapter.MyViewHolder(binder);
                }
            }

            binding.rvItems.setAdapter(new GenderAdapter());
            mBottomSheetDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public static void showSnackBar(Activity context,String message) throws Exception {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(context.findViewById(R.id.parentMain).getWindowToken(), 0);
            Snackbar snackbar =
                    Snackbar.make(context.findViewById(R.id.parentMain), message, Snackbar.LENGTH_LONG);
            snackbar.show();
        } catch (Exception e) {
            throw new Exception("parentMain id should be root Layout of each layout");
        }
    }

//
//    public static void showSnackBar(Activity context, int stringId) {
//        showSnackBar(context, context.getString(stringId));
//    }

//    public static void showSnackBar(Activity context, String msg) {
//        try {
//            if (context == null)
//                return;
//
//            TSnackbar snackbar = TSnackbar.make(context.getWindow().getDecorView().getRootView(), msg, TSnackbar.LENGTH_LONG);
//            TSnackbar.SnackbarLayout layout = (TSnackbar.SnackbarLayout) snackbar.getView();
//            layout.setBackgroundColor(Color.TRANSPARENT);
//            layout.setPadding(10, 100, 10, 0);
//            // Hide the default text
//            TextView textView = layout.findViewById(R.id.snackbar_text);
//            textView.setVisibility(View.INVISIBLE);
//
//            // Inflate our custom view
//            View snackView = context.getLayoutInflater().inflate(R.layout.custom_snackbar_view, null);
//            snackView.setLayoutParams(new ViewGroup.LayoutParams(
//                    ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT));
//            TextView txtMsg = snackView.findViewById(R.id.tvMsg);
//            txtMsg.setTypeface(TypefaceCache.getInstance().getTypeface(TypefaceCache.POPPINS_MEDIUM));
//            txtMsg.setText(msg);
//            txtMsg.setTextColor(ContextCompat.getColor(context,R.color.white));
//
//            // Add the view to the Snackbar's layout
//            layout.addView(snackView, 0);
//
//            // Show the Snackbar
//            snackbar.show();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


    public static void showBottomSheetCurrentStatusDialog(final Activity context, OnSearchableDialog onSearchableDialog) {

        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(context);
        mBottomSheetDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        View sheetView = context.getLayoutInflater().inflate(R.layout.dialog_current_booking_status, null);

        final DialogCurrentBookingStatusBinding binding = DialogCurrentBookingStatusBinding.bind(sheetView);

        mBottomSheetDialog.setContentView(binding.getRoot());
        View v1 = (View) sheetView.getParent();
        v1.setBackgroundColor(Color.TRANSPARENT);

        binding.tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });

        binding.tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();

                if(onSearchableDialog !=null) onSearchableDialog.onItemSelected(null);
            }
        });
        mBottomSheetDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {

                 //setS(DialogFragment.STYLE_NORMAL, R.style.DialogStyle)
                // In a previous life I used this method to get handles to the positive and negative buttons
                // of a dialog in order to change their Typeface. Good ol' days.

                BottomSheetDialog d = (BottomSheetDialog) dialog;

                // This is gotten directly from the source of BottomSheetDialog
                // in the wrapInBottomSheet() method
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(R.id.design_bottom_sheet);

                // Right here!
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);

                d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

            }
        });
        mBottomSheetDialog.show();
    }

}
