package com.viame.viame_partner.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import androidx.lifecycle.LiveData
import com.viame.viame_partner.MobileData
import com.viame.viame_partner.Vpn
import com.viame.viame_partner.WifiData
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ConnectivityReceiver @Inject constructor(private val context: Context) :
    LiveData<ConnectionModel?>()
{
    override fun onActive() {
        super.onActive()
        val filter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        context.registerReceiver(networkReceiver, filter)
    }

    override fun onInactive() {
        super.onInactive()
        context.unregisterReceiver(networkReceiver)
    }

    private val networkReceiver: BroadcastReceiver =
        object : BroadcastReceiver() {
            override fun onReceive(
                context: Context,
                intent: Intent
            ) {
                try {
                    if (intent.extras != null) {
                        val connectivityManager =
                            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                        val activeNetwork = connectivityManager.activeNetworkInfo
                        val isConnected = activeNetwork != null &&
                                activeNetwork.isConnectedOrConnecting
                        if (isConnected) {
                            when (activeNetwork!!.type) {
                                ConnectivityManager.TYPE_WIFI -> postValue(
                                    ConnectionModel(
                                        WifiData,
                                        true
                                    )
                                )
                                ConnectivityManager.TYPE_MOBILE -> postValue(
                                    ConnectionModel(
                                        MobileData,
                                        true
                                    )
                                )
                                ConnectivityManager.TYPE_VPN -> postValue(
                                    ConnectionModel(
                                        Vpn,
                                        false
                                    )
                                )
                            }
                        } else {
                            postValue(ConnectionModel(0, false))
                        }
                    }
                } catch (e: Exception) {
                    Timber.e(
                        "%s Catched Error! Not able to connect to Internet!",
                        TAG
                    )
                }
            }
        }

    companion object {
        private const val TAG = "ConnectivityReceiver"
    }

}