package com.viame.viame_partner.utils;


import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;

import java.util.HashMap;
import java.util.Map;

/**
 * Cache for typefaces to avoid building them from resources each time
 *
 *  TypefaceCache.getInstance().getTypeface(TypefaceCache.POPPINS_BOLD)
 *  TypefaceCache.getInstance().getTypeface(TypefaceCache.POPPINS_SEMI_BOLD)
 *  TypefaceCache.getInstance().getTypeface(TypefaceCache.POPPINS_LIGHT)
 *  TypefaceCache.getInstance().getTypeface(TypefaceCache.POPPINS_MEDIUM)
 *  TypefaceCache.getInstance().getTypeface(TypefaceCache.POPPINS_REGULAR)
 */
public class TypefaceCache
{

    public static final String POPPINS_BOLD = "font/poppins_bold.ttf";
    public static final String POPPINS_SEMI_BOLD = "font/poppins_semibold.ttf";
    public static final String POPPINS_LIGHT = "font/poppins_light.ttf";
    public static final String POPPINS_MEDIUM = "font/poppins_medium.ttf";
    public static final String POPPINS_REGULAR = "font/poppins.ttf";


    private AssetManager assetManager;
    private Map<String, Typeface> fontsCache;
    private static TypefaceCache instance;

    private TypefaceCache() {
        fontsCache = new HashMap<>();
    }

    public static TypefaceCache getInstance() {
        if (instance == null) {
            instance = new TypefaceCache();
        }
        return instance;
    }

    public void init(Context context) {
        this.assetManager = context.getAssets();
    }

    public Typeface getTypeface(String fontName) {
        synchronized (fontsCache) {

            if (!fontsCache.containsKey(fontName)) {
                Typeface t = Typeface.createFromAsset(assetManager, fontName);
                fontsCache.put(fontName, t);
            }
            return fontsCache.get(fontName);
        }
    }
}
