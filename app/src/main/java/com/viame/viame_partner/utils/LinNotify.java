package com.viame.viame_partner.utils;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.widget.RemoteViews;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationCompat.Builder;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import com.viame.viame_partner.ConstKt;
import com.viame.viame_partner.R;
import com.viame.viame_partner.prefs.PrefEntity;
import com.viame.viame_partner.prefs.Preferences;
import com.viame.viame_partner.service.BigTextBookingIntentService;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Random;

import static androidx.core.app.NotificationCompat.PRIORITY_DEFAULT;

/**
 * TODO notification bar tool BLCS
 *
 *
 * private static void sendNotify()
 *        {
 * 		Intent intent = new Intent();
 * 		nullIntent = PendingIntent.getActivity(mContext, 0, intent, 0);
 * 		mNotifiviews = new RemoteViews(mContext.getPackageName(),
 * 				R.layout.custom_notify);
 * 		mNotifiviews.setViewVisibility(R.id.tv_custom_notify_number, View.VISIBLE);
 * 		mNotifiviews.setViewVisibility(R.id.pb_custom_notify, View.VISIBLE);
 *         LinNotify.show(mContext,"","",mNotifiviews,LinNotify.NEW_MESSAGE,null);
 *    }
 *
 * 	private static void updateNotify(int loadedLen)
 *    {
 * 		int progress = loadedLen * 100 / FILE_LEN;
 * 		mNotifiviews.setTextViewText(R.id.tv_custom_notify_number, progress + "%");
 * 		mNotifiviews.setProgressBar(R.id.pb_custom_notify, FILE_LEN, loadedLen,
 * 				false);
 * 		LinNotify.show(mContext,"","",mNotifiviews,LinNotify.NEW_MESSAGE,null);
 *    }
 *
 * 	private static void finishNotify()
 *    {
 * 		mNotifiviews.setTextViewText(R.id.tv_custom_notify_number,  "100%");
 * 		Intent installAppIntent = getInstallAppIntent(APK_UPGRADE);
 * 		PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0,installAppIntent, 0);
 * 		mNotifiviews.setTextViewText(R.id.tv_custom_notify_finish, "下载完成，请点击进行安装");
 * 		mNotifiviews.setViewVisibility(R.id.tv_custom_notify_number, View.INVISIBLE);
 * 		mNotifiviews.setViewVisibility(R.id.pb_custom_notify, View.GONE);
 * 		mNotifiviews.setViewVisibility(R.id.tv_custom_notify_finish, View.VISIBLE);
 * 		LinNotify.show(mContext,"","",mNotifiviews,LinNotify.NEW_MESSAGE,contentIntent);
 *    }
 *
 */
public class LinNotify {

    public static final String Ticker = "ViaMe";
    public static final String CHECK_OP_NO_THROW = "checkOpNoThrow";
    public static final String OP_POST_NOTIFICATION = "OP_POST_NOTIFICATION";
    public static int notifyId = 0;

    /**
     * TODO adaptation Android8.0 create notification channel
     * tips: can be written in MainActivity, can also be written in the Application, can actually be written anywhere in the program,
     * Just be sure to call it before the notification pops up. And the code that creates the notification channel will only be created the first time it is executed.
     * Each time you create a code system, it will detect that the notification channel already exists, so it will not be created repeatedly, and it will not affect any efficiency.
     */
    public static void setNotificationChannel(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = ConstKt.NOTIFICATION_CHANNEL_ID;
            String channelName = ConstKt.NOTIFICATION_CHANNEL_NAME;
            createNotificationChannel(context, channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
        }
    }

    /**
     * Create a configuration notification channel
     * @param channelId   渠道id
     * @param channelName 渠道nanme
     * @param importance  优先级
     */
    @TargetApi(Build.VERSION_CODES.O)
    private static void createNotificationChannel(Context context, String channelId, String channelName, int importance)
    {
        NotificationChannel channel = new NotificationChannel(channelId, channelName, importance);
        channel.setShowBadge(false);//Do not use the channel for the channel
//        channel.setGroup(channelId); //Set channel group id
        // Configure the properties of the notification channel
//        channel .setDescription("Description of the channel");
        // Set the flash when the notification appears (if supported by the android device)
        channel.enableLights(true);
        // Set the vibration when the notification appears (if the android device supports it)
        channel.enableVibration(false);
        //Set the phone as above: 1 second, 2 seconds, 1 second, 3 seconds
//        channel.setVibrationPattern(new long[]{1000, 2000, 1000,3000});
        channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);//Set whether the lock screen displays notifications
        channel.setLightColor(Color.BLUE);
        channel.setBypassDnd(true);//Set whether you can bypass the do not disturb mode
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.createNotificationChannel(channel);
    }

    /**
     * Create a channel group (if you have more notification channels, you can divide the channel group)
     * @param groupId
     * @param groupName
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    public static void createNotifycationGroup(Context context,String groupId, String groupName) {
        NotificationChannelGroup group = new NotificationChannelGroup(groupId, groupName);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

        notificationManager.createNotificationChannelGroup(group);
    }

    /**
     * TODO: Send a notification (refresh the previous notification)
     *
     * @param context
     * @param contentTitle 标题
     * @param contentText  内容
     */
    public static void show(Context context, String contentTitle, String contentText, PendingIntent pendingIntent) {
        show(context, contentTitle, contentText,null, 0, ConstKt.NOTIFICATION_CHANNEL_ID, pendingIntent);
    }

    /**
     * TODO: Send a custom notification (refresh the previous notification)
     *
     * @param context
     * @param contentTitle 标题
     * @param contentText  内容
     */
    public static void show(Context context, String contentTitle, String contentText,RemoteViews views ,PendingIntent pendingIntent) {
        show(context, contentTitle, contentText, views,0, ConstKt.NOTIFICATION_CHANNEL_ID, pendingIntent);
    }

    /**
     * Send a notification (refresh the previous notification, specify the notification channel)
     *
     * @param contentTitle 标题
     * @param contentText  内容
     * @param channelId    渠道id
     */
    public static void show(Context context, String contentTitle, String contentText, String channelId, PendingIntent pendingIntent) {
        show(context, contentTitle, contentText,null, 0, channelId, pendingIntent);
    }

    /**
     * Send a custom notification (refresh the previous notification, specify the notification channel)
     *
     * @param contentTitle 标题
     * @param contentText  内容
     * @param channelId    渠道id
     */
    public static void show(Context context, String contentTitle, String contentText,RemoteViews views, String channelId, PendingIntent pendingIntent) {
        show(context, contentTitle, contentText,views, 0, channelId, pendingIntent);
    }

    /**
     * Send multiple notifications (default notification channel)
     *
     * @param contentTitle 标题
     * @param contentText  内容
     */
    public static void showMuch(Context context, String contentTitle, String contentText, PendingIntent pendingIntent) {
        show(context, contentTitle, contentText,null, ++notifyId, ConstKt.NOTIFICATION_CHANNEL_ID, pendingIntent);
    }

    /**
     * Send multiple custom notifications (default notification channel)
     *
     * @param contentTitle 标题
     * @param contentText  内容
     */
    public static void showMuch(Context context, String contentTitle, String contentText,RemoteViews views, PendingIntent pendingIntent) {
        show(context, contentTitle, contentText,views, ++notifyId, ConstKt.NOTIFICATION_CHANNEL_ID, pendingIntent);
    }

    /**
     * Send multiple notifications (specify notification channels)
     *
     * @param contentTitle 标题
     * @param contentText  内容
     * @param channelId    渠道id
     */
    public static void showMuch(Context context, String contentTitle, String contentText, String channelId, PendingIntent pendingIntent) {
        show(context, contentTitle, contentText,null, ++notifyId, channelId, pendingIntent);
    }


    /**
     * Send multiple custom notifications (specify notification channels)
     *
     * @param contentTitle 标题
     * @param contentText  内容
     * @param channelId    渠道id
     */
    public static void showMuch(Context context, String contentTitle, String contentText, String channelId,RemoteViews views, PendingIntent pendingIntent) {
        show(context, contentTitle, contentText,views, ++notifyId, channelId, pendingIntent);
    }

    /**
     * Send notifications (set default: large icon / small icon / subtitle / subtitle / priority / first pop up text)
     *
     * @param contentTitle 标题
     * @param contentText  内容
     * @param notifyId     通知栏id
     * @param channelId    设置渠道id
     * @param pendingIntent          意图类
     */
    public static void show(Context context, String contentTitle, String contentText, RemoteViews views,int notifyId, String channelId, PendingIntent pendingIntent) {
        show(context, R.drawable.ic_viame_notification, R.drawable.ic_viame_notification, contentTitle, null, contentText, PRIORITY_DEFAULT, null,views ,notifyId, channelId, pendingIntent);
    }


    /**
     * Send notifications (set default: large icon / small icon / subtitle / subtitle / priority / first pop up text) / ticker / subtext
     *
     * @param contentTitle 标题
     * @param contentText  内容
     * @param notifyId     通知栏id
     * @param channelId    设置渠道id
     * @param pendingIntent          意图类
     */
    public static void show(Context context, String contentTitle, String contentText, RemoteViews views,int notifyId, String subText, String ticker ,
                            String channelId, PendingIntent pendingIntent) {
        show(context, R.drawable.ic_viame_notification, R.drawable.ic_viame_notification, contentTitle, subText, contentText, PRIORITY_DEFAULT,
                ticker,views ,notifyId, channelId, pendingIntent);
    }

    /**
     * 发送通知
     *
     * @param largeIcon    大图标
     * @param smallIcon    小图标
     * @param contentTitle 标题
     * @param subText      小标题/副标题
     * @param contentText  内容
     * @param priority     优先级
     * @param ticker       The text displayed on the status bar when the notification first pops up
     * @param notifyId     定义是否显示多条通知栏
     * @param pendingIntent          意图类
     */
    @SuppressLint("WrongConstant")
    public static void show(Context context, int largeIcon,
                            int smallIcon, String contentTitle,
                            String subText, String contentText,
                            int priority, String ticker, RemoteViews view,
                            int notifyId, String channelId, PendingIntent pendingIntent) {
        //flags
        // FLAG_ONE_SHOT: indicates that this PendingIntent can only be used once.
        // FLAG_IMMUTABLE: indicates that the created PendingIntent should be immutable
        // FLAG_NO_CREATE : Indicates that if the described PendingIntent does not yet exist, only null is returned instead of creating it.
        // FLAG_CANCEL_CURRENT : Indicates that if the described PendingIntent already exists, it should generate a new PendingIntent, cancel the PendingIntent before
        // FLAG_UPDATE_CURRENT : Indicates that if the described PendingIntent already exists, it is retained, but its extra data is replaced with the content of this new Intent

        //Get notification service manager
        NotificationManagerCompat manager = NotificationManagerCompat.from(context);
        //Determine if the app notification is open
        if (!openNotificationChannel(context, manager, channelId)) return;

        //Create NEW_MESSAGE channel notification bar. This constructor is recommended at API level 26.1.0 Builder (context, channel name)
        NotificationCompat.Builder builder =
                (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) ?
                new NotificationCompat.Builder(context, channelId)
                : new Builder(context);

        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), largeIcon == 0 ? R.mipmap.ic_launcher : largeIcon)) //Set the large icons shown in the ticker and notifications.
                .setSmallIcon(smallIcon == 0 ? R.mipmap.ic_launcher : smallIcon) // icon
                .setContentText(TextUtils.isEmpty(contentText)? null : contentText) // content
                .setContentTitle(TextUtils.isEmpty(contentTitle) ? null : contentTitle) // title
                .setSubText(TextUtils.isEmpty(subText) ? null : subText) // Subtitle of APP name
                .setPriority(NotificationCompat.PRIORITY_HIGH) // Set the priority PRIORITY_DEFAULT
                .setTicker(TextUtils.isEmpty(ticker) ? Ticker : ticker) // Set the text displayed on the status bar when the notification first pops up
                .setContent(view)
                .setCategory(Notification.CATEGORY_REMINDER)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(TextUtils.isEmpty(contentText)? null : contentText))
                .setWhen(System.currentTimeMillis()) // Set the timestamp of the notification sent
                .setShowWhen(true)//Set whether to display the timestamp
                .setAutoCancel(true)// After clicking the notification, the notification disappears on the notification bar.
                .setDefaults(Notification.DEFAULT_ALL)// Set default alert tone, vibration mode, lighting, etc. Default notification options used
                .setContentIntent(pendingIntent) // Set notification click events
                // ToDo: Show notification icon and title in lock screen status
                // 1. VISIBILITY_PUBLIC displays this notification completely on all lock screens
                // 2, VISIBILITY_PRIVATE hides sensitive or private information on the security lock screen
                // 3, VISIBILITY_SECRET does not display any part
                .setVisibility(Notification.VISIBILITY_PRIVATE)//Some phones have no effect
                .setFullScreenIntent(pendingIntent,false)//Hanging notification 8.0 needs to be manually opened
//                .setColorized(true)
//                .setGroupSummary(true)//将此通知设置为一组通知的组摘要
//                .setGroup(NEW_GROUP)//使用组密钥
//                .setDeleteIntent(pendingIntent)//当用户直接从通知面板清除通知时 发送意图
//                .setFullScreenIntent(pendingIntent,true)
//                .setContentInfo("大文本")//在通知的右侧设置大文本。
//                .setContent(RemoteViews RemoteView)//设置自定义通知栏
//                .setColor(Color.parseColor("#ff0000"))
//                .setLights()//希望设备上的LED闪烁的argb值以及速率
//                .setTimeoutAfter(3000)//指定取消此通知的时间（如果尚未取消）。
        ;

        // Notification bar id
        manager.notify(notifyId, builder.build()); // The minimum API required for the build() method is 16,
    }



    @SuppressLint("WrongConstant")
    public static void showBigTextStyle(Context context, int largeIcon,
                                        int smallIcon, String contentTitle, String summaryTitle,
                                        String subText, String bigContentText,
                                        String ticker, RemoteViews view,
                                        int notifyId, String channelId, PendingIntent pendingIntent, Bundle data,Boolean isOnGoing) {
        try {
            //flags
            // FLAG_ONE_SHOT: indicates that this PendingIntent can only be used once.
            // FLAG_IMMUTABLE: indicates that the created PendingIntent should be immutable
            // FLAG_NO_CREATE : Indicates that if the described PendingIntent does not yet exist, only null is returned instead of creating it.
            // FLAG_CANCEL_CURRENT : Indicates that if the described PendingIntent already exists, it should generate a new PendingIntent, cancel the PendingIntent before
            // FLAG_UPDATE_CURRENT : Indicates that if the described PendingIntent already exists, it is retained, but its extra data is replaced with the content of this new Intent
            Random random = new Random();
            int randomNumber = random.nextInt(1000) + 1000;
            NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
            bigTextStyle.bigText(TextUtils.isEmpty(bigContentText)? null : bigContentText);
            bigTextStyle.setBigContentTitle(TextUtils.isEmpty(contentTitle)? null : contentTitle);
            // Summary line after the detail section in the big form of the template.
            // Note: To improve readability, don't overload the user with info. If Summary Text
            // doesn't add critical information, you should skip it.
            bigTextStyle.setSummaryText(TextUtils.isEmpty(summaryTitle)? null : summaryTitle);
            NotificationCompat.Action openAppAction = null, acceptBookingAction = null;
            if(data != null) {
                // 4. Create additional Actions (Intents) for the Notification.

                // In our case, we create two additional actions: a Snooze action and a Dismiss action.
                // Snooze Action.
                Intent acceptBookingIntent = new Intent(context, BigTextBookingIntentService.class);
                acceptBookingIntent.setAction(BigTextBookingIntentService.ACTION_ACCEPT_BOOKING);
                acceptBookingIntent.putExtras(data);

                PendingIntent acceptBookingPendingIntent = PendingIntent.getService(context,
                        randomNumber, acceptBookingIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                acceptBookingAction =
                        new NotificationCompat.Action.Builder(
                                R.drawable.ic_viame_notification,
                                context.getString(R.string.accept_booking),
                                acceptBookingPendingIntent)
                                .build();

                int randomNumber2 = random.nextInt(1000) + 1000;
                // Dismiss Action.
                Intent openAppIntent = new Intent(context, BigTextBookingIntentService.class);
                openAppIntent.setAction(BigTextBookingIntentService.ACTION_OPEN_APP);
                openAppIntent.putExtras(data);

                PendingIntent dismissPendingIntent = PendingIntent.getService(context, randomNumber2, openAppIntent, PendingIntent.FLAG_UPDATE_CURRENT);
               openAppAction =
                        new NotificationCompat.Action.Builder(
                                R.drawable.ic_viame_notification,
                                context.getString(R.string.open_booking_in_app),
                                dismissPendingIntent)
                                .build();
            }

            //Get notification service manager
            NotificationManagerCompat manager = NotificationManagerCompat.from(context);
            //Determine if the app notification is open
            if (!openNotificationChannel(context, manager, channelId)) return;

            //Create NEW_MESSAGE channel notification bar. This constructor is recommended at API level 26.1.0 Builder (context, channel name)
            NotificationCompat.Builder builder =
                    (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) ?
                            new NotificationCompat.Builder(context, channelId)
                            : new Builder(context);


            // 0 : Start without a delay
            // 400 : Vibrate for 400 milliseconds
            // 200 : Pause for 200 milliseconds
            // 400 : Vibrate for 400 milliseconds
            long[] mVibratePattern = new long[]{1000, 1000, 2000, 1000, 3000};

            builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), largeIcon == 0 ? R.mipmap.ic_launcher : largeIcon)) //Set the large icons shown in the ticker and notifications.
                    .setSmallIcon(smallIcon == 0 ? R.mipmap.ic_launcher : smallIcon) // icon
                    .setContentText(TextUtils.isEmpty(bigContentText)? null : bigContentText) // content
                    .setContentTitle(TextUtils.isEmpty(contentTitle) ? null : contentTitle) // title
                    .setSubText(TextUtils.isEmpty(subText) ? null : subText) // Subtitle of APP name
                    .setContent(view)
                    .setOngoing(isOnGoing)
                    .setAutoCancel(true) // After clicking the notification, the notification disappears on the notification bar.
                    .setVibrate(mVibratePattern)
                    .setLights(ContextCompat.getColor(context, R.color.color_primary), 3000, 3000)
                    .setTicker(TextUtils.isEmpty(ticker) ? null : ticker)
                    // Set primary color (important for Wear 2.0 Notifications).
                    .setColor(ContextCompat.getColor(context, R.color.color_primary))
                    .setStyle(bigTextStyle)
                    .setWhen(System.currentTimeMillis()) // Set the timestamp of the notification sent
                    .setShowWhen(true)//Set whether to display the timestamp


                    // ToDo: Show notification icon and title in lock screen status
                    // 1. VISIBILITY_PUBLIC displays this notification completely on all lock screens
                    // 2, VISIBILITY_PRIVATE hides sensitive or private information on the security lock screen
                    // 3, VISIBILITY_SECRET does not display any part
                    .setVisibility(Notification.VISIBILITY_PUBLIC) //Some phones have no effect

                    // .setColorized(true)
                    // SIDE NOTE: Auto-bundling is enabled for 4 or more notifications on API 24+ (N+)
                    // devices and all Wear devices. If you have more than one notification and
                    // you prefer a different summary notification, set a group key and create a
                    // summary notification via
                    //.setGroupSummary(true)//Set this notification as a group summary for a group of notifications
                    //.setGroup(NEW_GROUP)//Use group key
                    .setCategory(Notification.CATEGORY_REMINDER);

            //.setDeleteIntent(pendingIntent)//Send an intent when the user clears the notification directly from the notification panel
            //.setFullScreenIntent(pendingIntent,true)
            //.setContent(RemoteViews RemoteView)//Setting up a custom notification bar
            //.setColor(Color.parseColor("#ff0000"))
            //.setLights()//The argb value and rate at which the LED on the device is expected to blink
            //.setTimeoutAfter(3000)//Specify the time to cancel this notification if it has not already been canceled.

            if(data != null)
            {
                // Adds additional actions specified above.

                builder.setDefaults(Notification.PRIORITY_HIGH); // Set default alert tone, vibration mode, lighting, etc. Default notification options used
                builder.setPriority(NotificationCompat.PRIORITY_HIGH);
                builder.setSound(Uri.parse("android.resource://" +context.getPackageName() + "/" + R.raw.new_booking_notify));
                builder.addAction(openAppAction);
                builder.addAction(acceptBookingAction);
            }
            else
            {
              //builder.setDefaults(Notification.PRIORITY_LOW); // Set default alert tone, vibration mode, lighting, etc. Default notification options used
              //builder.setPriority(NotificationCompat.PRIORITY_LOW);
            }

            if(Preferences.getPreferenceBoolean(PrefEntity.IS_MAIN_ACTIVITY_IN_FOREGROUND))
            {
                builder.setContentIntent(pendingIntent);
            }
            else
            {
                builder.setFullScreenIntent(pendingIntent, true); // Set notification click events
            }
            manager.notify(notifyId, builder.build()); // The minimum API required for the build() method is 16,
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Determine if the app channel notification is open (adapt to 8.0)
     * @return True open
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static Boolean openNotificationChannel(Context context, NotificationManagerCompat manager, String channelId) {
        //Determine if the notification is open
        try {
            if (!isNotificationEnabled(context)) {
                toNotifySetting(context, null);
                return false;
            }
            //Determine if the channel notification is open
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = manager.getNotificationChannel(channelId);
                if (channel.getImportance() == NotificationManagerCompat.IMPORTANCE_NONE)
                {
                    //Did not open to the settings interface
                    toNotifySetting(context, channel.getId());
                    return false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Determine if the app notification is open
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static boolean isNotificationEnabled(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
            return notificationManagerCompat.areNotificationsEnabled();
        }
        AppOpsManager mAppOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        Class appOpsClass = null;
        /* Context.APP_OPS_MANAGER */
        try {
            appOpsClass = Class.forName(AppOpsManager.class.getName());
            Method checkOpNoThrowMethod = appOpsClass.getMethod(CHECK_OP_NO_THROW, Integer.TYPE, Integer.TYPE,
                    String.class);
            Field opPostNotificationValue = appOpsClass.getDeclaredField(OP_POST_NOTIFICATION);
            int value = (Integer) opPostNotificationValue.get(Integer.class);
            return ((Integer) checkOpNoThrowMethod.invoke(mAppOps, value, context.getApplicationInfo().uid, context.getPackageName()) == AppOpsManager.MODE_ALLOWED);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Manually open app notifications
     */
    public static void toNotifySetting(Context context, String channelId) {
        try {
            Intent intent = new Intent();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) { //Adapt to 8.0 and above (8.0 needs to open the application notification first, then open the channel notification)
                if(TextUtils.isEmpty(channelId)){
                    intent.setAction(Settings.ACTION_APP_NOTIFICATION_SETTINGS);
                    intent.putExtra(Settings.EXTRA_APP_PACKAGE, context.getPackageName());
                }else{
                    intent.setAction(Settings.ACTION_CHANNEL_NOTIFICATION_SETTINGS);
                    intent.putExtra(Settings.EXTRA_APP_PACKAGE, context.getPackageName());
                    intent.putExtra(Settings.EXTRA_CHANNEL_ID, channelId);
                }
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {//Adapt to 5.0 and above 5.0
                intent.setAction(Settings.ACTION_APP_NOTIFICATION_SETTINGS);
                intent.putExtra("app_package", context.getPackageName());
                intent.putExtra("app_uid", context.getApplicationInfo().uid);
            } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {// Adapt to 4.4 and above 4.4
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                intent.setData(Uri.fromParts("package", context.getPackageName(), null));
            } else {
                intent.setAction(Settings.ACTION_SETTINGS);
            }
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}