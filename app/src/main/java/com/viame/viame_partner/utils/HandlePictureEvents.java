package com.viame.viame_partner.utils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import com.viame.viame_partner.ConstKt;
import com.viame.viame_partner.R;
import com.yalantis.ucrop.UCrop;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Objects;

import timber.log.Timber;

import static android.os.Build.VERSION_CODES.N;

/**
 * <h>HandlePictureEvents</h>
 * this class open the popup for the option to take the image
 * after it takes the the, it crops the image
 * and then upload it to amazon
 */

public class HandlePictureEvents
{
    private Activity mcontext = null;
    private String takenNewImage;
    private String destinationCropImage;
    public File newFile;
    public File destinationFile;
    private Fragment fragment = null;

    public HandlePictureEvents(Activity mcontext , Fragment fragment)
    {
        this.fragment = fragment;
        this.mcontext = mcontext;
    }

    /**
     * <h>openDialog</h>
     * <p>
     * this dialog have the option to choose whether to take picture
     * or open gallery or cancel the dialog
     * </p>
     */

    public void openDialog()
    {
        try {
            takenNewImage = ConstKt.APP_NAME+"_"+System.nanoTime()+".png";
            CreateOrClearDirectory directory = CreateOrClearDirectory.getInstance();

            File folder = new File(Environment.getExternalStorageDirectory()+File.separator+ConstKt.PARENT_FOLDER_PATH);
            if(!folder.exists())
                folder.mkdirs();

            newFile = directory.getAlbumStorageDir(mcontext, ConstKt.PARENT_FOLDER_PATH,false);
            final Resources resources = mcontext.getResources();

            ArrayList<String> options = new ArrayList<>();
            options.add(resources.getString(R.string.TakePhoto));
            options.add(resources.getString(R.string.ChoosefromGallery));
            options.add(resources.getString(R.string.action_cancel));

            Alerts.showBottomSheetListDialog(mcontext, options,
                    resources.getString(R.string.AddPhoto), new Alerts.OnSearchableDialog() {
                @Override
                public void onItemSelected(Object o) {
                    String s = (String) o;
                    if (s.equals(resources.getString(R.string.TakePhoto)))
                    {
                        takePicFromCamera();
                    }
                    else if (s.equals(resources.getString(R.string.ChoosefromGallery))) {
                        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                        photoPickerIntent.setType("image/*");
                        if(fragment!=null)
                            fragment.startActivityForResult(photoPickerIntent, ConstKt.GALLERY_PIC);
                        else
                            mcontext.startActivityForResult(photoPickerIntent, ConstKt.GALLERY_PIC);
                    }
                    else if (s.equals(resources.getString(R.string.action_cancel))){

                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * <h1>takePicFromCamera</h1>
     * <p>
     * This method is got called, when user chooses to take photos from camera.
     * </p>
     */
    private void takePicFromCamera()
    {
        String state;
        try
        {
            takenNewImage = "";
            state = Environment.getExternalStorageState();
            takenNewImage = ConstKt.APP_NAME+"_"+System.nanoTime()+".png";

            File folder = new File(Environment.getExternalStorageDirectory()+File.separator+ConstKt.PARENT_FOLDER_PATH);
            if(!folder.exists())
                folder.mkdirs();

            Uri newProfileImageUri;
            if (Environment.MEDIA_MOUNTED.equals(state))
                newFile = new File(Environment.getExternalStorageDirectory()+File.separator+ ConstKt.PARENT_FOLDER_PATH+File.separator,takenNewImage);
            else
                newFile = new File(mcontext.getFilesDir()+File.separator+ ConstKt.PARENT_FOLDER_PATH+File.separator,takenNewImage);
            if(Build.VERSION.SDK_INT>=N)
                newProfileImageUri = FileProvider.getUriForFile(mcontext, mcontext.getApplicationContext().getPackageName() + ".provider", newFile);
            else
                newProfileImageUri = Uri.fromFile(newFile);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, newProfileImageUri);
            intent.putExtra("return-data", true);
            if(fragment!=null)
                fragment.startActivityForResult(intent, ConstKt.CAMERA_PIC);
            else
                mcontext.startActivityForResult(intent, ConstKt.CAMERA_PIC);
        }
        catch (Exception e)
        {
            Timber.e("profile fragment cannot take picture: %s", e.getMessage());
        }
    }




    /**
     * <h2>startCropImage</h2>
     * <p>
     * This method got called when cropping starts done.
     * </p>
     * @param newFile image file to be cropped
     */
    public File startCropImage(File newFile)
    {
        try
        {
            destinationCropImage = ConstKt.APP_NAME+"_crop"+System.currentTimeMillis()+".png";

            if(fragment != null)
            {
                UCrop.of(Uri.fromFile(newFile), Uri.fromFile(new File(mcontext.getCacheDir(), destinationCropImage)))
                        .start(mcontext, fragment, ConstKt.CROP_IMAGE);
            }
            else
            {
                UCrop.of(Uri.fromFile(newFile), Uri.fromFile(new File(mcontext.getCacheDir(), destinationCropImage)))
                        .start(mcontext, ConstKt.CROP_IMAGE);
            }

            Timber.e("Input_Image "+Uri.fromFile(newFile)+" Exist "+newFile.exists()+ "\nDestination "+Uri.fromFile(new File(mcontext.getCacheDir(), destinationCropImage))
                    +" Exist "+new File(mcontext.getCacheDir(), destinationCropImage).exists());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newFile;
    }

    /**
     * <h2>gallery</h2>
     * <p>
     * This method is got called, when user chooses to take photos from camera.
     * </p>
     * @param data uri data given by gallery
     */
    public File gallery(Uri data)
    {
        try {
            String state = Environment.getExternalStorageState();
            takenNewImage = ConstKt.APP_NAME+ System.nanoTime() + ".png";

            File folder = new File(Environment.getExternalStorageDirectory()+File.separator+ConstKt.PARENT_FOLDER_PATH);
            if(!folder.exists())
                folder.mkdirs();

            if (Environment.MEDIA_MOUNTED.equals(state)) {
                newFile = new File(Environment.getExternalStorageDirectory() + File.separator + ConstKt.PARENT_FOLDER_PATH+ File.separator, takenNewImage);
            } else {
                newFile = new File(mcontext.getFilesDir() + File.separator + ConstKt.PARENT_FOLDER_PATH+ File.separator, takenNewImage);
            }
            InputStream inputStream = mcontext.getContentResolver().openInputStream(data);
            FileOutputStream fileOutputStream = new FileOutputStream(newFile);
            Utils.copyStream(inputStream, fileOutputStream);
            fileOutputStream.close();
            inputStream.close();
            // newProfileImageUri = Uri.fromFile(newFile);
            startCropImage(newFile);

        } catch (Exception e)
        {
            e.printStackTrace();

            newFile = new File(mcontext.getFilesDir() + File.separator + ConstKt.PARENT_FOLDER_PATH+ File.separator, takenNewImage);

            try {
                InputStream inputStream = mcontext.getContentResolver().openInputStream(data);
                FileOutputStream fileOutputStream = new FileOutputStream(newFile);
                Utils.copyStream(inputStream, fileOutputStream);
                fileOutputStream.close();
                inputStream.close();
                // newProfileImageUri = Uri.fromFile(newFile);
                startCropImage(newFile);

            } catch (IOException ex) {
                ex.printStackTrace();
            }

        }
        return newFile;
    }

    public File saveBitmapToFile(Bitmap bitmap){
        File newFile = null;
        if(bitmap!=null){
            try {

                String state = Environment.getExternalStorageState();
               String takenNewImage = ConstKt.APP_NAME+ System.nanoTime() + ".png";

                File folder = new File(Environment.getExternalStorageDirectory()+File.separator+ConstKt.PARENT_FOLDER_PATH);
                if(!folder.exists())
                    folder.mkdirs();

                if (Environment.MEDIA_MOUNTED.equals(state)) {
                    newFile = new File(Environment.getExternalStorageDirectory()+ File.separator + ConstKt.PARENT_FOLDER_PATH+ File.separator, takenNewImage);
                } else {
                    newFile = new File(mcontext.getFilesDir() + File.separator + ConstKt.PARENT_FOLDER_PATH+ File.separator, takenNewImage);
                }

                FileOutputStream outputStream = null;
                try {
                    outputStream = new FileOutputStream(newFile.getAbsolutePath()); //here is set your file path where you want to save or also here you can set file object directly

                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream); // bitmap is your Bitmap instance, if you want to compress it you can compress reduce percentage
                    // PNG is a lossless format, the compression factor (100) is ignored
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (outputStream != null) {
                            outputStream.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // Refresh External storage that file is in SDCARD
                    if(newFile != null && newFile.exists()) {
                        mcontext.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(newFile)));
                        MediaScannerConnection.scanFile(mcontext, new String[]{newFile.toString()}, new String[]{newFile.getName()}, null);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return newFile;
    }

    /**
     * <h1>stringToJsonAndPublish</h1>
     * <p>
     *    This method is used to convert our string into json file and then publish on amazon.
     *    </p>
     * @param fileName contains the name of file.
     * @param uri contains the uri.
     * @return the json object.
     */
    private JSONObject stringToJsonAndPublish(String fileName, Uri uri) {
        JSONObject message = new JSONObject();
        try {
            message.put("type", "image");
            message.put("filename", fileName);
            message.put("uri", uri.toString());
            message.put("uploaded", "inprocess");
            message.put("confirm", false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return message;
    }
}
