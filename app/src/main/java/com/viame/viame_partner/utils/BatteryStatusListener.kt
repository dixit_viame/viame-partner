package com.viame.viame_partner.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import androidx.lifecycle.LiveData


/**
 * A live data for monitoring Android battery.
 */
class BatteryStatusListener(context: Context?) :
    LiveData<BatteryInfo?>() {
    /**
     * The battery broadcast receiver.
     */
    private var batteryReceiver: BroadcastReceiver? = null
    /**
     * The application context.
     */
    private val context: Context

    override fun onActive() {
        super.onActive()
        registerBatteryReceiver()
    }

    private fun registerBatteryReceiver() {
        if (batteryReceiver == null) {
            batteryReceiver = BatteryReceiver()
        }
        val intentFilter = IntentFilter(Intent.ACTION_BATTERY_CHANGED)
        context.registerReceiver(batteryReceiver, intentFilter)
    }

    override fun onInactive() {
        super.onInactive()
        context.unregisterReceiver(batteryReceiver)
    }

    /**
     * The battery broadcast receiver.
     */
    private inner class BatteryReceiver : BroadcastReceiver() {
        override fun onReceive(
            context: Context,
            intent: Intent
        ) {
            setBatteryInfo(intent)
        }
    }

    private fun setBatteryInfo(intent: Intent) {
        val status =
            intent.getIntExtra(BatteryManager.EXTRA_STATUS, UNKNOWN)
        val plugged =
            intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, UNKNOWN)
        val health =
            intent.getIntExtra(BatteryManager.EXTRA_HEALTH, UNKNOWN)
        val level =
            intent.getIntExtra(BatteryManager.EXTRA_LEVEL, UNKNOWN)
        val temperature =
            intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, UNKNOWN)
        val scale =
            intent.getIntExtra(BatteryManager.EXTRA_SCALE, UNKNOWN)
        val voltage =
            intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, UNKNOWN)
        val info = BatteryInfo()
        info.status = status
        info.health = health
        info.level = level
        info.plugged = plugged
        info.temperature = temperature
        info.scale = scale
        info.voltage = voltage
        value = info
    }

    companion object {
        const val UNKNOWN = -1
    }

    init {
        requireNotNull(context) { "context must not be null" }
        this.context = context.applicationContext
    }
}

class BatteryInfo {
    /**
     * The current battery status.
     */
    var status = 0
    /**
     * The current battery health.
     */
    var health = 0
    /**
     * The current battery plugged mode.
     */
    var plugged = 0
    /**
     * The current battery level.
     */
    var level = 0
    /**
     * The maximum battery level.
     */
    var scale = 0
    /**
     * The current battery temperature.
     */
    var temperature = 0
    /**
     * The current battery voltage.
     */
    var voltage = 0

    val percent: Int
        get() {
            val percent = level.toFloat() / scale
            return (percent * 100).toInt()
        }

    override fun toString(): String {
        return "BatteryInfo{" +
                "status=" + status +
                ", health=" + health +
                ", plugged=" + plugged +
                ", level=" + level +
                ", scale=" + scale +
                ", percent=" + percent +
                ", temperature=" + temperature +
                ", voltage=" + voltage +
                '}'
    }
}