package com.viame.viame_partner.aws

import android.content.Context
import android.os.Environment
import android.os.Environment.getExternalStoragePublicDirectory
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobile.client.AWSMobileClient
import com.amazonaws.mobile.client.Callback
import com.amazonaws.mobile.client.UserStateDetails
import com.amazonaws.mobile.config.AWSConfiguration
import com.amazonaws.regions.Regions
import com.viame.viame_partner.APP_NAME
import com.viame.viame_partner.PARENT_FOLDER_PATH
import com.viame.viame_partner.application.CatalogApplication
import com.viame.viame_partner.utils.CreateOrClearDirectory
import timber.log.Timber
import java.io.File
import java.util.concurrent.CountDownLatch

class AWSHelperImpl(private val context: Context): AWSHelper {


    // Cognito Provider
    override fun getCredentialsProvider(): CognitoCachingCredentialsProvider {

        val poolIdString: String = AWSConfiguration(context).optJsonObject("CredentialsProvider")
            .getJSONObject("CognitoIdentity")
            .getJSONObject("Default")
            .getString("PoolId")

        val regionString : String = AWSConfiguration(context)
            .optJsonObject("S3TransferUtility")
            .getString("Region")

        return CognitoCachingCredentialsProvider(
                context,
                poolIdString,
            Regions.fromName(regionString))
    }


    // Cognito Provider
    override fun getAWSCredentialsProvider(): AWSCredentialsProvider {
        lateinit var sMobileClient :AWSCredentialsProvider
        val latch = CountDownLatch(1)
        AWSMobileClient.getInstance()
            .initialize(context, object : Callback<UserStateDetails?> {
                override fun onResult(result: UserStateDetails?) {
                    latch.countDown()
                }

                override fun onError(e: Exception) {
                    Timber.e(e, "onError: ")
                    latch.countDown()
                }
            })
        try {
            latch.await()
            sMobileClient = AWSMobileClient.getInstance()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    return sMobileClient
    }



//
//    override fun getUserPool(): CognitoUserPool {
//        return CognitoUserPool(context, userPoolId, clientId, clientSecret, identityPoolRegion)
//    }

    override fun getS3BucketName(): String {
        val BucketString = AWSConfiguration(context)
            .optJsonObject("S3TransferUtility")
            .getString("Bucket")
        return BucketString
    }

    override fun getS3BucketRegion(): String {
        val regionString = AWSConfiguration(context)
            .optJsonObject("S3TransferUtility")
            .getString("Region")
        return regionString
    }

    override fun getStorageDirectory(): File  {
        val directory = CreateOrClearDirectory.getInstance()
        val storageDirectory = directory.getAlbumStorageDir(CatalogApplication.instance, PARENT_FOLDER_PATH + "/Profile_Pictures", false)

        if (!storageDirectory.exists()) {
            try {
                storageDirectory.mkdirs()
            } catch (se: SecurityException) {
                Timber.e(se)
            }
        }
        return storageDirectory
    }


}
