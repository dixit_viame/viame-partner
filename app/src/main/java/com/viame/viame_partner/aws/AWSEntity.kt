package com.viame.viame_partner.aws

import java.util.*

/**
 * Created by sjugurdzija on 4/21/2017.
 */

data class AWSEntity(val id: Long? = -1,
                     val fileName: String = "",
                     val imagePath: String = "",
                     val lastModified: Long = 0){

    override fun equals(other: Any?): Boolean {
        return if(other == null || other !is AWSEntity){
            false
        } else {
            fileName == other.fileName
        }
    }

    override fun hashCode(): Int {
        return Objects.hashCode(fileName)
    }
}