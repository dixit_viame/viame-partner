package com.viame.viame_partner.aws

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.CannedAccessControlList
import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.services.s3.model.S3ObjectSummary
import com.viame.viame_partner.AMAZON_BUCKET
import com.viame.viame_partner.AMAZON_BUCKET_BASE_URL
import io.reactivex.Observable
import io.reactivex.subjects.ReplaySubject
import timber.log.Timber
import java.io.File


class AWSRepository(private val s3: AmazonS3Client,
                    private val transferUtility: TransferUtility,
                    private val awsHelper: AWSHelper) {

    fun uploadDrawing(Path: AWSEntity) : Observable<AWSEntity> {

        val downloadSubject = ReplaySubject.create<AWSEntity>()
        val objectMetadata = ObjectMetadata()
        objectMetadata.addUserMetadata("lastModified", Path.lastModified.toString())
        val uploadObserver = transferUtility.upload(AMAZON_BUCKET,Path.fileName, File(Path.imagePath))

        uploadObserver.setTransferListener(object : TransferListener {
            override fun onStateChanged(id: Int, state: TransferState) {
                if (state == TransferState.COMPLETED) {
                    Timber.e("state completed ")
                    downloadSubject.onNext(AWSEntity(fileName = Path.fileName, imagePath = AMAZON_BUCKET_BASE_URL+Path.fileName, lastModified = Path.lastModified))
                } else if (state == TransferState.FAILED) {
                    downloadSubject.onError(Throwable("state failed",
                        Throwable("state failed")
                    ))
                    Timber.e("state failed")
                }
            }

            override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {

            }

            override fun onError(id: Int, ex: Exception) {
                Timber.e("""ex ${ex.message}cause ${ex.cause}""")
            }
        })

        return downloadSubject
    }

    fun downloadDrawing(drawing: AWSEntity): Observable<AWSEntity> {

        val downloadSubject = ReplaySubject.create<AWSEntity>()
        val imageFile = getImageFile("ViaMe_Partner_Profile.png")
        val downloadObserver = transferUtility.download(drawing.fileName, imageFile)

        downloadObserver.setTransferListener(object : TransferListener {
            override fun onStateChanged(id: Int, state: TransferState) {
                if (state == TransferState.COMPLETED) {
                    Timber.e("state completed")
                    downloadSubject.onNext(AWSEntity(fileName = drawing.fileName, imagePath = imageFile.absolutePath, lastModified = drawing.lastModified))
                } else if (state == TransferState.FAILED) {
                    downloadSubject.onError(Throwable("state failed",
                        Throwable("state failed")
                    ))
                    Timber.e("state failed")
                }
            }

            override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
            }

            override fun onError(id: Int, ex: Exception) {
                Timber.e("""ex ${ex.message}cause ${ex.cause}""")
            }
        })

        return downloadSubject
    }

    fun getListOfRemoteDrawings(): Observable<List<AWSEntity>> {
        return io.reactivex.Observable.fromCallable {
            val list: MutableList<AWSEntity> = mutableListOf()
            val s3ObjList: List<S3ObjectSummary>? = s3.listObjects(awsHelper.getS3BucketName()).objectSummaries
            s3ObjList?.forEach { obj ->
                val fileName = obj.key.substringAfter('/')
                val lastModified = fileName.substring(8, fileName.indexOf('.')).toLong()
                list.add(AWSEntity(fileName = fileName, lastModified = lastModified))
            }
            list
        }
    }

    private fun getImageFile(filename: String): File {
        val dir = awsHelper.getStorageDirectory()

        val file = File(dir, filename)
        if (file.exists()) {
            file.delete()
        }
        file.createNewFile()
        return file
    }

    companion object {
        val TAG: String = AWSRepository::class.java.simpleName
    }
}