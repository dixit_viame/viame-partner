package com.viame.viame_partner.aws

import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool
import java.io.File

interface AWSHelper {
    fun getCredentialsProvider(): CognitoCachingCredentialsProvider
    fun getAWSCredentialsProvider(): AWSCredentialsProvider
    fun getStorageDirectory(): File
    fun getS3BucketName(): String
    fun getS3BucketRegion(): String
}