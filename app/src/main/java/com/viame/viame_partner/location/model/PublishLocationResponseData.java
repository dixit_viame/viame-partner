package com.viame.viame_partner.location.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PublishLocationResponseData {

    @SerializedName("flag")
    @Expose
    private Integer flag;
    @SerializedName("bid")
    @Expose
    private Integer bid;

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public Integer getBid() {
        return bid;
    }

    public void setBid(Integer bid) {
        this.bid = bid;
    }
}
