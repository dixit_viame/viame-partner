package com.viame.viame_partner.location.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PublishLocationResponseModel {

    @SerializedName("errNum")
    @Expose
    private Integer errNum;
    @SerializedName("errFlag")
    @Expose
    private Integer errFlag;
    @SerializedName("errMsg")
    @Expose
    private String errMsg;
    @SerializedName("data")
    @Expose
    private PublishLocationResponseData data;

    public Integer getErrNum() {
        return errNum;
    }

    public void setErrNum(Integer errNum) {
        this.errNum = errNum;
    }

    public Integer getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(Integer errFlag) {
        this.errFlag = errFlag;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public PublishLocationResponseData getData() {
        return data;
    }

    public void setData(PublishLocationResponseData data) {
        this.data = data;
    }

}
