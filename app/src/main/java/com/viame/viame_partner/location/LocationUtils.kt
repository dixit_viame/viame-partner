package com.viame.viame_partner.location

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.viame.viame_partner.KILOMETER
import com.viame.viame_partner.METER
import com.viame.viame_partner.NAUTICAL_MILES
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.set
import kotlin.math.*


fun Distance(
    previousLat: Double,
    previousLong: Double,
    currentLat: Double,
    currentLong: Double,
    unit: String
): Double {
    val earthRadius = 3958.75 // miles (or 6371.0 kilometers)
    val dLat = Math.toRadians(currentLat - previousLat)
    val dLng = Math.toRadians(currentLong - previousLong)
    val sindLat = sin(dLat / 2)
    val sindLng = sin(dLng / 2)
    val a = sindLat.pow(2.0) + sindLng.pow(2.0) * cos(Math.toRadians(previousLat)) * cos(Math.toRadians(currentLat))
    val c = 2 * atan2(sqrt(a), sqrt(1 - a))

    var dist = earthRadius * c
    if (KILOMETER == unit) // Kilometer
    {
        dist *= 1.609344
    } else if (NAUTICAL_MILES == unit) // Nautical Miles
    {
        dist *= 0.8684
    } else if (METER == unit) // meter
    {
        dist *= 1609.344
    }
    return dist
}

fun parse(jObject: JSONObject): List<List<HashMap<String, String>>>? {
    val routes: MutableList<List<HashMap<String, String>>> =
        java.util.ArrayList()
    var jRoutes: JSONArray?
    var jLegs: JSONArray?
    var jSteps: JSONArray?
    try {
        jRoutes = jObject.getJSONArray("routes")
        /** Traversing all routes  */
        for (i in 0 until jRoutes.length()) {
            jLegs = (jRoutes[i] as JSONObject).getJSONArray("legs")
            val path = java.util.ArrayList<HashMap<String, String>>()
            /** Traversing all legs  */
            for (j in 0 until jLegs.length()) {
                jSteps = (jLegs[j] as JSONObject).getJSONArray("steps")
                /** Traversing all steps  */
                for (k in 0 until jSteps.length()) {
                    var polyline : String
                    polyline =
                        ((jSteps[k] as JSONObject)["polyline"] as JSONObject)["points"] as String
                    val list: List<LatLng> =
                        decodePoly(polyline)
                    /** Traversing all points  */
                    for (l in list.indices) {
                        val hm =
                            HashMap<String, String>()
                        hm["lat"] = java.lang.Double.toString(list[l].latitude)
                        hm["lng"] = java.lang.Double.toString(list[l].longitude)
                        path.add(hm)
                    }
                }
                routes.add(path)
            }
        }
    } catch (e: JSONException) {
        e.printStackTrace()
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return routes
}


fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor? {
    val vectorDrawable =
        ContextCompat.getDrawable(context, vectorResId)
    vectorDrawable!!.setBounds(
        0,
        0,
        vectorDrawable.intrinsicWidth,
        vectorDrawable.intrinsicHeight
    )
    val bitmap = Bitmap.createBitmap(
        vectorDrawable.intrinsicWidth,
        vectorDrawable.intrinsicHeight,
        Bitmap.Config.ARGB_8888
    )
    val canvas = Canvas(bitmap)
    vectorDrawable.draw(canvas)
    return BitmapDescriptorFactory.fromBitmap(bitmap)
}

fun getURL(from : LatLng, to : LatLng, key:String) : String {
    val origin = "origin=" + from.latitude + "," + from.longitude
    val dest = "destination=" + to.latitude + "," + to.longitude
    val sensor = "sensor=false"
    val keys = "key=$key"
    val avoid = "avoid=tolls"
    val region = "region=ae"
    return "https://maps.googleapis.com/maps/api/directions/json?$origin&$dest&$sensor&$avoid&$region&$keys"
}

/**
 * Method to decode polyline points
 * Courtesy : https://jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
 */
fun decodePoly(encoded: String): List<LatLng> {
    val poly = ArrayList<LatLng>()
    var index = 0
    val len = encoded.length
    var lat = 0
    var lng = 0

    while (index < len) {
        var b: Int
        var shift = 0
        var result = 0
        do {
            b = encoded[index++].toInt() - 63
            result = result or (b and 0x1f shl shift)
            shift += 5
        } while (b >= 0x20)
        val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
        lat += dlat

        shift = 0
        result = 0
        do {
            b = encoded[index++].toInt() - 63
            result = result or (b and 0x1f shl shift)
            shift += 5
        } while (b >= 0x20)
        val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
        lng += dlng

        val p = LatLng(lat.toDouble() / 1E5,
            lng.toDouble() / 1E5)
        poly.add(p)
    }

    return poly
}


fun vibratePhone(context: Context,timeInMilis : Long) {

    // 0 : Start without a delay
    // 400 : Vibrate for 400 milliseconds
    // 200 : Pause for 200 milliseconds
    // 400 : Vibrate for 400 milliseconds
    // 0 : Start without a delay
// 400 : Vibrate for 400 milliseconds
// 200 : Pause for 200 milliseconds
// 400 : Vibrate for 400 milliseconds
    val mVibratePattern = longArrayOf(0, 400, 200, 400)

    val vibrator = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
    if (Build.VERSION.SDK_INT >= 26) {
        if(timeInMilis > 0)
        {
            vibrator.vibrate(
                VibrationEffect.createOneShot(
                    timeInMilis,
                    VibrationEffect.DEFAULT_AMPLITUDE
                )
            )
        }
        else {
            vibrator.vibrate(
                VibrationEffect.createWaveform(
                    mVibratePattern,
                    VibrationEffect.DEFAULT_AMPLITUDE
                )
            )
        }
    } else {
        if(timeInMilis > 0)
        {
            vibrator.vibrate(timeInMilis)
        }
        else
        {
            vibrator.vibrate(mVibratePattern, -1)
        }
    }
}