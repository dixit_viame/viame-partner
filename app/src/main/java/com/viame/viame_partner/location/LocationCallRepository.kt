package com.viame.viame_partner.location

import android.annotation.SuppressLint
import android.app.Application
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.viame.viame_partner.database.LocationDao
import com.viame.viame_partner.database.LocationTable
import com.viame.viame_partner.database.RoomDatabaseHelper
import com.viame.viame_partner.extension.checkLocationPermission
import com.viame.viame_partner.extension.isGPSEnabled
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import io.reactivex.Flowable
import io.reactivex.Single
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

class LocationCallRepository @Inject constructor(
        private val application: Application,
        private val database: RoomDatabaseHelper
) {

    @SuppressLint("MissingPermission")
    fun getLocation() {
        /*
         * One time location request
         */
        if (application.isGPSEnabled() && application.checkLocationPermission()) {
            LocationServices.getFusedLocationProviderClient(application)
                    ?.lastLocation
                    ?.addOnSuccessListener { location: android.location.Location? ->
                        if (location != null) {

                                Preferences.setPreference(PrefEntity.LATITUDE_CURRENT, location.latitude.toString())
                                Preferences.setPreference(PrefEntity.LONGITUDE_CURRENT, location.longitude.toString())

                                Timber.e("current location lat ${location.latitude} long ${location.longitude}")

                                var locationTable = LocationTable(0, location.latitude,
                                    location.longitude, System.currentTimeMillis(),location.altitude.toString(),location.speed.toString(),
                                    location.bearing.toString(),location.provider,location.accuracy.toString())

                                saveLocation(locationTable)
                        }
                }
        }
    }

    fun saveLocation(location: LocationTable) = GlobalScope.launch {
        database.locationDao().insert(location)
    }

    fun getSavedAllLocation(): Flowable<List<LocationTable>> = database.locationDao().selectAll()

    fun getSavedLatestLocation(): Single<List<LocationTable>> = database.locationDao().selectLastTwoRecords()

    fun deleteLocationWhenLogout(): Single<Int> = Single.just(database.locationDao().deleteAll())
}