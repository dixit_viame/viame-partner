package com.viame.viame_partner.location.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PublishLocationModel {

    @SerializedName("lg")
    @Expose
    private Double lg;
    @SerializedName("lt")
    @Expose
    private Double lt;
    @SerializedName("vt")
    @Expose
    private String vt;
    @SerializedName("pubnubStr")
    @Expose
    private String pubnubStr;
    @SerializedName("app_version")
    @Expose
    private String appVersion;
    @SerializedName("battery_Per")
    @Expose
    private String batteryPer;
    @SerializedName("location_check")
    @Expose
    private String locationCheck;
    @SerializedName("device_type")
    @Expose
    private String deviceType;
    @SerializedName("location_Heading")
    @Expose
    private Double locationBearing;
    @SerializedName("transit")
    @Expose
    private String transit;

    @SerializedName("accuracy")
    @Expose
    String accuracy;

    @SerializedName("altitude")
    @Expose
    String altitude;

    @SerializedName("provider")
    @Expose
    String provider;

    @SerializedName("speed")
    @Expose
    String speed;

    @SerializedName("timestamp")
    @Expose
    Long timestamp;

    public String getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(String accuracy) {
        this.accuracy = accuracy;
    }

    public String getAltitude() {
        return altitude;
    }

    public void setAltitude(String altitude) {
        this.altitude = altitude;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Double getLg() {
        return lg;
    }

    public void setLg(Double lg) {
        this.lg = lg;
    }

    public Double getLt() {
        return lt;
    }

    public void setLt(Double lt) {
        this.lt = lt;
    }

    public String getVt() {
        return vt;
    }

    public void setVt(String vt) {
        this.vt = vt;
    }

    public String getPubnubStr() {
        return pubnubStr;
    }

    public void setPubnubStr(String pubnubStr) {
        this.pubnubStr = pubnubStr;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getBatteryPer() {
        return batteryPer;
    }

    public void setBatteryPer(String batteryPer) {
        this.batteryPer = batteryPer;
    }

    public String getLocationCheck() {
        return locationCheck;
    }

    public void setLocationCheck(String locationCheck) {
        this.locationCheck = locationCheck;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public Double getLocationBearing() {
        return locationBearing;
    }

    public void setLocationBearing(Double locationBearing) {
        this.locationBearing = locationBearing;
    }

    public String getTransit() {
        return transit;
    }

    public void setTransit(String transit) {
        this.transit = transit;
    }

}
