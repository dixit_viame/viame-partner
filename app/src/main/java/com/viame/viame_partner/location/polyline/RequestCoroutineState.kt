package com.viame.viame_partner.location.polyline

/**
 * Created by jaroslawmichalik on 23.02.2018
 */
enum class STATE {
    START, LOADING, END
}

enum class STATUS {
    SUCCESS, ERROR, NONE
}