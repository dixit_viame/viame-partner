package com.viame.viame_partner.firebase

import android.annotation.SuppressLint
import android.app.Activity
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.viame.viame_partner.R
import com.viame.viame_partner.application.CatalogApplication
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import timber.log.Timber
import java.util.*


@SuppressLint("TimberArgCount")
    fun callFirebaseToGetRegistrationId()
    {
        if(FirebaseInstanceId.getInstance().instanceId !=null) {
            FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        Timber.e("getInstanceId failed", task.exception)
                        return@OnCompleteListener
                    }

                    // Get new Instance ID token
                    val token = task.result?.token

                    // Log and toast
                    Timber.d("Registration Token: %s", token)
                    Preferences.setPreference(PrefEntity.FIREBASE_MESSEGING_REG_ID, token)
                })
        }
    }

@SuppressLint("TimberArgCount")
fun callFirebaseToSubscribeTopic(topicname:String)
{
    FirebaseMessaging.getInstance().subscribeToTopic(topicname)
        .addOnCompleteListener { task ->
            var msg = CatalogApplication.instance?.getString(R.string.msg_subscribed)
            if (!task.isSuccessful) {
                msg = CatalogApplication.instance?.getString(R.string.msg_subscribe_failed)
            }
            Timber.w("Subscribe topic : %s", msg)
        }


}

fun unsubscribeAllTopics(topicname: String)
{
    FirebaseMessaging.getInstance().unsubscribeFromTopic(topicname)
}

fun isGooglePlayServicesAvailable(activity: Activity?): Boolean {
    val googleApiAvailability = GoogleApiAvailability.getInstance()
    val status = googleApiAvailability.isGooglePlayServicesAvailable(activity)
    if (status != ConnectionResult.SUCCESS) {
        if (googleApiAvailability.isUserResolvableError(status)) {
            googleApiAvailability.getErrorDialog(activity, status, 2404).show()
        }
        return false
    }
    return true
}
