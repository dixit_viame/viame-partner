package com.viame.viame_partner.firebase

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.os.PowerManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.viame.viame_partner.*
import com.viame.viame_partner.application.CatalogApplication
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.ui.bookingaccept.model.AcknowledgeBookingInputModel
import com.viame.viame_partner.ui.bookingaccept.model.BookingAcceptOutput
import com.viame.viame_partner.ui.bookingaccept.model.BookingData
import com.viame.viame_partner.ui.bookingaccept.model.SimpleMessageDataModel
import com.viame.viame_partner.ui.main.MainActivity
import com.viame.viame_partner.ui.transfer_order.model.BookingTransferFirebaseModel
import com.viame.viame_partner.utils.LinNotify
import com.viame.viame_partner.workmanager.startAlarmService
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.*
import javax.inject.Inject


class MyFirebaseMessagingService @Inject constructor(): FirebaseMessagingService() {

    @Inject
    lateinit var apiInterface : ApiInterface

    override fun onCreate() {
        super.onCreate()
        AndroidInjection.inject(this)
    }
    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     * The registration token may change when:
     *
     * The app deletes Instance ID
     * The app is restored on a new device
     * The user uninstalls/reinstall the app
     * The user clears app data.
     */
    @SuppressLint("TimberArgCount")
    override fun onNewToken(s: String) {
        super.onNewToken(s)
        Timber.e("Registration Token: %s", s)
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        Preferences.setPreference(PrefEntity.FIREBASE_MESSEGING_REG_ID, s)
    }

    @SuppressLint("TimberArgCount", "InvalidWakeLockTag")
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        try {
            // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
            Timber.e("From:%s", remoteMessage.from)

            /*{action=11, bookingData={"amount":"35.00","helpers":0,"drop_adr":"Business Bay, Prime Tower 2707 - دبي - United Arab Emirates",
            "distance":"6.00","booking_time":"2020-2-17 10:44:00","pickup_adr":"Capital Golden Tower - Dubai - United Arab Emirates",
            "channel_customer":"slave_44ec7cdaa2ae9241","payment_type":"Cash","booking_type":"Now","serverTime":1581921877,
            "drop_zone":"Dubai","bid":1581921877668,"client_name":"ViaMe","pickZone":"Dubai","ExpiryTimer":30,"statusCode":1,"status":"New"}}*/

            // Check if message contains a data payload.
            if (remoteMessage.data.isNotEmpty()) {

                try {
                    if (Preferences.getPreferenceBoolean(PrefEntity.IS_LOGIN) &&
                        Preferences.getPreferenceInt(PrefEntity.DRIVER_PRESENCE_STATUS) == DriverStatusType.ONLINE.id
                    ) {

                        if (remoteMessage.data.containsKey("a") && remoteMessage.data["a"] == "11") {
                            return
                        }

                        if (remoteMessage.data.containsKey("action") && remoteMessage.data["action"].equals("11"))
                        {
                            Timber.d("Message data payload:%s", remoteMessage.data)
                            val mJSONOBJ = JSONObject(remoteMessage.data["bookingData"]!!)
                            val mbookingDataResponse =
                                Gson().fromJson(mJSONOBJ.toString(), BookingData::class.java)

                            // Acknowledge API
                            try {
                                val headerMap = HashMap<String,String>()
                                headerMap["Content-Type"] = "application/json;charset=UTF-8"
                                headerMap["authorization"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
                                headerMap["token"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
                                headerMap["userType"] = USER_TYPE.toString()

                                val mAcknowledgeBookingInputModel = AcknowledgeBookingInputModel()
                                mAcknowledgeBookingInputModel.entBookingId = mbookingDataResponse.bid.toString()
                                mAcknowledgeBookingInputModel.pingId = "1"
                                mAcknowledgeBookingInputModel.serverTime = mbookingDataResponse.serverTime.toString()

                                val posts: Call<String> = apiInterface.callAckBookingMethod(headerMap,mAcknowledgeBookingInputModel)

                                //Enqueue the call
                                posts.enqueue(object : Callback<String> {

                                    @SuppressLint("TimberArgCount")
                                    override fun onFailure(call: Call<String>, t: Throwable)
                                    {
                                        Timber.e(t.message.toString())
                                    }

                                    @SuppressLint("TimberArgCount")
                                    override fun onResponse(call: Call<String>, response: Response<String>) =
                                        try {
                                            if(response.isSuccessful) {
                                                Timber.e(response.body().toString())
                                            }
                                            else
                                            {
                                                Timber.e(response.errorBody().toString())
                                            }
                                        } catch (e: Exception) {
                                            e.printStackTrace()
                                        }
                                })
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                            try {// Wake lock device
                                val pm = getSystemService(Context.POWER_SERVICE) as PowerManager

                                val isScreenOn: Boolean
                                isScreenOn =  pm.isInteractive()
                                Timber.e("screen on.................................$isScreenOn")
                                if (!isScreenOn) {

                                    var tag = "com.viame.viame_partner:LOCK"

                                    if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M && Build.MANUFACTURER == "Huawei") {
                                        tag = "LocationManagerService"
                                    }

                                    val wl: PowerManager.WakeLock = pm.newWakeLock(
                                        PowerManager.FULL_WAKE_LOCK or PowerManager.ACQUIRE_CAUSES_WAKEUP or PowerManager.ON_AFTER_RELEASE,
                                        tag
                                    )
                                    wl.acquire(20000)

                                    Timber.e("screen on.................................acquire")
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }


                            if (mbookingDataResponse != null) {

                                val intent = Intent(this, MainActivity::class.java)
                                intent.flags =
                                    Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK

                                val rndInt = ((Date().getTime() / 1000L) % Integer.MAX_VALUE).toInt()

                                val mBundle = Bundle()
                                mBundle.putInt(MainActivity.NOTIFICATION_ACTION, rndInt)

                                mBundle.putString(
                                    MainActivity.NOTIFICATION_COME_FROM,
                                    "Notification"
                                )
                                mBundle.putParcelable(
                                    MainActivity.NOTIFICATION_DATA,
                                    mbookingDataResponse
                                )
                                mBundle.putInt(MainActivity.NOTIFICATION_BOOKING_RESULT, 3)
                                intent.putExtras(mBundle)

                                if (Preferences.getPreferenceBoolean(
                                        PrefEntity.IS_MAIN_ACTIVITY_IN_FOREGROUND))
                                {
                                    android.os.Handler(Looper.getMainLooper()).post {
                                        startActivity(intent)
                                    }
                                }

                                val pendingIntent = PendingIntent.getActivity(
                                    baseContext,
                                    rndInt,
                                    intent,
                                    PendingIntent.FLAG_UPDATE_CURRENT
                                )

                                LinNotify.showBigTextStyle(
                                    baseContext,
                                    R.drawable.ic_viame_notification,
                                    R.drawable.ic_viame_notification,
                                    getString(R.string.new_booking),
                                    "Bid = " + mbookingDataResponse.bid,
                                    getString(R.string.new_booking),
                                    "Pickup: " + mbookingDataResponse.pickupAdr + "\nDropoff: " + mbookingDataResponse.dropAdr + "\nBookingType: COD\nAmount: " + mbookingDataResponse.amount + "" +
                                            "\nClientName: " + mbookingDataResponse.clientName,
                                    getString(R.string.new_booking),
                                    null,
                                    rndInt,
                                    NOTIFICATION_CHANNEL_ID,
                                    pendingIntent,
                                    mBundle, false)
                            }
                        }
                        // action transfer booking  {action=10, data={"drop":{"latitude":25.17808160256075,"longitude":55.27429383248091},"driver_name":"Dixit Patel",
                        // "payment_type":"Cash","driver_id":"5e2d210cc953dc001351bcb3","transfer_text":"Dixit  transferred to you the 1580024898958.","total_amount":"35.00"
                        // ,"drop_address":"Dubai - United Arab Emirates","pickup":{"latitude":25.135247957750863,"longitude":55.28358634561301},"pickup_address":
                        // "Capital Golden Tower - Dubai - United Arab Emirates","customer_name":"ViaMe","bid":1580024898958,"booking_timestamp":1580024884}, message=Dixit
                        // transferred to you the 1580024898958.}

                        else if (remoteMessage.data.containsKey("action") && remoteMessage.data["action"].equals("12"))
                        {
                            Timber.d("Message data payload:%s", remoteMessage.data)

                            val mJSONOBJ = JSONObject(remoteMessage.data["data"]!!)

                            val mbookingDataResponse =
                                Gson().fromJson(
                                    mJSONOBJ.toString(),
                                    BookingTransferFirebaseModel::class.java
                                )

                            val intent = Intent(this, MainActivity::class.java)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK

                            val pendingIntent = PendingIntent.getActivity(
                                baseContext,
                                BOOKING_TRANSFER_ID,
                                intent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                            )

                            LinNotify.show(
                                baseContext,
                                getString(R.string.booking_transferred_title),
                                mbookingDataResponse.transferText,
                                pendingIntent
                            )

                        }
                        // This is the device push that make awake device every 1 min.
                        else if (remoteMessage.data.containsKey("action") && remoteMessage.data["action"].equals("26"))
                        {

                            Timber.e("--------- Device Please awake ---------")
                            try {// Wake lock device
                                val pm = getSystemService(Context.POWER_SERVICE) as PowerManager

                                val isScreenOn =  pm.isInteractive()
                                Timber.e("screenDoze ${pm.isDeviceIdleMode}  screen on........$isScreenOn")
                                if (!isScreenOn)
                                {
                                    val status = Preferences.getPreferenceInt(PrefEntity.DRIVER_PRESENCE_STATUS)

                                    if(status == DriverStatusType.ONLINE.id && !Preferences.getPreferenceBoolean(PrefEntity.IS_MAIN_ACTIVITY_IN_FOREGROUND))
                                    {
                                        val mPendingIntent = PendingIntent.getActivity(
                                            CatalogApplication.instance!!,
                                            ONLINE_OFFLINE_NOTIFICATION_ID,
                                            Intent(CatalogApplication.instance!!, MainActivity::class.java), PendingIntent.FLAG_UPDATE_CURRENT)

                                        LinNotify.showBigTextStyle(
                                            CatalogApplication.instance!!,
                                            R.drawable.ic_online,
                                            R.drawable.ic_viame_notification,
                                            CatalogApplication.instance!!.getString(R.string.app_name),
                                            "",
                                            "",
                                            "You are online",
                                            CatalogApplication.instance!!.getString(R.string.app_name), null, ONLINE_OFFLINE_NOTIFICATION_ID, NOTIFICATION_CHANNEL_ID, mPendingIntent, null, true)
                                    }
                                    var tag = "com.viame.viame_partner:LOCK"

                                    if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M && Build.MANUFACTURER == "Huawei") {
                                        tag = "LocationManagerService"
                                    }

                                    val wl: PowerManager.WakeLock = pm.newWakeLock(
                                        PowerManager.FULL_WAKE_LOCK or PowerManager.ACQUIRE_CAUSES_WAKEUP or PowerManager.ON_AFTER_RELEASE,
                                        tag
                                    )
                                    wl.acquire(20000)

                                    Timber.e("screenDoze ${pm.isDeviceIdleMode}  screen on........acquire")
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                            android.os.Handler(Looper.getMainLooper()).post {
                                // Start Location Sending Work
                                startAlarmService(baseContext, ALARM_MANAGER_NOTIFICATION_CODE)


                            }
                        }// This is the notification comes when pushed from admin panel
                        else if (remoteMessage.data.containsKey("action") && remoteMessage.data["action"].equals("20"))
                        {
                            Timber.d("Admin Message data payload:%s", remoteMessage.data)
                            val rndInt = ((Date().getTime() / 1000L) % Integer.MAX_VALUE).toInt()

                            val mJSONOBJ = JSONObject(remoteMessage.data["data"]!!)

                            val mSimpleMessageDataModel = Gson().fromJson(mJSONOBJ.toString(), SimpleMessageDataModel::class.java)

                            val mPendingIntent = PendingIntent.getActivity(
                                CatalogApplication.instance!!,
                                rndInt,
                                Intent(CatalogApplication.instance!!, MainActivity::class.java), PendingIntent.FLAG_UPDATE_CURRENT)

                            LinNotify.showBigTextStyle(
                                CatalogApplication.instance!!,
                                R.drawable.ic_online,
                                R.drawable.ic_viame_notification,
                                mSimpleMessageDataModel.title,
                                "",
                                "",
                                mSimpleMessageDataModel.body,
                                CatalogApplication.instance!!.getString(R.string.app_name), null, rndInt, NOTIFICATION_CHANNEL_ID, mPendingIntent, null, true)

                        }
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
            // Check if message contains a notification payload.
            if (remoteMessage.notification != null) {
                Timber.d("Message Notification Body:%s", remoteMessage.notification!!.body)
                val status = Preferences.getPreferenceInt(PrefEntity.DRIVER_PRESENCE_STATUS)

                // Only you will get all notification when you will online
                if (status == DriverStatusType.ONLINE.id) {

                    val intent = Intent(this, MainActivity::class.java)

                    val pendingIntent = PendingIntent.getActivity(
                        baseContext,
                        1251, intent, PendingIntent.FLAG_UPDATE_CURRENT
                    )

                    LinNotify.show(
                        baseContext,
                        "New Booking",
                        remoteMessage.notification!!.body,
                        NOTIFICATION_CHANNEL_ID,
                        pendingIntent
                    )
                }
            }
            // Also if you intend on generating your own notifications as a result of a received FCM
            // message, here is where that should be initiated. See sendNotification method below.
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        private const val TAG = "MyFirebaseMessagingService"
    }
}