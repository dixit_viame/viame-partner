package com.viame.viame_partner.service

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.*
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.common.api.Status
import com.google.android.gms.common.api.internal.TaskApiCall
import com.google.android.gms.gcm.GcmNetworkManager
import com.google.android.gms.gcm.OneoffTask
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.viame.viame_partner.*
import com.viame.viame_partner.database.LocationTable
import com.viame.viame_partner.extension.fromSingleToMain
import com.viame.viame_partner.location.Distance
import com.viame.viame_partner.location.LocationCallRepository
import com.viame.viame_partner.location.model.PublishLocationModel
import com.viame.viame_partner.location.model.PublishLocationResponseModel
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.rx.Scheduler
import com.viame.viame_partner.utils.Utils
import dagger.android.DaggerService
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class MyLocationBindService : DaggerService() , LocationListener,
GoogleApiClient.ConnectionCallbacks,
GoogleApiClient.OnConnectionFailedListener{

    private var mLocationRequest: LocationRequest? = null
    private var mGoogleApiClient: GoogleApiClient? = null

    @Inject
    lateinit var repository: LocationCallRepository

    @Inject
    lateinit var scheduler: Scheduler

    @Inject
    lateinit var apiInterface: ApiInterface

    private val INTERVAL = 1000 * 10 //15 sec
        .toLong()

    private val CONFIG_CACHE_EXPIRY = 600 // 10 minutes.

    private val FASTEST_INTERVAL = 1000 * 10 // 15 sec
        .toLong()
    private val SMALLEST_DISPLACEMENT = 0.25f //quarter of a meter

    private var mWakelock: PowerManager.WakeLock? = null
    private var mFirebaseRemoteConfig: FirebaseRemoteConfig? = null


    private fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.setInterval(INTERVAL)
        mLocationRequest!!.setFastestInterval(FASTEST_INTERVAL)
        mLocationRequest!!.setSmallestDisplacement(SMALLEST_DISPLACEMENT) //added
        mLocationRequest!!.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
    }

    override fun onCreate() {
        super.onCreate()

        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance()
        val configSettings: FirebaseRemoteConfigSettings = FirebaseRemoteConfigSettings.Builder()
            .setMinimumFetchIntervalInSeconds(20)
            .build()
        mFirebaseRemoteConfig!!.setConfigSettingsAsync(configSettings)
        mFirebaseRemoteConfig!!.setDefaultsAsync(R.xml.remote_config_defaults)

        createLocationRequest()
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addApi(LocationServices.API)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .build()
        mGoogleApiClient?.connect();
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)

        if (mGoogleApiClient?.isConnected()!!) {
            startLocationUpdates();
        }

        return Service.START_STICKY
    }

    private var mMessenger: Messenger? = null

    override fun onBind(intent: Intent): IBinder {
        Timber.e("Foregound location bind call")
        mMessenger = Messenger(IncomingHandler(this))
        return mMessenger!!.getBinder()
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            stopLocationUpdates();
            if(mGoogleApiClient != null)
            mGoogleApiClient?.disconnect()
            if(mWakelock != null)
            mWakelock?.release()
        } catch (e: Exception) {
        }
    }

    /**
     * Handler of incoming messages from clients.
     */
    @SuppressLint("HandlerLeak")
    internal class IncomingHandler(private val applicationContext: Context) :
        Handler() {
        override fun handleMessage(msg: Message) {
        }
    }

    private fun fetchRemoteConfig() {
        var cacheExpiration: Int = CONFIG_CACHE_EXPIRY
        if (mFirebaseRemoteConfig!!.info.configSettings.isDeveloperModeEnabled) {
            cacheExpiration = 0
        }
        mFirebaseRemoteConfig!!.fetch(cacheExpiration.toLong())
            .addOnSuccessListener {
                Timber.e("Remote config fetched")
                mFirebaseRemoteConfig!!.activate()
            }
    }

    private fun shutdownAndScheduleStartup(whefn: Long) {
        Timber.e("overnight shutdown, seconds to startup: $whefn")
        val task = OneoffTask.Builder() //com.google.android.gms.gcm.Task
            .setService(TrackerTaskService::class.java)
            .setExecutionWindow(whefn, whefn + 60)
            .setUpdateCurrent(true)
            .setTag(TrackerTaskService.TAG)
            .setRequiredNetwork(com.google.android.gms.gcm.Task.NETWORK_STATE_ANY)
            .setRequiresCharging(false)
            .build()
        GcmNetworkManager.getInstance(this).schedule(task)
        stopSelf()
    }

    @SuppressLint("CheckResult")
    override fun onLocationChanged(location: Location?) {
        fetchRemoteConfig();
        val hour = Calendar.getInstance()[Calendar.HOUR_OF_DAY].toLong()
        val startupSeconds =
            (mFirebaseRemoteConfig?.getLong("SLEEP_HOURS_DURATION")?.times(3600)) as Long
        if (hour == mFirebaseRemoteConfig?.getLong("SLEEP_HOUR_OF_DAY")) {
            shutdownAndScheduleStartup(startupSeconds)
            return
        }

        if (location != null) {
            Preferences.setPreference(PrefEntity.LATITUDE_CURRENT, location.latitude.toString())
            Preferences.setPreference(PrefEntity.LONGITUDE_CURRENT, location.longitude.toString())
            Timber.e("Foregound location lat ${location.latitude} long ${location.longitude}")

            var locationTable = LocationTable(
                0,
                location.latitude,
                location.longitude,
                System.currentTimeMillis(),
                location.altitude.toString(),
                location.speed.toString(),
                location.bearing.toString(),
                location.provider,
                location.accuracy.toString())

            repository.saveLocation(locationTable)

            repository.getSavedLatestLocation()
                .fromSingleToMain(scheduler)
                .subscribe(
                    { onNext ->
                        CalculateRouteArray(ArrayList(onNext))
                    },
                    { onError ->
                        println(" $onError")
                    })
        }
    }


    private fun startLocationUpdates() {
        val pendingResult: PendingResult<Status> =
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
    }

    private fun stopLocationUpdates() {
        if(mGoogleApiClient?.isConnected!!) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this
            )
        }
    }

    @SuppressLint("InvalidWakeLockTag")
    override fun onConnected(p0: Bundle?) {
        // Hold a partial wake lock to keep CPU awake when the we're tracking location.
        val powerManager =
            getSystemService(Context.POWER_SERVICE) as PowerManager
        mWakelock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK or PowerManager.ACQUIRE_CAUSES_WAKEUP or PowerManager.ON_AFTER_RELEASE, "MyWakelockTag")
        mWakelock?.acquire(20000 /*10 minutes*/)

        Timber.e("Foregound location connect")
        startLocationUpdates()

    }

    override fun onConnectionSuspended(p0: Int) {
        TODO("Not yet implemented")
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        TODO("Not yet implemented")
    }

    private var previousLattitude : Double = 0.0
    private var strayLattitude : Double = 0.0
    private var strayLongitude : Double = 0.0
    private var previousLongitude : Double = 0.0
    private var bearing  : Double = 0.0

    @SuppressLint("BinaryOperationInTimber")
    private fun CalculateRouteArray(onLocations: java.util.ArrayList<LocationTable>) : Int {
        try
        {
            if(onLocations.size > 0)
            {
                val onLocationResult = ArrayList<LocationTable>(2)
                onLocationResult.add(onLocations[0])
                onLocationResult.add(onLocations[0])

                if (onLocationResult.size <= 2) {
                    onLocationResult[1].latitude = onLocationResult[0].latitude
                    onLocationResult[1].longitude = onLocationResult[0].longitude
                } else {
                    return 0
                }
                if (onLocationResult.isNotEmpty()) {
                    previousLattitude = onLocationResult[1].latitude
                    previousLongitude = onLocationResult[1].longitude
                }

                val curLat: Double = onLocationResult[0].latitude
                val curLong: Double = onLocationResult[0].longitude
                bearing = onLocationResult[0].bearing.toDouble()
                val dis: Double = Distance(
                    previousLattitude,
                    previousLongitude,
                    curLat,
                    curLong,
                    METER
                )
                val distanceInMtr: Double
                var disFromStaryPts = -1.0

                if (strayLattitude != -1.0 && strayLongitude != -1.0) {
                    disFromStaryPts = Distance(
                        strayLattitude, strayLongitude, curLat, curLong,
                        METER
                    )
                }
                if (dis >= Preferences.getPreferenceString(PrefEntity.MIN_DIST_FOR_ROUTE_ARRRAY).toInt() && dis <= 300 || disFromStaryPts != -1.0 &&
                    disFromStaryPts >= Preferences.getPreferenceString(PrefEntity.MIN_DIST_FOR_ROUTE_ARRRAY).toInt() && disFromStaryPts <= 300
                ) {
                    strayLattitude = onLocationResult[0].latitude.also { previousLattitude = it }
                    strayLongitude = onLocationResult[0].longitude.also { previousLongitude = it }

                    distanceInMtr = if (dis > disFromStaryPts) dis else disFromStaryPts
                    val jsonArray =
                        JSONArray(Preferences.getPreferenceString(PrefEntity.BOOKING_DETAILS))

                    for (x in 0 until jsonArray.length()) {
                        val jsonObject: JSONObject = jsonArray.get(x) as JSONObject

                        if (jsonObject.getInt("status") == MBStatusType.ARRIVED.id ||
                            jsonObject.getInt("status") == MBStatusType.LOADED.id
                        ) {

                            var distancespd =
                                jsonObject.getDouble("distance") / Preferences.getPreferenceString(
                                    PrefEntity.DISTANCE_CONVERSION_UNIT
                                ).toDouble()
                            distancespd += distanceInMtr
                            Timber.e("Myservice Total distance $distancespd")

                            Preferences.setPreference(
                                PrefEntity.DISTANCE_IN_DOUBLE,
                                distancespd.toString()
                            )

                            val distanceKm =
                                distancespd * Preferences.getPreferenceString(PrefEntity.DISTANCE_CONVERSION_UNIT).toDouble()
                            val strDouble = String.format(Locale.US, "%.2f", distanceKm)
                            Timber.e(
                                "MyService Distance format: $strDouble   BID " + jsonObject.getString(
                                    "bid"
                                )
                            )
                            jsonObject.put("distance", strDouble)
                            Preferences.setPreference(PrefEntity.DISTANCE, strDouble)
                        }
                    }

                    Preferences.setPreference(PrefEntity.BOOKING_DETAILS, jsonArray.toString())

                    publishLocation(curLat, curLong, 1, bearing,onLocationResult[0].accuracy,onLocationResult[0].altitude,onLocationResult[0].provider,
                        onLocationResult[0].speed,onLocationResult[0].timestamp)

                } else {
                    if (dis > 300 && disFromStaryPts > 300) {
                        strayLattitude = curLat
                        strayLongitude = curLong
                    } else {
                        publishLocation(curLat, curLong, 1, bearing,onLocationResult[0].accuracy,onLocationResult[0].altitude,onLocationResult[0].provider,
                            onLocationResult[0].speed,onLocationResult[0].timestamp)

                    }
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return 0
    }

    private fun publishLocation(
        curLat: Double,
        curLong: Double,
        transit: Int,
        bearing: Double,
        accuracy: String,
        altitude: String,
        provider: String,
        speed: String,
        timestamp: Long
    ) {
        var pubnubStr = ""
        if(Preferences.getPreferenceString(PrefEntity.BOOKING_DETAILS).isNotEmpty())
        {
            val jsonArray = JSONArray(Preferences.getPreferenceString(PrefEntity.BOOKING_DETAILS))

            for (x in 0 until jsonArray.length()) {
                val jsonObject: JSONObject = jsonArray.get(x) as JSONObject

                pubnubStr = if (pubnubStr.isEmpty()) {
                    jsonObject.getString("bid") + "|" + (if(jsonObject.has("custChn")) jsonObject.getString("custChn") else "") + "|" + jsonObject.getString(
                        "status"
                    )
                } else {
                    pubnubStr + ", " + jsonObject.getString("bid") + "|" + (if(jsonObject.has("custChn")) jsonObject.getString("custChn") else "") + "|" + jsonObject.getString(
                        "status"
                    )
                }
            }
        }

        val reqObject = PublishLocationModel()
        reqObject.lg = curLong
        reqObject.lt = curLat
        reqObject.vt = Preferences.getPreferenceString(PrefEntity.VEHICLE_TYPE_ID)
        reqObject.pubnubStr = pubnubStr
        reqObject.appVersion = Utils.getAppVersion(baseContext)
        reqObject.batteryPer = Preferences.getPreferenceInt(PrefEntity.BATTERY_PERCENTAGE).toString()
        if(Preferences.getPreferenceBoolean(PrefEntity.GPS_STATUS))
            reqObject.locationCheck = "1"
        else
            reqObject.locationCheck = "0"
        reqObject.deviceType = DEVICE_TYPE.toString()
        reqObject.locationBearing = bearing
        reqObject.transit = transit.toString()
        reqObject.accuracy = accuracy
        reqObject.altitude = altitude
        reqObject.provider = provider
        reqObject.speed = speed
        reqObject.timestamp = timestamp

        val headerMap = HashMap<String,String>()
        headerMap["Content-Type"] = "application/json;charset=UTF-8"
        headerMap["authorization"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["token"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["userType"] = USER_TYPE.toString()

        val posts: Call<PublishLocationResponseModel> = apiInterface.callPublishLocationMethod(headerMap,reqObject)!!

        //Enqueue the call
        posts.enqueue(object : Callback<PublishLocationResponseModel>
        {
            @SuppressLint("TimberArgCount")
            override fun onFailure(call: Call<PublishLocationResponseModel>, t: Throwable)
            {
                Timber.e(t.message.toString())
            }

            @SuppressLint("TimberArgCount")
            override fun onResponse(call: Call<PublishLocationResponseModel>, response: Response<PublishLocationResponseModel>)
            {
                try
                {
                    if(response.isSuccessful)
                    {
                        Timber.e(response.body().toString())
                    }
                    else
                    {
                        Timber.e(response.code().toString())
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
}
