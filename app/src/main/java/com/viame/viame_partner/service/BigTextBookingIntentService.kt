package com.viame.viame_partner.service

import android.annotation.SuppressLint
import android.app.IntentService
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.core.app.NotificationManagerCompat
import androidx.lifecycle.Observer
import androidx.work.ListenableWorker
import com.viame.viame_partner.*
import com.viame.viame_partner.extension.fromSingleToMain
import com.viame.viame_partner.location.LocationCallRepository
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.rx.Scheduler
import com.viame.viame_partner.ui.bookingaccept.model.BookingAcceptInput
import com.viame.viame_partner.ui.bookingaccept.model.BookingAcceptOutput
import com.viame.viame_partner.ui.bookingaccept.model.BookingData
import com.viame.viame_partner.ui.main.MainActivity
import com.viame.viame_partner.ui.main.MainActivity.Companion.NOTIFICATION_ACTION
import com.viame.viame_partner.ui.main.MainActivity.Companion.NOTIFICATION_BOOKING_RESULT
import com.viame.viame_partner.utils.Alerts
import com.viame.viame_partner.utils.ConnectivityReceiver
import com.viame.viame_partner.utils.Utils
import dagger.android.DaggerIntentService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.HashMap
import javax.inject.Inject


class BigTextBookingIntentService : DaggerIntentService("BigTextBookingIntentService") {

    private lateinit var bookingData: BookingData
    private  var notificationId: Int = 0

    @Inject
    lateinit var apiInterface : ApiInterface

    @Inject
    lateinit var repository : LocationCallRepository

    @Inject
    lateinit var scheduler : Scheduler

    companion object{
        const val ACTION_ACCEPT_BOOKING = "com.viame.viame_partner.action.ACCEPT_BOOKING"
        const val ACTION_OPEN_APP = "com.viame.viame_partner.action.OPEN_APP"
    }

    override fun onHandleIntent(intent: Intent?) {

        if(intent !=null)
        {
            if (intent.getStringExtra(MainActivity.NOTIFICATION_COME_FROM) == "Notification") {
                bookingData = intent.getParcelableExtra(MainActivity.NOTIFICATION_DATA) as BookingData

                notificationId = intent.getIntExtra(NOTIFICATION_ACTION,0)
            }

            when (intent.action) {
                ACTION_ACCEPT_BOOKING -> {
                    handleActionAcceptBooking(intent.extras)
                }
                ACTION_OPEN_APP -> {
                    intent.putExtra(NOTIFICATION_BOOKING_RESULT, 3)
                    handleActionOpenApp(intent.extras)
                }
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    @SuppressLint("CheckResult")
    private fun handleActionAcceptBooking(param1: Bundle?) {

        try {
            if(Utils.isNetworkAvailable(baseContext))
            {
                            var mbookingAcceptInput = BookingAcceptInput()
                            mbookingAcceptInput.bid = "${bookingData.bid}"
                            mbookingAcceptInput.entStatus = MBStatusType.ON_THE_WAY.id.toString()

                            mbookingAcceptInput.lattitude = Preferences.getPreferenceString(PrefEntity.LATITUDE_CURRENT).toDouble()
                            mbookingAcceptInput.longitude = Preferences.getPreferenceString(PrefEntity.LONGITUDE_CURRENT).toDouble()

                            apiInterface.let {

                                val headerMap = HashMap<String,String>();
                                headerMap["Content-Type"] = "application/json;charset=UTF-8";
                                headerMap["authorization"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN);
                                headerMap["token"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN);
                                headerMap["userType"] = USER_TYPE.toString();

                                val posts: Call<BookingAcceptOutput> = apiInterface.callRespondToBookingMethod(headerMap,mbookingAcceptInput)

                                //Enqueue the call
                                posts.enqueue(object : Callback<BookingAcceptOutput> {

                                    @SuppressLint("TimberArgCount")
                                    override fun onFailure(call: Call<BookingAcceptOutput>, t: Throwable)
                                    {
                                        param1!!.putInt(NOTIFICATION_BOOKING_RESULT, 3)
                                        handleActionOpenApp(param1)
                                        Timber.e(t.message.toString())
                                    }

                                    @SuppressLint("TimberArgCount")
                                    override fun onResponse(call: Call<BookingAcceptOutput>, response: Response<BookingAcceptOutput>) =
                                        try {
                                            if(response.isSuccessful) {
                                                val mLogoutResponseModel = response.body()
                                                if(mLogoutResponseModel?.errFlag == 0)
                                                {
                                                    param1!!.putInt(NOTIFICATION_BOOKING_RESULT,3)
                                                    handleActionOpenApp(param1)
                                                    Toast.makeText(baseContext ,"Booking Accepted!", Toast.LENGTH_LONG).show()
                                                }
                                                else
                                                {
                                                    param1!!.putInt(NOTIFICATION_BOOKING_RESULT,1)
                                                    handleActionOpenApp(param1)
                                                    Toast.makeText(baseContext ,mLogoutResponseModel?.errMsg, Toast.LENGTH_LONG).show()
                                                }
                                                Timber.e(response.body().toString())
                                            }
                                            else
                                            {
                                                param1!!.putInt(NOTIFICATION_BOOKING_RESULT,1)
                                                handleActionOpenApp(param1)
                                                Toast.makeText(baseContext ,"Status code "+response.code(), Toast.LENGTH_LONG).show()
                                            }
                                        } catch (e: Exception) {
                                            e.printStackTrace()

                                            param1!!.putInt(NOTIFICATION_BOOKING_RESULT,1)
                                            handleActionOpenApp(param1)
                                            Toast.makeText(baseContext ,getString(R.string.something_goes_wrong), Toast.LENGTH_LONG).show()
                                        }
                                })
                            }
                 }
                 else
                 {
                     param1!!.putInt(NOTIFICATION_BOOKING_RESULT,3)
                     handleActionOpenApp(param1)
                 }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private fun handleActionOpenApp(param1: Bundle?)
    {
        val notificationManagerCompat =
            NotificationManagerCompat.from(applicationContext)
        notificationManagerCompat.cancel(notificationId)

        val manager = this@BigTextBookingIntentService.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.cancel(notificationId)

        val intent   = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        intent.putExtras(param1!!)
        startActivity(intent)
    }

}
