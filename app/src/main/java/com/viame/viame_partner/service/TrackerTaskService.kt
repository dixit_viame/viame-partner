package com.viame.viame_partner.service

import android.content.Intent
import com.google.android.gms.gcm.GcmNetworkManager
import com.google.android.gms.gcm.GcmTaskService
import com.google.android.gms.gcm.TaskParams
import com.viame.viame_partner.ui.main.MainActivity
import timber.log.Timber


class TrackerTaskService : GcmTaskService() {

    companion object{
        var TAG = TrackerTaskService::class.simpleName
    }

    override fun onRunTask(p0: TaskParams?): Int {
        Timber.e("-------------RunTask------------")
        val start = Intent(this, MainActivity::class.java)
        start.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(start)
        return GcmNetworkManager.RESULT_SUCCESS
    }


}