package com.viame.viame_partner.workmanager

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.work.*
import timber.log.Timber
import java.util.concurrent.ExecutionException


//fun StartPeriodicLocationCallJob(context: Context) : PeriodicWorkRequest {
//        val constraints = Constraints.Builder()
//            .setRequiresCharging(false)
//            .setRequiredNetworkType(NetworkType.CONNECTED)
//            .build()
//
//        val periodicWorkRequest = PeriodicWorkRequest.Builder(NotifyLocationBackGroundWork::class.java, 15, TimeUnit.MINUTES)
//            .addTag(NotifyLocationBackGroundWork.TAG)
//            .setConstraints(constraints)
//            .build()
//       // WorkManager.getInstance(context).enqueueUniquePeriodicWork(NotifyLocationBackGroundWork.TAG,ExistingPeriodicWorkPolicy.KEEP,periodicWorkRequest)
//        Timber.e("---------Fire Periodic WorkManager---------")
//    return periodicWorkRequest
//}

fun StartOneTimeLocationCallJob() : OneTimeWorkRequest{

        val constraints = Constraints.Builder()
            .setRequiresCharging(false)
            .setRequiresDeviceIdle(false)
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        val oneTimeWorkRequest =
            OneTimeWorkRequest.Builder(NotifyLocationBackGroundWork::class.java)
                .setConstraints(constraints)
                .addTag(NotifyLocationBackGroundWork.TAG)
                .build()

        Timber.e("---------Fire WorkManager---------")

       // var workmanager = WorkManager.getInstance(context)
       // workmanager.enqueueUniqueWork(NotifyLocationBackGroundWork.TAG, ExistingWorkPolicy.KEEP,oneTimeWorkRequest)
        return oneTimeWorkRequest
}

fun isMyWorkerRunning(tag: String?, context: Context): Boolean {
    val status: List<WorkInfo>?
    try {
        status = WorkManager.getInstance(context).getWorkInfosByTag(tag!!).get()
        for (workStatus in status) {
            if (workStatus.getState() === WorkInfo.State.RUNNING
                || workStatus.getState() === WorkInfo.State.ENQUEUED
            ) {
                return true
            }
        }
        return false
    } catch (e: InterruptedException) {
        e.printStackTrace()
    } catch (e: ExecutionException) {
        e.printStackTrace()
    }
    return false
}

fun stopLocationSendWorkManager(context: Context, tag: String) {
    WorkManager.getInstance(context).cancelAllWorkByTag(tag)
}

fun startAlarmService(context: Context, mAlarmRequestCode: Int)
{
    val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

    val notificationIntent = Intent("android.media.action.DISPLAY_NOTIFICATION")
    notificationIntent.setClass(context, AlarmReceiver::class.java)
    notificationIntent.addCategory("android.intent.category.DEFAULT")

    val broadcast = PendingIntent.getBroadcast(
        context,
        mAlarmRequestCode,
        notificationIntent,
        PendingIntent.FLAG_UPDATE_CURRENT
    )

    alarmManager.setRepeating(
        AlarmManager.RTC_WAKEUP,
        System.currentTimeMillis() + 3000L,
        60000L, broadcast
    )
}

fun stopAlarmService(context: Context, mAlarmRequestCode: Int)
{
    val notificationIntent = Intent("android.media.action.DISPLAY_NOTIFICATION")
    notificationIntent.setClass(context, AlarmReceiver::class.java)
    notificationIntent.addCategory("android.intent.category.DEFAULT")

    val broadcastPendingIntent = PendingIntent.getBroadcast(
        context,
        mAlarmRequestCode,
        notificationIntent,
        PendingIntent.FLAG_UPDATE_CURRENT
    )

    val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    alarmManager.cancel(broadcastPendingIntent)

}