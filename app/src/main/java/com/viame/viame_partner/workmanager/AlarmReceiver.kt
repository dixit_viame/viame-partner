package com.viame.viame_partner.workmanager

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.work.ExistingWorkPolicy
import androidx.work.WorkManager
import dagger.android.AndroidInjection
import timber.log.Timber

class AlarmReceiver : BroadcastReceiver()
{

    override fun onReceive(context: Context?, intent: Intent?) {
        AndroidInjection.inject(this, context)
        Timber.e("Alarm Called")

        if (isMyWorkerRunning(NotifyLocationBackGroundWork.TAG, context!!))
        {
            stopLocationSendWorkManager(context,NotifyLocationBackGroundWork.TAG)
        }

        if (!isMyWorkerRunning(NotifyLocationBackGroundWork.TAG, context))
        {
             val workmanager = WorkManager.getInstance(context)
             workmanager.enqueueUniqueWork(NotifyLocationBackGroundWork.TAG, ExistingWorkPolicy.KEEP, StartOneTimeLocationCallJob())

        }
    }

}