package com.viame.viame_partner.workmanager

import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.os.Looper
import androidx.lifecycle.MutableLiveData
import androidx.work.ListenableWorker
import androidx.work.Worker
import androidx.work.WorkerFactory
import androidx.work.WorkerParameters
import com.squareup.inject.assisted.Assisted
import com.squareup.inject.assisted.AssistedInject
import com.viame.viame_partner.*
import com.viame.viame_partner.database.LocationTable
import com.viame.viame_partner.extension.fromSingleToMain
import com.viame.viame_partner.location.Distance
import com.viame.viame_partner.location.LocationCallRepository
import com.viame.viame_partner.location.model.PublishLocationModel
import com.viame.viame_partner.location.model.PublishLocationResponseModel
import com.viame.viame_partner.network.APIRequestResponseHandler
import com.viame.viame_partner.network.ApiInterface
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.rx.Scheduler
import com.viame.viame_partner.utils.ConnectivityReceiver
import com.viame.viame_partner.utils.Utils
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.*
import javax.inject.Inject
import javax.inject.Provider


class NotifyLocationBackGroundWork @AssistedInject constructor(
    @Assisted private val appContext: Context,
    @Assisted private val params: WorkerParameters,
    private val repository : LocationCallRepository,
    private val scheduler : Scheduler,
    private val connectivityReceiver : ConnectivityReceiver,
    private val apiInterface: ApiInterface
) : Worker(appContext, params) {

    @AssistedInject.Factory
    interface Factory : ChildWorkerFactory

    companion object {
        var TAG = NotifyLocationBackGroundWork.toString()
    }

    val location: MutableLiveData<APIRequestResponseHandler<List<LocationTable>>> =
        MutableLiveData()

    /**
     * Override this method to do your actual background processing.  This method is called on a
     * background thread - you are required to <b>synchronously</b> do your work and return the
     * {@link Result} from this method.  Once you return from this
     * method, the Worker is considered to have finished what its doing and will be destroyed.
     * <p>
     * A Worker is given a maximum of ten minutes to finish its execution and return a
     * {@link Result}.  After this time has expired, the Worker will
     * be signalled to stop.
     *
     * @return The {@link Result} of the computation; note that
     * dependent work will not execute if you use
     * {@link Result#failure()} or
     */
    @SuppressLint("WrongThread", "CheckResult")
    override fun doWork(): Result {
        return try {
            if(Preferences.getPreferenceBoolean(PrefEntity.IS_LOGIN) && Preferences.getPreferenceInt(PrefEntity.DRIVER_PRESENCE_STATUS) == DriverStatusType.ONLINE.id)
            {
                Handler(Looper.getMainLooper()).post {
                    connectivityReceiver.observeForever { observe ->
                        if (observe != null && observe.isConnected) {
                            repository.getLocation()
                            repository.getSavedLatestLocation()
                                .fromSingleToMain(scheduler)
                                .subscribe(
                                    { onNext ->
                                        println(" $onNext")
                                        CalculateRouteArray(ArrayList(onNext))
                                        Result.success()
                                    },
                                    { onError ->
                                        println(" $onError")
                                        Result.failure()
                                    })
                        }
                    }
                }
                Result.success()
            }
            else
            {
                Result.failure()
            }
        } catch (throwable: Throwable) {
            Timber.e("Error Sending Notification %s", throwable.message)
            Result.failure()
        }
    }

    private var previousLattitude : Double = 0.0
    private var strayLattitude : Double = 0.0
    private var strayLongitude : Double = 0.0
    private var previousLongitude : Double = 0.0
    private var bearing  : Double = 0.0


    @SuppressLint("BinaryOperationInTimber")
    private fun CalculateRouteArray(onLocations: ArrayList<LocationTable>) : Int {
        try
        {
        if(onLocations.size > 0)
        {
            val onLocationResult = ArrayList<LocationTable>(2)
            onLocationResult.add(onLocations[0])
            onLocationResult.add(onLocations[0])

            if (onLocationResult.size <= 2) {
                onLocationResult[1].latitude = onLocationResult[0].latitude
                onLocationResult[1].longitude = onLocationResult[0].longitude
            } else {
                return 0
            }
            if (onLocationResult.isNotEmpty()) {
                previousLattitude = onLocationResult[1].latitude
                previousLongitude = onLocationResult[1].longitude
            }

            val curLat: Double = onLocationResult[0].latitude
            val curLong: Double = onLocationResult[0].longitude
            bearing = onLocationResult[0].bearing.toDouble()
            val dis: Double = Distance(
                previousLattitude,
                previousLongitude,
                curLat,
                curLong,
                METER
            )
            val distanceInMtr: Double
            var disFromStaryPts = -1.0

            if (strayLattitude != -1.0 && strayLongitude != -1.0) {
                disFromStaryPts = Distance(
                    strayLattitude, strayLongitude, curLat, curLong,
                    METER
                )
            }
            if (dis >= Preferences.getPreferenceString(PrefEntity.MIN_DIST_FOR_ROUTE_ARRRAY).toInt() && dis <= 300 || disFromStaryPts != -1.0 &&
                disFromStaryPts >= Preferences.getPreferenceString(PrefEntity.MIN_DIST_FOR_ROUTE_ARRRAY).toInt() && disFromStaryPts <= 300
            ) {
                strayLattitude = onLocationResult[0].latitude.also { previousLattitude = it }
                strayLongitude = onLocationResult[0].longitude.also { previousLongitude = it }

                distanceInMtr = if (dis > disFromStaryPts) dis else disFromStaryPts
                val jsonArray =
                    JSONArray(Preferences.getPreferenceString(PrefEntity.BOOKING_DETAILS))

                for (x in 0 until jsonArray.length()) {
                    val jsonObject: JSONObject = jsonArray.get(x) as JSONObject

                    if (jsonObject.getInt("status") == MBStatusType.ARRIVED.id ||
                        jsonObject.getInt("status") == MBStatusType.LOADED.id
                    ) {

                        var distancespd =
                            jsonObject.getDouble("distance") / Preferences.getPreferenceString(
                                PrefEntity.DISTANCE_CONVERSION_UNIT
                            ).toDouble()
                        distancespd += distanceInMtr
                        Timber.e("Myservice Total distance $distancespd")

                        Preferences.setPreference(
                            PrefEntity.DISTANCE_IN_DOUBLE,
                            distancespd.toString()
                        )

                        val distanceKm =
                            distancespd * Preferences.getPreferenceString(PrefEntity.DISTANCE_CONVERSION_UNIT).toDouble()
                        val strDouble = String.format(Locale.US, "%.2f", distanceKm)
                        Timber.e(
                            "MyService Distance format: $strDouble   BID " + jsonObject.getString(
                                "bid"
                            )
                        )
                        jsonObject.put("distance", strDouble)
                        Preferences.setPreference(PrefEntity.DISTANCE, strDouble)
                    }
                }

                Preferences.setPreference(PrefEntity.BOOKING_DETAILS, jsonArray.toString())

                publishLocation(curLat, curLong, 1, bearing,onLocationResult[0].accuracy,onLocationResult[0].altitude,onLocationResult[0].provider,onLocationResult[0].speed,onLocationResult[0].timestamp)

            } else {
                if (dis > 300 && disFromStaryPts > 300) {
                    strayLattitude = curLat
                    strayLongitude = curLong
                } else {
                    publishLocation(curLat, curLong, 1, bearing,onLocationResult[0].accuracy,onLocationResult[0].altitude,onLocationResult[0].provider,onLocationResult[0].speed,onLocationResult[0].timestamp)

                }
            }
          }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return 0
    }

    private fun publishLocation(
        curLat: Double,
        curLong: Double,
        transit: Int,
        bearing: Double,
        accuracy: String,
        altitude: String,
        provider: String,
        speed: String,
        timestamp: Long
    ) {

        var pubnubStr = ""
        if(Preferences.getPreferenceString(PrefEntity.BOOKING_DETAILS).isNotEmpty())
        {
            val jsonArray = JSONArray(Preferences.getPreferenceString(PrefEntity.BOOKING_DETAILS))

            for (x in 0 until jsonArray.length()) {
                val jsonObject: JSONObject = jsonArray.get(x) as JSONObject

                pubnubStr = if (pubnubStr.isEmpty()) {
                    jsonObject.getString("bid") + "|" + (if(jsonObject.has("custChn")) jsonObject.getString("custChn") else "") + "|" + jsonObject.getString(
                        "status"
                    )
                } else {
                    pubnubStr + ", " + jsonObject.getString("bid") + "|" + (if(jsonObject.has("custChn")) jsonObject.getString("custChn") else "") + "|" + jsonObject.getString(
                        "status"
                    )
                }
            }
        }

        val reqObject = PublishLocationModel()
            reqObject.lg = curLong
            reqObject.lt = curLat
            reqObject.vt = Preferences.getPreferenceString(PrefEntity.VEHICLE_TYPE_ID)
            reqObject.pubnubStr = pubnubStr
            reqObject.appVersion = Utils.getAppVersion(appContext)
            reqObject.batteryPer = Preferences.getPreferenceInt(PrefEntity.BATTERY_PERCENTAGE).toString()
            if(Preferences.getPreferenceBoolean(PrefEntity.GPS_STATUS))
            reqObject.locationCheck = "1"
            else
            reqObject.locationCheck = "0"
            reqObject.deviceType = DEVICE_TYPE.toString()
            reqObject.locationBearing = bearing
            reqObject.transit = transit.toString()
            reqObject.accuracy = accuracy
            reqObject.altitude = altitude
            reqObject.provider = provider
            reqObject.speed = speed
            reqObject.timestamp = timestamp

        val headerMap = HashMap<String,String>()
        headerMap["Content-Type"] = "application/json;charset=UTF-8"
        headerMap["authorization"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["token"] = Preferences.getPreferenceString(PrefEntity.AUTH_TOKEN)
        headerMap["userType"] = USER_TYPE.toString()

        val posts: Call<PublishLocationResponseModel> = apiInterface.callPublishLocationMethod(headerMap,reqObject)!!

        //Enqueue the call
        posts.enqueue(object : Callback<PublishLocationResponseModel>
        {
            @SuppressLint("TimberArgCount")
            override fun onFailure(call: Call<PublishLocationResponseModel>, t: Throwable)
            {
                Timber.e(t.message.toString())
            }

            @SuppressLint("TimberArgCount")
            override fun onResponse(call: Call<PublishLocationResponseModel>, response: Response<PublishLocationResponseModel>)
            {
                try
                {
                    if(response.isSuccessful)
                    {
                        Timber.e(response.body().toString())
                    }
                    else
                    {
                        Timber.e(response.code().toString())
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })

    }
}



interface ChildWorkerFactory {
    fun create(appContext: Context, params: WorkerParameters): ListenableWorker
}
 open class SampleWorkerFactory @Inject constructor(
    private val workerFactories: Map<Class<out ListenableWorker>, @JvmSuppressWildcards Provider<ChildWorkerFactory>>
) : WorkerFactory() {
    override fun createWorker(
        appContext: Context,
        workerClassName: String,
        workerParameters: WorkerParameters
    ): ListenableWorker?
    {
        val foundEntry =
            workerFactories.entries.find { Class.forName(workerClassName).isAssignableFrom(it.key) }
        val factoryProvider = foundEntry?.value
            ?: throw IllegalArgumentException("unknown worker class name: $workerClassName")
        return factoryProvider.get().create(appContext, workerParameters)
    }
}
