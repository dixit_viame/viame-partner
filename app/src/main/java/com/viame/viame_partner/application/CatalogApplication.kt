package com.viame.viame_partner.application

import androidx.appcompat.app.AppCompatDelegate
import androidx.work.Configuration
import androidx.work.WorkManager
import com.google.firebase.FirebaseApp
import com.viame.viame_partner.BuildConfig
import com.viame.viame_partner.dagger.components.DaggerMainAppComponent
import com.viame.viame_partner.prefs.PrefEntity
import com.viame.viame_partner.prefs.Preferences
import com.viame.viame_partner.utils.LinNotify
import com.viame.viame_partner.utils.TypefaceCache
import com.viame.viame_partner.workmanager.SampleWorkerFactory
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import timber.log.Timber
import timber.log.Timber.DebugTree
import javax.inject.Inject

/** Catalog application class that provides support for using dispatching Dagger injectors.  */
class CatalogApplication : DaggerApplication() {

    companion object {
        @JvmStatic
        @get:Synchronized
        var instance: CatalogApplication? = null
            private set
        const val TAG = "CatalogApplication"

        init {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        }
    }

    @Inject
    lateinit var factory : SampleWorkerFactory

    override fun applicationInjector(): AndroidInjector<CatalogApplication?>? {
        return DaggerMainAppComponent.builder().create(this)
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        WorkManager.initialize(this, Configuration.Builder().setWorkerFactory(factory).build())

        TypefaceCache.getInstance().init(this)
        LinNotify.setNotificationChannel(this)
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }


        AppCompatDelegate.setDefaultNightMode(
            AppCompatDelegate.MODE_NIGHT_YES
        )
        Preferences.setPreference(
            PrefEntity.PREFERENCE_NIGHT_MODE,
            true
        )
    }
}